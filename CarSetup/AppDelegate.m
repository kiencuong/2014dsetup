//
//  AppDelegate.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "CSWellcomeViewController.h"
#import "BrandModel.h"
#import "Sheets.h"
#import "Crittercism.h"
#import "CSCustomNavigationViewController.h"
@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize checkCancelDownload;
@synthesize checkCancelDownloadFavorite;
@synthesize checkViewDownload;
@synthesize navigationWellcomeView;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [Crittercism enableWithAppID: @"5321b4952e2e1f74c6000007"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        [application setStatusBarStyle:UIStatusBarStyleLightContent];
//        self.window.clipsToBounds =YES;
//        self.window.frame =  CGRectMake(0,20,self.window.frame.size.width,self.window.frame.size.height-20);
    }
    
    // Override point for customization after application launch.
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
           wellcomeView  = [[CSWellcomeViewController alloc]initWithNibName:@"CSWellcomeViewController" bundle:nil];
    } else {
        wellcomeView  = [[CSWellcomeViewController alloc]initWithNibName:@"CSWellcomeViewController_ipad" bundle:nil];
    }
//    NSMutableArray *arrayA = [[NSMutableArray alloc]initWithCapacity:1];
//    NSString*b = [arrayA objectAtIndex:13];
    if (![CSUtility loadStatusLogin:CHECK_APP_STARTUP_FIRST]){
        [CSUtility saveStatusLogin:NO forKey:CHECK_APP_STARTUP_FIRST];
        [CSUtility saveInfoUserLogin:@"" forKey:USERNAME_LOGIN_SUCCESS];
        [CSUtility saveInfoUserLogin:@"" forKey:PASSWORD_LOGIN_SUCCESS];
    }

   navigationWellcomeView = [[CSCustomNavigationViewController alloc]initWithRootViewController:wellcomeView];
    [navigationWellcomeView setNavigationBarHidden:YES];
    
    self.window.rootViewController = navigationWellcomeView;
    self.window.backgroundColor = [UIColor whiteColor];
//    [self customizedApperance];
    [self.window makeKeyAndVisible];
    return YES;
}
+ (AppDelegate *)shareAppDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

//- (void)customizedApperance
//{
//    //Customize the look of the UINavBar for iOS5 devices
//    if ([[UINavigationBar class]respondsToSelector:@selector(appearance)]) {
//        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"title_bg.png"] forBarMetrics:UIBarMetricsDefault];
//        [[UINavigationBar appearance]setFrame:CGRectMake(0.0f, 0.0f, 320.0f,44.0f)];
//        UIImage *buttonBack = [[UIImage imageNamed:@"back_btn.png"]
//                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
//        
//        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBack
//                                                          forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    }
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     [self saveContext];
}
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    NSUInteger orientations = UIInterfaceOrientationMaskAllButUpsideDown;
    
    if(self.window.rootViewController){
        UIViewController *presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        orientations = [presentedViewController supportedInterfaceOrientations];
    }
//    NSLog(@"orientations interface : %d",orientations);
    return orientations;
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
#pragma  mark download sheets in background
- (void) autoDownloadSheet:(NSMutableArray *)arraySheet{
    if (!autoDowwnloadSheet) {
        autoDowwnloadSheet = [[CSGetInfoSheetManager alloc]init];
    }
    
    if (checkViewDownload == 0) {
        checkCancelDownload = NO;
        autoDowwnloadSheet.arrayDowloadSheet = arraySheet;
        autoDowwnloadSheet.index = 0;
        [CSUtility saveStatusLogin:YES forKey:CHECK_IS_DOWNLOADING_SHEETS];
        [autoDowwnloadSheet performSelectorInBackground:@selector(dowloadListsheetFull) withObject:nil];
    } else {
        checkCancelDownloadFavorite = NO;
        autoDowwnloadSheet.arrayDowloadFavoriteSheet = arraySheet;
        autoDowwnloadSheet.indexFavorite = 0;
        [CSUtility saveStatusLogin:YES forKey:CHECK_IS_DOWNLOADING_SHEETS_FAVORITE];
        [autoDowwnloadSheet performSelectorInBackground:@selector(dowloadListFavoritesheetFull) withObject:nil];
            
    }

}



#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Carsetup" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Carsetup.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Private Coredata methods
//insert to database
- (void) create:(MySetupBrankModel *) brandModel{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Grab the Label entity
     BrandModel *brandmodel = [NSEntityDescription insertNewObjectForEntityForName:@"BrandModel" inManagedObjectContext:context];
    
    // Set label name
    brandmodel.brandName = brandModel._brand_name;
    brandmodel.modelName = brandModel._model_name;
    brandmodel.userName = [CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS];
    
    brandmodel.brandID = [NSNumber numberWithInt:(int)brandModel._brand_id];
    brandmodel.modelID = [NSNumber numberWithInt:(int)brandModel._model_id];
    brandmodel.checkMysetup = [NSNumber numberWithInt:(int)brandModel.checkMySetup];
    
    for (MySetupModel *sheetBrandMode in brandModel.sheetOfBrankModel) {
        // Insert the sheetbrandModel
        Sheets *sheet = [NSEntityDescription insertNewObjectForEntityForName:@"Sheets" inManagedObjectContext:context];
        
        // Set the artist attributes
        sheet.modelID = [NSNumber numberWithInt:(int)sheetBrandMode._modelId];
        sheet.brandID = [NSNumber numberWithInt:(int)sheetBrandMode._brandId];
        sheet.userName = [CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS];
        sheet.date = sheetBrandMode._date;
        sheet.idSheet = [NSNumber numberWithInt:(int)sheetBrandMode._id];
        sheet.raceName = sheetBrandMode._raceName;
        sheet.checkMysetup = [NSNumber numberWithInt:(int)brandModel.checkMySetup];
        // Set relationships
        [brandmodel addSheetRecordObject:sheet];
        [sheet setBrandModel:brandmodel];
    }
    // Save everything
    NSError *error = nil;
    if ([context save:&error]) {
        NSLog(@"The create branch model save was successful!");
    } else {
        NSLog(@"The create branch model save wasn't successful: %@", [error userInfo]);
    }
}

// get data from database
- (NSMutableArray *) read:(NSInteger)checkMysetup withUserName:(NSString *) username{
    NSMutableArray *arrayBrandModelSheets = [[NSMutableArray alloc]init];
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Construct a fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BrandModel"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"checkMysetup == %d && userName == %@", checkMysetup,username];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (BrandModel *brandmodel in fetchedObjects) {
        // Log the label details
        MySetupBrankModel *brandModelSheet = [[MySetupBrankModel alloc]init];
        brandModelSheet._brand_id = [brandmodel.brandID intValue];
        brandModelSheet._model_id = [brandmodel.modelID intValue];
        brandModelSheet._brand_name = brandmodel.brandName;
        brandModelSheet._model_name = brandmodel.modelName;
        brandModelSheet.checkMySetup = [brandmodel.checkMysetup intValue];
//        NSLog(@"brand model:%@ - %@", brandmodel.brandName, brandmodel.modelName);
        
//        NSSet *sheets = brandmodel.newSheet;
        NSSet *sheets = [brandmodel valueForKeyPath:@"sheetRecord"];
        
        for (Sheets *sheet in sheets) {
            MySetupModel *sheetBrandModel = [[MySetupModel alloc]init];
            sheetBrandModel._id = [sheet.idSheet intValue];
            sheetBrandModel._brandId = [sheet.brandID intValue];
            sheetBrandModel._modelId = [sheet.modelID intValue];
            sheetBrandModel._date = sheet.date;
            sheetBrandModel._raceName = sheet.raceName;
            
            [brandModelSheet.sheetOfBrankModel addObject:sheetBrandModel];
//            NSLog(@"sheets:%@ %@", sheet.raceName, sheet.date);
        }
        [arrayBrandModelSheets addObject:brandModelSheet];
    }
    return arrayBrandModelSheets;
}
- (void) delete :(NSInteger)checkDeleteMysetup withUserName:(NSString *) username{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //  We're looking to grab an artist
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BrandModel"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    // We specify that we only want
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"checkMysetup == %d && userName == %@", checkDeleteMysetup,username];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (BrandModel *brandModel in fetchedObjects) {
        [context deleteObject:brandModel];
    }
    // Save everything
    if ([context save:&error]) {
        NSLog(@"The delete save was successful!");
    } else {
        NSLog(@"The delete save wasn't successful: %@", [error localizedDescription]);
    }
}
- (void) deleteSheet :(NSInteger) checkSetup withid:(NSInteger)idsheet withUserName:(NSString *) username{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //  We're looking to grab an artist
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Sheets"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    // We specify that we only want
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"checkMysetup == %d && idSheet == %d && userName == %@", checkSetup,idsheet,username];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (Sheets *sheet in fetchedObjects) {
        [context deleteObject:sheet];
    }
    // Save everything
    if ([context save:&error]) {
        NSLog(@"The delete save was successful!");
    } else {
        NSLog(@"The delete save wasn't successful: %@", [error localizedDescription]);
    }
}
- (void) deleteBrandModel :(NSInteger) checkSetup withbrandid:(NSInteger) brandId withmodelID:(NSInteger) modelID withUserName:(NSString *) username{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //  We're looking to grab an artist
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"BrandModel"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    // We specify that we only want
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"checkMysetup == %d && brandID == %d && modelID == %d && userName == %@", checkSetup,brandId,modelID,username];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (Sheets *sheet in fetchedObjects) {
        [context deleteObject:sheet];
    }
    // Save everything
    if ([context save:&error]) {
        NSLog(@"The delete brandModel save was successful!");
    } else {
        NSLog(@"The delete brandModel save wasn't successful: %@", [error localizedDescription]);
    }
}
- (void) createSheetInfo:(SheetFullInfoModel *) sheetFullModel
{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Grab the Label entity
    SheetInfo *sheetModel = [NSEntityDescription insertNewObjectForEntityForName:@"SheetInfo" inManagedObjectContext:context];
    
    sheetModel.id =[NSNumber numberWithInt: (int)sheetFullModel._ID];
    sheetModel.brand_id = [NSNumber numberWithInt: (int)sheetFullModel._brandID];
    sheetModel.model_id = [NSNumber numberWithInt:(int)sheetFullModel._modelID];
    sheetModel.type_id = [NSNumber numberWithInt:(int)sheetFullModel._typeID];
    sheetModel.name = sheetFullModel.name;
    sheetModel.name_race = sheetFullModel.name_race;
    sheetModel.track_size = sheetFullModel._track_size;
    sheetModel.track_type = sheetFullModel._track_type;
    sheetModel.track_name = sheetFullModel.track;
    sheetModel.traction_level = sheetFullModel._traction_level;
    sheetModel.track_surface = sheetFullModel._track_surface;
    sheetModel.conditions = [sheetFullModel._track_condition JSONRepresentation];
    sheetModel.isFavorite = [NSNumber numberWithInt:(int)sheetFullModel._isFavorite];
    sheetModel.json_detail = sheetFullModel.jsonDetail;
    sheetModel.date = sheetFullModel.date;
    sheetModel.city = sheetFullModel.city;
    sheetModel.country = sheetFullModel.country;
    
    // Save everything
    NSError *error = nil;
    if ([context save:&error]) {
        NSLog(@"The create save was successful!");
    } else {
        NSLog(@"The create save wasn't successful: %@", [error userInfo]);
    }
}
// get data from database
- (SheetFullInfoModel *) readSheetInfo:(NSInteger)sheetID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Construct a fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetInfo"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d", sheetID];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    SheetFullInfoModel *sheetFullInfo;
    for (SheetInfo *sheetInfo in fetchedObjects) {
        sheetFullInfo = [[SheetFullInfoModel alloc]init];
        sheetFullInfo._ID = [sheetInfo.id intValue];
        sheetFullInfo._brandID = [sheetInfo.brand_id intValue];
        sheetFullInfo._modelID = [sheetInfo.model_id intValue];
        sheetFullInfo._typeID = [sheetInfo.type_id intValue];
        sheetFullInfo._track_type = sheetInfo.track_type;
        sheetFullInfo._track_size = sheetInfo.track_size;
        sheetFullInfo._track_condition = [sheetInfo.conditions JSONValue];
        sheetFullInfo._traction_level = sheetInfo.traction_level;
        sheetFullInfo._track_surface = sheetInfo.track_surface;
        sheetFullInfo._isFavorite = [sheetInfo.isFavorite intValue];
        sheetFullInfo.jsonDetail = sheetInfo.json_detail;
        sheetFullInfo.name = sheetInfo.name;
        sheetFullInfo.track = sheetInfo.track_name;
        sheetFullInfo.name_race = sheetInfo.name_race;
        sheetFullInfo.date = sheetInfo.date;
        sheetFullInfo.city = sheetInfo.city;
        sheetFullInfo.country = sheetInfo.country;
  
    }
    return sheetFullInfo;
}
- (void) deleteSheetInfo:(NSInteger)idsheet
{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //  We're looking to grab an artist
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SheetInfo"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    // We specify that we only want
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %d",idsheet];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    for (SheetInfo *sheetInfo in fetchedObjects) {
        [context deleteObject:sheetInfo];
    }
    // Save everything
    if ([context save:&error]) {
        NSLog(@"The delete sheet info was successful!");
    } else {
        NSLog(@"The delete sheet info wasn't successful: %@", [error localizedDescription]);
    }
}
@end
