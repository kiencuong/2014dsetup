//
//  CSSignInViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CSHomeViewController;
@class CSProgress;
#import "SBJson.h"
@interface CSSignInViewController : UIViewController<CSCustomNavigationBarDelegate, UITextFieldDelegate>{
    
    IBOutlet UITextField *usernameTexfield;
    IBOutlet UITextField *passwordTextfield;
    IBOutlet UIButton *checkButton;
    IBOutlet UIImageView *bgTextFieldImage;
    IBOutlet UILabel *rememberLabel;
    IBOutlet UIImageView *logoImage;
    
    CSProgress *progress;
    CSHomeViewController *homeViewcontroller;
    UIButton *_doneButton;
    __weak IBOutlet UIView *viewLogin;
    int heightIOS7;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *logoImage;
@property (strong, nonatomic) IBOutlet UILabel *rememberLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgTextFieldImage;
@property (strong, nonatomic) IBOutlet UITextField *usernameTexfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (strong, nonatomic) IBOutlet UIButton *checkButton;

- (IBAction)checkButtonClick:(id)sender;
- (IBAction)viewLoginClick:(id)sender;

@end
