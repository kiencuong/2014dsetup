//
//  CSSignInViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSignInViewController.h"
#import "CSProgress.h"
#import "CSHomeViewController.h"
#import "AFNetworking.h"
@interface CSSignInViewController ()

@end

@implementation CSSignInViewController
@synthesize usernameTexfield;
@synthesize passwordTextfield;
@synthesize checkButton;
@synthesize logoImage;
@synthesize rememberLabel;
@synthesize bgTextFieldImage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    navigationCustomBar.title = @"SIGN IN";
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [navigationCustomBar addBackButton];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
        [navigationCustomBar addSignInButton:@"SignInButton.png"];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
    }else {
        [navigationCustomBar addBackButton_ipad];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
        [navigationCustomBar addSignInButton:@"SignInButton_ipad.png"];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar_ipad.png"];
    }

    heightIOS7 = DETECT_OS7? 20 :0;
    navigationCustomBar.delegate = self;
    [self.view addSubview:navigationCustomBar];
    usernameTexfield.delegate = self;
    passwordTextfield.delegate = self;
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    if (screenSize.height == 568) {
        logoImage.frame = CGRectMake(140, 76+heightIOS7, 40, 40);
//        bgTextFieldImage.frame = CGRectMake(20, 145, 280, 91);
//        usernameTexfield.frame = CGRectMake(60, 145, 238, 44);
//        passwordTextfield.frame = CGRectMake(60, 190, 238, 44);
        viewLogin.frame = CGRectMake(viewLogin.frame.origin.x, viewLogin.frame.origin.y+10+heightIOS7, viewLogin.frame.size.width, viewLogin.frame.size.width);
        checkButton.frame = CGRectMake(85, 250+heightIOS7, 25, 25);
        rememberLabel.frame = CGRectMake(120, 250+heightIOS7, 120, 21);
        
    }
    
    if ([[CSUtility loadInfoUserLogin:PASSWORD_LOGIN_SUCCESS] isEqualToString:@""]) {
        [usernameTexfield becomeFirstResponder];
    } else {
        usernameTexfield.text = [CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS];
        passwordTextfield.text = [CSUtility loadInfoUserLogin:PASSWORD_LOGIN_SUCCESS];
        [checkButton setSelected:YES];
    }
}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

//login user 
-(void)shoulSignIn{
    
    if ([usernameTexfield.text isEqualToString:@""] || [passwordTextfield.text isEqualToString:@""]) {
        [self alertStatus:@"Check your username and password" :@"Invalid Login" ] ;
    } else {
        [usernameTexfield resignFirstResponder];
        [passwordTextfield resignFirstResponder];
        if (![CSUtility checkNetworkAvailable]) {
            [self alertStatus:@"Network connection needed for this feature!" :@""];
            return;
        }
        [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
        NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                usernameTexfield.text, @"username",passwordTextfield.text,@"password",
                                nil];
        [httpClient postPath:@"api/user/login" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]; 
            NSDictionary *results = [responseString JSONValue];
            NSInteger error = [[results objectForKey:@"status"]intValue];
            
            
            if(error == 1)
            {
                NSDictionary *loginResult = [results objectForKey:@"data"];
                
                NSString *user_ID = [loginResult objectForKey:@"token"];
                NSString *firstLastName = [NSString stringWithFormat:@"%@ %@",[loginResult objectForKey:@"first_name"],[loginResult objectForKey:@"last_name"]];
                [CSUtility saveInfoUserLogin:user_ID forKey:ID_USER_LOGIN_SUCCESS];
                [CSUtility saveInfoUserLogin:firstLastName forKey:FIRSTLAST_NAME];
                [CSUtility saveInfoUserLogin:usernameTexfield.text forKey:USERNAME_LOGIN_SUCCESS];
                if ([checkButton isSelected]) {
                    [CSUtility saveStatusLogin:YES forKey:CHECK_USER_LOGIN_SUCCESS];
                    
                    [CSUtility saveInfoUserLogin:passwordTextfield.text forKey:PASSWORD_LOGIN_SUCCESS];
                }
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
                    homeViewcontroller = [[CSHomeViewController alloc]initWithNibName:@"CSHomeViewController" bundle:nil];
                }else {
                    homeViewcontroller = [[CSHomeViewController alloc]initWithNibName:@"CSHomeViewController_ipad" bundle:nil];
                }
                [self.navigationController pushViewController:homeViewcontroller animated:YES];
            } else if (error == 0) {
                [self alertStatus:@"Username or Password is incorrect!" :@"Message"];
                
            }else {
                [self alertStatus:@"The Account have been expiration date!" :@"Message"];
            }
            [self stopLoading];

            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
            [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
            [self stopLoading];
        }];
        
        
        
        return;
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    //    [dangNhapButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    usernameTexfield = nil;
    [self setUsernameTexfield:nil];
    passwordTextfield = nil;
    [self setPasswordTextfield:nil];
    checkButton = nil;
    [self setCheckButton:nil];
    [super viewDidUnload];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
- (IBAction)checkButtonClick:(id)sender {
    if ([checkButton isSelected]) {
        [checkButton setSelected:NO];
        [CSUtility saveInfoUserLogin:@"" forKey:USERNAME_LOGIN_SUCCESS];
        [CSUtility saveInfoUserLogin:@"" forKey:PASSWORD_LOGIN_SUCCESS];
    } else {
        [checkButton setSelected:YES];
    }
}

- (IBAction)viewLoginClick:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}
@end
