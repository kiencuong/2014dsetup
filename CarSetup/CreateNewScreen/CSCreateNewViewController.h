//
//  CSCreateNewViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCreateNewSheetViewEdittingViewController.h"
#import "CSCreateNewManager.h"
@class CSProgress;
@interface CSCreateNewViewController : UIViewController<CSCustomNavigationBarDelegate, UIPickerViewDataSource, UIPickerViewDelegate,CreateNewManagerDelegate>{
    IBOutlet UIButton *selectTypeButton;
    IBOutlet UIButton *selectBrandButton;
    IBOutlet UIButton *selectModelButton;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    IBOutlet UIButton *createNewButton;
    IBOutlet UIButton *doneButtonSelectPickerView;

    IBOutlet UIButton *prevButton;
    IBOutlet UIButton *nextButton;
    IBOutlet UIView *subviewPicker;
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIView *viewSelectInfoCar;
    
    NSMutableArray *_arrayTypeName;
    NSMutableArray *_arrayBrandName;
    NSMutableArray *_arrayModelName;
    
    NSInteger _indexRowTypeName;
    NSInteger _indexRowBrandName;
    NSInteger _indexRowModelName;
    NSInteger _indexButton;
    
    NSString *_typeName;
    NSString *_brandName;
    NSString *_modelName;
    NSString *_date;
    NSString *_stringBrandName;
    
    BOOL _checkButtonTypeClick;
    CGSize _screenSize;
    UIView *popupView;
    CSCreateNewManager *createNewManager;
    CSProgress *progress;
    NSMutableArray *_arraySheetInfo;
    int heightIOS7;
    
}
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) IBOutlet UIButton *selectTypeButton;
@property (strong, nonatomic) IBOutlet UIButton *selectBrandButton;
@property (strong, nonatomic) IBOutlet UIButton *selectModelButton;
@property (strong, nonatomic) IBOutlet UIButton *createNewButton;
@property (strong, nonatomic) IBOutlet UIView *subviewPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIButton *prevButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIView *viewSelectInfoCar;
@property (strong, nonatomic) CSCreateNewManager *createNewManager;

- (IBAction)selectTypeButtonClick:(id)sender;
- (IBAction)selectBrandButtonClick:(id)sender;
- (IBAction)selectModelButtonClick:(id)sender;
- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
- (IBAction)createNewButtonClick:(id)sender;
- (IBAction)doneButtonSelectPickerViewClick:(id)sender;
- (IBAction)subViewPickerClick:(id)sender;
- (IBAction)prevButtonClick:(id)sender;
- (IBAction)nextButtonClick:(id)sender;

@end
