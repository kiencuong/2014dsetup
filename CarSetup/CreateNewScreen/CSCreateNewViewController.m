//
//  CSCreateNewViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCreateNewViewController.h"
#import "CSProgress.h"
@interface CSCreateNewViewController ()

@end

@implementation CSCreateNewViewController
@synthesize aboutButton;
@synthesize signOutButton;
@synthesize selectTypeButton;
@synthesize selectBrandButton;
@synthesize selectModelButton;
@synthesize createNewButton;
@synthesize pickerView;
@synthesize subviewPicker;
@synthesize prevButton;
@synthesize nextButton;
@synthesize viewSelectInfoCar;
@synthesize createNewManager;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrayTypeName = [[NSMutableArray alloc]init];
        _arrayBrandName = [[NSMutableArray alloc]init];
        _arrayModelName = [[NSMutableArray alloc]init];
        _arraySheetInfo = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    heightIOS7 = DETECT_OS7? 20 :0;
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    [self initView];
    createNewManager = [[CSCreateNewManager alloc]init];
    createNewManager.delegate = self;
    [self loadListCreateNew];
}

// button back click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)initView {
    CGSize sizeIconNavigationBar;
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        sizeIconNavigationBar = CGSizeMake(38, 36);
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar:@"icon-CreateNewNavigationBar@2x"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    }else {
        sizeIconNavigationBar = CGSizeMake(32, 32);
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-CreateNewNavigationBar_ipad@2x"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
    }
    navigationCustomBar.delegate = self;
    [self.view addSubview:navigationCustomBar];
    [navigationCustomBar addImageLogo];
    [aboutButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    selectTypeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectBrandButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectBrandButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectModelButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectModelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    _screenSize = screenBound.size;
    if (_screenSize.height == 568) {
        createNewButton.frame = CGRectMake(21, 375+heightIOS7, 278, 45);
        subviewPicker.frame = CGRectMake(0, heightIOS7,_screenSize.width, _screenSize.height-20);
    }
    UIView *uibg =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenSize.width, _screenSize.height)];
    uibg.backgroundColor = [UIColor blackColor];
    uibg.opaque = YES;
    uibg.alpha = 0.3;
    popupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenSize.width, _screenSize.height)];
    popupView.backgroundColor = [UIColor clearColor];
    popupView.opaque = YES;
    popupView.alpha = 1.0;
    [self.view addSubview:popupView];
    [popupView addSubview:uibg];
    [popupView insertSubview:subviewPicker aboveSubview:uibg];
    popupView.hidden=YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark get list sheet
- (void)loadListCreateNew{
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [createNewManager loadListCreateNew];
    
}
- (void) finishGetlistCreateNew{
    _date = createNewManager.dateString;
    _arrayTypeName = createNewManager._arrayType;
    _arraySheetInfo = createNewManager._arraySheet;
    [self stopLoading];
    if (_checkButtonTypeClick) {
        _checkButtonTypeClick = NO;
        [selectTypeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}
#pragma mark check sheet exist

- (void)checkSheetInfoExist{
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Connection fail. Please check it again." :@"Notification"];
        return;
    }
    int brandID = [[_arrayBrandName objectAtIndex:_indexRowBrandName]_brand_id];
    int modelID = [[_arrayModelName objectAtIndex:_indexRowModelName]_model_id];
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [createNewManager checkSheetExist:brandID :modelID];
}
- (void) finishcheckSheetExist{
    [self stopLoading];
    if (createNewManager.checkSheetExist) {
        CSCreateNewSheetViewEdittingViewController *sheetViewEditting;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            sheetViewEditting = [[CSCreateNewSheetViewEdittingViewController alloc] initWithNibName:@"CSCreateNewSheetViewEdittingViewController" bundle:nil];
        } else {
            sheetViewEditting = [[CSCreateNewSheetViewEdittingViewController alloc] initWithNibName:@"CSCreateNewSheetViewEdittingViewController_ipad" bundle:nil];
            
        }
        
        sheetViewEditting._typeID = [[_arrayTypeName objectAtIndex:_indexRowTypeName]_type_id];
        sheetViewEditting._brandID = [[_arrayBrandName objectAtIndex:_indexRowBrandName]_brand_id];
        sheetViewEditting._modelID = [[_arrayModelName objectAtIndex:_indexRowModelName]_model_id];
        sheetViewEditting._dateSheet = _date;
        [self.navigationController pushViewController:sheetViewEditting animated:YES];
    }
    
}
#pragma mark action view
- (IBAction)selectTypeButtonClick:(id)sender {
    
    if ([_arrayTypeName count]==0) {
        _checkButtonTypeClick = YES;
        [self loadListCreateNew];
    } else {
    [popupView setHidden:NO];
    _indexButton=0;
    [self checkButtonSelected];
    }
}

- (IBAction)selectBrandButtonClick:(id)sender {
    [popupView setHidden:NO];
    _indexButton=1;
    [self checkButtonSelected];
}

- (IBAction)selectModelButtonClick:(id)sender {
    [popupView setHidden:NO];
    _indexButton=2;
    [self checkButtonSelected];
}

- (IBAction)aboutButtonClick:(id)sender {
    [aboutButton setSelected:YES];
    [signOutButton setSelected:NO];
    
}
#pragma mark check button popup
-(void)checkButtonSelected{
    int cellSelect;
    switch (_indexButton) {
        case 0:{
            prevButton.enabled = NO;
            nextButton.enabled = YES;
            doneButtonSelectPickerView.enabled = NO;
            [selectTypeButton setSelected:YES];
            [selectModelButton setSelected:NO];
            [selectBrandButton setSelected:NO];
            viewSelectInfoCar.frame = CGRectMake(0, 44+heightIOS7, 320, 300);
            if (!_typeName) {
                _typeName = [[_arrayTypeName objectAtIndex:0]_type_name];
                selectBrandButton.enabled = YES;
            }
            [selectTypeButton setTitle:_typeName forState:UIControlStateNormal];
            cellSelect = _indexRowTypeName;
            break;
        }
        case 1:{
            prevButton.enabled = YES;
            nextButton.enabled = YES;
            doneButtonSelectPickerView.enabled = NO;
            [selectBrandButton setSelected:YES];
            [selectModelButton setSelected:NO];
            [selectTypeButton setSelected:NO];
            if (_screenSize.height == 568) {
                viewSelectInfoCar.frame = CGRectMake(0, 44+heightIOS7, 320, 300);
            } else {
            viewSelectInfoCar.frame = CGRectMake(0,-20+heightIOS7, 320, 300);
            }
            [self selectBrand: [[_arrayTypeName objectAtIndex:_indexRowTypeName]_type_id]];
             
            if (!_brandName) {
                _brandName = [[_arrayBrandName objectAtIndex:0]_brand_name];
                selectModelButton.enabled = YES;
            }
            [selectBrandButton setTitle:_brandName forState:UIControlStateNormal];
            cellSelect = _indexRowBrandName;
            break;
        }
            
        default:{
            nextButton.enabled = NO;
            prevButton.enabled = YES;
            doneButtonSelectPickerView.enabled = YES;
            [selectModelButton setSelected:YES];
            [selectTypeButton setSelected:NO];
            [selectBrandButton setSelected:NO];
            if (_screenSize.height == 568) {
                viewSelectInfoCar.frame = CGRectMake(0,0+heightIOS7, 320, 300);
            } else {
            viewSelectInfoCar.frame = CGRectMake(0, -100+heightIOS7, 320, 300);
            }
            [self selectModel:[[_arrayTypeName objectAtIndex:_indexRowTypeName]_type_id] :[[_arrayBrandName objectAtIndex:_indexRowBrandName]_brand_id]];
            if (!_modelName) {
                _modelName = [[_arrayModelName objectAtIndex:0]_model_name];
            }
            [selectModelButton setTitle:_modelName forState:UIControlStateNormal];
            cellSelect = _indexRowModelName;
            break;
        }
    }
    [pickerView reloadAllComponents];
    [pickerView selectRow:cellSelect inComponent:0 animated:YES];
}

- (void)selectBrand: (NSInteger)typeId {
    [_arrayBrandName removeAllObjects];
    _stringBrandName = @"";
    for (int j =0;j <[_arraySheetInfo count]; j++) {
        if (typeId == [[_arraySheetInfo objectAtIndex:j]_type_id])
        {
            if ([_stringBrandName isEqualToString: [[_arraySheetInfo objectAtIndex:j]_brand_name]]) {
                continue;
            }
            _stringBrandName =  [[_arraySheetInfo objectAtIndex:j]_brand_name];
            [_arrayBrandName addObject:[_arraySheetInfo objectAtIndex:j]];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_brand_name"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //            NSArray *sortedArray;
    //            sortedArray = [searchModel._arrayType sortedArrayUsingDescriptors:sortDescriptors];
    [_arrayBrandName sortUsingDescriptors:sortDescriptors];
    
    
}

- (void)selectModel: (NSInteger)typeId :(NSInteger)brandId  {
    [_arrayModelName removeAllObjects];
    for (int j =0;j <[_arraySheetInfo count]; j++) {
        
        if (typeId == [[_arraySheetInfo objectAtIndex:j]_type_id] && brandId == [[_arraySheetInfo objectAtIndex:j]_brand_id])
        {
            [_arrayModelName addObject:[_arraySheetInfo objectAtIndex:j]];
        }
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_model_id"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //            NSArray *sortedArray;
    //            sortedArray = [searchModel._arrayType sortedArrayUsingDescriptors:sortDescriptors];
    [_arrayModelName sortUsingDescriptors:sortDescriptors];
    
}

- (IBAction)signOutButtonClick:(id)sender {
    [aboutButton setSelected:NO];
    [signOutButton setSelected:YES];
    [CSUtility saveStatusLogin:NO forKey:CHECK_USER_LOGIN_SUCCESS  ];
    [CSUtility saveInfoUserLogin:@"" forKey:ID_USER_LOGIN_SUCCESS];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)createNewButtonClick:(id)sender {
    if (_modelName && _typeName && _brandName ) {
        
        [self checkSheetInfoExist];
    } else {
    [self alertStatus:@"Make a selection for each field" :@"Empty Fields"];
    }
}

- (IBAction)doneButtonSelectPickerViewClick:(id)sender {
    [popupView setHidden:YES];
    [selectTypeButton setSelected:NO];
    [selectModelButton setSelected:NO];
    [selectBrandButton setSelected:NO];
    viewSelectInfoCar.frame = CGRectMake(0, 44+heightIOS7, 320, 300);
}

- (IBAction)subViewPickerClick:(id)sender {
    [popupView setHidden:YES];
    [selectTypeButton setSelected:NO];
    [selectModelButton setSelected:NO];
    [selectBrandButton setSelected:NO];
    viewSelectInfoCar.frame = CGRectMake(0, 44+heightIOS7, 320, 300);
}

- (IBAction)prevButtonClick:(id)sender {
    _indexButton--;
    [self checkButtonSelected];
}

- (IBAction)nextButtonClick:(id)sender {
    _indexButton++;
    [self checkButtonSelected];
}
- (void)viewDidUnload {
    createNewButton = nil;
    [self setCreateNewButton:nil];
    subviewPicker = nil;
    [self setSubviewPicker:nil];
    pickerView = nil;
    [self setPickerView:nil];
    doneButtonSelectPickerView = nil;
    prevButton = nil;
    [self setPrevButton:nil];
    [self setNextButton:nil];
    nextButton = nil;
    viewSelectInfoCar = nil;
    [self setViewSelectInfoCar:nil];
    [super viewDidUnload];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

#pragma mark interface Orientation
// only run in iOS 6

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark - picker view

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//Here we are setting the number of rows in the pickerview to the number of objects of the NSArray. You should understand this since we covered it in the tableview tutorial.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    switch (_indexButton) {
        case 0:
            return [_arrayTypeName count];
            break;
        case 1:
            return [_arrayBrandName count];
            break;
            
        default:
            return [_arrayModelName count];
            break;
    }
}

//Here we are setting the values of the NSArray to the pickerview's rows. This is pretty much identical to the way you load a table view.
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch (_indexButton) {
        case 0:
            return [[_arrayTypeName objectAtIndex:row]_type_name];
            break;
        case 1:
            return [[_arrayBrandName objectAtIndex:row]_brand_name];
            break;
            
        default:
            return [[_arrayModelName objectAtIndex:row]_model_name];
            break;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch (_indexButton) {
        case 0:{
            if (_indexRowTypeName != row) {
                _indexRowBrandName = 0;
                _indexRowModelName = 0;
                _modelName = nil;
                _brandName = nil;
                [selectBrandButton setTitle:@"" forState:UIControlStateNormal];
                [selectModelButton setTitle:@"" forState:UIControlStateNormal];
                selectModelButton.enabled = NO;
            }
                _indexRowTypeName = row;
                _typeName = [[_arrayTypeName objectAtIndex:row]_type_name];
                [selectTypeButton setTitle:_typeName forState:UIControlStateNormal];
                break;
            }
            
        case 1:
            {
                if (_indexRowBrandName != row) {
                    _indexRowModelName = 0;
                    _modelName = nil;
                    [selectModelButton setTitle:@"" forState:UIControlStateNormal];
                }
                _indexRowBrandName = row;
                _brandName = [[_arrayBrandName objectAtIndex:row]_brand_name];
                [selectBrandButton setTitle:_brandName forState:UIControlStateNormal];
                break;
            }
            
        default:
            _indexRowModelName = row;
            _modelName = [[_arrayModelName objectAtIndex:row]_model_name];
            [selectModelButton setTitle:_modelName forState:UIControlStateNormal];
            break;
    }
  }
-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
@end
