//
//  CSCreateNewSheetViewEdittingViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/28/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCreateNewSheetViewEdittingViewController.h"
#import "CSHomeViewController.h"
#import "CSMySetupViewController.h"
#import "CSWellcomeViewController.h"
#import "CSSignInViewController.h"
@interface CSCreateNewSheetViewEdittingViewController ()

@end

@implementation CSCreateNewSheetViewEdittingViewController
@synthesize iconNavigationBarImage;
@synthesize webViewEdditing;
@synthesize _typeID;
@synthesize _brandID;
@synthesize _modelID;
@synthesize _dateSheet;
@synthesize createNewSheetManager;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        createNewSheetManager = [[CSCreateNewSheetManager alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    _screenSize = screenBound.size;
    webViewEdditing.scrollView.delegate = self;
    createNewSheetManager.delegate = self;
    [self createDoneButton];

    [webViewEdditing loadHTMLString:[self getContentFormHTML]  baseURL:nil];
}

- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)shouldGoSave{
    [webViewEdditing stringByEvaluatingJavaScriptFromString:@"getInfoSheet();"];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark custom button done in ipad keyboard
- (void)doneButton:(id)sender {
    
    [self.view endEditing:YES];
}

- (void)createDoneButton {
	// create custom button
    
    if (_doneButton == nil) {
        
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.adjustsImageWhenHighlighted = NO;
        [_doneButton setImage:[UIImage imageNamed:@"doneButton.png"] forState:UIControlStateNormal];
        [_doneButton setImage:[UIImage imageNamed:@"doneButton.png"] forState:UIControlStateHighlighted];
        
        [_doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
        
        _doneButton.hidden = YES;  // we hide/unhide him from here on in with the appropriate method
    }
}

- (void)hideDoneButton
{
    [_doneButton removeFromSuperview];
    _doneButton.hidden = YES;
}

- (void)unhideDoneButton
{
    // this here is a check that prevents NSRangeException crashes that were happening on retina devices
    int windowCount = [[[UIApplication sharedApplication] windows] count];
    if (windowCount < 2) {
        return;
    }
    
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex: 1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++) {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        
        // so the first time you unhide, it gets put on one subview, but in subsequent tries, it gets put on another.  this is why we have to keep adding and removing him from its superview.
        
        // THIS IS THE HACK BELOW.  I MEAN, PROPERLY HACKY!
        if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
        {
            _doneButton.frame = CGRectMake(keyboard.frame.size.width-53-35, -3, 106, 53);
            [keyboard addSubview: _doneButton];
        }
        else if([[keyboard description] hasPrefix:@"<UIKeyboardA"] == YES)
        {
            _doneButton.frame = CGRectMake(keyboard.frame.size.width-53-35, -3, 106, 53);
            [keyboard addSubview: _doneButton];
        }
    }
    
    _doneButton.hidden = NO;
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    /* No longer listen for keyboard */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    
    [super viewWillDisappear:animated];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    [self unhideDoneButton];
    // unused.
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    [self unhideDoneButton];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    // unused
    [self hideDoneButton];
}
- (void)keyboardDidHide:(NSNotification *)notification
{
    // unused
    [self hideDoneButton];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    NSLog(@"%@ for textField %i", NSStringFromSelector(_cmd), textField.tag);
    
    [self hideDoneButton];
    
    return YES;
}


- (void) viewWillAppear:(BOOL)animated {
    /* Listen for keyboard */
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    [super viewWillAppear:animated];
    
}
#pragma mark create new sheet
- (void)loadCreateNewSheet{
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Connection fail. Please check it again." :@"Notification"];
        return;
    }
    
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [createNewSheetManager loadCreateNewSheet:sheetFullModel];
    
}
- (void) finishloadCreateNewSheet{
    if (createNewSheetManager.checkLoginAgain) {
        
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
    }
    [self stopLoading];
    
    CSMySetupViewController *mysetupViewcontroller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        mysetupViewcontroller = [[CSMySetupViewController alloc]initWithNibName:@"CSMySetupViewController" bundle:nil];
    } else {
        mysetupViewcontroller = [[CSMySetupViewController alloc]initWithNibName:@"CSMySetupViewController_ipad" bundle:nil];
    }
    if (createNewSheetManager.checkCreateNewSuccess) {
        NSArray *viewControllers = self.navigationController.viewControllers;
        NSMutableArray *newViewControllers = [NSMutableArray array];
        for (id object in viewControllers) {
            if ([object isKindOfClass:[CSWellcomeViewController class]]) {
                [newViewControllers addObject:object];
            } else if ([object isKindOfClass:[CSSignInViewController class]]) {
                [newViewControllers addObject:object];
            } else if ([object isKindOfClass:[CSHomeViewController class]]) {
                [newViewControllers addObject:object];
            }
        }
        [newViewControllers addObject:mysetupViewcontroller];
    
        self.navigationController.viewControllers = newViewControllers;
        [self.navigationController popViewControllerAnimated:YES];
    }
    
//    [self.navigationController setViewControllers:newViewControllers animated:YES];

//    if (createNewSheetManager.checkCreateNewSuccess) {
//        CSHomeViewController *homeView;
//        for (id object in [self.navigationController viewControllers]) {
//            if ([object isKindOfClass:[CSHomeViewController class]]) {
//                homeView = object;
//            }
//        }
//        homeView._checkCreateNew = YES;
//        [self.navigationController popToViewController:homeView animated:YES];
//    }

}
- (NSString *) getContentFormHTML{
    NSError *error;
    NSString *html;
    NSString *contentHtml;
    if (_typeID == 1) {
        switch (_brandID) {
            case 1:
            {
                
                switch (_modelID) {
                    case 1:{
                        html= [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_22" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 12:{
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLR22T" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22t" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_22T" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 14:{
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLR22SCT" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22SCT" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_22SCT" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                        
                    case 34: {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLRSCTE2.0" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_SCTE2.0" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_SCTE_2.0" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 46: {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR22_22.0" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR22_22.0" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR22_22.0" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 49: {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR22-4" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR22-4" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR22-4" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 50: {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30" ofType:@"png"], _dateSheet];
                        break;
                    }

                        

                }
                break;
            }
            case 7:{
                if (_modelID == 45) {
                    
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_SV2" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_SV2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Schumacher_SV2" ofType:@"png"], _dateSheet];
                }else if(_modelID == 13){
                    
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_K1" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_K1" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Schumacher_K1" ofType:@"jpg"], _dateSheet];
                }

                
                break;
            }
            case 8:
            {
                switch (_modelID) {
                    case 15:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_SC10.2" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_SC10.2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_SC10.2" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 17:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_B42" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_B42" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 18:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_T42" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_T42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_T42" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 19:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_B442" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B442" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_B442" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 48:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_C42" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_C42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_C42" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 53:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_B5" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B5" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_B5" ofType:@"png"], _dateSheet];
                        break;
                    }
                    case 54:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_B5M" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B5M" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_B5M" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                }
                break;
            }
            case 9:{
                if (_modelID == 36) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_RB6" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_RB6" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Kyosho_RB6" ofType:@"png"], _dateSheet];

                }else if (_modelID == 37){
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R" ofType:@"png"], _dateSheet];
                }

                
                break;
            }
            case 10:
            {
                if (_modelID == 40) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2" ofType:@"png"], _dateSheet];
                       
                }
                break;
            }
            case 12:
            {
                switch (_modelID) {
                    case 22:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DESC210" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DESC210" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Durango_DESC210" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 23:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DEX210" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DEX210" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Durango_DEX210" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 24:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DESC410" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DESC410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Durango_DESC410" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 25:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DEX410" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DEX410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Durango_DEX410" ofType:@"png"], _dateSheet];
                        break;
                    }
                }
                break;
            }
            case 29:
            {
                switch (_modelID) {
                    case 51:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2" ofType:@"png"], _dateSheet];
                        break;
                    }
                        
                    case 52:
                    {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID" ofType:@"png"], _dateSheet];
                        break;
                    }

                }
                break;
                
            }
            case 30:
            {
                if (_modelID == 56) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_SCT410" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_SCT410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Tekno_SCT410" ofType:@"png"], _dateSheet];
                    
                } else if (_modelID == 60) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"png"], _dateSheet];
                    
                } else if (_modelID == 61) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_ET48" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_ET48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Tekno_ET48" ofType:@"png"], _dateSheet];
                    
                }
                break;
            }
            case 31:
            {
                if (_modelID == 57) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_D413" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_D413" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Hotbodies_D413" ofType:@"png"], _dateSheet];
                    
                }
                break;
            }
            default:
                break;
        }

    } else if(_typeID ==2)
    {
        switch (_brandID) {
            case 13:
            {
                
                if (_modelID == 26) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_S411" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_S411" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Serpent_S411" ofType:@"png"], _dateSheet];
                }
                break;
            }
    
            case 17:
            {
                
                if (_modelID == 29) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX" ofType:@"png"], _dateSheet];
                }
                break;
            }
            case 23:
            {
                
                if (_modelID == 39) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Yokomo_BD7" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Yokomo_BD7" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Yokomo_BD7" ofType:@"png"], _dateSheet];
                }
                break;
            }
            case 24:{
                if (_modelID == 41) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5" ofType:@"png"], _dateSheet];
                    
                }
                break;
            }
            case 28:{
                if (_modelID == 47) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Awesomatix" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Awesomatix" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Awesomatix" ofType:@"png"], _dateSheet];
                    
                }
                break;
            }


        }
    }
    else if(_typeID ==3)
    {
        switch (_brandID) {
            case 20:
            {
                
                if (_modelID == 35) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR_8ight30" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR_8ight30" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"TLR_8ight30" ofType:@"png"], _dateSheet];
                }
               
                break;
            }
            case 11:
            {
                if (_modelID == 21) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Mugen_MBX7" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Mugen_MBX7" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Mugen_MBX7" ofType:@"png"], _dateSheet];

                }
                break;
            }
            case 14:
            {
                if (_modelID == 27) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_811B" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_811B" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Serpent_811B" ofType:@"png"], _dateSheet];

                }
                break;
            }

                
                
                
            case 16:
            {
                
                if (_modelID == 28) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_D812" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_D812" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Hotbodies_D812" ofType:@"png"], _dateSheet];

                }
                 break;
            }
            case 27:
            {
                
                if (_modelID == 44) {
                        html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_RC82" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                        contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_RC82" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Associated_RC82" ofType:@"png"], _dateSheet];
                }
                break;
            }
        case 21:
        {
            if (_modelID == 16) {
                html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_MP9" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_MP9" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Kyosho_MP9" ofType:@"png"], _dateSheet];
            }
            break;
        }
            case 25:
            {
                if (_modelID == 42) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo" ofType:@"png"], _dateSheet];
                }
                break;
            }
            case 26:
            {
                if (_modelID == 43) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR" ofType:@"png"], _dateSheet];
                }
                break;
            }
            case 32:
            {
                if (_modelID == 58) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_NB48" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_NB48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Tekno_NB48" ofType:@"png"], _dateSheet];
                } else if(_modelID == 59) {
                    html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_NT48" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
                    contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_NT48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"Tekno_NT48" ofType:@"png"], _dateSheet];
                }
                break;
            }

        }
    }

    return contentHtml;

}
#pragma mark interface Orientation
// only run in iOS 5 & iOS 4.3
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (_checkRotation < 2) {
        [self updateViewPortait];
        _checkRotation++;
        return UIInterfaceOrientationMaskPortrait;
    }
    NSInteger rotationDevice = [[UIDevice currentDevice] orientation];
    if (rotationDevice <=2 ) {
        [self updateViewPortait];
        return UIInterfaceOrientationMaskPortrait;
        
    } else {
        [self updateViewLanscape];
        return UIInterfaceOrientationMaskLandscape;
    }
}

// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
        if (_checkRotation < 2) {
            [self updateViewPortait];
            _checkRotation++;
            return UIInterfaceOrientationMaskPortrait;
        }
        NSInteger rotationDevice = [[UIDevice currentDevice] orientation];
        if (rotationDevice <=2 ) {
            [self updateViewPortait];
            return UIInterfaceOrientationMaskPortrait;
            
        } else {
            [self updateViewLanscape];
            return UIInterfaceOrientationMaskLandscape;
        }
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
    
    CGSize contentSize = webViewEdditing.scrollView.contentSize;
    CGSize viewSize = self.webViewEdditing.bounds.size;
    
    _scaleWebview = (float) viewSize.width / contentSize.width;
    
}
-(void) updateViewPortait {
    if (_scaleWebview == 1) {
        [webViewEdditing stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.0;"];
        
    }
    float ratioHeigthWidth;
    CGSize contentSize = webViewEdditing.scrollView.contentSize;
    CGSize viewSize = self.webViewEdditing.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webViewEdditing.scrollView.minimumZoomScale = ratioHeigthWidth;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((_screenSize.width-38)/2, 3+heightIOS7, 38, 36);
    } else {
        iconNavigationBarImage.frame = CGRectMake((_screenSize.width-32)/2, 6+heightIOS7, 32, 32);
    }
}

-(void) updateViewLanscape {
    if (_scaleWebview == 1) {
        
        // for ipad
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            [webViewEdditing stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.4;"];
            // for iphone
        }else{
            if (_screenSize.height == 568) {
                [webViewEdditing stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.9;"];
            } else {
                [webViewEdditing stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.6;"];
            }
        }
        
    }
    float ratioHeigthWidth;
    CGSize contentSize = webViewEdditing.scrollView.contentSize;
    CGSize viewSize = self.webViewEdditing.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webViewEdditing.scrollView.minimumZoomScale = ratioHeigthWidth;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((_screenSize.height-38)/2, 3+heightIOS7, 38, 36);
        
    } else {
        iconNavigationBarImage.frame = CGRectMake((_screenSize.height-32)/2, 6+heightIOS7, 32, 32);
    }
}

#pragma mark webview delegate
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSArray *urlParamsArray;
	NSString *url = [[request URL] absoluteString];
    if([url rangeOfString:@"%7B" options:NSCaseInsensitiveSearch].length > 0){
         urlParamsArray= [url componentsSeparatedByString:@"/%7B"];
         NSString *jsonString = [[[NSString stringWithFormat:@"%@%@", @"%7B", [urlParamsArray objectAtIndex:1]] stringByReplacingOccurrencesOfString:@"/%22" withString:@"\\\%22"]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@" string convert :%@",jsonString);
        [self getInfoSheetFromHTML:jsonString];
        return NO;
    } else {
        if([url rangeOfString:@"param" options:NSCaseInsensitiveSearch].length > 0){
            urlParamsArray= [url componentsSeparatedByString:@"param="];
            NSInteger idField = [[urlParamsArray objectAtIndex:1]intValue];
            switch (idField) {
                case 1:
                    [self alertStatus:@"Form can't be send.\nPlease fill name field.":@"" ];
                    break;
                case 2:
                    [self alertStatus:@"Form can't be send.\nPlease fill track field.":@"" ];
                    break;
                case 3:
                    [self alertStatus:@"Form can't be send.\nPlease fill city field.":@"" ];
                    break;
                case 4:
                    [self alertStatus:@"Form can't be send.\nPlease fill race field.":@"" ];
                    break;
                    
                default: 
                    [self alertStatus:@"Form can't be send.\nPlease fill country field.":@"" ];
                    break;
            }
            return NO;
        }
        return YES;
    }
}

- (void)getInfoSheetFromHTML:(NSString *)stringJsonHTML {
//    "Dfgfgdf /""
    stringJsonHTML = [stringJsonHTML stringByReplacingOccurrencesOfString:@"\\\"" withString:@"&#34"];
    NSDictionary *results = [stringJsonHTML JSONValue];
    NSMutableDictionary *sheetInfo = [results objectForKey:@"sheets"];
        sheetFullModel = nil;
        sheetFullModel = [[SheetFullInfoModel alloc]init];
        sheetFullModel._typeID = _typeID;
        sheetFullModel._brandID = _brandID;
        sheetFullModel._modelID = _modelID;
        sheetFullModel.date = _dateSheet;
    sheetFullModel._track_size = [sheetInfo objectForKey:@"track_size"];
    sheetFullModel._traction_level = [sheetInfo objectForKey:@"traction_level"];
    sheetFullModel._track_type = [sheetInfo objectForKey:@"track_type"];
    sheetFullModel._track_surface = [sheetInfo objectForKey:@"track_surface"];
    
    
    
    if ([sheetInfo objectForKey:@"name"]) {
        sheetFullModel.name = [sheetInfo objectForKey:@"name"];
    }
    if ([sheetInfo objectForKey:@"track"]) {
        sheetFullModel.track= [sheetInfo objectForKey:@"track"];
    }
    if ([sheetInfo objectForKey:@"city"]) {
        sheetFullModel.city = [sheetInfo objectForKey:@"city"];
    }
    
    if ([sheetInfo objectForKey:@"country"]) {
        sheetFullModel.country = [sheetInfo objectForKey:@"country"];
    }
    
    if ([sheetInfo objectForKey:@"race"]) {
        sheetFullModel.name_race = [sheetInfo objectForKey:@"race"];
    }
    
    
    if ([sheetInfo objectForKey:@"track_condition"]) {
        sheetFullModel._track_condition = [sheetInfo objectForKey:@"track_condition"];
    }
    if (![sheetInfo objectForKey:@"password"]|| ![sheetInfo objectForKey:@"check_password"]) {
        [sheetInfo setObject:@"" forKey:@"password"];
    } else sheetFullModel.password = [sheetInfo objectForKey:@"password"];
    sheetFullModel.jsonDetail =[[[sheetInfo JSONRepresentation]stringByReplacingOccurrencesOfString:@"/n" withString:@"\\r\\n"]stringByReplacingOccurrencesOfString:@"&#34" withString:@"\\\""];
    [self loadCreateNewSheet];

    
}
- (IBAction)saveButtonClick:(id)sender {
    [self.view endEditing:YES];
    [webViewEdditing stringByEvaluatingJavaScriptFromString:@"getInfoSheets();"];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (createNewSheetManager.checkLoginAgain) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
}
-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
- (void)viewDidUnload {
    webViewEdditing = nil;
    [self setWebViewEdditing:nil];
    [super viewDidUnload];
}
@end
