//
//  CSCreateNewSheetViewEdittingViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/28/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SheetFullInfoModel.h"
#import "SBJson.h"
#import "CSCreateNewSheetManager.h"
#import "CSProgress.h"
@class CSHomeViewController;
@interface CSCreateNewSheetViewEdittingViewController : UIViewController<CSCustomNavigationBarDelegate, CSCreateNewSheetManagerDelegate, UIWebViewDelegate,UIScrollViewDelegate>{

    IBOutlet UIImageView *iconNavigationBarImage;
    IBOutlet UIWebView *webViewEdditing;
    UIButton *_doneButton;
    
    NSInteger _checkRotation;
    NSInteger _typeID;
    NSInteger _brandID;
    NSInteger _modelID;
    NSInteger _scaleWebview;
    
    NSString *_dateSheet;
    CGSize _screenSize;
    SheetFullInfoModel *sheetFullModel;
    CSCreateNewSheetManager *createNewSheetManager;
    CSProgress *progress;
    int heightIOS7;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *iconNavigationBarImage;
@property (strong, nonatomic) IBOutlet UIWebView *webViewEdditing;
@property (assign, nonatomic) NSInteger _typeID;
@property (assign, nonatomic) NSInteger _brandID;
@property (assign, nonatomic) NSInteger _modelID;
@property (strong, nonatomic) NSString *_dateSheet;
@property (strong, nonatomic) CSCreateNewSheetManager *createNewSheetManager;

- (IBAction)backButtonClick:(id)sender;
- (IBAction)saveButtonClick:(id)sender;
@end
