//
//  CSCreateNewManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/4/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCreateNewManager.h"
#import "AFNetworking.h"
@implementation CSCreateNewManager
@synthesize delegate;
@synthesize _arrayType;
@synthesize _arraySheet;
@synthesize dateString;
@synthesize checkSheetExist;
- (id)init{
    self = [super init];
    if (self) {
        _arrayType = [[NSMutableArray alloc]init];
        _arraySheet = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)loadListCreateNew{
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:@"api/user/typeBrandModel" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            NSDictionary *getListCategoryResult = [results objectForKey:@"data"];
            dateString = [getListCategoryResult objectForKey:@"date"];
            NSArray *getListTypes = [getListCategoryResult objectForKey:@"types"];
            
            for (NSDictionary *typeInfo in getListTypes) {
                MySetupBrankModel *typeCarInfo = [[MySetupBrankModel alloc]init];
                typeCarInfo._type_id = [[typeInfo objectForKey:@"id"] intValue];
                typeCarInfo._type_name = [typeInfo objectForKey:@"name"];
                
                [_arrayType addObject:typeCarInfo];
            }
            
            NSArray *getListsheetsResult = [getListCategoryResult objectForKey:@"data"];
            for (NSDictionary *carInfo in getListsheetsResult) {
                MySetupBrankModel *createModel = [[MySetupBrankModel alloc]init];
                createModel._type_id= [[carInfo objectForKey:@"type_id"] intValue];
                createModel._type_name = [carInfo objectForKey:@"type_name"];
                createModel._brand_id = [[carInfo objectForKey:@"brand_id"]intValue];
                createModel._brand_name = [carInfo objectForKey:@"brand_name"];
                createModel._model_id = [[carInfo objectForKey:@"model_id"] intValue];
                createModel._model_name = [carInfo objectForKey:@"model_name"];
                
                [_arraySheet addObject:createModel];
            }
            
            
        } else {
            
            [self alertStatus:message :@"Message"];
            
        }
        if ([delegate respondsToSelector:@selector(finishGetlistCreateNew)])
            [delegate finishGetlistCreateNew];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
        if ([delegate respondsToSelector:@selector(finishGetlistCreateNew)])
            [delegate finishGetlistCreateNew];
    }];
    
}

- (void)checkSheetExist:(NSInteger) brandID :(NSInteger) modelID{
    
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:brandID],@"brand",[NSNumber numberWithInt:modelID],@"model", nil];
    [httpClient postPath:@"api/sheet/getTemplate" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            checkSheetExist = YES;
            
        } else {
            checkSheetExist = NO;
            [self alertStatus:message :@"Message"];
            
        }
        if ([delegate respondsToSelector:@selector(finishcheckSheetExist)])
            [delegate finishcheckSheetExist];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
        if ([delegate respondsToSelector:@selector(finishcheckSheetExist)])
            [delegate finishcheckSheetExist];
    }];
    
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
