//
//  CSDeleteMySetupSheet.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/8/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
#import "AppDelegate.h"
@protocol deleteSheetManagerDelegate;
@interface CSDeleteMySetupSheet : NSObject{
    BOOL checkRemoveSheetSuccess;
    BOOL checkLoginAgain;

}
@property (nonatomic, assign) BOOL checkLoginAgain;
@property (nonatomic, assign) id<deleteSheetManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkRemoveSheetSuccess;
- (void)deleteSheet:(NSInteger)_id;
@end

@protocol deleteSheetManagerDelegate <NSObject>
@optional
- (void)finishdeleteSheet;

@end