//
//  CSGetInfoSheetManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/9/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SheetModel.h"
#import "SBJson.h"
#import "SheetFullInfoModel.h"
#import "AppDelegate.h"
@protocol GetInfoSheetManagerDelegate;
@interface CSGetInfoSheetManager : NSObject{
    id <GetInfoSheetManagerDelegate> _delegate;
    SheetFullInfoModel *_sheetModel;
    NSInteger index;
    NSMutableArray *arrayDowloadSheet;
    NSMutableArray *arrayDowloadFavoriteSheet;
    BOOL checkLoginAgain;
}
- (void)loadDataListSetup: (NSInteger) _idSheet;
@property (nonatomic, assign) id<GetInfoSheetManagerDelegate> delegate;
@property (nonatomic, strong) SheetFullInfoModel *_sheetModel;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger indexFavorite;
@property (nonatomic, strong) NSMutableArray *arrayDowloadSheet;
@property (nonatomic, strong) NSMutableArray *arrayDowloadFavoriteSheet;
@property (nonatomic, assign) BOOL checkLoginAgain;
-(NSString *)getContentHtml:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR22T:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR22_22_0:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR22_4:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR22SCT:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLRSCTE:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR_8ight30:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTLR8ightE:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlHotbodies_D812:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlHotbodies_D413:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlHotbodies_TCXX:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlAssociated_SC:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_B42:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_T42:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_C42:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_B442:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_RC82:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_B5:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAssociated_B5M:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlDurango_DESC210:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlDurango_DEX210:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlDurango_DESC410:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlDurango_DEX410:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlYokomo_BMAX2:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlYokomo_BD7:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSchumacher_SV2:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSchumacher_K1:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSchumacher_Mi5:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlKyosho_RB6:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlKyosho_MP9:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlKyosho_UltimaSCR:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlSerpent_S411:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSerpent_SRX2:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSerpent_SRX2_MID:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlMugen_MBX7:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlSerpent_811B:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlAgama_A8Evo:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlJQProducts_TheCAR:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlAwesomatix:(SheetFullInfoModel *)sheetFull;

-(NSString *)getContentHtmlTekno_SCT410:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTekno_NB48:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTekno_NT48:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTekno_EB482:(SheetFullInfoModel *)sheetFull;
-(NSString *)getContentHtmlTekno_ET48:(SheetFullInfoModel *)sheetFull;

-(SheetFullInfoModel *)parserJsonSheetFull:(SheetFullInfoModel *)sheetFull;
- (void)dowloadListsheetFull;
- (void)dowloadListFavoritesheetFull;

@end;

@protocol GetInfoSheetManagerDelegate <NSObject>
@optional
- (void)finishLoadInfoSheet;
@end
