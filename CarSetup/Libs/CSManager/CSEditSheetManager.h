//
//  CSEditSheetManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/18/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SheetFullInfoModel.h"
#import "SBJson.h"
@protocol CSEditSheetManagerDelegate;
@interface CSEditSheetManager : NSObject{
    id <CSEditSheetManagerDelegate> _delegate;
    BOOL checkEditNewSuccess;
    BOOL checkLoginAgain;

}
@property (nonatomic, assign) id <CSEditSheetManagerDelegate> delegate;

@property (nonatomic, assign) BOOL checkEditNewSuccess;
- (void)EditSheet:(SheetFullInfoModel *)sheetInfo;
- (SheetFullInfoModel *)getInfoSheetFromHTML:(NSString *)stringJsonHTML;
@property (nonatomic, assign) BOOL checkLoginAgain;
@end

@protocol CSEditSheetManagerDelegate <NSObject>

@optional
- (void) finishEditSheet;
@end
