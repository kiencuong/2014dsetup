//
//  CSFavoriteManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MySetupModel.h"
#import "MySetupBrankModel.h"
#import "SBJson.h"
#import "AppDelegate.h"
@protocol FavoriteManagerDelegate;
@interface CSFavoriteManager : NSObject{
    id <FavoriteManagerDelegate> _delegate;
    NSMutableArray *_arrayNameBrankModel;
    NSMutableArray *_arraySheet;
    MySetupModel *_mySetupModel;
    MySetupBrankModel *_mySetupBrankModel;
    NSMutableArray *arraySheetFavoriteDownload;
    BOOL checkLoginAgain;
}
- (void)loadDataListSetup;
@property (nonatomic, assign) id<FavoriteManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkAddSuccess;
@property (nonatomic, strong) NSMutableArray *_arrayNameBrankModel;
@property (nonatomic, strong) NSMutableArray *arraySheetFavoriteDownload;
@property (nonatomic, assign) BOOL checkLoginAgain;
@end;

@protocol FavoriteManagerDelegate <NSObject>
@optional
- (void)finishGetListSetupSheet;
@end

