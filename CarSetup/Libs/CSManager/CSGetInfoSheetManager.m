//
//  CSGetInfoSheetManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/9/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSGetInfoSheetManager.h"
#import "AFNetworking.h"
@implementation CSGetInfoSheetManager
@synthesize delegate;
@synthesize _sheetModel;
@synthesize arrayDowloadSheet;
@synthesize index;
@synthesize checkLoginAgain;
@synthesize indexFavorite;
@synthesize arrayDowloadFavoriteSheet;
- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

-(NSString *)getContentHtml:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"indexEdit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface, [sheetFull._track_condition JSONRepresentation], [sheetFull.front_suspension_caster JSONRepresentation], [sheetFull.front_suspension_kick_angle JSONRepresentation],  [sheetFull.front_suspension_spindle_type JSONRepresentation], [sheetFull.rear_suspension_chassis_configuration JSONRepresentation], [sheetFull.rear_suspension_roll_center JSONRepresentation], sheetFull.front_suspension_camber_link,  sheetFull.front_suspension_note,sheetFull.tires_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_22" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race, sheetFull.front_suspension_toe, sheetFull.front_suspension_ride_height, sheetFull.front_suspension_camber, sheetFull.front_suspension_sway_bar ,sheetFull.front_suspension_oil, sheetFull.front_suspension_piston, sheetFull.front_suspension_spring, sheetFull.front_suspension_shock_limiters, sheetFull.front_suspension_shock_location, sheetFull.front_suspension_bump_steer, sheetFull.rear_suspension_toe, sheetFull.rear_suspension_anti_squat, sheetFull.rear_suspension_ride_height, sheetFull.rear_suspension_camber, sheetFull.rear_suspension_rear_hub_spacing, sheetFull.rear_suspension_hex_width, sheetFull.rear_suspension_sway_bar, sheetFull.rear_suspension_oil, sheetFull.rear_suspension_piston, sheetFull.rear_suspension_spring, sheetFull.rear_suspension_shock_limiters, sheetFull.rear_suspension_shock_location, sheetFull.rear_suspension_camber_link, sheetFull.rear_suspension_wing_wickerbill, sheetFull.rear_suspension_batterry_position, sheetFull.spacer_1, sheetFull.spacer_2, sheetFull.spacer_3, sheetFull.spacer_4, sheetFull.spacer_5, sheetFull.spacer_6, sheetFull.electronics_radio, sheetFull.electronics_servo, sheetFull.electronics_esc, sheetFull.electronics_initial_brake, sheetFull.electronics_drag_brake, sheetFull.electronics_throttle_profile, sheetFull.electronics_timing_advance, sheetFull.electronics_throttle_brake_expo, sheetFull.electronics_servo_expo, sheetFull.electronics_throttle_brake_epa, sheetFull.electronics_motor, sheetFull.electronics_pinion, sheetFull.electronics_spur, sheetFull.electronics_battery, sheetFull.weight_of_each_piece, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.additive_front, sheetFull.additive_rear, sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlTLR22T:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLR22T_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22t" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface, [sheetFull._track_condition JSONRepresentation], [sheetFull.front_suspension_caster JSONRepresentation], [sheetFull.front_suspension_kick_angle JSONRepresentation],  [sheetFull.rear_suspension_chassis_configuration JSONRepresentation], [sheetFull.rear_suspension_roll_center JSONRepresentation], [sheetFull.front_shock_type JSONRepresentation],[sheetFull.rear_differential_type JSONRepresentation],[sheetFull.rear_suspension_shock_type JSONRepresentation], [sheetFull.rear_suspension_differential JSONRepresentation],sheetFull.front_suspension_camber_link,  sheetFull.front_suspension_note,sheetFull.tires_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_22T" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race, sheetFull.front_suspension_toe, sheetFull.front_suspension_ride_height, sheetFull.front_suspension_camber, sheetFull.front_suspension_sway_bar ,sheetFull.front_suspension_oil, sheetFull.front_suspension_piston, sheetFull.front_suspension_spring, sheetFull.front_suspension_shock_limiters, sheetFull.front_suspension_shock_location, sheetFull.front_suspension_bump_steer,sheetFull._rear_suspension_gear_diff, sheetFull.rear_suspension_toe, sheetFull.rear_suspension_anti_squat, sheetFull.rear_suspension_ride_height, sheetFull.rear_suspension_camber, sheetFull.rear_suspension_rear_hub_spacing, sheetFull.rear_suspension_hex_width, sheetFull.rear_suspension_sway_bar, sheetFull.rear_suspension_oil, sheetFull.rear_suspension_piston, sheetFull.rear_suspension_spring, sheetFull.rear_suspension_shock_limiters, sheetFull.rear_suspension_shock_location, sheetFull.rear_suspension_camber_link, sheetFull._rear_suspension_spoiler, sheetFull.rear_suspension_batterry_position, sheetFull.spacer_1, sheetFull.spacer_2, sheetFull.spacer_3, sheetFull.spacer_4, sheetFull.spacer_5, sheetFull.spacer_6, sheetFull.electronics_radio, sheetFull.electronics_servo, sheetFull.electronics_esc, sheetFull.electronics_initial_brake, sheetFull.electronics_drag_brake, sheetFull.electronics_throttle_profile, sheetFull.electronics_timing_advance, sheetFull.electronics_throttle_brake_expo, sheetFull.electronics_servo_expo, sheetFull.electronics_throttle_brake_epa, sheetFull.electronics_motor, sheetFull.electronics_pinion, sheetFull.electronics_spur, sheetFull.electronics_battery, sheetFull.weight_of_each_piece, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.additive_front, sheetFull.additive_rear, sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlTLR22_22_0:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR22_22.0_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR22_22.0" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation], [sheetFull.front_casters JSONRepresentation], [sheetFull.front_trail JSONRepresentation],  [sheetFull.front_kick JSONRepresentation], [sheetFull.front_springs JSONRepresentation], [sheetFull.front_pivot JSONRepresentation],[sheetFull.front_kick_shim JSONRepresentation],[sheetFull.front_bump_stud JSONRepresentation], [sheetFull.front_ack_stud JSONRepresentation],[sheetFull.chassi JSONRepresentation],[sheetFull.rollcenter JSONRepresentation],[sheetFull.rear_springs JSONRepresentation],
                             sheetFull.front_notes,
                             sheetFull.tires_notes,

                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR22_22.0" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             
                             
                             sheetFull.front_toe, sheetFull.front_height, sheetFull.front_camber, sheetFull.front_swaybar ,sheetFull.front_oil, sheetFull.front_piston, sheetFull.front_color, sheetFull.front_limiters, sheetFull.front_location, sheetFull.front_bump,sheetFull.front_ack, sheetFull.front_camlink, sheetFull.spacer1, sheetFull.spacer2, sheetFull.spacer3, sheetFull.spacer4, sheetFull.rear_toe, sheetFull.antisquat, sheetFull.rear_height, sheetFull.rear_camber, sheetFull.rear_hubString, sheetFull.hub_width, sheetFull.rear_swaybar, sheetFull.rear_oil, sheetFull.rear_piston, sheetFull.rear_color, sheetFull.rear_limiters, sheetFull.rear_camlink, sheetFull.rear_location, sheetFull.wing, sheetFull.batt_position, sheetFull.spacer5, sheetFull.spacer6, sheetFull.radio, sheetFull.servo, sheetFull.esc, sheetFull.brake, sheetFull.drag_brake, sheetFull.throttle_profile, sheetFull.timing, sheetFull.throttle_brake, sheetFull.servo_expo, sheetFull.brake_epa, sheetFull.motor, sheetFull.pinion, sheetFull.spur, sheetFull.battery, sheetFull.weight, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.add_front, sheetFull.add_rear,sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlTLR22_4:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR22-4_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR22-4" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation], [sheetFull.front_caster JSONRepresentation], [sheetFull.front_spring JSONRepresentation],  [sheetFull.front_bump_stud JSONRepresentation], [sheetFull.rollcenter JSONRepresentation], [sheetFull.rear_spring JSONRepresentation],
                             
                             sheetFull.front_notes,
                             sheetFull.rear_notes,
                             sheetFull.tires_notes,
                             
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR22-4" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             
                             
                             sheetFull.front_toe, sheetFull.front_height, sheetFull.front_camber, sheetFull.front_swaybar ,sheetFull.front_oil, sheetFull.front_piston, sheetFull.front_color, sheetFull.front_limiters, sheetFull.front_location, sheetFull.front_bump,sheetFull.front_camlink,sheetFull.front_ack,  sheetFull.spacer1,sheetFull.rear_toe, sheetFull.antisquat, sheetFull.rear_height, sheetFull.rear_camber, sheetFull.rear_hub,sheetFull.hub_width,
                             sheetFull.rear_swaybar, sheetFull.rear_oil, sheetFull.rear_piston, sheetFull.rear_color, sheetFull.rear_limiters, sheetFull.rear_camlink, sheetFull.rear_location, sheetFull.wing, sheetFull.batt_position,sheetFull.radio, sheetFull.servo, sheetFull.esc, sheetFull.brake, sheetFull.drag_brake, sheetFull.throttle_profile, sheetFull.timing, sheetFull.throttle_brake, sheetFull.servo_expo, sheetFull.brake_epa, sheetFull.motor, sheetFull.pinion, sheetFull.spur, sheetFull.battery, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.add_front, sheetFull.add_rear,sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlTLR22SCT:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLR22SCT_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_22SCT" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface, [sheetFull._track_condition JSONRepresentation], [sheetFull.front_suspension_caster JSONRepresentation], [sheetFull.front_suspension_kick_angle JSONRepresentation],  [sheetFull.rear_suspension_chassis_configuration JSONRepresentation], [sheetFull.rear_suspension_roll_center JSONRepresentation], [sheetFull.front_shock_type JSONRepresentation],[sheetFull.rear_differential_type JSONRepresentation],[sheetFull.rear_suspension_shock_type JSONRepresentation], [sheetFull.rear_suspension_differential JSONRepresentation],sheetFull.front_suspension_camber_link,  sheetFull.front_suspension_note,sheetFull.tires_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_22SCT" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race, sheetFull.front_suspension_toe, sheetFull.front_suspension_ride_height, sheetFull.front_suspension_camber, sheetFull.front_suspension_sway_bar ,sheetFull.front_suspension_oil, sheetFull.front_suspension_piston, sheetFull.front_suspension_spring, sheetFull.front_suspension_shock_limiters, sheetFull.front_suspension_shock_location, sheetFull.front_suspension_bump_steer,sheetFull._rear_suspension_gear_diff, sheetFull.rear_suspension_toe, sheetFull.rear_suspension_anti_squat, sheetFull.rear_suspension_ride_height, sheetFull.rear_suspension_camber, sheetFull.rear_suspension_rear_hub_spacing, sheetFull.rear_suspension_hex_width, sheetFull.rear_suspension_sway_bar, sheetFull.rear_suspension_oil, sheetFull.rear_suspension_piston, sheetFull.rear_suspension_spring, sheetFull.rear_suspension_shock_limiters, sheetFull.rear_suspension_camber_link, sheetFull.rear_suspension_shock_location, sheetFull.rear_suspension_batterry_position, sheetFull.spacer_1, sheetFull.spacer_2, sheetFull.spacer_3, sheetFull.spacer_4, sheetFull.spacer_5, sheetFull.spacer_6, sheetFull.electronics_radio, sheetFull.electronics_servo, sheetFull.electronics_esc, sheetFull.electronics_initial_brake, sheetFull.electronics_drag_brake, sheetFull.electronics_throttle_profile, sheetFull.electronics_timing_advance, sheetFull.electronics_throttle_brake_expo, sheetFull.electronics_servo_expo, sheetFull.electronics_throttle_brake_epa, sheetFull.electronics_motor, sheetFull.electronics_pinion, sheetFull.electronics_spur, sheetFull.electronics_battery, sheetFull.weight_of_each_piece, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.additive_front, sheetFull.additive_rear, sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlTLRSCTE:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_TLRSCTE2.0_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"tlr_SCTE2.0" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.front_suspension_middle,sheetFull.front_suspension_top,sheetFull.rear_suspension_top,sheetFull.rear_suspension_ab,sheetFull.rear_suspension_middle,sheetFull.rear_suspension_bottom,sheetFull.front_suspension_bottom,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_suspension_shock_type JSONRepresentation], [sheetFull.rear_suspension_shock_type JSONRepresentation],sheetFull.front_suspension_notes,sheetFull.tires_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_SCTE_2.0" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race, sheetFull.front_suspension_toe, sheetFull.front_suspension_ride_height, sheetFull.front_suspension_camber,sheetFull.front_suspension_ackerman, sheetFull.front_suspension_sway_bar ,sheetFull.front_suspension_oil, sheetFull.front_suspension_piston, sheetFull.front_suspension_spring, sheetFull.front_suspension_shock_limiters,sheetFull. front_suspension_shock_length,sheetFull.front_suspension_shock_location,sheetFull.front_suspension_shock_bump_steer,sheetFull.front_suspension_shock_camber_link,sheetFull.front_suspension_shock_front_diff_fluid, sheetFull.front_suspension_shock_center_diff_fluid,sheetFull.rear_suspension_toe, sheetFull.rear_suspension_anti_squat, sheetFull.rear_suspension_ride_height, sheetFull.rear_suspension_camber, sheetFull.rear_suspension_hub_spacing,sheetFull.rear_suspension_sway_bar, sheetFull.rear_suspension_oil, sheetFull.rear_suspension_piston, sheetFull.rear_suspension_spring, sheetFull.rear_suspension_shock_limiters, sheetFull.rear_suspension_camber_link, sheetFull.rear_suspension_shock_location,sheetFull.rear_suspension_shock_length,sheetFull.rear_suspension_diff_fluid,sheetFull.rear_suspension_battery_position_diff_fluid, sheetFull.electronics_radio, sheetFull.electronics_servo, sheetFull.electronics_esc, sheetFull.electronics_initial_brake, sheetFull.electronics_drag_brake, sheetFull.electronics_throttle_profile, sheetFull.electronics_timing_advance, sheetFull.electronics_throttle_brake_expo, sheetFull.electronics_servo_expo, sheetFull.electronics_throttle_brake_epa, sheetFull.electronics_motor, sheetFull.electronics_pinion, sheetFull.electronics_spur, sheetFull.electronics_battery, sheetFull.weight_of_each_piece, sheetFull.tires_front, sheetFull.tires_rear, sheetFull.compound_front, sheetFull.compound_rear, sheetFull.insert_front, sheetFull.insert_rear, sheetFull.additive_front, sheetFull.additive_rear, sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlTLR_8ight30:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR_8ight30_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR_8ight30" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.front_bump_steer,sheetFull.front_ackerman,sheetFull.front_ackerman_bottom, sheetFull.wing_setup_top,sheetFull.wing_setup ,sheetFull.wing_setup_bottom,[sheetFull._track_condition JSONRepresentation],[sheetFull.differential_front JSONRepresentation],[sheetFull.differential_center JSONRepresentation],[sheetFull.rear_suspension_roll_center JSONRepresentation],[sheetFull.wing_setup_wing_type JSONRepresentation],[sheetFull.wing_setup_wing_spacer JSONRepresentation],[sheetFull.wing_setup_wicker_bill JSONRepresentation],sheetFull.differential_notes,sheetFull.wing_setup_notes,sheetFull.clutch_shoes_spring,sheetFull.tire_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_8ight30" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_suspension_toe, sheetFull.front_suspension_ride_height, sheetFull.front_suspension_camber, sheetFull.front_suspension_caster,sheetFull.front_suspension_wheel_hex,sheetFull.front_suspension_sway_bar ,sheetFull.front_suspension_piston_oil,sheetFull.front_suspension_spring,sheetFull.front_suspension_limiter_droop,sheetFull.front_suspension_shock_length,sheetFull.front_suspension_steering,sheetFull.front_suspension_bump_steer,sheetFull.front_suspension_camber_link,sheetFull.front_suspension_shock_location,sheetFull.front_suspension_battery_type,sheetFull.differential_wire_dia_1,sheetFull.differential_qty_spring,sheetFull.differential_ramp_plate,sheetFull.differential_grease,sheetFull.differential_diff_fluid,sheetFull.differential_wire_dia_2,sheetFull.differential_qty_spring_2,sheetFull.differential_ramp_plate_2,sheetFull.differential_grease_2,sheetFull.differential_diff_fluid_2,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_ride_height,sheetFull.rear_suspension_camber,sheetFull.rear_suspension_rear_hub,sheetFull.rear_suspension_wheel_hex,sheetFull.rear_suspension_sway_bar,sheetFull.rear_suspension_piston_oil,sheetFull.rear_suspension_spring,sheetFull.rear_suspension_limiter_droop,sheetFull.rear_suspension_shock_length,sheetFull.rear_suspension_camber_link,sheetFull.rear_suspension_shock_location,sheetFull.rear_suspension_diff_fluid,sheetFull.engine_engine,sheetFull.engine_grow_plug,sheetFull.engine_pipe_header,sheetFull.engine_fuel,sheetFull.engine_head_clearance,sheetFull.engine_gearing,sheetFull.tire_front_type,sheetFull.tire_front_compound,sheetFull.tire_front_insert,sheetFull.tire_rear_type,sheetFull.tire_rear_compound,sheetFull.tire_rear_insert,sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlHotbodies_D812:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_D812_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_D812" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_tower_mounts JSONRepresentation],[sheetFull.front_camber_mounts JSONRepresentation],[sheetFull.front_block_inserts JSONRepresentation],[sheetFull.front_arm_mounts JSONRepresentation],[sheetFull.steering_position JSONRepresentation],[sheetFull.steering_block_material JSONRepresentation],[sheetFull.rear_tower_mounts JSONRepresentation],[sheetFull.rear_camber_mounts JSONRepresentation],[sheetFull.rf_inserts JSONRepresentation],[sheetFull.rear_arm_mounts JSONRepresentation],[sheetFull.wing_position JSONRepresentation],[sheetFull.hub_insert JSONRepresentation],[sheetFull.front_hub_mounts JSONRepresentation],[sheetFull.rr_inserts JSONRepresentation],[sheetFull.rear_toeplate JSONRepresentation],[sheetFull.rear_hub_mounts JSONRepresentation],
                             sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.chassis_notes,sheetFull.front_tire_notes,sheetFull.rear_tire_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Hotbodies_D812" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.steering_type,
                             sheetFull.top_hub_spacer,
                             sheetFull.bottom_hub_spacer,
                             sheetFull.front_ride_height,
                             sheetFull.front_camber,
                             sheetFull.front_toe,
                             sheetFull.front_swaybar,
                             sheetFull.front_shock_length,
                             sheetFull.front_oil,
                             sheetFull.front_piston,
                             sheetFull.front_bladder,
                             sheetFull.front_spring,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.rear_ride_height,
                             sheetFull.rear_toe,
                             sheetFull.rear_swaybar,
                             sheetFull.rear_shock_length,
                             sheetFull.rear_oil,
                             sheetFull.rear_piston,
                             sheetFull.rear_bladder,
                             sheetFull.rear_spring,
                             sheetFull.front_arm_caps,
                             sheetFull.spacer_1,
                             sheetFull.spacer_2,
                             sheetFull.spacer_3,
                             sheetFull.spacer_4,
                             sheetFull.rear_arm_caps,
                             sheetFull.spacer_5,
                             sheetFull.pipe,
                             sheetFull.spacer_6,
                             sheetFull.engine_brand,
                             sheetFull.spur,
                             sheetFull.pinion,
                             sheetFull.clutch,
                             sheetFull.plug,
                             sheetFull.fuel,
                             sheetFull.front_brand,
                             sheetFull.front_compound,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.rear_tire,
                             sheetFull.rear_compound,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.front_oil_brand,
                             sheetFull.front_oil_weight,
                             sheetFull.center_oil_brand,
                             sheetFull.center_oil_weight,
                             sheetFull.rear_oil_brand,
                             sheetFull.rear_oil_weight,
                             sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlHotbodies_D413:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_D413_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_D413" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,[sheetFull._track_condition JSONRepresentation],
                             [sheetFull.kickup JSONRepresentation],
                             [sheetFull.front_tower JSONRepresentation],
                             [sheetFull.front_link JSONRepresentation],
                             [sheetFull.antisquat JSONRepresentation],
                             [sheetFull.rear_tower JSONRepresentation],
                             [sheetFull.rear_link JSONRepresentation],
                             [sheetFull.hub_link JSONRepresentation],
                             [sheetFull.front_arm JSONRepresentation],
                             [sheetFull.rear_arm JSONRepresentation],
                             [sheetFull.hub_hole JSONRepresentation],
                             [sheetFull.battery JSONRepresentation],
                             [sheetFull.position JSONRepresentation],
                             [sheetFull.ackside JSONRepresentation],
                             [sheetFull.ackplus JSONRepresentation],
                             [sheetFull.toe JSONRepresentation],
                             [sheetFull.winglevel JSONRepresentation],
                             sheetFull.front_notes,sheetFull.rear_notes,sheetFull.front_tire,sheetFull.rear_tire,sheetFull.front_insert,sheetFull.rear_insert,sheetFull.general_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Hotbodies_D413" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_innermm,
                             sheetFull.front_outtermm,
                             sheetFull.spindle_top,
                             sheetFull.spindle_bot,
                             sheetFull.ff,
                             sheetFull.fr,
                             sheetFull.arm_plates,
                             sheetFull.front_height,
                             sheetFull.front_camber,
                             sheetFull.front_toe,
                             sheetFull.front_swaybar,
                             sheetFull.front_oil,
                             sheetFull.front_piston,
                             sheetFull.front_color,
                             sheetFull.front_length,
                             sheetFull.rear_inner,
                             sheetFull.rf,
                             sheetFull.rr,
                             sheetFull.rear_arm_plates,
                             sheetFull.rear_height,
                             sheetFull.rear_camber,
                             sheetFull.rear_toe,
                             sheetFull.rear_swaybar,
                             sheetFull.rear_oil,
                             sheetFull.rear_color,
                             sheetFull.rear_piston,
                             sheetFull.rear_length,
                             sheetFull.motor,
                             sheetFull.pinion,
                             sheetFull.spur,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.front_brand,
                             sheetFull.front_wt,
                             sheetFull.center_brand,
                             sheetFull.center_wt,
                             sheetFull.rear_brand,
                             sheetFull.rear_wt,
                             sheetFull.steerarm,
                             sheetFull.bumpshim,
                             sheetFull.studshim,
                             sheetFull.armshimf,
                             sheetFull.armshimr,
                             sheetFull.password];
    return contentHtml;
}


-(NSString *)getContentHtmlHotbodies_TCXX:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_tower_mounts JSONRepresentation],[sheetFull.front_toe_mount JSONRepresentation],[sheetFull.front_ballstud_postion JSONRepresentation],[sheetFull.front_arm_mounts JSONRepresentation],[sheetFull.front_swaybar_mount JSONRepresentation],[sheetFull.rear_tower_mounts JSONRepresentation],[sheetFull.rear_toe_mount JSONRepresentation],[sheetFull.rear_ballstud_postion JSONRepresentation],[sheetFull.rear_swaybar_mount JSONRepresentation],[sheetFull.steering_position JSONRepresentation],sheetFull.chassis_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Hotbodies_TCXX" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.spacer_1,
                             sheetFull.spacer_f,
                             sheetFull.spacer_r,
                             sheetFull.spacer_2,
                             sheetFull.front_ride_height,
                             sheetFull.front_camber,
                             sheetFull.front_droop,
                             sheetFull.front_swaybar,
                             sheetFull.front_toe,
                             sheetFull.caster,
                             sheetFull.front_wheelspacer,
                             sheetFull.front_wheelhub,
                             sheetFull.front_piston,
                             sheetFull.front_oil,
                             sheetFull.front_bladder,
                             sheetFull.front_spring,
                             sheetFull.front_shock_length,
                             sheetFull.rear_piston,
                             sheetFull.rear_oil,
                             sheetFull.rear_bladder,
                             sheetFull.rear_spring,
                             sheetFull.rear_shock_length,
                             sheetFull.spacer_3,
                             sheetFull.rear_spacer_f,
                             sheetFull.rear_spacer_r,
                             sheetFull.spacer_4,
                             sheetFull.rear_ride_height,
                             sheetFull.rear_camber,
                             sheetFull.rear_droop,
                             sheetFull.rear_swaybar,
                             sheetFull.rear_wheelspacer,
                             sheetFull.rear_wheelhub,
                             sheetFull.front_brand,
                             sheetFull.front_compound,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.front_treatment,
                             sheetFull.rear_tire,
                             sheetFull.rear_compound,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.rear_treatment,
                             sheetFull.spacer_5,
                             sheetFull.spacer_6,
                             sheetFull.spacer_7,
                             sheetFull.spacer_8,
                             sheetFull.spacer_9,
                             sheetFull.spacer_10,
                             sheetFull.spacer_11,
                             sheetFull.spacer_12,
                             sheetFull.front_type,
                             sheetFull.front_note,
                             sheetFull.rear_type,
                             sheetFull.rear_note,
                             sheetFull.motor,
                             sheetFull.spur,
                             sheetFull.battery,
                             sheetFull.pinion,
                             sheetFull.body,
                             sheetFull.esc,
                             sheetFull.wing,
                             sheetFull.note,
                             sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlAssociated_SC:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_SC10.2_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_SC10.2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.track_camber,sheetFull.track_toe,sheetFull.track_washer,sheetFull.track_ride_height_alpha,sheetFull.track_ride_height_io,sheetFull.track_field_ride_steering,sheetFull.rear_suspension_track_camber,sheetFull.track_suspension_toe,sheetFull.track_rear_washer,sheetFull.track_rear_abc,sheetFull.track_rear_io,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_suspension_caster_block JSONRepresentation],[sheetFull.rear_chassis_brace JSONRepresentation],[sheetFull.rear_suspension_wheelbase JSONRepresentation],[sheetFull.rear_hub_carrier JSONRepresentation],[sheetFull.rear_aluminum JSONRepresentation],[sheetFull.rear_anti_rollbar JSONRepresentation],[sheetFull.electronics_drivetrain_pads JSONRepresentation],[sheetFull.electronics_drivetrain_diff JSONRepresentation],[sheetFull.electronics_drivetrain_tq JSONRepresentation],[sheetFull.front_suspension_shock_type JSONRepresentation],[sheetFull.rear_suspension_shock_type JSONRepresentation],[sheetFull.front_suspension_caster JSONRepresentation],sheetFull.electronics_drivetrain_comment ,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_SC10.2" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_suspension_camber, sheetFull.front_suspension_toe,sheetFull.front_suspension_washer,sheetFull.front_suspension_ride_height,sheetFull.front_suspension_washer2,sheetFull.rear_suspension_camber,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_washer,sheetFull.rear_suspension_right_height,sheetFull.front_shock_spring,sheetFull.front_shock_piston,sheetFull.front_shock_oil,sheetFull.front_shock_limiter,sheetFull.rear_shock_spring,sheetFull.rear_shock_piston,sheetFull.rear_shock_oil,sheetFull.rear_shock_limiter,sheetFull.electronics_motor,sheetFull.electronics_pinion,sheetFull.electronics_spur_gear,sheetFull.electronics_batteries,sheetFull.electronics_battery,sheetFull.electronics_ballast,sheetFull.electronics_chassis,sheetFull.electronics_radio,sheetFull.electronics_throttle,sheetFull.electronics_throttle2,sheetFull.electronics_servo,sheetFull.electronics_steering_expo,sheetFull.electronics_esc,sheetFull.electronics_esc_setting,sheetFull.electronics_front_tires,sheetFull.electronics_front_compound,sheetFull.electronics_front_insert,sheetFull.electronics_front_wheel,sheetFull.electronics_rear_tires,sheetFull.electronics_rear_compound,sheetFull.electronics_rear_insert,sheetFull.electronics_rear_wheel,sheetFull.electronics_drivetrain_slipper,sheetFull.electronics_drivetrain_notes,sheetFull.electronics_drivetrain_diff_notes,sheetFull.electronics_drivetrain_other_body,sheetFull.electronics_drivetrain_other_wing,sheetFull.electronics_drivetrain_other_wing_angle,sheetFull.electronics_drivetrain_race_qualify,sheetFull.electronics_drivetrain_race_main,sheetFull.electronics_drivetrain_race_finish,sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlAssociated_B42:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_B42_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.track_camber,sheetFull.track_washer,sheetFull.track_ride_height_alpha,sheetFull.track_ride_height_io,sheetFull.track_field_ride_steering,sheetFull.rear_suspension_track_camber,sheetFull.track_rear_washer,sheetFull.track_rear_abc,sheetFull.track_rear_io,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_suspension_caster_block JSONRepresentation],[sheetFull.rear_chassis_brace JSONRepresentation],[sheetFull.rear_suspension_wheelbase JSONRepresentation],[sheetFull.rear_hub_carrier JSONRepresentation],[sheetFull.rear_aluminum JSONRepresentation],[sheetFull.rear_anti_rollbar JSONRepresentation],[sheetFull.electronics_drivetrain_pads JSONRepresentation],[sheetFull.electronics_drivetrain_diff JSONRepresentation],[sheetFull.electronics_drivetrain_tq JSONRepresentation],[sheetFull.front_suspension_shock_type JSONRepresentation],[sheetFull.rear_suspension_shock_type JSONRepresentation],[sheetFull.front_suspension_caster JSONRepresentation],sheetFull.electronics_drivetrain_comment ,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_B42" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_suspension_camber, sheetFull.front_suspension_toe,sheetFull.front_suspension_washer,sheetFull.front_suspension_ride_height,sheetFull.front_suspension_washer2,sheetFull.rear_suspension_camber,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_washer,sheetFull.rear_suspension_right_height,sheetFull.front_shock_spring,sheetFull.front_shock_piston,sheetFull.front_shock_oil,sheetFull.front_shock_limiter,sheetFull.rear_shock_spring,sheetFull.rear_shock_piston,sheetFull.rear_shock_oil,sheetFull.rear_shock_limiter,sheetFull.electronics_motor,sheetFull.electronics_pinion,sheetFull.electronics_spur_gear,sheetFull.electronics_batteries,sheetFull.electronics_battery,sheetFull.electronics_ballast,sheetFull.electronics_chassis,sheetFull.electronics_radio,sheetFull.electronics_throttle,sheetFull.electronics_throttle2,sheetFull.electronics_servo,sheetFull.electronics_steering_expo,sheetFull.electronics_esc,sheetFull.electronics_esc_setting,sheetFull.electronics_front_tires,sheetFull.electronics_front_compound,sheetFull.electronics_front_insert,sheetFull.electronics_front_wheel,sheetFull.electronics_rear_tires,sheetFull.electronics_rear_compound,sheetFull.electronics_rear_insert,sheetFull.electronics_rear_wheel,sheetFull.electronics_drivetrain_slipper,sheetFull.electronics_drivetrain_notes,sheetFull.electronics_drivetrain_diff_notes,sheetFull.electronics_drivetrain_other_body,sheetFull.electronics_drivetrain_other_wing,sheetFull.electronics_drivetrain_other_wing_angle,sheetFull.electronics_drivetrain_race_qualify,sheetFull.electronics_drivetrain_race_main,sheetFull.electronics_drivetrain_race_finish,sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlAssociated_T42:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_T42_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_T42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.track_camber,sheetFull.track_washer,sheetFull.track_ride_height_alpha,sheetFull.track_ride_height_io,sheetFull.track_field_ride_steering,sheetFull.rear_suspension_track_camber,sheetFull.track_rear_washer,sheetFull.track_rear_abc,sheetFull.track_rear_io,[sheetFull._track_condition JSONRepresentation],[sheetFull.front_suspension_caster_block JSONRepresentation],[sheetFull.front_suspension_caster JSONRepresentation],[sheetFull.rear_chassis_brace JSONRepresentation],[sheetFull.rear_suspension_wheelbase JSONRepresentation],[sheetFull.rear_hub_carrier JSONRepresentation],[sheetFull.rear_aluminum JSONRepresentation],[sheetFull.rear_anti_rollbar JSONRepresentation],[sheetFull.electronics_drivetrain_pads JSONRepresentation],[sheetFull.electronics_drivetrain_diff JSONRepresentation],[sheetFull.electronics_drivetrain_tq JSONRepresentation],[sheetFull.front_suspension_shock_type JSONRepresentation],[sheetFull.rear_suspension_shock_type JSONRepresentation],sheetFull.electronics_drivetrain_comment ,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_T42" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_suspension_camber, sheetFull.front_suspension_toe,sheetFull.front_suspension_washer,sheetFull.front_suspension_ride_height,sheetFull.front_suspension_washer2,sheetFull.rear_suspension_camber,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_washer,sheetFull.rear_suspension_right_height,sheetFull.front_shock_spring,sheetFull.front_shock_piston,sheetFull.front_shock_oil,sheetFull.front_shock_limiter,sheetFull.rear_shock_spring,sheetFull.rear_shock_piston,sheetFull.rear_shock_oil,sheetFull.rear_shock_limiter,sheetFull.electronics_motor,sheetFull.electronics_pinion,sheetFull.electronics_spur_gear,sheetFull.electronics_batteries,sheetFull.electronics_battery,sheetFull.electronics_ballast,sheetFull.electronics_chassis,sheetFull.electronics_radio,sheetFull.electronics_throttle,sheetFull.electronics_throttle2,sheetFull.electronics_servo,sheetFull.electronics_steering_expo,sheetFull.electronics_esc,sheetFull.electronics_esc_setting,sheetFull.electronics_front_tires,sheetFull.electronics_front_compound,sheetFull.electronics_front_insert,sheetFull.electronics_front_wheel,sheetFull.electronics_rear_tires,sheetFull.electronics_rear_compound,sheetFull.electronics_rear_insert,sheetFull.electronics_rear_wheel,sheetFull.electronics_drivetrain_slipper,sheetFull.electronics_drivetrain_notes,sheetFull.electronics_drivetrain_diff_notes,sheetFull.electronics_drivetrain_other_body,sheetFull.electronics_drivetrain_other_wing,sheetFull.electronics_drivetrain_other_wing_angle,sheetFull.electronics_drivetrain_race_qualify,sheetFull.electronics_drivetrain_race_main,sheetFull.electronics_drivetrain_race_finish,sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlAssociated_B442:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Associated_B442_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B442" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.track_camber,sheetFull.track_washer,sheetFull.track_ride_height_io,sheetFull.track_field_ride_steering,sheetFull.rear_suspension_track_camber,sheetFull.track_rear_washer,sheetFull.track_rear_abc,sheetFull.track_rear_io,[NSString stringWithFormat:@"%@",sheetFull.front_suspension_caster_block],[sheetFull._track_condition JSONRepresentation],[sheetFull.front_suspension_caster JSONRepresentation],[sheetFull.rear_suspension_wheelbase JSONRepresentation],[sheetFull.rear_hub_carrier JSONRepresentation],[sheetFull.rear_aluminum JSONRepresentation],[sheetFull.electronics_drivetrain_pads JSONRepresentation],[sheetFull.electronics_drivetrain_tq JSONRepresentation],[sheetFull.battery_placement JSONRepresentation],sheetFull.electronics_drivetrain_comment,sheetFull.front_suspension_notes,sheetFull.rear_suspension_notes, sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_B442" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_suspension_washer,sheetFull.front_suspension_camber, sheetFull.front_suspension_toe,sheetFull.front_suspension_ride_height,sheetFull.front_suspension_washers,sheetFull.rear_suspension_camber,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_right_height,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_washer,sheetFull.front_shock_spring,sheetFull.front_shock_piston,sheetFull.front_shock_oil,sheetFull.front_shock_limiter,sheetFull.electronics_motor,sheetFull.electronics_pinion,sheetFull.electronics_spur_gear,sheetFull.electronics_batteries,sheetFull.electronics_ballast,sheetFull.electronics_chassis,sheetFull.electronics_front_tires,sheetFull.electronics_front_compound,sheetFull.electronics_front_insert,sheetFull.electronics_front_wheel,sheetFull.electronics_drivetrain_slipper,sheetFull.electronics_drivetrain_notes,sheetFull.electronics_drivetrain_race_finish,sheetFull.electronics_drivetrain_diff_rear,sheetFull.electronics_drivetrain_diff_notes,sheetFull.rear_shock_spring,sheetFull.rear_shock_piston,sheetFull.rear_shock_oil,sheetFull.rear_shock_limiter,sheetFull.electronics_radio,sheetFull.electronics_throttle,sheetFull.electronics_throttle2,sheetFull.electronics_servo,sheetFull.electronics_steering_expo,sheetFull.electronics_esc,sheetFull.electronics_esc_setting,sheetFull.electronics_rear_tires,sheetFull.electronics_rear_compound,sheetFull.electronics_rear_insert,sheetFull.electronics_rear_wheel,sheetFull.electronics_drivetrain_other_body,sheetFull.electronics_drivetrain_other_wing,sheetFull.electronics_drivetrain_other_wing_angle,sheetFull.electronics_drivetrain_race_qualify,sheetFull.electronics_drivetrain_race_main2,sheetFull.electronics_drivetrain_race_finish2,
                             sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlAssociated_C42:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_C42_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_C42" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             
                             sheetFull.track_camber,sheetFull.track_washer,sheetFull.track_ride_height_alpha,sheetFull.track_ride_height_io,sheetFull.track_field_ride_steering,sheetFull.rear_suspension_track_camber,sheetFull.track_rear_washer,sheetFull.track_rear_abc,sheetFull.track_rear_io,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_suspension_caster_block JSONRepresentation],[sheetFull.front_suspension_caster JSONRepresentation],[sheetFull.rear_suspension_wheelbase JSONRepresentation],[sheetFull.rear_hub_carrier JSONRepresentation],[sheetFull.rear_aluminum JSONRepresentation],[sheetFull.rear_anti_rollbar JSONRepresentation],
                             [sheetFull.electronics_drivetrain_pads JSONRepresentation],[sheetFull.electronics_drivetrain_diff JSONRepresentation],[sheetFull.electronics_drivetrain_tq JSONRepresentation],
                             
                             
                             sheetFull.electronics_drivetrain_comment, sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_C42" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_suspension_camber, sheetFull.front_suspension_toe,sheetFull.front_suspension_washer,sheetFull.front_suspension_ride_height,sheetFull.front_suspension_washer2,
                             sheetFull.rear_suspension_camber,sheetFull.rear_suspension_toe,sheetFull.rear_suspension_anti_squat,sheetFull.rear_suspension_right_height,sheetFull.front_shock_spring,sheetFull.front_shock_piston,sheetFull.front_shock_oil,sheetFull.front_shock_limiter,
                             
                             
                             sheetFull.rear_shock_spring,
                             sheetFull.rear_shock_piston,
                             sheetFull.rear_shock_oil,
                             sheetFull.rear_shock_limiter,
                             sheetFull.electronics_motor,
                             sheetFull.electronics_pinion,
                             sheetFull.electronics_spur_gear,
                             sheetFull.electronics_batteries,
                             sheetFull.electronics_battery,
                             sheetFull.electronics_ballast,
                             sheetFull.electronics_chassis,
                             sheetFull.electronics_radio,
                             sheetFull.electronics_throttle,
                             sheetFull.electronics_throttle2,
                             sheetFull.electronics_servo,
                             sheetFull.electronics_steering_expo,
                             sheetFull.electronics_esc,
                             sheetFull.electronics_esc_setting,
                             sheetFull.electronics_front_tires,
                             sheetFull.electronics_front_compound,
                             sheetFull.electronics_front_insert,
                             sheetFull.electronics_front_wheel,
                             sheetFull.electronics_rear_tires,
                             sheetFull.electronics_rear_compound,
                             sheetFull.electronics_rear_insert,
                             sheetFull.electronics_rear_wheel,
                             sheetFull.electronics_drivetrain_slipper,
                             sheetFull.electronics_drivetrain_notes,
                             sheetFull.electronics_drivetrain_diff_notes,
                             sheetFull.electronics_drivetrain_other_body,
                             sheetFull.electronics_drivetrain_other_wing,
                             sheetFull.electronics_drivetrain_other_wing_angle,
                             sheetFull.electronics_drivetrain_race_qualify,
                             sheetFull.electronics_drivetrain_race_main,
                             sheetFull.electronics_drivetrain_race_finish,
                             sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlAssociated_RC82:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_RC82_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_RC82" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.steering_position,
                             sheetFull.tower_position,
                             sheetFull.front_hub,
                             sheetFull.front_arm_mount,
                             sheetFull.rear_tower_position,
                             sheetFull.camber_position,
                             sheetFull.rear_hub_mount,
                             sheetFull.hub_position,
                             sheetFull.hub_position_io,
                             [sheetFull._track_condition JSONRepresentation],[sheetFull.front_rollbar JSONRepresentation],[sheetFull.steering_rack JSONRepresentation],[sheetFull.pin_bushing_front_position JSONRepresentation],[sheetFull.pin_bushing_rear_position JSONRepresentation],[sheetFull.b_plate JSONRepresentation],[sheetFull.a_plate JSONRepresentation],[sheetFull.rear_rollbar JSONRepresentation],[sheetFull.wheelbase JSONRepresentation],[sheetFull.d_plate JSONRepresentation],[sheetFull.c_plate JSONRepresentation],[sheetFull.chassis_brace JSONRepresentation],[sheetFull.wheel_hex JSONRepresentation],[sheetFull.caster_blocks JSONRepresentation],[sheetFull.rear_hubs JSONRepresentation],[sheetFull.tq JSONRepresentation],
                             sheetFull.weight,sheetFull.wing_type,sheetFull.comments, sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_RC82" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.bumpsteer,
                             sheetFull.front_camber,
                             sheetFull.caster,
                             sheetFull.toe,
                             sheetFull.front_ride,
                             sheetFull.pin_bushing_front,
                             sheetFull.pin_bushing_rear,
                             sheetFull.kickup,
                             sheetFull.rear_camber,
                             sheetFull.rear_ride,
                             sheetFull.toe_bushing,
                             sheetFull.squat_bushing,
                             sheetFull.front_spring,
                             sheetFull.front_cap,
                             sheetFull.front_oil,
                             sheetFull.front_length,
                             sheetFull.front_piston,
                             sheetFull.front_rebound,
                             sheetFull.rear_spring,
                             sheetFull.rear_cap,
                             sheetFull.rear_oil,
                             sheetFull.rear_length,
                             sheetFull.rear_piston,
                             sheetFull.rear_rebound,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.diff_notes,
                             sheetFull.body_type,
                             sheetFull.gap,
                             sheetFull.spur,
                             sheetFull.bell,
                             sheetFull.shoes,
                             sheetFull.springs,
                             sheetFull.engine,
                             sheetFull.restrictor,
                             sheetFull.temp,
                             sheetFull.muffler,
                             sheetFull.plug,
                             sheetFull.fuel,
                             sheetFull.front_tires,
                             sheetFull.front_compound,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.rear_tires,
                             sheetFull.rear_compound,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.tire_notes,
                             sheetFull.radio,
                             sheetFull.brake_bias,
                             sheetFull.t_servo,
                             sheetFull.s_servo,
                             sheetFull.t_expo,
                             sheetFull.b_expo,
                             sheetFull.s_expo,
                             sheetFull.dualrate,
                             sheetFull.qualify,
                             sheetFull.main,
                             sheetFull.finish,sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlAssociated_B5:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_B5_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B5" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_armmount,
                             sheetFull.front_outstud,
                             sheetFull.a_hub,
                             sheetFull.b_hub,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.rear_armmount,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_trail JSONRepresentation],
                             [sheetFull.front_axle JSONRepresentation],
                             [sheetFull.hub_spacing JSONRepresentation],
                             [sheetFull.rollbar JSONRepresentation],
                             [sheetFull.a_up JSONRepresentation],
                             [sheetFull.b_up JSONRepresentation],
                             [sheetFull.hub_insert JSONRepresentation],
                             [sheetFull.difftype JSONRepresentation],
                             [sheetFull.size JSONRepresentation],
                             [sheetFull.surface JSONRepresentation],
                             [sheetFull.traction JSONRepresentation],
                             [sheetFull.moisture JSONRepresentation],
                             [sheetFull.condition JSONRepresentation],
                             [sheetFull.wing_angle JSONRepresentation],
                             [sheetFull.tq JSONRepresentation],
                             sheetFull.general_notes,
                             sheetFull.front_notes,
                             sheetFull.rear_notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_B5" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_camber,
                             sheetFull.front_toe,
                             sheetFull.front_height,
                             sheetFull.front_armtype,
                             sheetFull.front_towertype,
                             sheetFull.caster_insert,
                             sheetFull.kickup,
                             sheetFull.front_bump,
                             sheetFull.steering_stop,
                             sheetFull.cbs1,
                             sheetFull.cbs2,
                             sheetFull.front_ackstud,
                             sheetFull.front_instud_spacer,
                             sheetFull.rear_camber,
                             sheetFull.rear_height,
                             sheetFull.rear_toe,
                             sheetFull.antisquat,
                             sheetFull.rear_instud_spacer,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.throttle_epa,
                             sheetFull.brake_epa,
                             sheetFull.esc,
                             sheetFull.esc_settings,
                             sheetFull.motor,
                             sheetFull.wind,
                             sheetFull.timing,
                             sheetFull.pinion,
                             sheetFull.spur,
                             sheetFull.battery,
                             sheetFull.battery_position,
                             sheetFull.setting,
                             sheetFull.diffoil,
                             sheetFull.clutch_type,
                             sheetFull.clutch_pads,
                             sheetFull.clutch_notes,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.front_limiters,
                             sheetFull.rear_limiters,
                             sheetFull.front_cup,
                             sheetFull.rear_cup,
                             sheetFull.front_stroke,
                             sheetFull.rear_stroke,
                             sheetFull.front_free,
                             sheetFull.rear_free,
                             sheetFull.shock_note,
                             sheetFull.ambient,
                             sheetFull.track_temp,
                             sheetFull.track_note,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.wing_notes,
                             sheetFull.body_notes,
                             sheetFull.qualify,
                             sheetFull.main,
                             sheetFull.finish,
                             sheetFull.lap,
                             sheetFull.weight,
                             sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlAssociated_B5M:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Associated_B5M_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Associated_B5M" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_armmount,
                             sheetFull.front_outstud,
                             sheetFull.a_hub,
                             sheetFull.b_hub,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.rear_armmount,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_trail JSONRepresentation],
                             [sheetFull.front_axle JSONRepresentation],
                             [sheetFull.hub_spacing JSONRepresentation],
                             [sheetFull.rollbar JSONRepresentation],
                             [sheetFull.a_up JSONRepresentation],
                             [sheetFull.b_up JSONRepresentation],
                             [sheetFull.hub_insert JSONRepresentation],
                             [sheetFull.difftype JSONRepresentation],
                             [sheetFull.size JSONRepresentation],
                             [sheetFull.surface JSONRepresentation],
                             [sheetFull.traction JSONRepresentation],
                             [sheetFull.moisture JSONRepresentation],
                             [sheetFull.condition JSONRepresentation],
                             [sheetFull.wing_angle JSONRepresentation],
                             [sheetFull.tq JSONRepresentation],
                             sheetFull.general_notes,
                             sheetFull.front_notes,
                             sheetFull.rear_notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Associated_B5M" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_camber,
                             sheetFull.front_toe,
                             sheetFull.front_height,
                             sheetFull.front_armtype,
                             sheetFull.front_towertype,
                             sheetFull.caster_insert,
                             sheetFull.kickup,
                             sheetFull.front_bump,
                             sheetFull.steering_stop,
                             sheetFull.cbs1,
                             sheetFull.cbs2,
                             sheetFull.front_ackstud,
                             sheetFull.front_instud_spacer,
                             sheetFull.rear_camber,
                             sheetFull.rear_height,
                             sheetFull.rear_toe,
                             sheetFull.antisquat,
                             sheetFull.rear_instud_spacer,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.throttle_epa,
                             sheetFull.brake_epa,
                             sheetFull.esc,
                             sheetFull.esc_settings,
                             sheetFull.motor,
                             sheetFull.wind,
                             sheetFull.timing,
                             sheetFull.pinion,
                             sheetFull.spur,
                             sheetFull.battery,
                             sheetFull.battery_position,
                             sheetFull.setting,
                             sheetFull.diffoil,
                             sheetFull.clutch_type,
                             sheetFull.clutch_pads,
                             sheetFull.clutch_notes,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.front_limiters,
                             sheetFull.rear_limiters,
                             sheetFull.front_cup,
                             sheetFull.rear_cup,
                             sheetFull.front_stroke,
                             sheetFull.rear_stroke,
                             sheetFull.front_free,
                             sheetFull.rear_free,
                             sheetFull.shock_note,
                             sheetFull.ambient,
                             sheetFull.track_temp,
                             sheetFull.track_note,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.wing_notes,
                             sheetFull.body_notes,
                             sheetFull.qualify,
                             sheetFull.main,
                             sheetFull.finish,
                             sheetFull.lap,
                             sheetFull.weight,
                             sheetFull.password];
    return contentHtml;
}


-(NSString *)getContentHtmlDurango_DESC210:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DESC210_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DESC210" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.front_caster,sheetFull.front_steering_insert,sheetFull.front_tower_mount,sheetFull.front_caster_hole,sheetFull.front_camber_mount,sheetFull.front_arm_mount,sheetFull.rear_toe,sheetFull.rear_hub_mount,sheetFull.rear_tower_mount,sheetFull.rear_arm_mount,sheetFull.weight,sheetFull.chassis_type,sheetFull.kickup,sheetFull.motor_postion,sheetFull.fcb,sheetFull.battery_postion,sheetFull.gear,sheetFull.front_shock_size,sheetFull.rear_shock_size,sheetFull.diff_type,[sheetFull._track_condition JSONRepresentation],sheetFull.front_notes,sheetFull.rear_notes,sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.tyre_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Durango_DESC210" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_ride_height,sheetFull.front_camber,sheetFull.front_toe,sheetFull.front_swaybar,sheetFull.front_swaybar_link,sheetFull.front_droop,sheetFull.front_top_hub_spacer,sheetFull.bump_steer_washers,sheetFull.front_rear_block_spacer,sheetFull.front_bottom_hub_spacer,sheetFull.front_block_spacer,sheetFull.front_camber_spacers,sheetFull.front_spring,sheetFull.front_shock_rebound,sheetFull.front_shock_preload,sheetFull.front_oil,sheetFull.front_piston,sheetFull.front_shock_length,sheetFull.rear_ride_height,sheetFull.rear_camber,sheetFull.rear_swaybar,sheetFull.rear_swaybar_link,sheetFull.rear_droop,sheetFull.rear_wing,sheetFull.rear_ballstud_spacers,sheetFull.rear_spring,sheetFull.rear_shock_rebound,sheetFull.rear_shock_preload,sheetFull.rear_oil,sheetFull.rear_piston,sheetFull.rear_shock_length,sheetFull.motor,sheetFull.spur,sheetFull.pinion,sheetFull.battery,sheetFull.esc_settings,sheetFull.diff_oil, sheetFull.body,sheetFull.wheelbase_front_shims,sheetFull.wheelbase_rear_shims,sheetFull.front_brand,sheetFull.front_tread,sheetFull.front_compound,sheetFull.front_insert,sheetFull.front_wheel,sheetFull.rear_brand,sheetFull.rear_tread,sheetFull.rear_compound,sheetFull.rear_insert,sheetFull.rear_wheel,sheetFull.qual_position,sheetFull.final_position,sheetFull.avg_lap,sheetFull.best_lap,sheetFull.race_time,sheetFull.password];
    return contentHtml;
}
-(NSString *)getContentHtmlDurango_DEX210:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DEX210_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DEX210" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.front_caster,sheetFull.front_steering_insert,sheetFull.front_tower_mount,sheetFull.front_caster_hole,sheetFull.front_camber_mount,sheetFull.front_arm_mount,sheetFull.rear_toe,sheetFull.rear_hub_mount,sheetFull.rear_tower_mount,sheetFull.rear_arm_mount,sheetFull.weight,sheetFull.chassis_type,sheetFull.kickup,sheetFull.motor_postion,sheetFull.fcb,sheetFull.battery_postion,sheetFull.gear,sheetFull.front_shock_size,sheetFull.rear_shock_size,sheetFull.diff_type,
                             
                             sheetFull.chassis_length,[sheetFull._track_condition JSONRepresentation],sheetFull.front_notes,sheetFull.rear_notes,sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.tyre_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Durango_DEX210" ofType:@"png"], sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_ride_height,sheetFull.front_camber,sheetFull.front_toe,sheetFull.front_swaybar,sheetFull.front_swaybar_link,sheetFull.front_droop,sheetFull.front_top_hub_spacer,sheetFull.bump_steer_washers,sheetFull.front_rear_block_spacer,sheetFull.front_bottom_hub_spacer,sheetFull.front_block_spacer,sheetFull.front_camber_spacers,sheetFull.front_spring,sheetFull.front_shock_rebound,sheetFull.front_shock_preload,sheetFull.front_oil,sheetFull.front_piston,sheetFull.front_shock_length,sheetFull.rear_ride_height,sheetFull.rear_camber,sheetFull.rear_swaybar,sheetFull.rear_swaybar_link,sheetFull.rear_droop,sheetFull.rear_wing,sheetFull.rear_ballstud_spacers,sheetFull.rear_spring,sheetFull.rear_shock_rebound,sheetFull.rear_shock_preload,sheetFull.rear_oil,sheetFull.rear_piston,sheetFull.rear_shock_length,sheetFull.motor,sheetFull.spur,sheetFull.pinion,sheetFull.battery,sheetFull.esc_settings,sheetFull.diff_oil, sheetFull.body,sheetFull.wheelbase_front_shims,sheetFull.wheelbase_rear_shims,sheetFull.front_brand,sheetFull.front_tread,sheetFull.front_compound,sheetFull.front_insert,sheetFull.front_wheel,sheetFull.rear_brand,sheetFull.rear_tread,sheetFull.rear_compound,sheetFull.rear_insert,sheetFull.rear_wheel,sheetFull.qual_position,sheetFull.final_position,sheetFull.avg_lap,sheetFull.best_lap,sheetFull.race_time,sheetFull.password];
    
    
    return contentHtml;
}


-(NSString *)getContentHtmlDurango_DESC410:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DESC410_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DESC410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface, sheetFull.front_bottom_hub_mount,sheetFull.front_tower_mount,sheetFull.front_arm_mount,sheetFull.front_camber_mount,sheetFull.rear_tower_mount,sheetFull.rear_hub_mount,sheetFull.rear_bottom_hub_mount,sheetFull.rear_hub_toe,sheetFull.rear_arm_mount,sheetFull.rear_camber_mount,sheetFull.ballast_position,sheetFull.fcb,sheetFull._2s,sheetFull._024,sheetFull.front_caster,[sheetFull._track_condition JSONRepresentation],[sheetFull.diff_lock JSONRepresentation],sheetFull.front_notes,sheetFull.rear_notes,sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.tyre_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Durango_DESC410" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_ride_height,sheetFull.front_camber,sheetFull.front_caster,sheetFull.front_toe,sheetFull.front_diff_oil,sheetFull.front_swaybar,sheetFull.front_swaybar_link,sheetFull.front_top_spindle_spacer,sheetFull.top_ballstud_washers,sheetFull.front_bottom_spindle_spacer,sheetFull.front_spring,sheetFull.front_shock_rebound,sheetFull.front_shock_preload,sheetFull.front_oil,sheetFull.front_piston,sheetFull.front_shock_length,sheetFull.rear_ride_height,sheetFull.rear_camber,sheetFull.rear_diff_oil,sheetFull.rear_swaybar,sheetFull.rear_swaybar_link,sheetFull.rear_droop,sheetFull.rear_spring,sheetFull.rear_shock_rebound,sheetFull.rear_shock_preload,sheetFull.rear_oil,sheetFull.rear_piston,sheetFull.rear_shock_length,sheetFull.motor,sheetFull.spur,sheetFull.pinion,sheetFull.battery,sheetFull.esc_settings,sheetFull.body,sheetFull.mm1,sheetFull.mm2,sheetFull.slipper,sheetFull.mm3,sheetFull.mm4,sheetFull.wheelbase_front_shims,sheetFull.wheelbase_rear_shims,sheetFull.front_brand,sheetFull.front_tread,sheetFull.front_compound,sheetFull.front_insert,sheetFull.front_wheel,sheetFull.rear_brand,sheetFull.rear_tread,sheetFull.rear_compound,sheetFull.rear_insert,sheetFull.rear_wheel,sheetFull.qual_position,sheetFull.final_position,sheetFull.avg_lap,sheetFull.best_lap,sheetFull.race_time,sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlDurango_DEX410:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"index_Durango_DEX410_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Durango_DEX410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,sheetFull.front_hub_mount,sheetFull.front_tower_mount,sheetFull.front_camber_mount,sheetFull.front_arm_mount,sheetFull.pc1,sheetFull.pc2,sheetFull.ballast_position,sheetFull.fcb,sheetFull._2s,sheetFull.rear_spacer,sheetFull.front_shock_size,sheetFull.rear_shock_size,sheetFull.rear_hub_mount, sheetFull.rear_hub_toe,sheetFull.rear_camber_mount,sheetFull.rear_arm_mount,sheetFull.rear_tower_mount,[sheetFull._track_condition JSONRepresentation],sheetFull.front_notes,sheetFull.rear_notes,sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.tyre_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Durango_DEX410" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,sheetFull.front_ride_height,sheetFull.front_camber,sheetFull.front_caster,sheetFull.front_toe,sheetFull.front_diff_oil,sheetFull.front_swaybar,sheetFull.front_swaybar_link,sheetFull.front_top_spindle_spacer,sheetFull.top_ballstud_washers,sheetFull.front_bottom_hub_spacer,sheetFull.front_spring,sheetFull.front_shock_rebound,sheetFull.front_shock_preload,sheetFull.front_oil,sheetFull.front_piston,sheetFull.front_shock_length,sheetFull.rear_ride_height,sheetFull.rear_camber,sheetFull.rear_diff_oil,sheetFull.rear_swaybar,sheetFull.rear_swaybar_link,sheetFull.rear_droop,sheetFull.rear_wing,sheetFull.rear_spring,sheetFull.rear_shock_rebound,sheetFull.rear_shock_preload,sheetFull.rear_oil,sheetFull.rear_piston,sheetFull.rear_shock_length,sheetFull.motor,sheetFull.spur,sheetFull.pinion,sheetFull.battery,sheetFull.esc_settings,sheetFull.body,sheetFull.mm1,sheetFull.mm2,sheetFull.mm3,sheetFull.mm4,sheetFull.mm5,sheetFull.wheelbase_front_shims,sheetFull.wheelbase_rear_shims,sheetFull.front_brand,sheetFull.front_tread,sheetFull.front_compound,sheetFull.front_insert,sheetFull.front_wheel,sheetFull.rear_brand,sheetFull.rear_tread,sheetFull.rear_compound,sheetFull.rear_insert,sheetFull.rear_wheel,sheetFull.qual_position,sheetFull.final_position,sheetFull.avg_lap,sheetFull.best_lap,sheetFull.race_time,sheetFull.password];
    return contentHtml;
}

-(NSString *)getContentHtmlYokomo_BMAX2:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.radio,
                             sheetFull.front_tower_mount,
                             sheetFull.front_camber_mount,
                             sheetFull.camber_sn,
                             sheetFull.steering_rack,
                             sheetFull.front_arm_mount,
                             sheetFull.bulkhead_sn,
                             sheetFull.camber_radio,
                             sheetFull.toe_radio,
                             sheetFull.caster_block_mount,
                             sheetFull.caster_spacer,
                             sheetFull.spindle_spacer,
                             sheetFull.spring,
                             sheetFull.front_piston,
                             sheetFull.shock_bladder,
                             sheetFull.rear_tower_mount,
                             sheetFull.rear_camber_mount,
                             sheetFull.rear_arm_mount,
                             sheetFull.hub_toe,
                             sheetFull.vertical_hub_mount,
                             sheetFull.rear_hub_mount,
                             sheetFull.hub_position,
                             sheetFull.rear_camber,
                             sheetFull.antisquat_block,
                             sheetFull.antisquat_spacer,
                             sheetFull.rear_spring,
                             sheetFull.rear_piston,
                             sheetFull.rear_shock_bladder,
                             sheetFull.motor_position,
                             sheetFull.diff_type,
                             sheetFull.rotor_type,
                             sheetFull.esc,
                             sheetFull.battery_placement,
                             sheetFull.chassis_position,
                             sheetFull.wing,
                             sheetFull.field_rear_piston_2,
                             sheetFull.field_front_piston_2,
                             
                             [sheetFull._track_condition JSONRepresentation],[sheetFull.wing_cut JSONRepresentation],
                             sheetFull.comment,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Yokomo_B-MAX2" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_ballstud_spacers,
                             sheetFull.front_ride_height,
                             sheetFull.front_camber,
                             sheetFull.front_toe,
                             sheetFull.bump_steer_washers,
                             sheetFull.front_weight,
                             sheetFull.front_shock_length,
                             sheetFull.front_shock_rebound,
                             sheetFull.front_spring,
                             sheetFull.front_oil,
                             sheetFull.front_piston_text,
                             sheetFull.front_shock_limiters,
                             sheetFull.rear_ballstud_spacers,
                             sheetFull.rear_ride_height,
                             sheetFull.rear_vertical_ballstud_spacers,
                             sheetFull.rear_camber_text,
                             sheetFull.rear_weight_block,
                             sheetFull.rear_sway_bar,
                             sheetFull.rear_shock_length,
                             sheetFull.rear_shock_rebound,
                             sheetFull.rear_spring_text,
                             sheetFull.rear_oil,
                             sheetFull.rear_piston_text,
                             sheetFull.rear_shock_limiters,
                             sheetFull.diff_oil,
                             sheetFull.motor,
                             sheetFull.rotor_type_text,
                             sheetFull.spur,
                             sheetFull.pinion,
                             sheetFull.battery,
                             sheetFull.esc_type,
                             sheetFull.front_tyre,
                             sheetFull.front_insert,
                             sheetFull.front_wheels,
                             sheetFull.rear_tyre,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheels,
                             sheetFull.rear_diff_oil,sheetFull.password];
    return contentHtml;
}


-(NSString *)getContentHtmlYokomo_BD7:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Yokomo_BD7_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Yokomo_BD7" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             
                             [sheetFull.front_diff_type JSONRepresentation],
                             [sheetFull.front_tower_mounts JSONRepresentation],
                             [sheetFull.front_wjsj JSONRepresentation],
                             [sheetFull.front_piece_serparate JSONRepresentation],
                             [sheetFull.front_diff_position JSONRepresentation],
                             [sheetFull.front_camber_position JSONRepresentation],
                             [sheetFull.front_hub_material JSONRepresentation],
                             [sheetFull.front_wheel_hub JSONRepresentation],
                             [sheetFull.steering_block_material JSONRepresentation],
                             [sheetFull.steering_block JSONRepresentation],
                             [sheetFull.arm_material JSONRepresentation],
                             [sheetFull.caster_angle JSONRepresentation],
                             [sheetFull.rear_diff_type JSONRepresentation],
                             [sheetFull.rear_tower_mounts JSONRepresentation],
                             [sheetFull.rear_wjsj JSONRepresentation],
                             [sheetFull.rear_piece_serparate JSONRepresentation],
                             [sheetFull.rear_camber_position JSONRepresentation],
                             [sheetFull.rear_hub_position JSONRepresentation],
                             [sheetFull.rear_wheel_hub JSONRepresentation],
                             [sheetFull.rear_hub_material JSONRepresentation],
                             [sheetFull.rear_diff_position JSONRepresentation],
                             [sheetFull.rear_arm_material JSONRepresentation],
                             [sheetFull.rear_arm_length JSONRepresentation],
                             [sheetFull.rear_caster_angle JSONRepresentation],
                             [sheetFull.centerlink_block JSONRepresentation],
                             
                             sheetFull.front_shock_notes,sheetFull.rear_shock_notes,sheetFull.front_tire_notes,sheetFull.rear_tire_notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Yokomo_BD7" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.top_ballstud_washers,
                             sheetFull.front_spacer_ff,
                             sheetFull.front_spacer_fr,
                             sheetFull.front_sus_block_ff,
                             sheetFull.front_sus_block_fr,
                             sheetFull.front_camber_position_1,
                             sheetFull.front_camber_position_2,
                             sheetFull.bump_steer_washers,
                             sheetFull.centerlink_washers,
                             sheetFull.wheelbase_front_f,
                             sheetFull.wheelbase_front_r,
                             sheetFull.front_ride_height,
                             sheetFull.front_camber,
                             sheetFull.caster_angle_text,
                             sheetFull.front_swaybar,
                             sheetFull.front_droop_pin,
                             sheetFull.front_droop_arm,
                             sheetFull.front_toe,
                             sheetFull.front_oil,
                             sheetFull.front_piston,
                             
                             sheetFull.front_bladder,
                             sheetFull.front_spring,
                             sheetFull.front_oring,
                             sheetFull.front_shock_rebound,
                             sheetFull.rear_spacer_rf,
                             sheetFull.rear_spacer_rr,
                             sheetFull.rear_sus_block_rf,
                             sheetFull.rear_sus_block_rr,
                             sheetFull.rear_camber_position_1,
                             sheetFull.rear_camber_position_2,
                             sheetFull.rear_hub_position_1,
                             sheetFull.rear_hub_position_2,
                             sheetFull.wheelbase_rear_f,
                             sheetFull.wheelbase_rear_r,
                             sheetFull.rear_ride_height,
                             sheetFull.rear_camber,
                             sheetFull.rear_caster_angle_text,
                             sheetFull.rear_swaybar,
                             sheetFull.rear_droop_pin,
                             sheetFull.rear_droop_arm,
                             sheetFull.rear_toe,
                             sheetFull.rear_spring,
                             sheetFull.rear_piston,
                             sheetFull.rear_oil,
                             sheetFull.rear_bladder,
                             sheetFull.rear_oring,
                             sheetFull.rear_rebound,
                             sheetFull.motor,
                             sheetFull.spur,
                             sheetFull.spur_pitch,
                             sheetFull.pinion,
                             sheetFull.gear_ratio,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.battery,
                             sheetFull.esc,
                             sheetFull.front_tire,
                             sheetFull.front_compound,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.front_treatment,
                             sheetFull.rear_tire,
                             sheetFull.rear_compound,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.rear_treatment
                             ,sheetFull.password];
    
    
    return contentHtml;
}

-(NSString *)getContentHtmlSchumacher_SV2:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_SV2_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_SV2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.ball_socket JSONRepresentation],
                             [sheetFull.rear_piston_holes JSONRepresentation],
                             [sheetFull.front_piston_holes JSONRepresentation],
                             [sheetFull.rear_rollbar JSONRepresentation],
                             [sheetFull.rear_strap JSONRepresentation],
                             [sheetFull.rear_shock_position JSONRepresentation],
                             [sheetFull.rear_wheelbase JSONRepresentation],
                             [sheetFull.rear_tower_mounts JSONRepresentation],
                             [sheetFull.rear_hub_mounts JSONRepresentation],
                             [sheetFull.rear_arm_mounts JSONRepresentation],
                             [sheetFull.rear_hub JSONRepresentation],
                             [sheetFull.rear_alloy_hub JSONRepresentation],
                             [sheetFull.rear_mount JSONRepresentation],
                             [sheetFull.front_tower_mounts JSONRepresentation],
                             [sheetFull.front_steering_mount JSONRepresentation],
                             [sheetFull.front_hub_mounts JSONRepresentation],
                             [sheetFull.front_yoke JSONRepresentation],
                             [sheetFull.front_arm_mounts JSONRepresentation],
                             [sheetFull.front_hub JSONRepresentation],
                             [sheetFull.front_mount JSONRepresentation],
                             [sheetFull.ext_gears JSONRepresentation],
                             [sheetFull.diff_height JSONRepresentation],
                             [sheetFull.topdeck_link JSONRepresentation],
                             
                             sheetFull.notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Schumacher_SV2" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race
                             ,sheetFull.rear_oil
                             ,sheetFull.rear_spring
                             ,sheetFull.rear_shock_length
                             ,sheetFull.front_oil
                             ,sheetFull.front_spring
                             ,sheetFull.front_shock_length
                             ,sheetFull.rear_antisquat
                             ,sheetFull.rear_camber
                             ,sheetFull.rear_linkwasher1
                             ,sheetFull.rear_linkwasher2
                             ,sheetFull.front_linkwasher1
                             ,sheetFull.front_camber
                             ,sheetFull.front_toe
                             ,sheetFull.esc
                             ,sheetFull.servo
                             ,sheetFull.reciever
                             ,sheetFull.battery
                             ,sheetFull.motor
                             ,sheetFull.spur
                             ,sheetFull.pinion
                             ,sheetFull.rear_wheels
                             ,sheetFull.rear_tyre
                             ,sheetFull.rear_insert
                             ,sheetFull.front_wheels
                             ,sheetFull.front_tyre
                             ,sheetFull.front_insert
                             ,sheetFull.weight_added
                             ,sheetFull.cell_layout
                             ,sheetFull.front_ride_height
                             ,sheetFull.rear_ride_height
                             ,sheetFull.additional_washers
                             ,sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlSchumacher_K1:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_K1_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_K1" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_anti_rollbar JSONRepresentation],
                             [sheetFull.front_arm_mounts JSONRepresentation],
                             [sheetFull.front_tower_mounts JSONRepresentation],
                             [sheetFull.front_dink_mount JSONRepresentation],
                             [sheetFull.front_blocks JSONRepresentation],
                             [sheetFull.front_rake JSONRepresentation],
                             [sheetFull.front_yoke JSONRepresentation],
                             [sheetFull.front_drilled JSONRepresentation],
                             [sheetFull.front_shock_cap JSONRepresentation],
                             [sheetFull.rear_shock_cap JSONRepresentation],
                             [sheetFull.rear_drilled JSONRepresentation],
                             [sheetFull.rear_anti_rollbar JSONRepresentation],
                             [sheetFull.rear_wheelbase JSONRepresentation],
                             [sheetFull.rear_toe JSONRepresentation],
                             [sheetFull.rear_blocks JSONRepresentation],
                             [sheetFull.rear_tower_mounts JSONRepresentation],
                             [sheetFull.rear_hub_mounts JSONRepresentation],
                             [sheetFull.rear_arm_mounts JSONRepresentation],
                             [sheetFull.rear_dink_mount JSONRepresentation],
                             [sheetFull.rear_hub JSONRepresentation],
                             [sheetFull.rear_alloy_hub JSONRepresentation],
                             [sheetFull.rear_anti_squat JSONRepresentation],
                             [sheetFull.rear_pin_high JSONRepresentation],
                             [sheetFull.rear_pin_low JSONRepresentation],
                             [sheetFull.spur JSONRepresentation],
                             [sheetFull.cell_position JSONRepresentation],
                             [sheetFull.front_diff_type JSONRepresentation],
                             [sheetFull.rear_diff_type JSONRepresentation],
                             [sheetFull.chassis_type JSONRepresentation],
                             
                             sheetFull.notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Schumacher_K1" ofType:@"jpg"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_camber,
                             sheetFull.front_ride_height,
                             sheetFull.front_toe,
                             sheetFull.front_inner_link_washers,
                             sheetFull.front_outter_link_washers,
                             sheetFull.front_track_washers,
                             sheetFull.front_oil,
                             sheetFull.front_spring,
                             sheetFull.front_piston,
                             sheetFull.front_hole_size,
                             sheetFull.front_shock_length,
                             sheetFull.rear_oil,
                             sheetFull.rear_spring,
                             sheetFull.rear_piston,
                             sheetFull.rear_hole_size,
                             sheetFull.rear_shock_length,
                             sheetFull.rear_track_washers_1,
                             sheetFull.rear_track_washers_2,
                             sheetFull.rear_camber,
                             sheetFull.rear_ride_height,
                             sheetFull.rear_antisquat_washers1,
                             sheetFull.rear_antisquat_washers2,
                             sheetFull.rear_link_washers,
                             sheetFull.esc,
                             sheetFull.servo,
                             sheetFull.reciever,
                             sheetFull.battery,
                             sheetFull.motor,
                             sheetFull.pinion,
                             sheetFull.rear_wheels,
                             sheetFull.rear_tyre,
                             sheetFull.rear_insert,
                             sheetFull.front_wheels,
                             sheetFull.front_tyre,
                             sheetFull.front_insert,
                             sheetFull.front_diff_oil,
                             sheetFull.rear_diff_oil,
                             sheetFull.car_weight,
                             sheetFull.weight_added,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlSchumacher_Mi5:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.rear_diff_type JSONRepresentation],
                             [sheetFull.rear_groove JSONRepresentation],
                             [sheetFull.rear_pip JSONRepresentation],
                             [sheetFull.rear_shaft JSONRepresentation],
                             [sheetFull.rear_rollbar JSONRepresentation],
                             [sheetFull.rear_tower_mounts JSONRepresentation],
                             [sheetFull.rear_wheelbase JSONRepresentation],
                             [sheetFull.rear_block JSONRepresentation],
                             [sheetFull.rear_underball JSONRepresentation],
                             [sheetFull.rear_underbracket JSONRepresentation],
                             [sheetFull.rear_shock_cap JSONRepresentation],
                             [sheetFull.rear_drilled JSONRepresentation],
                             [sheetFull.front_diff_type JSONRepresentation],
                             [sheetFull.front_groove JSONRepresentation],
                             [sheetFull.front_yoke JSONRepresentation],
                             [sheetFull.servo_saver JSONRepresentation],
                             [sheetFull.front_shaft JSONRepresentation],
                             [sheetFull.front_anti_rollbar JSONRepresentation],
                             [sheetFull.steering_mount JSONRepresentation],
                             [sheetFull.front_tower_mounts JSONRepresentation],
                             [sheetFull.front_hubmount JSONRepresentation],
                             [sheetFull.front_underball JSONRepresentation],
                             [sheetFull.front_underbracket JSONRepresentation],
                             [sheetFull.front_block JSONRepresentation],
                             [sheetFull.front_shock_cap JSONRepresentation],
                             [sheetFull.front_drilled JSONRepresentation],
                             [sheetFull.chassis_type JSONRepresentation],
                             [sheetFull.cell_position JSONRepresentation],
                             [sheetFull.gear_pitch JSONRepresentation],
                             [sheetFull.warmers JSONRepresentation],
                             [sheetFull.rear_hubgroove JSONRepresentation],
                             
                             
                             sheetFull.notes,sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Schumacher_Mi5" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.rear_diff_oil,
                             sheetFull.rear_hubwasher,
                             sheetFull.rear_studwasher,
                             sheetFull.rear_droop,
                             sheetFull.rear_ride_heght,
                             sheetFull.rear_camber,
                             sheetFull.rear_washer2,
                             sheetFull.front_suspension_washer,
                             sheetFull.rear_washer3,
                             sheetFull.rear_oil,
                             sheetFull.rear_spring,
                             sheetFull.rear_piston,
                             sheetFull.rear_hole_size,
                             sheetFull.rear_shock_length,
                             sheetFull.rear_rebound,
                             sheetFull.front_diff_oil,
                             sheetFull.front_hubwasher,
                             sheetFull.front_washer1,
                             sheetFull.front_washer2,
                             sheetFull.front_washer3,
                             sheetFull.front_washer4,
                             sheetFull.front_washer5,
                             sheetFull.front_washer6,
                             sheetFull.front_droop,
                             sheetFull.front_ride_heght,
                             sheetFull.front_camber,
                             sheetFull.front_toein,
                             sheetFull.front_toeout,
                             sheetFull.front_oil,
                             sheetFull.front_spring,
                             sheetFull.front_piston,
                             sheetFull.front_hole_size,
                             sheetFull.front_shock_length,
                             sheetFull.front_rebound,
                             sheetFull.chassis,
                             sheetFull.topdeck,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.esc,
                             sheetFull.servo,
                             sheetFull.reciever,
                             sheetFull.cells,
                             sheetFull.motor,
                             sheetFull.rotor,
                             sheetFull.timing,
                             sheetFull.spur,
                             sheetFull.pinion,
                             sheetFull.wheels,
                             sheetFull.tyre,
                             sheetFull.insert,
                             sheetFull.tyre_additive,
                             sheetFull.front_time,
                             sheetFull.rear_time,
                             sheetFull.tempature,
                             sheetFull.time,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlKyosho_RB6:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_RB6_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_RB6" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.front_suspension_image_1,
                             sheetFull.front_suspension_image_2,
                             sheetFull.front_suspension_image_3,
                             sheetFull.rear_suspension_image_1,
                             sheetFull.rear_suspension_image_1,
                             sheetFull.rear_suspension_rr,
                             sheetFull.rear_suspension_mid,
                             sheetFull.rear_suspension_anti_squat,
                             sheetFull.rear_suspension_image_3,
                             sheetFull.battery_placement,
                             sheetFull.rear_suspension_image_2,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [ sheetFull.front_suspension_toe JSONRepresentation],
                             [sheetFull.front_suspension_weight_placement JSONRepresentation],
                             [sheetFull.front_suspension_ball_stud JSONRepresentation],
                             [sheetFull.front_suspension_axle_spacer JSONRepresentation],
                             [sheetFull.front_suspension_caster JSONRepresentation],
                             [sheetFull.front_suspension_spacer JSONRepresentation],
                             [sheetFull.front_suspension_caster_block JSONRepresentation],
                             [sheetFull.front_suspension_caster_block_position JSONRepresentation],
                             [sheetFull.front_suspension_ball_stud_la246 JSONRepresentation],
                             [sheetFull.front_suspension_axle_height_spacer JSONRepresentation],
                             [sheetFull.rear_suspension_ball_stud JSONRepresentation],
                             [sheetFull.rear_suspension_sus_holder_spacer_f JSONRepresentation],
                             [sheetFull.rear_suspension_sus_holder_spacer_r JSONRepresentation],
                             [sheetFull.rear_suspension_sway_bar JSONRepresentation],
                             [sheetFull.rear_suspension_axle_spacer JSONRepresentation],
                             [sheetFull.rear_suspension_lower_sus_holder_normal JSONRepresentation],
                             [sheetFull.rear_suspension_rear_toe JSONRepresentation],
                             [sheetFull.motor_position JSONRepresentation],
                             [sheetFull.diff JSONRepresentation],
                             [sheetFull.shock_ball_end_type_front JSONRepresentation],
                             [sheetFull.shock_ball_end_type_rear JSONRepresentation],
                             [sheetFull.truck_condition JSONRepresentation],
                             [sheetFull.wing JSONRepresentation],
                             [sheetFull.wing_mount JSONRepresentation],
                             [sheetFull.wing_angle JSONRepresentation],
                             [sheetFull.battery_placement_foam JSONRepresentation],
                             [sheetFull.battery_type JSONRepresentation],
                             [sheetFull.rear_suspension_lower_sus_holder_alumi JSONRepresentation],
                             sheetFull.tire_wheel_traction,sheetFull.comment,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Kyosho_RB6" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_suspension_toe_text,
                             sheetFull.front_suspension_tie_rod,
                             sheetFull.front_suspension_weight_placement_bulk_inside_1,
                             sheetFull.front_suspension_weight_placement_bulk_inside_2,
                             sheetFull.front_suspension_weight_placement_skid_plate_1,
                             sheetFull.front_suspension_weight_placement_skid_plate_2,
                             sheetFull.front_suspension_upper_spacer,
                             sheetFull.front_suspension_camber,
                             sheetFull.front_suspension_axle_spacer_out,
                             sheetFull.front_suspension_axle_spacer_in,
                             sheetFull.front_suspension_wheel_hub,
                             sheetFull.front_suspension_ball_stud_text,
                             sheetFull.front_suspension_ackerman_steer_spacer,
                             sheetFull.front_suspension_axle_height_spacer_text_1,
                             sheetFull.front_suspension_axle_height_spacer_text_2,
                             sheetFull.front_suspension_bump_steer_spacer,
                             sheetFull.front_suspension_wheelbase_f,
                             sheetFull.front_suspension_wheelbase_r,
                             sheetFull.front_suspension_comment,
                             sheetFull.rear_suspension_upper_spacer,
                             sheetFull.rear_suspension_anti_squat,
                             sheetFull.rear_suspension_sway_bar_text,
                             sheetFull.rear_suspension_camber,
                             sheetFull.rear_suspension_universal,
                             sheetFull.rear_suspension_swing_shaft,
                             sheetFull.rear_suspension_axle_spacer_out,
                             sheetFull.rear_suspension_axle_spacer_in,
                             sheetFull.rear_suspension_comment,
                             sheetFull.rear_suspension_upper_spacer_2,
                             sheetFull.rear_suspension_wheelbase_1_f,
                             sheetFull.rear_suspension_wheelbase_1_r,
                             sheetFull.rear_suspension_wheelbase_2_f,
                             sheetFull.rear_suspension_wheelbase_2_r,
                             sheetFull.diff_gear,
                             sheetFull.shock_shock_piston_front,
                             sheetFull.shock_shock_piston_rear,
                             sheetFull.shock_shock_oil_front,
                             sheetFull.shock_shock_oil_rear,
                             sheetFull.shock_shock_spring_front,
                             sheetFull.shock_shock_spring_rear,
                             sheetFull.shock_limiter_front_in,
                             sheetFull.shock_limiter_front_out,
                             sheetFull.shock_limiter_rear_in,
                             sheetFull.shock_limiter_rear_out,
                             sheetFull.shock_shock_length_a_front,
                             sheetFull.shock_shock_length_a_rear,
                             sheetFull.shock_shock_length_b_front,
                             sheetFull.shock_shock_length_b_rear,
                             sheetFull.shock_o_ring_front,
                             sheetFull.shock_o_ring_rear,
                             sheetFull.wing_lip,
                             sheetFull.chassis_ride_height_f,
                             sheetFull.chassis_ride_height_r,
                             sheetFull.battery_type_type,
                             sheetFull.other_motor,
                             sheetFull.other_esc,
                             sheetFull.other_pinion,
                             sheetFull.tire_front,
                             sheetFull.tire_rear,
                             sheetFull.tire_insert_front,
                             sheetFull.tire_insert_rear,
                             sheetFull.tire_wheel_front,
                             sheetFull.tire_wheel_rear,
                             
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlKyosho_MP9:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_MP9_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_MP9" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.upper_arm_position,
                             sheetFull.upper_arm_position_right,
                             sheetFull.sterring_plate_radio,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_piston JSONRepresentation],
                             [sheetFull.rear_piston JSONRepresentation],
                             [sheetFull.skid_angle_upper_front JSONRepresentation],
                             [sheetFull.skid_angle_upper_rear JSONRepresentation],
                             [sheetFull.skid_angle_under_front JSONRepresentation],
                             [sheetFull.upper_arm_left_position JSONRepresentation],
                             [sheetFull.front_tread JSONRepresentation],
                             [sheetFull.front_hub JSONRepresentation],
                             [sheetFull.shock_stay_front JSONRepresentation],
                             [sheetFull.shock_stay_rear JSONRepresentation],
                             [sheetFull.sus_arm_front JSONRepresentation],
                             [sheetFull.rear_shock_stay JSONRepresentation],
                             [sheetFull.diff_gear_oil_front JSONRepresentation],
                             [sheetFull.diff_gear_oil_center JSONRepresentation],
                             [sheetFull.diff_gear_oil_rear JSONRepresentation],
                             [sheetFull.toe_angle JSONRepresentation],
                             [sheetFull.rear_roll_center JSONRepresentation],
                             [sheetFull.rear_roll_centerL JSONRepresentation],
                             [sheetFull.upper_arm_rear_shock JSONRepresentation],
                             [sheetFull.upper_arm_rear_wheelside JSONRepresentation],
                             [sheetFull.rear_hub_carrier JSONRepresentation],
                             [sheetFull.sterring_plate_option JSONRepresentation],
                             [sheetFull.rear_hub_position JSONRepresentation],
                             [sheetFull.chassis_brace JSONRepresentation],
                             [sheetFull.sus_arm_real JSONRepresentation],
                             [sheetFull.rear_tread_front_side JSONRepresentation],
                             [sheetFull.rear_tread_rear_side JSONRepresentation],
                             [sheetFull.wing_angle_right JSONRepresentation],
                             [sheetFull.wheel_hub_right JSONRepresentation],
                             [sheetFull.sus_arm_rear JSONRepresentation],
                             
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Kyosho_MP9" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.spur_t_big1,
                             sheetFull.spur_t_big2,
                             sheetFull.spur_t,
                             sheetFull.spur_t2,
                             sheetFull.spur_t3,
                             sheetFull.spur_t4,
                             sheetFull.spur_carbon,
                             sheetFull.spur_aluminum,
                             sheetFull.tires,
                             sheetFull.compounds,
                             sheetFull.inners,
                             sheetFull.wheel,
                             sheetFull.body_top,
                             sheetFull.front_shock_types,
                             sheetFull.rear_shock_type,
                             sheetFull.shock_setup_oil,
                             sheetFull.shock_setup_oil_rear,
                             sheetFull.shock_setup_spring,
                             sheetFull.shock_setup_spring_rear,
                             sheetFull.shock_front_pressure_sponge,
                             sheetFull.shock_rear_pressure_sponge,
                             sheetFull.shock_rideheight_front,
                             sheetFull.shock_rideheight_rear,
                             sheetFull.shock_rebound_front,
                             sheetFull.shock_rebound_rear,
                             sheetFull.shock_camberangle_front,
                             sheetFull.shock_camberangle_rear,
                             sheetFull.oil_front,
                             sheetFull.oil_center,
                             sheetFull.oil_rear,
                             sheetFull.toe_angle_front,
                             sheetFull.wheelbase_front_side,
                             sheetFull.wheelbase_rear_side,
                             sheetFull.unti_roll_bar_front_above,
                             sheetFull.unti_roll_bar_front_below,
                             sheetFull.unti_roll_bar_rear_above,
                             sheetFull.unti_roll_bar_rear_below,
                             sheetFull.weight_right,
                             sheetFull.weight_right_front,
                             sheetFull.weight_right_rear,
                             
                             
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlKyosho_UltimaSCR:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.front_suspension_image_1,
                             sheetFull.front_suspension_image_2,
                             sheetFull.front_suspension_shock_mount,
                             sheetFull.front_suspension_camber,
                             sheetFull.rear_suspension_image_1,
                             sheetFull.rear_suspension_image_2,
                             sheetFull.rear_suspension_image_3,
                             sheetFull.rear_suspension_image_4,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_suspension_toe JSONRepresentation],
                             [sheetFull.front_suspension_weight_placement JSONRepresentation],
                             [sheetFull.front_suspension_ball_stud JSONRepresentation],
                             [sheetFull.front_suspension_shock_mount_checbox JSONRepresentation],
                             [sheetFull.front_suspension_axle_spacer JSONRepresentation],
                             [sheetFull.front_suspension_caster JSONRepresentation],
                             [sheetFull.front_suspension_spacer JSONRepresentation],
                             [sheetFull.front_suspension_ball_stud_1 JSONRepresentation],
                             [sheetFull.front_suspension_axle_height_spacer JSONRepresentation],
                             [sheetFull.front_suspension_wheelbase JSONRepresentation],
                             
                             [sheetFull.rear_suspension_ball_stud JSONRepresentation],
                             [sheetFull.rear_suspension_sus_holder_spacer_f JSONRepresentation],
                             [sheetFull.rear_suspension_sus_holder_spacer_r JSONRepresentation],
                             [sheetFull.rear_suspension_sway_bar JSONRepresentation],
                             [sheetFull.rear_suspension_axle_spacer JSONRepresentation],
                             [sheetFull.rear_suspension_lower_sus_holder JSONRepresentation],
                             [sheetFull.rear_suspension_rear_toe JSONRepresentation],
                             [sheetFull.diff JSONRepresentation],
                             [sheetFull.shock_top_type_front JSONRepresentation],
                             [sheetFull.shock_top_type_rear JSONRepresentation],
                             [sheetFull.shock_ball_end_type_front JSONRepresentation],
                             [sheetFull.shock_ball_end_type_rear JSONRepresentation],
                             [sheetFull.truck_condition JSONRepresentation],
                             [sheetFull.kind_spring JSONRepresentation],
                             [sheetFull.body_battery_placement JSONRepresentation],
                             [sheetFull.body_battery_post JSONRepresentation]
                             ,sheetFull.comment,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Kyosho_UltimaSC-R" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_suspension_toe_text,
                             sheetFull.front_suspension_weight_placement_bulk_inside_1,
                             sheetFull.front_suspension_weight_placement_bulk_inside_2,
                             sheetFull.front_suspension_weight_placement_skid_plate_1,
                             sheetFull.front_suspension_weight_placement_skid_plate_2,
                             sheetFull.front_suspension_upper_spacer,
                             sheetFull.front_suspension_camber_text,
                             sheetFull.front_suspension_axle_spacer_out,
                             
                             sheetFull.front_suspension_axle_spacer_in,
                             sheetFull.front_suspension_ackerman_steer_spacer,
                             sheetFull.front_suspension_bump_steer_spacer,
                             sheetFull.front_suspension_comment,
                             sheetFull.rear_suspension_upper_spacer,
                             sheetFull.rear_suspension_anti_squat,
                             sheetFull.rear_suspension_upper_spacer_2,
                             sheetFull.rear_suspension_wheelbase_f,
                             sheetFull.rear_suspension_wheelbase_r,
                             sheetFull.rear_suspension_sway_bar_text,
                             sheetFull.rear_suspension_camber,
                             sheetFull.rear_suspension_axle_spacer_out,
                             sheetFull.rear_suspension_axle_spacer_in,
                             sheetFull.rear_suspension_comment,
                             sheetFull.diff_gear,
                             sheetFull.shock_shock_piston_front,
                             sheetFull.shock_shock_piston_rear,
                             sheetFull.shock_shock_oil_front,
                             sheetFull.shock_shock_oil_rear,
                             sheetFull.shock_shock_spring_front,
                             sheetFull.shock_shock_spring_rear,
                             sheetFull.shock_limiter_front_in,
                             sheetFull.shock_limiter_front_out,
                             sheetFull.shock_limiter_rear_in,
                             sheetFull.shock_limiter_rear_out,
                             sheetFull.shock_shock_length_a_front,
                             sheetFull.shock_shock_length_a_rear,
                             sheetFull.body_wing,
                             sheetFull.body_body,
                             sheetFull.body_lip_hight,
                             sheetFull.body_ride_height_f,
                             sheetFull.body_ride_height_r,
                             sheetFull.tire_front,
                             sheetFull.tire_rear,
                             sheetFull.tire_insert_front,
                             sheetFull.tire_insert_rear,
                             sheetFull.tire_wheel_front,
                             sheetFull.tire_wheel_rear,
                             sheetFull.tire_traction_compound,
                             sheetFull.electronic_motor,
                             sheetFull.electronic_pinion,
                             sheetFull.electronic_battery,
                             sheetFull.electronic_esc,
                             sheetFull.electronic_radio,
                             sheetFull.electronic_servo,
                             sheetFull.electronic_throttle_brake_epa,
                             sheetFull.electronic_throttle_brake_expo,
                             sheetFull.electronic_initial_brake,
                             sheetFull.electronic_drag_brake,
                             sheetFull.electronic_throttle_profile,
                             sheetFull.electronic_steering_expo,
                             sheetFull.electronic_steering_speed,
                             
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlSerpent_S411:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_S411_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_S411" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.front_steering_position,
                             sheetFull.front_ballstud_position,
                             sheetFull.rear_hub_position,
                             sheetFull.rear_swaybar_position,
                             sheetFull.rear_ballstud_position,
                             sheetFull.front_tower_mount,
                             sheetFull.front_arm_mount,
                             sheetFull.rear_tower_mount,
                             sheetFull.rear_arm_mount,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_diff_type JSONRepresentation],
                             [sheetFull.rear_diff_type JSONRepresentation],
                             [sheetFull.front_ball_diff JSONRepresentation],
                             [sheetFull.rear_ball_diff JSONRepresentation],
                             [sheetFull.middle_shaft JSONRepresentation],
                             [sheetFull.front_piston_holes JSONRepresentation],
                             [sheetFull.rear_piston_holes JSONRepresentation],
                             [sheetFull.front_hubs JSONRepresentation],
                             [sheetFull.front_hub_material JSONRepresentation],
                             [sheetFull.front_steering_material JSONRepresentation],
                             [sheetFull.front_offset JSONRepresentation],
                             [sheetFull.rear_offset JSONRepresentation],
                             [sheetFull.chassis_type JSONRepresentation],
                             [sheetFull.battery_position JSONRepresentation],
                             [sheetFull.top_deck JSONRepresentation],
                             sheetFull.notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Serpent_S411" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_diff_oil,
                             sheetFull.rear_diff_oil,
                             sheetFull.pinion,
                             sheetFull.spur,
                             sheetFull.final_drive,
                             sheetFull.rollout,
                             sheetFull.motor,
                             sheetFull.timing,
                             sheetFull.rotor,
                             sheetFull.armature,
                             sheetFull.esc,
                             sheetFull.battery,
                             sheetFull.program,
                             sheetFull.punch,
                             sheetFull.initial_brake,
                             sheetFull.auto_brake,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.shock_brand,
                             sheetFull.front_shock_oil,
                             sheetFull.front_spring,
                             sheetFull.rear_shock_oil,
                             sheetFull.rear_spring,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.front_tire,
                             sheetFull.rear_tire,
                             sheetFull.front_diameter,
                             sheetFull.rear_diameter,
                             sheetFull.front_insert,
                             sheetFull.rear_insert,
                             sheetFull.front_treatment,
                             sheetFull.rear_treatment,
                             sheetFull.body_position,
                             sheetFull.front_toe_out,
                             sheetFull.front_toe_in,
                             sheetFull.front_bracket_1,
                             sheetFull.front_bracket_2,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_bracket_1,
                             sheetFull.rear_wheelbases,
                             sheetFull.rear_bracket_2,
                             sheetFull.front_shim_1,
                             sheetFull.front_shim_2,
                             sheetFull.front_offset_2,
                             sheetFull.rear_shim_1,
                             sheetFull.rear_shim_2,
                             sheetFull.rear_offset_2,
                             sheetFull.front_downstop,
                             sheetFull.rear_downstop,
                             sheetFull.front_camber_negative,
                             sheetFull.front_camber_positive,
                             sheetFull.front_ride_height,
                             sheetFull.rear_camber_negative,
                             sheetFull.rear_camber_positive,
                             sheetFull.rear_ride_height,
                             sheetFull.shim_1,
                             sheetFull.battery_weight,
                             sheetFull.weight_1,
                             sheetFull.weight_2,
                             sheetFull.weight_3,
                             sheetFull.rollcenter_1,
                             sheetFull.rollcenter_2,
                             sheetFull.rollcenter_3,
                             sheetFull.rollcenter_4,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlMugen_MBX7:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Mugen_MBX7_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Mugen_MBX7" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_tesion_rod JSONRepresentation],
                             [sheetFull.front_uper_arm_position JSONRepresentation],
                             [sheetFull.front_uper_arm_position_below JSONRepresentation],
                             [sheetFull.front_lower_arm_position JSONRepresentation],
                             [sheetFull.front_lower_arm_position_below JSONRepresentation],
                             [sheetFull.front_shock_position JSONRepresentation],
                             [sheetFull.front_shock_pivot_ball JSONRepresentation],
                             [sheetFull.front_shock_damper_stay JSONRepresentation],
                             [sheetFull.steering_ackerman JSONRepresentation],
                             [sheetFull.rear_upper_arm_position JSONRepresentation],
                             [sheetFull.rear_lower_arm_position JSONRepresentation],
                             [sheetFull.rear_lower_arm_position_toe JSONRepresentation],
                             [sheetFull.rear_lower_arm_position_below JSONRepresentation],
                             [sheetFull.rear_shock_position JSONRepresentation],
                             [sheetFull.rear_shock_pivot_ball JSONRepresentation],
                             [sheetFull.rear_shock_damper_stay JSONRepresentation],
                             [sheetFull.engine_mount JSONRepresentation],
                             [sheetFull.wing_position JSONRepresentation],
                             sheetFull.comment,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Mugen_MBX7" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_suspension_ride_height,
                             sheetFull.front_suspension_toe_angle,
                             sheetFull.kingpin_ball_spacer,
                             sheetFull.front_rebound_stop,
                             sheetFull.front_wheel_hubs,
                             sheetFull.front_anti_roll_bar,
                             sheetFull.front_upper_arm_spacer,
                             sheetFull.front_lower_arm_spacer,
                             sheetFull.front_tesion,
                             sheetFull.front_shock_pistion,
                             sheetFull.front_shock_oil,
                             sheetFull.front_shock_spring,
                             sheetFull.front_shock_spring_adj,
                             sheetFull.rear_right_height,
                             sheetFull.rear_camber_angle,
                             sheetFull.rear_rebound_stop,
                             sheetFull.rear_antil_roll,
                             sheetFull.rear_lower_arm,
                             sheetFull.rear_upright_spacer,
                             sheetFull.rear_upper_arm,
                             sheetFull.rear_shock_pistion,
                             sheetFull.rear_shock_oil,
                             sheetFull.rear_shock_spring,
                             sheetFull.rear_shock_spring_adj,
                             sheetFull.engine_type,
                             sheetFull.engine_plug,
                             sheetFull.engine_gasket,
                             sheetFull.engine_reducer,
                             sheetFull.engine_muffler,
                             sheetFull.engine_fuel,
                             sheetFull.diff_oil_front,
                             sheetFull.diff_oil_center,
                             sheetFull.diff_oil_rear,
                             sheetFull.diff_oil_oring1,
                             sheetFull.diff_oil_oring2,
                             sheetFull.diff_oil_oring3,
                             sheetFull.tires_front_type,
                             sheetFull.tires_front_compound,
                             sheetFull.tires_front_insert,
                             sheetFull.tires_front_wheel,
                             sheetFull.tires_rear_type,
                             sheetFull.tires_rear_compound,
                             sheetFull.tires_rear_insert,
                             sheetFull.tires_rear_wheel,
                             sheetFull.clutch_beel,
                             sheetFull.clutch_spur_gear,
                             sheetFull.clutch_shoes,
                             sheetFull.clutch_spring,
                             sheetFull.body_w,
                             sheetFull.wing_w,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlSerpent_811B:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_811B_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_811B" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.holes_size_left,
                             sheetFull.front_tower_mount,
                             sheetFull.front_hub_mount,
                             sheetFull.front_arm_mount,
                             sheetFull.rear_tower_mount,
                             sheetFull.rear_inner_mount,
                             sheetFull.rear_hub_mount,
                             sheetFull.rear_arm_mount,
                             sheetFull.rear_wing_mount,
                             sheetFull.front_inner_mount,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.rear_rearbracket JSONRepresentation],
                             [sheetFull.clutch_type JSONRepresentation],
                             [sheetFull.brake_balancer JSONRepresentation],
                             [sheetFull.front_orange JSONRepresentation],
                             [sheetFull.front_red JSONRepresentation],
                             [sheetFull.front_pink JSONRepresentation],
                             [sheetFull.front_blue JSONRepresentation],
                             [sheetFull.front_purple JSONRepresentation],
                             [sheetFull.front_green JSONRepresentation],
                             [sheetFull.front_other_1 JSONRepresentation],
                             [sheetFull.rear_orange JSONRepresentation],
                             [sheetFull.rear_red JSONRepresentation],
                             [sheetFull.rear_pink JSONRepresentation],
                             [sheetFull.rear_blue JSONRepresentation],
                             [sheetFull.rear_purple JSONRepresentation],
                             [sheetFull.rear_green JSONRepresentation],
                             [sheetFull.rear_other_1 JSONRepresentation],
                             [sheetFull.front_hole_size JSONRepresentation],
                             [sheetFull.rear_hole_size JSONRepresentation],
                             [sheetFull.stock_wing JSONRepresentation],
                             [sheetFull.front_steering_material JSONRepresentation],
                             [sheetFull.front_hub_material JSONRepresentation],
                             [sheetFull.front_hex_adapter JSONRepresentation],
                             [sheetFull.rear_tower_upright JSONRepresentation],
                             [sheetFull.rear_hex_adapter JSONRepresentation],
                             [sheetFull.front_caster JSONRepresentation],
                             [sheetFull.front_spindle JSONRepresentation],
                             [sheetFull.front_frontinsert JSONRepresentation],
                             [sheetFull.front_rearinsert JSONRepresentation],
                             [sheetFull.front_brace JSONRepresentation],
                             [sheetFull.rear_brace JSONRepresentation],
                             [sheetFull.rear_frontinsert JSONRepresentation],
                             [sheetFull.rear_rearinsert JSONRepresentation],
                             [sheetFull.rear_frontbracket JSONRepresentation],
                             [sheetFull.front_swaybar JSONRepresentation],
                             [sheetFull.servo_saver JSONRepresentation],
                             [sheetFull.steering_bar JSONRepresentation],
                             [sheetFull.wheelbase_shims JSONRepresentation],
                             [sheetFull.rear_swaybar JSONRepresentation],
                             sheetFull.comments,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Serpent_811B" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_diff_oil,
                             sheetFull.center_diff_oil,
                             sheetFull.rear_diff_oil,
                             sheetFull.optinal_clutch,
                             sheetFull.aluminum_clutch,
                             sheetFull.aluminum_spring,
                             sheetFull.carbon_clutch,
                             sheetFull.carbon_spring,
                             sheetFull.clutch_bell,
                             sheetFull.spur,
                             sheetFull.balancer_1,
                             sheetFull.balancer_2,
                             sheetFull.balancer_3,
                             sheetFull.balancer_4,
                             sheetFull.balancer_5,
                             sheetFull.front_shock_oil,
                             sheetFull.rear_shock_oil,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_other,
                             sheetFull.rear_other,
                             sheetFull.tire_brand,
                             sheetFull.tire_type,
                             sheetFull.tire_insert,
                             sheetFull.wheel,
                             sheetFull.body_brand,
                             sheetFull.body_type,
                             sheetFull.wing_brand,
                             sheetFull.wing_brand,
                             sheetFull.engine_type,
                             sheetFull.manifold,
                             sheetFull.muffler,
                             sheetFull.plug,
                             sheetFull.fuel,
                             sheetFull.frontbracket_type,
                             sheetFull.rearbracket_type,
                             sheetFull.front_toe_out,
                             sheetFull.front_toe_in,
                             sheetFull.wing_location_rear,
                             sheetFull.front_camber_negative,
                             sheetFull.front_camber_positive,
                             sheetFull.front_shock_length,
                             sheetFull.front_ride_height,
                             sheetFull.rear_camber_negative,
                             sheetFull.rear_camber_positive,
                             sheetFull.rear_shock_length,
                             sheetFull.rear_ride_height,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlSerpent_SRX2:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.caster_stud,
                             sheetFull.tower_mount,
                             sheetFull.out_link,
                             sheetFull.arm_mount,
                             sheetFull.in_link,
                             sheetFull.hub_mount,
                             sheetFull.rear_tower,
                             sheetFull.rear_inner,
                             sheetFull.swaybar_mount,
                             sheetFull.shock_mount,
                             sheetFull.out_hub,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_swaybar JSONRepresentation],
                             [sheetFull.front_toe JSONRepresentation],
                             [sheetFull.bulkhead JSONRepresentation],
                             [sheetFull.bulkhead_in JSONRepresentation],
                             [sheetFull.cbinserts JSONRepresentation],
                             [sheetFull.cbinserts_deg JSONRepresentation],
                             [sheetFull.spindles JSONRepresentation],
                             [sheetFull.ackerman JSONRepresentation],
                             [sheetFull.rear_swaybar JSONRepresentation],
                             [sheetFull.toe_block JSONRepresentation],
                             [sheetFull.fl_block JSONRepresentation],
                             [sheetFull.fr_block JSONRepresentation],
                             [sheetFull.rl_block JSONRepresentation],
                             [sheetFull.rr_block JSONRepresentation],
                             [sheetFull.bulk_weight JSONRepresentation],
                             [sheetFull.hex JSONRepresentation],
                             [sheetFull.toe_type JSONRepresentation],
                             [sheetFull.rear_toe JSONRepresentation],
                             [sheetFull.wheelbase JSONRepresentation],
                             [sheetFull.battery_type JSONRepresentation],
                             [sheetFull.diff_height JSONRepresentation],
                             [sheetFull.diff_type JSONRepresentation],
                             sheetFull.comments,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Serpent_SRX2" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_barsize,
                             sheetFull.front_ride,
                             sheetFull.front_camber,
                             sheetFull.front_toedegrees,
                             sheetFull.caster_shims,
                             sheetFull.bump_shims,
                             sheetFull.inner_shims,
                             sheetFull.rear_barsize,
                             sheetFull.rear_ride,
                             sheetFull.rear_camber,
                             sheetFull.hub_shims,
                             sheetFull.rear_toedegrees,
                             sheetFull.rear_squat,
                             sheetFull.rear_shims,
                             sheetFull.pistonf,
                             sheetFull.pistonf_holes,
                             sheetFull.pistonr,
                             sheetFull.pistonr_holes,
                             sheetFull.oilf,
                             sheetFull.oilr,
                             sheetFull.springf,
                             sheetFull.springr,
                             sheetFull.limitersf,
                             sheetFull.limitersr,
                             sheetFull.lengthf,
                             sheetFull.lengthr,
                             sheetFull.reboundf,
                             sheetFull.reboundr,
                             sheetFull.bladderf,
                             sheetFull.bladderr,
                             sheetFull.bucketf,
                             sheetFull.bucketr,
                             sheetFull.weight,
                             sheetFull.front_bias,
                             sheetFull.rear_bias,
                             sheetFull.distance,
                             sheetFull.spur,
                             sheetFull.pinion,
                             sheetFull.ratio,
                             sheetFull.finaldrive,
                             sheetFull.diffoil,
                             sheetFull.radio,
                             sheetFull.esc,
                             sheetFull.motor,
                             sheetFull.servo,
                             sheetFull.battery,
                             sheetFull.body,
                             sheetFull.wing_rear,
                             sheetFull.wing_width,
                             sheetFull.wing_lip,
                             sheetFull.wing_angles,
                             sheetFull.wing_front,
                             sheetFull.front_tire,
                             sheetFull.front_tread,
                             sheetFull.front_comp,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.front_add,
                             sheetFull.rear_tire,
                             sheetFull.rear_tread,
                             sheetFull.rear_comp,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.rear_add,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlSerpent_SRX2_MID:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.caster_stud,
                             sheetFull.tower_mount,
                             sheetFull.out_link,
                             sheetFull.arm_mount,
                             sheetFull.in_link,
                             sheetFull.hub_mount,
                             sheetFull.rear_tower,
                             sheetFull.rear_inner,
                             sheetFull.swaybar_mount,
                             sheetFull.shock_mount,
                             sheetFull.out_hub,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_swaybar JSONRepresentation],
                             [sheetFull.front_toe JSONRepresentation],
                             [sheetFull.bulkhead JSONRepresentation],
                             [sheetFull.bulkhead_in JSONRepresentation],
                             [sheetFull.cbinserts JSONRepresentation],
                             [sheetFull.cbinserts_deg JSONRepresentation],
                             [sheetFull.spindles JSONRepresentation],
                             [sheetFull.ackerman JSONRepresentation],
                             [sheetFull.rear_swaybar JSONRepresentation],
                             [sheetFull.toe_block JSONRepresentation],
                             [sheetFull.fl_block JSONRepresentation],
                             [sheetFull.fr_block JSONRepresentation],
                             [sheetFull.rl_block JSONRepresentation],
                             [sheetFull.rr_block JSONRepresentation],
                             [sheetFull.bulk_weight JSONRepresentation],
                             [sheetFull.hex JSONRepresentation],
                             [sheetFull.toe_type JSONRepresentation],
                             [sheetFull.rear_toe JSONRepresentation],
                             [sheetFull.wheelbase JSONRepresentation],
                             [sheetFull.wing_mount JSONRepresentation],
                             [sheetFull.battery_type JSONRepresentation],
                             [sheetFull.diff_height JSONRepresentation],
                             [sheetFull.diff_type JSONRepresentation],
                             [sheetFull.bulk_weight JSONRepresentation],
                             sheetFull.comments,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Serpent_SRX2_MID" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             
                             sheetFull.front_barsize,
                             sheetFull.front_ride,
                             sheetFull.front_camber,
                             sheetFull.front_toedegrees,
                             sheetFull.caster_shims,
                             sheetFull.bump_shims,
                             sheetFull.inner_shims,
                             sheetFull.rear_barsize,
                             sheetFull.rear_ride,
                             sheetFull.rear_camber,
                             sheetFull.hub_shims,
                             sheetFull.rear_toedegrees,
                             sheetFull.rear_squat,
                             sheetFull.rear_shims,
                             sheetFull.pistonf,
                             sheetFull.pistonf_holes,
                             sheetFull.pistonr,
                             sheetFull.pistonr_holes,
                             sheetFull.oilf,
                             sheetFull.oilr,
                             sheetFull.springf,
                             sheetFull.springr,
                             sheetFull.limitersf,
                             sheetFull.limitersr,
                             sheetFull.lengthf,
                             sheetFull.lengthr,
                             sheetFull.reboundf,
                             sheetFull.reboundr,
                             sheetFull.bladderf,
                             sheetFull.bladderr,
                             sheetFull.bucketf,
                             sheetFull.bucketr,
                             sheetFull.weight,
                             sheetFull.front_bias,
                             sheetFull.rear_bias,
                             sheetFull.distance,
                             sheetFull.spur,
                             sheetFull.pinion,
                             sheetFull.ratio,
                             sheetFull.finaldrive,
                             sheetFull.diffoil,
                             sheetFull.radio,
                             sheetFull.esc,
                             sheetFull.motor,
                             sheetFull.servo,
                             sheetFull.battery,
                             sheetFull.body,
                             sheetFull.wing_rear,
                             sheetFull.wing_width,
                             sheetFull.wing_lip,
                             sheetFull.wing_angles,
                             sheetFull.wing_front,
                             sheetFull.front_tire,
                             sheetFull.front_tread,
                             sheetFull.front_comp,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.front_add,
                             sheetFull.rear_tire,
                             sheetFull.rear_tread,
                             sheetFull.rear_comp,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.rear_add,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlAgama_A8Evo:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.clutchbell,
                             sheetFull.shoes,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_tower JSONRepresentation],
                             [sheetFull.front_cambermount JSONRepresentation],
                             [sheetFull.front_arm_mounts JSONRepresentation],
                             [sheetFull.front_hub_mounts JSONRepresentation],
                             [sheetFull.casters JSONRepresentation],
                             [sheetFull.ff JSONRepresentation],
                             [sheetFull.fr JSONRepresentation],
                             [sheetFull.front_rollbar JSONRepresentation],
                             [sheetFull.rf JSONRepresentation],
                             [sheetFull.rr JSONRepresentation],
                             [sheetFull.rear_tower JSONRepresentation],
                             [sheetFull.rear_cambermount JSONRepresentation],
                             [sheetFull.rear_arm_mounts JSONRepresentation],
                             [sheetFull.rear_hub_mounts JSONRepresentation],
                             [sheetFull.wing JSONRepresentation],
                             [sheetFull.rear_rollbar JSONRepresentation],
                             [sheetFull.throttle_servo JSONRepresentation],
                             [sheetFull.ackerman JSONRepresentation],
                             [sheetFull.bumpsteer JSONRepresentation],
                             [sheetFull.battery JSONRepresentation],
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Agama_A8-Evo" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.engine,
                             sheetFull.gasket,
                             sheetFull.plug,
                             sheetFull.muffler,
                             sheetFull.fuel,
                             sheetFull.clutch_t,
                             sheetFull.clutch_spring,
                             sheetFull.front_type,
                             sheetFull.front_diff_oil,
                             sheetFull.front_ring_t,
                             sheetFull.front_pinion,
                             sheetFull.center_type,
                             sheetFull.center_diff_oil,
                             sheetFull.center_spur,
                             sheetFull.rear_type,
                             sheetFull.rear_diff_oil,
                             sheetFull.rear_ring_t,
                             sheetFull.rear_pinion,
                             sheetFull.front_rideheight,
                             sheetFull.front_tire_1,
                             sheetFull.front_tire_2,
                             sheetFull.front_camber,
                             sheetFull.front_offsets,
                             sheetFull.front_inner_1,
                             sheetFull.front_inner_2,
                             sheetFull.front_toe,
                             sheetFull.front_oil,
                             sheetFull.front_piston,
                             sheetFull.front_holes,
                             sheetFull.front_spring_1,
                             sheetFull.front_spring_2,
                             sheetFull.front_spacer,
                             sheetFull.front_length,
                             sheetFull.rollbar_other,
                             sheetFull.rollbar_space,
                             sheetFull.front_washer_mm,
                             sheetFull.front_washer_pcs,
                             sheetFull.rear_washer_mm,
                             sheetFull.rear_washer_pcs,
                             sheetFull.rear_oil,
                             sheetFull.rear_piston,
                             sheetFull.rear_holes,
                             sheetFull.rear_spring_1,
                             sheetFull.rear_spring_2,
                             sheetFull.rear_spacer,
                             sheetFull.rear_length,
                             sheetFull.rear_rideheight,
                             sheetFull.rear_tire_1,
                             sheetFull.rear_tire_2,
                             sheetFull.rear_camber,
                             sheetFull.rear_offsets,
                             sheetFull.rear_inner_1,
                             sheetFull.rear_inner_2,
                             sheetFull.rear_rollbar_other,
                             sheetFull.rear_rollbar_space,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlJQProducts_TheCAR:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.front_tower_mount,
                             sheetFull.top_kickup,
                             sheetFull.bottom_kickup,
                             sheetFull.rear_tower_mount,
                             sheetFull.antisquat,
                             sheetFull.toein,
                             sheetFull.engine_mountholes_1,
                             sheetFull.engine_mountholes_2,
                             sheetFull.engine_mountholes_3,
                             sheetFull.engine_mountholes_4,
                             sheetFull.engine_postion,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.front_ackerman JSONRepresentation],
                             [sheetFull.front_camber_mounts JSONRepresentation],
                             [sheetFull.front_arm_mounts JSONRepresentation],
                             [sheetFull.ball_type JSONRepresentation],
                             [sheetFull.front_hub_mounts JSONRepresentation],
                             [sheetFull.rear_camber_mounts JSONRepresentation],
                             [sheetFull.wing_mount JSONRepresentation],
                             [sheetFull.rear_arm_mounts JSONRepresentation],
                             [sheetFull.rear_hub_mounts JSONRepresentation],
                             [sheetFull.hub_pin_position JSONRepresentation],
                             [sheetFull.rear_hub_position JSONRepresentation],
                             [sheetFull.alloy_hub JSONRepresentation],
                             [sheetFull.engine_front JSONRepresentation],
                             sheetFull.notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"JQProducts_TheCAR" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.f_turnbuckle,
                             sheetFull.front_downtravel0,
                             sheetFull.front_rideheight,
                             sheetFull.front_downtravel,
                             sheetFull.front_swaybar,
                             sheetFull.front_balance,
                             sheetFull.front_camberlength,
                             sheetFull.front_camber,
                             sheetFull.steering_link,
                             sheetFull.front_toe,
                             sheetFull.rear_rideheight,
                             sheetFull.rear_downtravel,
                             sheetFull.rear_swaybar,
                             sheetFull.rear_balance,
                             sheetFull.rear_camberlength,
                             sheetFull.rear_camber,
                             sheetFull.r_turnbuckle,
                             sheetFull.rear_downtravel0,
                             sheetFull.weight_1,
                             sheetFull.weight_2,
                             sheetFull.weight_3,
                             sheetFull.weight_4,
                             sheetFull.weight_5,
                             sheetFull.weight_6,
                             sheetFull.weight_7,
                             sheetFull.total_weight,
                             sheetFull.car_weight,
                             sheetFull.front_oil,
                             sheetFull.front_piston,
                             sheetFull.front_spring,
                             sheetFull.front_preload,
                             sheetFull.front_total,
                             sheetFull.front_rebound,
                             sheetFull.rear_oil,
                             sheetFull.rear_piston,
                             sheetFull.rear_spring,
                             sheetFull.rear_preload,
                             sheetFull.rear_total,
                             sheetFull.rear_rebound,
                             sheetFull.front_brand,
                             sheetFull.front_compound,
                             sheetFull.front_insert,
                             sheetFull.front_wheel,
                             sheetFull.rear_tire,
                             sheetFull.rear_compound,
                             sheetFull.rear_insert,
                             sheetFull.rear_wheel,
                             sheetFull.tire_weight,
                             sheetFull.front_diff_oil,
                             sheetFull.front_gear,
                             sheetFull.center_oil,
                             sheetFull.center_gear,
                             sheetFull.rear_oil,
                             sheetFull.rear_gear,
                             sheetFull.engine,
                             sheetFull.pipe,
                             sheetFull.plug,
                             sheetFull.clutch,
                             sheetFull.fuel,
                             sheetFull.cspring,
                             sheetFull.lap,
                             sheetFull.runtime,
                             sheetFull.result,
                             sheetFull.qual,
                             sheetFull.finish,
                             
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlAwesomatix:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Awesomatix_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *headerHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Awesomatix" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.st24 JSONRepresentation],
                             [sheetFull.am18 JSONRepresentation],
                             [sheetFull.am26 JSONRepresentation],
                             [sheetFull.am06s JSONRepresentation],
                             [sheetFull.at21s JSONRepresentation],
                             [sheetFull.ff_mount JSONRepresentation],
                             [sheetFull.fr_mount JSONRepresentation],
                             [sheetFull.front_spindle_mount JSONRepresentation],
                             [sheetFull.front_low_arm JSONRepresentation],
                             [sheetFull.front_steeringarm JSONRepresentation],
                             [sheetFull.front_springs JSONRepresentation],
                             [sheetFull.front_damper JSONRepresentation],
                             [sheetFull.front_action JSONRepresentation],
                             [sheetFull.front_srs JSONRepresentation],
                             [sheetFull.front_drive JSONRepresentation],
                             [sheetFull.front_diffset JSONRepresentation],
                             [sheetFull.front_dogbone JSONRepresentation],
                             
                             [sheetFull.rf_mount JSONRepresentation],
                             [sheetFull.rr_mount JSONRepresentation],
                             [sheetFull.rear_low_arm JSONRepresentation],
                             [sheetFull.rear_steeringarm JSONRepresentation],
                             [sheetFull.rear_springs JSONRepresentation],
                             [sheetFull.rear_damper JSONRepresentation],
                             [sheetFull.rear_action JSONRepresentation],
                             [sheetFull.rear_srs JSONRepresentation],
                             [sheetFull.rear_drive JSONRepresentation],
                             [sheetFull.rear_diffset JSONRepresentation],
                             [sheetFull.rear_dogbone JSONRepresentation],
                             
                             [sheetFull.st27 JSONRepresentation],
                             [sheetFull.front_five JSONRepresentation],
                             [sheetFull.rear_eleven JSONRepresentation],
                             [sheetFull.chassis_types JSONRepresentation],
                             [sheetFull.front_six JSONRepresentation],
                             [sheetFull.center_eight JSONRepresentation],
                             [sheetFull.rear_three JSONRepresentation],
                             [sheetFull.front_hole JSONRepresentation],
                             [sheetFull.center_holes JSONRepresentation],
                             [sheetFull.rear_hole JSONRepresentation],
                             [sheetFull.topdecks JSONRepresentation],
                             [sheetFull.ackerman JSONRepresentation],
                             
                             [sheetFull.ackerman_position JSONRepresentation],
                             [sheetFull.am24 JSONRepresentation],
                             [sheetFull.fl_tire JSONRepresentation],
                             [sheetFull.fr_tire JSONRepresentation],
                             [sheetFull.rl_tire JSONRepresentation],
                             [sheetFull.rr_tire JSONRepresentation],
                             [sheetFull.motor_layout JSONRepresentation],
                             [sheetFull.servo_layout JSONRepresentation],
                             [sheetFull.esc_layout JSONRepresentation],
                             [sheetFull.bat_layout JSONRepresentation],
                             
                             sheetFull.comments,
                             sheetFull.check_Password];
    
    NSString *htmlBody = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Awesomatix_edit2" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];

    NSString *contentHtml = [NSString stringWithFormat:htmlBody,[[NSBundle mainBundle] pathForResource:@"Awesomatix" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                            
                            sheetFull.front_spacer_1,
                            sheetFull.front_spacer_2,
                            sheetFull.front_spacer_3,
                            sheetFull.front_spacer_4,
                            sheetFull.front_spacer_5,
                            sheetFull.front_spacer_6,
                            sheetFull.front_spacer_7,
                            sheetFull.front_gfmm,
                            sheetFull.front_spacer_8,
                            sheetFull.front_spacer_9,
                            sheetFull.front_camber,
                            sheetFull.front_caster,
                            sheetFull.front_toe,
                            sheetFull.front_ride_height,
                            sheetFull.front_downstop,
                            sheetFull.front_upstop,
                            sheetFull.front_stabilizer,
                            sheetFull.front_lower_arm,
                            sheetFull.front_steerarm,
                            sheetFull.front_wheelspacer,
                            sheetFull.f_spring,
                            sheetFull.f_damper,
                            sheetFull.front_diffoil,
                            sheetFull.rear_spacer_1,
                            sheetFull.rear_spacer_2,
                            sheetFull.rear_spacer_3,
                            sheetFull.rear_spacer_4,
                            sheetFull.rear_spacer_5,
                            sheetFull.rear_spacer_6,
                            sheetFull.rear_spacer_7,
                            sheetFull.rear_spacer_8,
                            sheetFull.rear_gfmm,
                            sheetFull.rear_spacer_9,
                            sheetFull.rear_spacer_10,
                            sheetFull.rear_camber,
                            sheetFull.rear_caster,
                            sheetFull.rear_toe,
                            sheetFull.rear_ride_height,
                            sheetFull.rear_downstop,
                            sheetFull.rear_upstop,
                            sheetFull.rear_stabilizer,
                            sheetFull.rear_lower_arm,
                            sheetFull.rear_steerarm,
                            sheetFull.rear_wheelspacer,
                            sheetFull.r_spring,
                            sheetFull.r_damper,
                            sheetFull.rear_diffoil,
                            sheetFull.weight_1,
                            sheetFull.weight_2,
                            sheetFull.weight_3,
                            sheetFull.weight_4,
                            sheetFull.weight_5,
                            sheetFull.weight_6,
                            sheetFull.topdeck_info,
                            sheetFull.steering_washer,
                            sheetFull.front_brand,
                            sheetFull.front_insert,
                            sheetFull.front_wheel,
                            sheetFull.front_additive,
                            sheetFull.rear_tire,
                            sheetFull.rear_insert,
                            sheetFull.rear_wheel,
                            sheetFull.rear_additive,
                            sheetFull.spur,
                            sheetFull.pinion,
                            sheetFull.fdr,
                            sheetFull.body,
                            sheetFull.motor,
                            sheetFull.servo,
                            sheetFull.esc,
                            sheetFull.battery,
                            sheetFull.reciever,
                            sheetFull.offset,
                            sheetFull.wing,
                            sheetFull.lap,
                            sheetFull.qual,
                            sheetFull.final,
                            sheetFull.esc_setting,
                            sheetFull.contact,
                            
                            sheetFull.password];
    headerHtml = [headerHtml stringByAppendingString:contentHtml];

    
    return headerHtml;
}



-(NSString *)getContentHtmlTLR8ightE:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             sheetFull.front_bump_steer,
                             sheetFull.front_ackerman,
                             sheetFull.front_ackerman_bottom,
                             sheetFull.wing_setup,
                             sheetFull.wing_setup_top,
                             sheetFull.wing_setup_bottom,
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.differential_front JSONRepresentation],
                             [sheetFull.differential_center JSONRepresentation],
                             [sheetFull.rear_suspension_roll_center JSONRepresentation],
                             [sheetFull.wing_setup_wing_type JSONRepresentation],
                             [sheetFull.wing_setup_wing_spacer JSONRepresentation],
                             [sheetFull.wing_setup_wicker_bill JSONRepresentation],
                             
                             sheetFull.differential_notes,
                             sheetFull.wing_setup_notes,
                             sheetFull.tire_notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"TLR_8ight_E30" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.front_suspension_toe,
                             sheetFull.front_suspension_ride_height,
                             sheetFull.front_suspension_camber,
                             sheetFull.front_suspension_caster,
                             sheetFull.front_suspension_wheel_hex,
                             sheetFull.front_suspension_sway_bar,
                             sheetFull.front_suspension_piston_oil,
                             sheetFull.front_suspension_spring,
                             sheetFull.front_suspension_limiter_droop,
                             sheetFull.front_suspension_shock_length,
                             sheetFull.front_suspension_steering,
                             sheetFull.front_suspension_bump_steer,
                             sheetFull.front_suspension_camber_link,
                             sheetFull.front_suspension_shock_location,
                             sheetFull.front_suspension_battery_type,
                             sheetFull.differential_wire_dia_1,
                             sheetFull.differential_qty_spring,
                             sheetFull.differential_ramp_plate,
                             sheetFull.differential_grease,
                             sheetFull.differential_diff_fluid,
                             sheetFull.differential_wire_dia_2,
                             sheetFull.differential_qty_spring_2,
                             sheetFull.differential_ramp_plate_2,
                             sheetFull.differential_grease_2,
                             sheetFull.differential_diff_fluid_2,
                             sheetFull.rear_suspension_toe,
                             sheetFull.rear_suspension_anti_squat,
                             sheetFull.rear_suspension_ride_height,
                             sheetFull.rear_suspension_camber,
                             sheetFull.rear_suspension_rear_hub,
                             sheetFull.rear_suspension_wheel_hex,
                             sheetFull.rear_suspension_sway_bar,
                             sheetFull.rear_suspension_piston_oil,
                             sheetFull.rear_suspension_spring,
                             sheetFull.rear_suspension_limiter_droop,
                             sheetFull.rear_suspension_shock_length,
                             sheetFull.rear_suspension_camber_link,
                             sheetFull.rear_suspension_shock_location,
                             sheetFull.rear_suspension_diff_fluid,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.esc,
                             sheetFull.brake,
                             sheetFull.drag_brake,
                             sheetFull.throttle_profile,
                             sheetFull.timing,
                             sheetFull.throttle_brake,
                             sheetFull.brake_epa,
                             sheetFull.servo_expo,
                             sheetFull.motor,
                             sheetFull.pinion,
                             sheetFull.spur,
                             sheetFull.battery,
                             sheetFull.tire_front_type,
                             sheetFull.tire_front_compound,
                             sheetFull.tire_front_insert,
                             sheetFull.tire_rear_type,
                             sheetFull.tire_rear_compound,
                             sheetFull.tire_rear_insert,
                             
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlTekno_SCT410:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_SCT410_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_SCT410" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.ack_hole,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_outstud,
                             sheetFull.front_armmount,
                             sheetFull.sweep,
                             sheetFull.kickup,
                             sheetFull.front_posttype,
                             sheetFull.front_posthieght,
                             sheetFull.rear_posttype,
                             sheetFull.rear_posthieght,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.hub_top,
                             sheetFull.hub_bot,
                             sheetFull.rear_armmount,
                             sheetFull.toein,
                             sheetFull.squat,
                             sheetFull.spur,
                             sheetFull.clutch,
                             sheetFull.brakes,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.chassis_brace JSONRepresentation],
                             
                             sheetFull.notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Tekno_SCT410" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.ack_top,
                             sheetFull.ack_bot,
                             sheetFull.bump_top,
                             sheetFull.bump_bot,
                             sheetFull.saver,
                             sheetFull.front_height,
                             sheetFull.rear_height,
                             sheetFull.front_camber,
                             sheetFull.rear_camber,
                             sheetFull.front_toe,
                             sheetFull.rear_toe,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.front_length,
                             sheetFull.rear_length,
                             sheetFull.body,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_wheelbase,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_shocktype,
                             sheetFull.rear_shocktype,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.esc,
                             sheetFull.battery,
                             sheetFull.motor,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.pinion,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlTekno_NB48:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_NB48_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_NB48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.ack_hole,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_outstud,
                             sheetFull.front_armmount,
                             sheetFull.sweep,
                             sheetFull.kickup,
                             sheetFull.wing_pos,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.hub_top,
                             sheetFull.hub_bot,
                             sheetFull.rear_armmount,
                             sheetFull.toein,
                             sheetFull.squat,
                             sheetFull.eng_pos,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.ack_ball JSONRepresentation],
                             [sheetFull.bump_ball JSONRepresentation],
                             [sheetFull.chassis_brace JSONRepresentation],
                             
                             sheetFull.notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Tekno_NB48" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.ack_top,
                             sheetFull.ack_bot,
                             sheetFull.bump_top,
                             sheetFull.bump_bot,
                             sheetFull.saver,
                             sheetFull.stop_washers,
                             sheetFull.front_height,
                             sheetFull.rear_height,
                             sheetFull.front_toe,
                             sheetFull.rear_toe,
                             sheetFull.front_camber,
                             sheetFull.rear_camber,
                             sheetFull.caster,
                             sheetFull.front_length,
                             sheetFull.rear_length,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.suspension_notes,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_wheelbase,
                             sheetFull.front_shocktype,
                             sheetFull.rear_shocktype,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_bladder,
                             sheetFull.rear_bladder,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.shock_notes,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.tread_front,
                             sheetFull.tread_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.diff_note,
                             sheetFull.engine,
                             sheetFull.pipe,
                             sheetFull.plug,
                             sheetFull.fuel,
                             sheetFull.battery,
                             sheetFull.servo_steer,
                             sheetFull.servo_gas,
                             sheetFull.clutch,
                             sheetFull.spur,
                             sheetFull.shoes,
                             sheetFull.springs,
                             sheetFull.fbias,
                             sheetFull.rbias,
                             sheetFull.password];
    
    return contentHtml;
}
-(NSString *)getContentHtmlTekno_NT48:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_NT48_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_NT48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.ack_hole,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_outstud,
                             sheetFull.front_armmount,
                             sheetFull.sweep,
                             sheetFull.kickup,
                             sheetFull.wing_pos,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.hub_top,
                             sheetFull.hub_bot,
                             sheetFull.rear_armmount,
                             sheetFull.toein,
                             sheetFull.squat,
                             sheetFull.eng_pos,
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.ack_ball JSONRepresentation],
                             [sheetFull.bump_ball JSONRepresentation],
                             [sheetFull.chassis_brace JSONRepresentation],
                             
                             sheetFull.notes,
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Tekno_NT48" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.ack_top,
                             sheetFull.ack_bot,
                             sheetFull.bump_top,
                             sheetFull.bump_bot,
                             sheetFull.saver,
                             sheetFull.stop_washers,
                             sheetFull.front_height,
                             sheetFull.rear_height,
                             sheetFull.front_toe,
                             sheetFull.rear_toe,
                             sheetFull.front_camber,
                             sheetFull.rear_camber,
                             sheetFull.caster,
                             sheetFull.front_length,
                             sheetFull.rear_length,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.suspension_notes,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_wheelbase,
                             sheetFull.front_shocktype,
                             sheetFull.rear_shocktype,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_bladder,
                             sheetFull.rear_bladder,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.shock_notes,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.tread_front,
                             sheetFull.tread_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.diff_note,
                             sheetFull.engine,
                             sheetFull.pipe,
                             sheetFull.plug,
                             sheetFull.fuel,
                             sheetFull.battery,
                             sheetFull.servo_steer,
                             sheetFull.servo_gas,
                             sheetFull.clutch,
                             sheetFull.spur,
                             sheetFull.shoes,
                             sheetFull.springs,
                             sheetFull.fbias,
                             sheetFull.rbias,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlTekno_EB482:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_EB482_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.ack_hole,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_outstud,
                             sheetFull.front_armmount,
                             sheetFull.sweep,
                             sheetFull.kickup,
                             sheetFull.wing_pos,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.hub_top,
                             sheetFull.hub_bot,
                             sheetFull.rear_armmount,
                             sheetFull.toein,
                             sheetFull.squat,
                             sheetFull.spur,
                             sheetFull.brakes,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.ack_ball JSONRepresentation],
                             [sheetFull.bump_ball JSONRepresentation],
                             
                             sheetFull.drive_notes,
                             sheetFull.notes,
                             
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.ack_top,
                             sheetFull.ack_bot,
                             sheetFull.bump_top,
                             sheetFull.bump_bot,
                             sheetFull.saver,
                             sheetFull.stop_washers,
                             sheetFull.front_height,
                             sheetFull.rear_height,
                             sheetFull.front_toe,
                             sheetFull.rear_toe,
                             sheetFull.front_camber,
                             sheetFull.rear_camber,
                             sheetFull.caster,
                             sheetFull.front_length,
                             sheetFull.rear_length,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.suspension_notes,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_wheelbase,
                             sheetFull.front_shocktype,
                             sheetFull.rear_shocktype,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_bladder,
                             sheetFull.rear_bladder,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.shock_notes,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.tread_front,
                             sheetFull.tread_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.diff_note,

                             sheetFull.battery,
                             sheetFull.esc,
                             sheetFull.motor,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.equip_notes,
                             sheetFull.pinion,
                             sheetFull.trak,
                             sheetFull.trak_springs,
                             sheetFull.password];
    
    return contentHtml;
}

-(NSString *)getContentHtmlTekno_ET48:(SheetFullInfoModel *)sheetFull{
    NSError *error;
    
    NSString *html = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Tekno_ET48_edit" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    NSString *contentHtml = [NSString stringWithFormat:html,[[NSBundle mainBundle] pathForResource:@"Tekno_ET48" ofType:@"css"],[[NSBundle mainBundle] pathForResource:@"jquery" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"form2js" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"jquerytoObject" ofType:@"js"],[[NSBundle mainBundle] pathForResource:@"json2" ofType:@"js"],sheetFull.country,sheetFull._traction_level, sheetFull._track_type, sheetFull._track_size, sheetFull._track_surface,
                             
                             sheetFull.ack_hole,
                             sheetFull.front_tower,
                             sheetFull.front_instud,
                             sheetFull.front_outstud,
                             sheetFull.front_armmount,
                             sheetFull.sweep,
                             sheetFull.kickup,
                             sheetFull.wing_pos,
                             sheetFull.rear_tower,
                             sheetFull.rear_instud,
                             sheetFull.hub_top,
                             sheetFull.hub_bot,
                             sheetFull.rear_armmount,
                             sheetFull.toein,
                             sheetFull.squat,
                             sheetFull.spur,
                             sheetFull.brakes,
                             
                             
                             
                             [sheetFull._track_condition JSONRepresentation],
                             [sheetFull.ack_ball JSONRepresentation],
                             [sheetFull.bump_ball JSONRepresentation],
                             
                             sheetFull.drive_notes,
                             sheetFull.notes,
                             
                             sheetFull.check_Password, [[NSBundle mainBundle] pathForResource:@"Tekno_EB482" ofType:@"png"],sheetFull.name, sheetFull.date, sheetFull.track, sheetFull.city, sheetFull.name_race,
                             sheetFull.ack_top,
                             sheetFull.ack_bot,
                             sheetFull.bump_top,
                             sheetFull.bump_bot,
                             sheetFull.saver,
                             sheetFull.stop_washers,
                             sheetFull.front_height,
                             sheetFull.rear_height,
                             sheetFull.front_toe,
                             sheetFull.rear_toe,
                             sheetFull.front_camber,
                             sheetFull.rear_camber,
                             sheetFull.caster,
                             sheetFull.front_length,
                             sheetFull.rear_length,
                             sheetFull.front_swaybar,
                             sheetFull.rear_swaybar,
                             sheetFull.suspension_notes,
                             sheetFull.body,
                             sheetFull.wing,
                             sheetFull.front_wheelbase,
                             sheetFull.rear_wheelbase,
                             sheetFull.front_shocktype,
                             sheetFull.rear_shocktype,
                             sheetFull.front_piston,
                             sheetFull.rear_piston,
                             sheetFull.front_oil,
                             sheetFull.rear_oil,
                             sheetFull.front_bladder,
                             sheetFull.rear_bladder,
                             sheetFull.front_rebound,
                             sheetFull.rear_rebound,
                             sheetFull.front_spring,
                             sheetFull.rear_spring,
                             sheetFull.shock_notes,
                             sheetFull.tires_front,
                             sheetFull.tires_rear,
                             sheetFull.tread_front,
                             sheetFull.tread_rear,
                             sheetFull.compound_front,
                             sheetFull.compound_rear,
                             sheetFull.insert_front,
                             sheetFull.insert_rear,
                             sheetFull.wheel_front,
                             sheetFull.wheel_rear,
                             sheetFull.tire_note,
                             sheetFull.front_diff,
                             sheetFull.center_diff,
                             sheetFull.rear_diff,
                             sheetFull.diff_note,
                             
                             sheetFull.battery,
                             sheetFull.esc,
                             sheetFull.motor,
                             sheetFull.radio,
                             sheetFull.servo,
                             sheetFull.equip_notes,
                             sheetFull.pinion,
                             sheetFull.trak,
                             sheetFull.trak_springs,
                             sheetFull.password];
    return contentHtml;
}


#pragma mark get info Sheets
- (void)loadDataListSetup: (NSInteger) _idSheet{
    NSString *post =[[NSString alloc] initWithFormat:XML_GET_INFO_SHEET,[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS],_idSheet];
    
    NSURL *url=[NSURL URLWithString:URL_GET_INFO_SHEET];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:60];
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //        NSLog(@"Response code: %d", [response statusCode]);
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseString = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        
        if(error == 1)
        {
            NSArray *listCarInfoResult = [results objectForKey:@"data"];
            
            for (NSDictionary *sheetInfo in listCarInfoResult) {
                SheetFullInfoModel *sheetModeFull = [[SheetFullInfoModel alloc]init];
                sheetModeFull._ID = [[sheetInfo objectForKey:@"id"] intValue];
                sheetModeFull._typeID = [[sheetInfo objectForKey:@"type_id"] intValue];
                sheetModeFull._brandID = [[sheetInfo objectForKey:@"brand_id"] intValue];
                sheetModeFull._modelID = [[sheetInfo objectForKey:@"model_id"] intValue];
                sheetModeFull._linkShareTwitter= [sheetInfo objectForKey:@"short_url"];
                sheetModeFull.date = [CSUtility convertDateSheetInfo:[sheetInfo objectForKey:@"date"]];
                sheetModeFull.name = [sheetInfo objectForKey:@"name"];
                sheetModeFull._brandName = [sheetInfo objectForKey:@"brand_name"];
                sheetModeFull._modelName = [sheetInfo objectForKey:@"model_name"];
                sheetModeFull.driver = [sheetInfo objectForKey:@"driver"];
                sheetModeFull.name_race = [sheetInfo objectForKey:@"race"];
                sheetModeFull._isFavorite = [[sheetInfo objectForKey:@"isFavorite"]intValue];
                sheetModeFull.city = [sheetInfo objectForKey:@"city"];
                sheetModeFull.country = [sheetInfo objectForKey:@"country"];
                sheetModeFull.track = [sheetInfo objectForKey:@"track_name"];
                sheetModeFull._track_size = [sheetInfo objectForKey:@"track_size"];
                sheetModeFull._traction_level = [sheetInfo objectForKey:@"traction"];
                sheetModeFull._track_type = [sheetInfo objectForKey:@"track_type"];
                sheetModeFull._track_surface = [sheetInfo objectForKey:@"surface"];
                sheetModeFull.jsonDetail = [[[sheetInfo objectForKey:@"json_detail"]stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@"&#13;&#10;"]stringByReplacingOccurrencesOfString:@"\\\"" withString:@"&#34"];
                
                [X_AppDelegate deleteSheetInfo:sheetModeFull._ID];
                [X_AppDelegate createSheetInfo:sheetModeFull];
                _sheetModel=[self parserJsonSheetFull:sheetModeFull];
            }
            
        } else {
            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
                checkLoginAgain = YES;
            } else {
                [self alertStatus:message :@"Message"];
            }
            
        }
    }  else {
        [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
    }
    if ([delegate respondsToSelector:@selector(finishLoadInfoSheet)])
        [delegate finishLoadInfoSheet];
}

- (void)dowloadListsheetFull{
    
    if (index>=[arrayDowloadSheet count] || [AppDelegate shareAppDelegate].checkCancelDownload) {
        NSLog(@"DOWNLOAD ALL SHEET SUCCESS!!!");
        [CSUtility saveStatusLogin:NO forKey:CHECK_IS_DOWNLOADING_SHEETS];
        return;
    }
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS], @"token",[NSNumber numberWithInt:[[arrayDowloadSheet objectAtIndex:index] intValue]],@"sheet_id",
                            nil];
    [httpClient postPath:@"api/user/infoSheet" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        
        if(error == 1)
        {
            NSArray *listCarInfoResult = [results objectForKey:@"data"];
            
            for (NSDictionary *sheetInfo in listCarInfoResult) {
                SheetFullInfoModel *sheetModeFull = [[SheetFullInfoModel alloc]init];
                sheetModeFull._ID = [[sheetInfo objectForKey:@"id"] intValue];
                sheetModeFull._typeID = [[sheetInfo objectForKey:@"type_id"] intValue];
                sheetModeFull._brandID = [[sheetInfo objectForKey:@"brand_id"] intValue];
                sheetModeFull._modelID = [[sheetInfo objectForKey:@"model_id"] intValue];
                sheetModeFull.date = [CSUtility convertDateSheetInfo:[sheetInfo objectForKey:@"date"]];
                sheetModeFull.name = [sheetInfo objectForKey:@"name"];
                sheetModeFull._brandName = [sheetInfo objectForKey:@"brand_name"];
                sheetModeFull._modelName = [sheetInfo objectForKey:@"model_name"];
                sheetModeFull.driver = [sheetInfo objectForKey:@"driver"];
                sheetModeFull.name_race = [sheetInfo objectForKey:@"race"];
                sheetModeFull._isFavorite = [[sheetInfo objectForKey:@"isFavorite"]intValue];
                sheetModeFull.city = [sheetInfo objectForKey:@"city"];
                sheetModeFull.country = [sheetInfo objectForKey:@"country"];
                sheetModeFull.track = [sheetInfo objectForKey:@"track_name"];
                sheetModeFull._track_size = [sheetInfo objectForKey:@"track_size"];
                sheetModeFull._traction_level = [sheetInfo objectForKey:@"traction"];
                sheetModeFull._track_type = [sheetInfo objectForKey:@"track_type"];
                sheetModeFull._track_surface = [sheetInfo objectForKey:@"surface"];
                sheetModeFull._track_condition = [sheetInfo objectForKey:@"conditions"];
                sheetModeFull.jsonDetail = [sheetInfo objectForKey:@"json_detail"];
                
                [X_AppDelegate deleteSheetInfo:sheetModeFull._ID];
                [X_AppDelegate createSheetInfo:sheetModeFull];
                index++;
                [self performSelector:@selector(dowloadListsheetFull)];
            }
            
        } else {
            index++;
            [self performSelector:@selector(dowloadListsheetFull)];
            
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        index++;
        [self performSelector:@selector(dowloadListsheetFull)];
    }];
    
    
}

- (void)dowloadListFavoritesheetFull{
    
    if (indexFavorite==[arrayDowloadFavoriteSheet count] || [AppDelegate shareAppDelegate].checkCancelDownloadFavorite) {
        NSLog(@"DOWNLOAD ALL SHEET FAVORITE SUCCESS!!!");
        [CSUtility saveStatusLogin:NO forKey:CHECK_IS_DOWNLOADING_SHEETS_FAVORITE];
        return;
    }
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS], @"token",[NSNumber numberWithInt:[[arrayDowloadFavoriteSheet objectAtIndex:indexFavorite] intValue]],@"sheet_id",
                            nil];
    [httpClient postPath:@"api/user/infoSheet" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        
        if(error == 1)
        {
            NSArray *listCarInfoResult = [results objectForKey:@"data"];
            
            for (NSDictionary *sheetInfo in listCarInfoResult) {
                SheetFullInfoModel *sheetModeFull = [[SheetFullInfoModel alloc]init];
                sheetModeFull._ID = [[sheetInfo objectForKey:@"id"] intValue];
                sheetModeFull._typeID = [[sheetInfo objectForKey:@"type_id"] intValue];
                sheetModeFull._brandID = [[sheetInfo objectForKey:@"brand_id"] intValue];
                sheetModeFull._modelID = [[sheetInfo objectForKey:@"model_id"] intValue];
                sheetModeFull.date = [CSUtility convertDateSheetInfo:[sheetInfo objectForKey:@"date"]];
                sheetModeFull.name = [sheetInfo objectForKey:@"name"];
                sheetModeFull._brandName = [sheetInfo objectForKey:@"brand_name"];
                sheetModeFull._modelName = [sheetInfo objectForKey:@"model_name"];
                sheetModeFull.driver = [sheetInfo objectForKey:@"driver"];
                sheetModeFull.name_race = [sheetInfo objectForKey:@"race"];
                sheetModeFull._isFavorite = [[sheetInfo objectForKey:@"isFavorite"]intValue];
                sheetModeFull.city = [sheetInfo objectForKey:@"city"];
                sheetModeFull.country = [sheetInfo objectForKey:@"country"];
                sheetModeFull.track = [sheetInfo objectForKey:@"track_name"];
                sheetModeFull._track_size = [sheetInfo objectForKey:@"track_size"];
                sheetModeFull._traction_level = [sheetInfo objectForKey:@"traction"];
                sheetModeFull._track_type = [sheetInfo objectForKey:@"track_type"];
                sheetModeFull._track_surface = [sheetInfo objectForKey:@"surface"];
                sheetModeFull._track_condition = [sheetInfo objectForKey:@"conditions"];
                sheetModeFull.jsonDetail = [sheetInfo objectForKey:@"json_detail"];
                
                [X_AppDelegate deleteSheetInfo:sheetModeFull._ID];
                [X_AppDelegate createSheetInfo:sheetModeFull];
                indexFavorite++;
                [self performSelector:@selector(dowloadListFavoritesheetFull)];
            }
            
        } else {
            indexFavorite++;
            [self performSelector:@selector(dowloadListFavoritesheetFull)];
            
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        indexFavorite++;
        [self performSelector:@selector(dowloadListFavoritesheetFull)];
    }];
    
    
}

-(SheetFullInfoModel *)parserJsonSheetFull:(SheetFullInfoModel *)sheetFull{
    NSDictionary *sheetInfoResult;
    sheetInfoResult = [sheetFull.jsonDetail JSONValue] ;
    for (id key  in sheetInfoResult) {
        if (((NSNull *)[sheetInfoResult objectForKey:key] == [NSNull null])) {
            [sheetInfoResult setValue:@"" forKey:key];
        }

    }

    
    if ([sheetInfoResult objectForKey:@"track_condition"]) {
        sheetFull._track_condition = [sheetInfoResult objectForKey:@"track_condition"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_type"]) {
        sheetFull.front_suspension_shock_type = [sheetInfoResult objectForKey:@"front_suspension_shock_type"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_caster"]) {
        sheetFull.front_suspension_caster = [sheetInfoResult objectForKey:@"front_suspension_caster"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_shock_type"]) {
        sheetFull.front_shock_type = [sheetInfoResult  objectForKey:@"front_shock_type"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_type"]) {
        sheetFull.front_shock_types = [sheetInfoResult  objectForKey:@"front_shock_type"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_differential_type"]) {
        sheetFull.rear_differential_type = [sheetInfoResult objectForKey:@"rear_differential_type"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_shock_type"]) {
        sheetFull.rear_suspension_shock_type = [sheetInfoResult objectForKey:@"rear_suspension_shock_type"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_gear_diff"]) {
        sheetFull._rear_suspension_gear_diff = [sheetInfoResult objectForKey:@"rear_suspension_gear_diff"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_differential"]) {
        sheetFull.rear_suspension_differential = [sheetInfoResult objectForKey:@"rear_suspension_differential"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_bottom"]) {
        sheetFull.front_suspension_bottom = [sheetInfoResult objectForKey:@"front_suspension_bottom"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_kick_angle"]) {
        sheetFull.front_suspension_kick_angle = [sheetInfoResult objectForKey:@"front_suspension_kick_angle"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_spindle_type"]) {
        sheetFull.front_suspension_spindle_type = [sheetInfoResult objectForKey:@"front_suspension_spindle_type"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_toe"]) {
        sheetFull.front_suspension_toe = [sheetInfoResult objectForKey:@"front_suspension_toe"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_ride_height"]) {
        sheetFull.front_suspension_ride_height = [sheetInfoResult objectForKey:@"front_suspension_ride_height"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_camber"]) {
        sheetFull.front_suspension_camber = [sheetInfoResult objectForKey:@"front_suspension_camber"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_sway_bar"]) {
        sheetFull.front_suspension_sway_bar = [sheetInfoResult objectForKey:@"front_suspension_sway_bar"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_oil"]) {
        sheetFull.front_suspension_oil = [sheetInfoResult objectForKey:@"front_suspension_oil"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_piston"]) {
        sheetFull.front_suspension_piston = [sheetInfoResult objectForKey:@"front_suspension_piston"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_spring"]) {
        sheetFull.front_suspension_spring = [sheetInfoResult objectForKey:@"front_suspension_spring"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_piston_oil"]) {
        sheetFull.front_suspension_piston_oil = [sheetInfoResult objectForKey:@"front_suspension_piston_oil"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_limiters"]) {
        sheetFull.front_suspension_shock_limiters = [sheetInfoResult objectForKey:@"front_suspension_shock_limiters"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_location"]) {
        sheetFull.front_suspension_shock_location = [sheetInfoResult objectForKey:@"front_suspension_shock_location"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_bump_steer"]) {
        sheetFull.front_suspension_bump_steer = [sheetInfoResult objectForKey:@"front_suspension_bump_steer"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_camber_link"]) {
        sheetFull.front_suspension_camber_link = [sheetInfoResult objectForKey:@"front_suspension_camber_link"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_note"]) {
        sheetFull.front_suspension_note = [sheetInfoResult objectForKey:@"front_suspension_note"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_notes"]) {
        sheetFull.front_suspension_notes = [sheetInfoResult objectForKey:@"front_suspension_notes"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_chassis_configuration"]) {
        sheetFull.rear_suspension_chassis_configuration = [sheetInfoResult objectForKey:@"rear_suspension_chassis_configuration"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_toe"]) {
        sheetFull.rear_suspension_toe = [sheetInfoResult objectForKey:@"rear_suspension_toe"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_hub_spacing"]) {
        sheetFull.rear_suspension_hub_spacing = [sheetInfoResult objectForKey:@"rear_suspension_hub_spacing"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_anti_squat"]) {
        sheetFull.rear_suspension_anti_squat = [sheetInfoResult objectForKey:@"rear_suspension_anti_squat"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_roll_center"]) {
        sheetFull.rear_suspension_roll_center = [sheetInfoResult objectForKey:@"rear_suspension_roll_center"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_ride_height"]) {
        sheetFull.rear_suspension_ride_height = [sheetInfoResult objectForKey:@"rear_suspension_ride_height"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_camber"]) {
        sheetFull.rear_suspension_camber = [sheetInfoResult objectForKey:@"rear_suspension_camber"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_rear_hub"]) {
        sheetFull.rear_suspension_rear_hub = [sheetInfoResult objectForKey:@"rear_suspension_rear_hub"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_rear_hub_spacing"]) {
        sheetFull.rear_suspension_rear_hub_spacing = [sheetInfoResult objectForKey:@"rear_suspension_rear_hub_spacing"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_hex_width"]) {
        sheetFull.rear_suspension_hex_width = [sheetInfoResult objectForKey:@"rear_suspension_hex_width"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_sway_bar"]) {
        sheetFull.rear_suspension_sway_bar = [sheetInfoResult objectForKey:@"rear_suspension_sway_bar"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_oil"]) {
        sheetFull.rear_suspension_oil = [sheetInfoResult objectForKey:@"rear_suspension_oil"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_piston"]) {
        sheetFull.rear_suspension_piston = [sheetInfoResult objectForKey:@"rear_suspension_piston"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_spring"]) {
        sheetFull.rear_suspension_spring = [sheetInfoResult objectForKey:@"rear_suspension_spring"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_shock_limiters"]) {
        sheetFull.rear_suspension_shock_limiters = [sheetInfoResult objectForKey:@"rear_suspension_shock_limiters"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_shock_location"]) {
        sheetFull.rear_suspension_shock_location = [sheetInfoResult objectForKey:@"rear_suspension_shock_location"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_camber_link"]) {
        sheetFull.rear_suspension_camber_link = [sheetInfoResult objectForKey:@"rear_suspension_camber_link"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_wing_wickerbill"]) {
        sheetFull.rear_suspension_wing_wickerbill = [sheetInfoResult objectForKey:@"rear_suspension_wing_wickerbill"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_spoiler"]) {
        sheetFull._rear_suspension_spoiler = [sheetInfoResult objectForKey:@"rear_suspension_spoiler"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_batterry_position"]) {
        sheetFull.rear_suspension_batterry_position = [sheetInfoResult objectForKey:@"rear_suspension_batterry_position"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_1"]) {
        sheetFull.spacer_1 = [sheetInfoResult objectForKey:@"spacer_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_2"]) {
        sheetFull.spacer_2 = [sheetInfoResult objectForKey:@"spacer_2"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_3"]) {
        sheetFull.spacer_3 = [sheetInfoResult objectForKey:@"spacer_3"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_4"]) {
        sheetFull.spacer_4 = [sheetInfoResult objectForKey:@"spacer_4"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_5"]) {
        sheetFull.spacer_5 = [sheetInfoResult objectForKey:@"spacer_5"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_6"]) {
        sheetFull.spacer_6 = [sheetInfoResult objectForKey:@"spacer_6"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_radio"]) {
        sheetFull.electronics_radio = [sheetInfoResult objectForKey:@"electronics_radio"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_servo"]) {
        sheetFull.electronics_servo = [sheetInfoResult objectForKey:@"electronics_servo"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_esc"]) {
        sheetFull.electronics_esc = [sheetInfoResult objectForKey:@"electronics_esc"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_initial_brake"]) {
        sheetFull.electronics_initial_brake = [sheetInfoResult objectForKey:@"electronics_initial_brake"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_drag_brake"]) {
        sheetFull.electronics_drag_brake = [sheetInfoResult objectForKey:@"electronics_drag_brake"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_throttle_profile"]) {
        sheetFull.electronics_throttle_profile = [sheetInfoResult objectForKey:@"electronics_throttle_profile"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_timing_advance"]) {
        sheetFull.electronics_timing_advance = [sheetInfoResult objectForKey:@"electronics_timing_advance"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_throttle_brake_expo"]) {
        sheetFull.electronics_throttle_brake_expo = [sheetInfoResult objectForKey:@"electronics_throttle_brake_expo"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"electronics_servo_expo"]) {
        sheetFull.electronics_servo_expo = [sheetInfoResult objectForKey:@"electronics_servo_expo"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_throttle_brake_epa"]) {
        sheetFull.electronics_throttle_brake_epa = [sheetInfoResult objectForKey:@"electronics_throttle_brake_epa"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_motor"]) {
        sheetFull.electronics_motor = [sheetInfoResult objectForKey:@"electronics_motor"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_pinion"]) {
        sheetFull.electronics_pinion = [sheetInfoResult objectForKey:@"electronics_pinion"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_spur"]) {
        sheetFull.electronics_spur = [sheetInfoResult objectForKey:@"electronics_spur"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_battery"]) {
        sheetFull.electronics_battery = [sheetInfoResult objectForKey:@"electronics_battery"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight_of_each_piece"]) {
        sheetFull.weight_of_each_piece = [sheetInfoResult objectForKey:@"weight_of_each_piece"];
    }
    
    if ([sheetInfoResult objectForKey:@"tires_front"]) {
        sheetFull.tires_front = [sheetInfoResult objectForKey:@"tires_front"];
    }
    
    if ([sheetInfoResult objectForKey:@"tires_rear"]) {
        sheetFull.tires_rear = [sheetInfoResult objectForKey:@"tires_rear"];
    }
    
    if ([sheetInfoResult objectForKey:@"tires_notes"]) {
        sheetFull.tires_notes = [sheetInfoResult objectForKey:@"tires_notes"];
    }
    
    if ([sheetInfoResult objectForKey:@"compound_front"]) {
        sheetFull.compound_front = [sheetInfoResult objectForKey:@"compound_front"];
    }
    
    if ([sheetInfoResult objectForKey:@"compound_rear"]) {
        sheetFull.compound_rear = [sheetInfoResult objectForKey:@"compound_rear"];
    }
    
    if ([sheetInfoResult objectForKey:@"insert_front"]) {
        sheetFull.insert_front = [sheetInfoResult objectForKey:@"insert_front"];
    }
    
    if ([sheetInfoResult objectForKey:@"insert_rear"]) {
        sheetFull.insert_rear = [sheetInfoResult objectForKey:@"insert_rear"];
    }
    
    if ([sheetInfoResult objectForKey:@"additive_front"]) {
        sheetFull.additive_front = [sheetInfoResult objectForKey:@"additive_front"];
    }
    
    if ([sheetInfoResult objectForKey:@"additive_rear"]) {
        sheetFull.additive_rear = [sheetInfoResult objectForKey:@"additive_rear"];
    }
    if ([[sheetInfoResult objectForKey:@"password"] isEqualToString:@""] || ![sheetInfoResult objectForKey:@"password"] ) {
        sheetFull.check_Password = @"";
    } else {
        sheetFull.password = [sheetInfoResult objectForKey:@"password"];
        sheetFull.check_Password = @"1";
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_front_diff_fluid"]) {
        sheetFull.front_suspension_shock_front_diff_fluid = [sheetInfoResult objectForKey:@"front_suspension_shock_front_diff_fluid"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_ackerman"]) {
        sheetFull.front_suspension_ackerman = [sheetInfoResult objectForKey:@"front_suspension_ackerman"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_length"] ) {
        sheetFull.front_suspension_shock_length = [sheetInfoResult objectForKey:@"front_suspension_shock_length"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_center_diff_fluid"] ) {
        sheetFull.front_suspension_shock_center_diff_fluid = [sheetInfoResult objectForKey:@"front_suspension_shock_center_diff_fluid"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_diff_fluid"]) {
        sheetFull.rear_suspension_diff_fluid = [sheetInfoResult objectForKey:@"rear_suspension_diff_fluid"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_shock_length"]) {
        sheetFull.rear_suspension_shock_length = [sheetInfoResult objectForKey:@"rear_suspension_shock_length"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_camber_link"]) {
        sheetFull.front_suspension_shock_camber_link = [sheetInfoResult objectForKey:@"front_suspension_shock_camber_link"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_battery_position_diff_fluid"]) {
        sheetFull.rear_suspension_battery_position_diff_fluid = [sheetInfoResult objectForKey:@"rear_suspension_battery_position_diff_fluid"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_shock_bump_steer"]) {
        sheetFull.rear_suspension_shock_bump_steer = [sheetInfoResult objectForKey:@"rear_suspension_shock_bump_steer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_caster_block"]) {
        sheetFull.front_suspension_caster_block = [sheetInfoResult objectForKey:@"front_suspension_caster_block"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_bump_steer"]) {
        sheetFull.front_suspension_shock_bump_steer = [sheetInfoResult objectForKey:@"front_suspension_shock_bump_steer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_chassis_brace"]) {
        sheetFull.rear_chassis_brace = [sheetInfoResult objectForKey:@"rear_chassis_brace"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase"]) {
        sheetFull.rear_suspension_wheelbase = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_carrier"]) {
        sheetFull.rear_hub_carrier = [sheetInfoResult objectForKey:@"rear_hub_carrier"];
    }if ([sheetInfoResult objectForKey:@"rear_aluminum"]) {
        sheetFull.rear_aluminum = [sheetInfoResult objectForKey:@"rear_aluminum"];
    }
    if ([sheetInfoResult objectForKey:@"rear_anti_rollbar"]) {
        sheetFull.rear_anti_rollbar = [sheetInfoResult objectForKey:@"rear_anti_rollbar"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_pads"]) {
        sheetFull.electronics_drivetrain_pads = [sheetInfoResult objectForKey:@"electronics_drivetrain_pads"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_diff"]) {
        sheetFull.electronics_drivetrain_diff = [sheetInfoResult objectForKey:@"electronics_drivetrain_diff"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_tq"]) {
        sheetFull.electronics_drivetrain_tq = [sheetInfoResult objectForKey:@"electronics_drivetrain_tq"];
    }
    if ([sheetInfoResult objectForKey:@"track_camber"]) {
        sheetFull.track_camber = [sheetInfoResult objectForKey:@"track_camber"];
    }
    if ([sheetInfoResult objectForKey:@"track_toe"]) {
        sheetFull.track_toe = [sheetInfoResult objectForKey:@"track_toe"];
    }
    if ([sheetInfoResult objectForKey:@"track_washer"]) {
        sheetFull.track_washer = [sheetInfoResult objectForKey:@"track_washer"];
    }
    if ([sheetInfoResult objectForKey:@"track_ride_height_alpha"]) {
        sheetFull.track_ride_height_alpha = [sheetInfoResult objectForKey:@"track_ride_height_alpha"];
    }    if ([sheetInfoResult objectForKey:@"track_ride_height_io"]) {
        sheetFull.track_ride_height_io = [sheetInfoResult objectForKey:@"track_ride_height_io"];
    }    if ([sheetInfoResult objectForKey:@"track_field_ride_steering"]) {
        sheetFull.track_field_ride_steering = [sheetInfoResult objectForKey:@"track_field_ride_steering"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_track_camber"]) {
        sheetFull.rear_suspension_track_camber = [sheetInfoResult objectForKey:@"rear_suspension_track_camber"];
    }
    if ([sheetInfoResult objectForKey:@"track_suspension_toe"]) {
        sheetFull.track_suspension_toe = [sheetInfoResult objectForKey:@"track_suspension_toe"];
    }
    if ([sheetInfoResult objectForKey:@"track_rear_washer"]) {
        sheetFull.track_rear_washer = [sheetInfoResult objectForKey:@"track_rear_washer"];
    }
    if ([sheetInfoResult objectForKey:@"track_rear_abc"]) {
        sheetFull.track_rear_abc = [sheetInfoResult objectForKey:@"track_rear_abc"];
    }
    if ([sheetInfoResult objectForKey:@"track_rear_io"]) {
        sheetFull.track_rear_io = [sheetInfoResult objectForKey:@"track_rear_io"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_comment"]) {
        sheetFull.electronics_drivetrain_comment = [sheetInfoResult objectForKey:@"electronics_drivetrain_comment"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_washer"]) {
        sheetFull.front_suspension_washer = [sheetInfoResult objectForKey:@"front_suspension_washer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_washer"]) {
        sheetFull.rear_suspension_washer = [sheetInfoResult objectForKey:@"rear_suspension_washer"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_oil"]) {
        sheetFull.front_shock_oil = [sheetInfoResult objectForKey:@"front_shock_oil"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_washer2"]) {
        sheetFull.front_suspension_washer2 = [sheetInfoResult objectForKey:@"front_suspension_washer2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_right_height"]) {
        sheetFull.rear_suspension_right_height = [sheetInfoResult objectForKey:@"rear_suspension_right_height"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_spring"]) {
        sheetFull.front_shock_spring = [sheetInfoResult objectForKey:@"front_shock_spring"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_piston"]) {
        sheetFull.front_shock_piston = [sheetInfoResult objectForKey:@"front_shock_piston"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_limiter"]) {
        sheetFull.front_shock_limiter = [sheetInfoResult objectForKey:@"front_shock_limiter"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_spring"]) {
        sheetFull.rear_shock_spring = [sheetInfoResult objectForKey:@"rear_shock_spring"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_piston"]) {
        sheetFull.rear_shock_piston = [sheetInfoResult objectForKey:@"rear_shock_piston"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_oil"]) {
        sheetFull.rear_shock_oil = [sheetInfoResult objectForKey:@"rear_shock_oil"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_limiter"]) {
        sheetFull.rear_shock_limiter = [sheetInfoResult objectForKey:@"rear_shock_limiter"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_spur_gear"]) {
        sheetFull.electronics_spur_gear = [sheetInfoResult objectForKey:@"electronics_spur_gear"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_batteries"]) {
        sheetFull.electronics_batteries = [sheetInfoResult objectForKey:@"electronics_batteries"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_ballast"]) {
        sheetFull.electronics_ballast = [sheetInfoResult objectForKey:@"electronics_ballast"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_chassis"]) {
        sheetFull.electronics_chassis = [sheetInfoResult objectForKey:@"electronics_chassis"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_throttle"]) {
        sheetFull.electronics_throttle = [sheetInfoResult objectForKey:@"electronics_throttle"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_throttle2"]) {
        sheetFull.electronics_throttle2 = [sheetInfoResult objectForKey:@"electronics_throttle2"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_steering_expo"]) {
        sheetFull.electronics_steering_expo = [sheetInfoResult objectForKey:@"electronics_steering_expo"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_esc_setting"]) {
        sheetFull.electronics_esc_setting = [sheetInfoResult objectForKey:@"electronics_esc_setting"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_front_tires"]) {
        sheetFull.electronics_front_tires = [sheetInfoResult objectForKey:@"electronics_front_tires"];
    }
    if ([sheetInfoResult objectForKey:@"front_tire"]) {
        sheetFull.front_tire = [sheetInfoResult objectForKey:@"front_tire"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronics_front_compound"]) {
        sheetFull.electronics_front_compound = [sheetInfoResult objectForKey:@"electronics_front_compound"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_front_insert"]) {
        sheetFull.electronics_front_insert = [sheetInfoResult objectForKey:@"electronics_front_insert"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_front_wheel"]) {
        sheetFull.electronics_front_wheel = [sheetInfoResult objectForKey:@"electronics_front_wheel"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_rear_compound"]) {
        sheetFull.electronics_rear_compound = [sheetInfoResult objectForKey:@"electronics_rear_compound"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_rear_insert"]) {
        sheetFull.electronics_rear_insert = [sheetInfoResult objectForKey:@"electronics_rear_insert"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_rear_wheel"]) {
        sheetFull.electronics_rear_wheel = [sheetInfoResult objectForKey:@"electronics_rear_wheel"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_slipper"]) {
        sheetFull.electronics_drivetrain_slipper = [sheetInfoResult objectForKey:@"electronics_drivetrain_slipper"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_rear_tires"]) {
        sheetFull.electronics_rear_tires = [sheetInfoResult objectForKey:@"electronics_rear_tires"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_notes"]) {
        sheetFull.electronics_drivetrain_notes = [sheetInfoResult objectForKey:@"electronics_drivetrain_notes"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_diff_notes"]) {
        sheetFull.electronics_drivetrain_diff_notes = [sheetInfoResult objectForKey:@"electronics_drivetrain_diff_notes"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_other_body"]) {
        sheetFull.electronics_drivetrain_other_body = [sheetInfoResult objectForKey:@"electronics_drivetrain_other_body"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_other_wing"]) {
        sheetFull.electronics_drivetrain_other_wing = [sheetInfoResult objectForKey:@"electronics_drivetrain_other_wing"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_other_wing_angle"]) {
        sheetFull.electronics_drivetrain_other_wing_angle = [sheetInfoResult objectForKey:@"electronics_drivetrain_other_wing_angle"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_race_qualify"]) {
        sheetFull.electronics_drivetrain_race_qualify = [sheetInfoResult objectForKey:@"electronics_drivetrain_race_qualify"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_race_main"]) {
        sheetFull.electronics_drivetrain_race_main = [sheetInfoResult objectForKey:@"electronics_drivetrain_race_main"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_race_finish"]) {
        sheetFull.electronics_drivetrain_race_finish = [sheetInfoResult objectForKey:@"electronics_drivetrain_race_finish"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_top"]) {
        sheetFull.front_suspension_top = [sheetInfoResult objectForKey:@"front_suspension_top"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_middle"]) {
        sheetFull.front_suspension_middle = [sheetInfoResult objectForKey:@"front_suspension_middle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_top"]) {
        sheetFull.rear_suspension_top = [sheetInfoResult objectForKey:@"rear_suspension_top"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_ab"]) {
        sheetFull.rear_suspension_ab = [sheetInfoResult objectForKey:@"rear_suspension_ab"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_middle"]) {
        sheetFull.rear_suspension_middle = [sheetInfoResult objectForKey:@"rear_suspension_middle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_bottom"]) {
        sheetFull.rear_suspension_bottom = [sheetInfoResult objectForKey:@"rear_suspension_bottom"];
    }
    if ([sheetInfoResult objectForKey:@"battery_placement"]) {
        sheetFull.battery_placement = [sheetInfoResult objectForKey:@"battery_placement"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_diff_rear"]) {
        sheetFull.electronics_drivetrain_diff_rear = [sheetInfoResult objectForKey:@"electronics_drivetrain_diff_rear"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_race_main2"]) {
        sheetFull.electronics_drivetrain_race_main2 = [sheetInfoResult objectForKey:@"electronics_drivetrain_race_main2"];
    }
    if ([sheetInfoResult objectForKey:@"electronics_drivetrain_race_finish2"]) {
        sheetFull.electronics_drivetrain_race_finish2 = [sheetInfoResult objectForKey:@"electronics_drivetrain_race_finish2"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_washers"]) {
        sheetFull.front_suspension_washers = [sheetInfoResult objectForKey:@"front_suspension_washers"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_notes"]) {
        sheetFull.rear_suspension_notes = [sheetInfoResult objectForKey:@"rear_suspension_notes"];
    }
    
    
    
    if ([sheetInfoResult objectForKey:@"front_caster"]) {
        sheetFull.front_caster = [sheetInfoResult objectForKey:@"front_caster"];
    }
    if ([sheetInfoResult objectForKey:@"front_steering_insert"]) {
        sheetFull.front_steering_insert = [sheetInfoResult objectForKey:@"front_steering_insert"];
    }
    if ([sheetInfoResult objectForKey:@"front_tower_mount"]) {
        sheetFull.front_tower_mount = [sheetInfoResult objectForKey:@"front_tower_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_caster_hole"]) {
        sheetFull.front_caster_hole = [sheetInfoResult objectForKey:@"front_caster_hole"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_mount"]) {
        sheetFull.front_camber_mount = [sheetInfoResult objectForKey:@"front_camber_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_arm_mount"]) {
        sheetFull.front_arm_mount = [sheetInfoResult objectForKey:@"front_arm_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_toe"]) {
        sheetFull.rear_toe = [sheetInfoResult objectForKey:@"rear_toe"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_mount"]) {
        sheetFull.rear_hub_mount = [sheetInfoResult objectForKey:@"rear_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower_mount"]) {
        sheetFull.rear_tower_mount = [sheetInfoResult objectForKey:@"rear_tower_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_mount"]) {
        sheetFull.rear_arm_mount = [sheetInfoResult objectForKey:@"rear_arm_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight"]) {
        sheetFull.weight = [sheetInfoResult objectForKey:@"weight"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_type"]) {
        sheetFull.chassis_type = [sheetInfoResult objectForKey:@"chassis_type"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_type"]) {
        sheetFull.chassis_types = [sheetInfoResult objectForKey:@"chassis_type"];
    }
    if ([sheetInfoResult objectForKey:@"diff_type"]) {
        sheetFull.diff_type = [sheetInfoResult objectForKey:@"diff_type"];
    }
    
    if ([sheetInfoResult objectForKey:@"kickup"]) {
        sheetFull.kickup = [sheetInfoResult objectForKey:@"kickup"];
    }
    if ([sheetInfoResult objectForKey:@"motor_postion"]) {
        sheetFull.motor_postion = [sheetInfoResult objectForKey:@"motor_postion"];
    }
    if ([sheetInfoResult objectForKey:@"battery_postion"]) {
        sheetFull.battery_postion = [sheetInfoResult objectForKey:@"battery_postion"];
    }
    if ([sheetInfoResult objectForKey:@"fcb"]) {
        sheetFull.fcb = [sheetInfoResult objectForKey:@"fcb"];
    }
    if ([sheetInfoResult objectForKey:@"gear"]) {
        sheetFull.gear = [sheetInfoResult objectForKey:@"gear"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_size"]) {
        sheetFull.front_shock_size = [sheetInfoResult objectForKey:@"front_shock_size"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_size"]) {
        sheetFull.rear_shock_size = [sheetInfoResult objectForKey:@"rear_shock_size"];
    }
    if ([sheetInfoResult objectForKey:@"front_notes"]) {
        sheetFull.front_notes = [sheetInfoResult objectForKey:@"front_notes"];
    }
    if ([sheetInfoResult objectForKey:@"rear_notes"]) {
        sheetFull.rear_notes = [sheetInfoResult objectForKey:@"rear_notes"];
    }
    if ([sheetInfoResult objectForKey:@"tyre_notes"]) {
        sheetFull.tyre_notes = [sheetInfoResult objectForKey:@"tyre_notes"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_notes"]) {
        sheetFull.front_shock_notes = [sheetInfoResult objectForKey:@"front_shock_notes"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_notes"]) {
        sheetFull.rear_shock_notes = [sheetInfoResult objectForKey:@"rear_shock_notes"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_ride_height"]) {
        sheetFull.front_ride_height = [sheetInfoResult objectForKey:@"front_ride_height"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber"]) {
        sheetFull.front_camber = [sheetInfoResult objectForKey:@"front_camber"];
    }
    if ([sheetInfoResult objectForKey:@"front_toe"]) {
        sheetFull.front_toe = [sheetInfoResult objectForKey:@"front_toe"];
    }
    if ([sheetInfoResult objectForKey:@"front_swaybar"]) {
        sheetFull.front_swaybar = [sheetInfoResult objectForKey:@"front_swaybar"];
    }
    if ([sheetInfoResult objectForKey:@"front_swaybar_link"]) {
        sheetFull.front_swaybar_link = [sheetInfoResult objectForKey:@"front_swaybar_link"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_spring"]) {
        sheetFull.front_spring = [sheetInfoResult objectForKey:@"front_spring"];
    }
    if ([sheetInfoResult objectForKey:@"front_spring"]) {
        sheetFull.front_springs = [sheetInfoResult objectForKey:@"front_spring"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_droop"]) {
        sheetFull.front_droop = [sheetInfoResult objectForKey:@"front_droop"];
    }
    if ([sheetInfoResult objectForKey:@"front_top_hub_spacer"]) {
        sheetFull.front_top_hub_spacer = [sheetInfoResult objectForKey:@"front_top_hub_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"bump_steer_washers"]) {
        sheetFull.bump_steer_washers = [sheetInfoResult objectForKey:@"bump_steer_washers"];
    }
    if ([sheetInfoResult objectForKey:@"front_rear_block_spacer"]) {
        sheetFull.front_rear_block_spacer = [sheetInfoResult objectForKey:@"front_rear_block_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_bottom_hub_spacer"]) {
        sheetFull.front_bottom_hub_spacer = [sheetInfoResult objectForKey:@"front_bottom_hub_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_block_spacer"]) {
        sheetFull.front_block_spacer = [sheetInfoResult objectForKey:@"front_block_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_spacers"]) {
        sheetFull.front_camber_spacers = [sheetInfoResult objectForKey:@"front_camber_spacers"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_rebound"]) {
        sheetFull.front_shock_rebound = [sheetInfoResult objectForKey:@"front_shock_rebound"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_preload"]) {
        sheetFull.front_shock_preload = [sheetInfoResult objectForKey:@"front_shock_preload"];
    }
    if ([sheetInfoResult objectForKey:@"front_piston"]) {
        sheetFull.front_piston = [sheetInfoResult objectForKey:@"front_piston"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_length"]) {
        sheetFull.front_shock_length = [sheetInfoResult objectForKey:@"front_shock_length"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ride_height"]) {
        sheetFull.rear_ride_height = [sheetInfoResult objectForKey:@"rear_ride_height"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber"]) {
        sheetFull.rear_camber = [sheetInfoResult objectForKey:@"rear_camber"];
    }
    if ([sheetInfoResult objectForKey:@"rear_swaybar"]) {
        sheetFull.rear_swaybar = [sheetInfoResult objectForKey:@"rear_swaybar"];
    }
    if ([sheetInfoResult objectForKey:@"rear_swaybar_link"]) {
        sheetFull.rear_swaybar_link = [sheetInfoResult objectForKey:@"rear_swaybar_link"];
    }
    if ([sheetInfoResult objectForKey:@"rear_droop"]) {
        sheetFull.rear_droop = [sheetInfoResult objectForKey:@"rear_droop"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wing"]) {
        sheetFull.rear_wing = [sheetInfoResult objectForKey:@"rear_wing"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ballstud_spacers"]) {
        sheetFull.rear_ballstud_spacers = [sheetInfoResult objectForKey:@"rear_ballstud_spacers"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spring"]) {
        sheetFull.rear_spring = [sheetInfoResult objectForKey:@"rear_spring"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spring"]) {
        sheetFull.rear_springs = [sheetInfoResult objectForKey:@"rear_spring"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_rebound"]) {
        sheetFull.rear_shock_rebound = [sheetInfoResult objectForKey:@"rear_shock_rebound"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spring_text"]) {
        sheetFull.rear_spring_text = [sheetInfoResult objectForKey:@"rear_spring_text"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_preload"]) {
        sheetFull.rear_shock_preload = [sheetInfoResult objectForKey:@"rear_shock_preload"];
    }
    if ([sheetInfoResult objectForKey:@"rear_oil"]) {
        sheetFull.rear_oil = [sheetInfoResult objectForKey:@"rear_oil"];
    }
    if ([sheetInfoResult objectForKey:@"rear_piston"]) {
        sheetFull.rear_piston = [sheetInfoResult objectForKey:@"rear_piston"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_length"]) {
        sheetFull.rear_shock_length = [sheetInfoResult objectForKey:@"rear_shock_length"];
    }
    if ([sheetInfoResult objectForKey:@"motor"]) {
        sheetFull.motor = [sheetInfoResult objectForKey:@"motor"];
    }
    if ([sheetInfoResult objectForKey:@"spur"]) {
        sheetFull.spur = [sheetInfoResult objectForKey:@"spur"];
    }
    if ([sheetInfoResult objectForKey:@"pinion"]) {
        sheetFull.pinion = [sheetInfoResult objectForKey:@"pinion"];
    }
    if ([sheetInfoResult objectForKey:@"battery"]) {
        sheetFull.battery = [sheetInfoResult objectForKey:@"battery"];
    }
    if ([sheetInfoResult objectForKey:@"esc_settings"]) {
        sheetFull.esc_settings = [sheetInfoResult objectForKey:@"esc_settings"];
        
    }  if ([sheetInfoResult objectForKey:@"diff_oil"]) {
        sheetFull.diff_oil = [sheetInfoResult objectForKey:@"diff_oil"];
    }
    if ([sheetInfoResult objectForKey:@"body"]) {
        sheetFull.body = [sheetInfoResult objectForKey:@"body"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_front_shims"]) {
        sheetFull.wheelbase_front_shims = [sheetInfoResult objectForKey:@"wheelbase_front_shims"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_rear_shims"]) {
        sheetFull.wheelbase_rear_shims = [sheetInfoResult objectForKey:@"wheelbase_rear_shims"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_rear_shims"]) {
        sheetFull.wheelbase_rear_shims = [sheetInfoResult objectForKey:@"wheelbase_rear_shims"];
    }
    if ([sheetInfoResult objectForKey:@"front_brand"]) {
        sheetFull.front_brand = [sheetInfoResult objectForKey:@"front_brand"];
    }
    if ([sheetInfoResult objectForKey:@"front_tread"]) {
        sheetFull.front_tread = [sheetInfoResult objectForKey:@"front_tread"];
    }
    if ([sheetInfoResult objectForKey:@"front_compound"]) {
        sheetFull.front_compound = [sheetInfoResult objectForKey:@"front_compound"];
    }
    if ([sheetInfoResult objectForKey:@"front_oil"]) {
        sheetFull.front_oil = [sheetInfoResult objectForKey:@"front_oil"];
    }
    if ([sheetInfoResult objectForKey:@"front_piston_text"]) {
        sheetFull.front_piston_text = [sheetInfoResult objectForKey:@"front_piston_text"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_insert"]) {
        sheetFull.front_insert = [sheetInfoResult objectForKey:@"front_insert"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheel"]) {
        sheetFull.front_wheel = [sheetInfoResult objectForKey:@"front_wheel"];
    }
    if ([sheetInfoResult objectForKey:@"rear_brand"]) {
        sheetFull.rear_brand = [sheetInfoResult objectForKey:@"rear_brand"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tread"]) {
        sheetFull.rear_tread = [sheetInfoResult objectForKey:@"rear_tread"];
    }
    if ([sheetInfoResult objectForKey:@"rear_compound"]) {
        sheetFull.rear_compound = [sheetInfoResult objectForKey:@"rear_compound"];
    }
    if ([sheetInfoResult objectForKey:@"rear_insert"]) {
        sheetFull.rear_insert = [sheetInfoResult objectForKey:@"rear_insert"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheel"]) {
        sheetFull.rear_wheel = [sheetInfoResult objectForKey:@"rear_wheel"];
    }
    if ([sheetInfoResult objectForKey:@"qual_position"]) {
        sheetFull.qual_position = [sheetInfoResult objectForKey:@"qual_position"];
    }    if ([sheetInfoResult objectForKey:@"avg_lap"]) {
        sheetFull.avg_lap = [sheetInfoResult objectForKey:@"avg_lap"];
    }
    if ([sheetInfoResult objectForKey:@"best_lap"]) {
        sheetFull.best_lap = [sheetInfoResult objectForKey:@"best_lap"];
    }
    if ([sheetInfoResult objectForKey:@"race_time"]) {
        sheetFull.race_time = [sheetInfoResult objectForKey:@"race_time"];
    }
    if ([sheetInfoResult objectForKey:@"final_position"]) {
        sheetFull.final_position = [sheetInfoResult objectForKey:@"final_position"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_length"]) {
        sheetFull.chassis_length = [sheetInfoResult objectForKey:@"chassis_length"];
    }
    if ([sheetInfoResult objectForKey:@"front_bottom_hub_mount"]) {
        sheetFull.front_bottom_hub_mount = [sheetInfoResult objectForKey:@"front_bottom_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_bottom_hub_mount"]) {
        sheetFull.rear_bottom_hub_mount = [sheetInfoResult objectForKey:@"rear_bottom_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_toe"]) {
        sheetFull.rear_hub_toe = [sheetInfoResult objectForKey:@"rear_hub_toe"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_mount"]) {
        sheetFull.rear_camber_mount = [sheetInfoResult objectForKey:@"rear_camber_mount"];
    }
    if ([sheetInfoResult objectForKey:@"ballast_position"]) {
        sheetFull.ballast_position = [sheetInfoResult objectForKey:@"ballast_position"];
    }
    if ([sheetInfoResult objectForKey:@"2s"]) {
        sheetFull._2s = [sheetInfoResult objectForKey:@"2s"];
    }
    if ([sheetInfoResult objectForKey:@"024"]) {
        sheetFull._024 = [sheetInfoResult objectForKey:@"024"];
    }
    if ([sheetInfoResult objectForKey:@"front_diff_oil"]) {
        sheetFull.front_diff_oil = [sheetInfoResult objectForKey:@"front_diff_oil"];
    }
    if ([sheetInfoResult objectForKey:@"front_top_spindle_spacer"]) {
        sheetFull.front_top_spindle_spacer = [sheetInfoResult objectForKey:@"front_top_spindle_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"top_ballstud_washers"]) {
        sheetFull.top_ballstud_washers = [sheetInfoResult objectForKey:@"top_ballstud_washers"];
    }
    if ([sheetInfoResult objectForKey:@"front_bottom_spindle_spacer"]) {
        sheetFull.front_bottom_spindle_spacer = [sheetInfoResult objectForKey:@"front_bottom_spindle_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"mm1"]) {
        sheetFull.mm1 = [sheetInfoResult objectForKey:@"mm1"];
    }
    if ([sheetInfoResult objectForKey:@"mm2"]) {
        sheetFull.mm2 = [sheetInfoResult objectForKey:@"mm2"];
    }
    if ([sheetInfoResult objectForKey:@"slipper"]) {
        sheetFull.slipper = [sheetInfoResult objectForKey:@"slipper"];
    }
    if ([sheetInfoResult objectForKey:@"mm3"]) {
        sheetFull.mm3 = [sheetInfoResult objectForKey:@"mm3"];
    }
    if ([sheetInfoResult objectForKey:@"mm4"]) {
        sheetFull.mm4 = [sheetInfoResult objectForKey:@"mm4"];
    }
    if ([sheetInfoResult objectForKey:@"diff_lock"]) {
        sheetFull.diff_lock = [sheetInfoResult objectForKey:@"diff_lock"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_hub_mount"]) {
        sheetFull.front_hub_mount = [sheetInfoResult objectForKey:@"front_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"pc1"]) {
        sheetFull.pc1 = [sheetInfoResult objectForKey:@"pc1"];
    }
    if ([sheetInfoResult objectForKey:@"pc2"]) {
        sheetFull.pc2 = [sheetInfoResult objectForKey:@"pc2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer"]) {
        sheetFull.rear_spacer = [sheetInfoResult objectForKey:@"rear_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"mm5"]) {
        sheetFull.mm5 = [sheetInfoResult objectForKey:@"mm5"];
    }
    
    
    
    if ([sheetInfoResult objectForKey:@"differential_front"]) {
        sheetFull.differential_front = [sheetInfoResult objectForKey:@"differential_front"];
    }
    if ([sheetInfoResult objectForKey:@"differential_center"]) {
        sheetFull.differential_center = [sheetInfoResult objectForKey:@"differential_center"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_wing_type"]) {
        sheetFull.wing_setup_wing_type = [sheetInfoResult objectForKey:@"wing_setup_wing_type"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_wing_spacer"]) {
        sheetFull.wing_setup_wing_spacer = [sheetInfoResult objectForKey:@"wing_setup_wing_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_wicker_bill"]) {
        sheetFull.wing_setup_wicker_bill = [sheetInfoResult objectForKey:@"wing_setup_wicker_bill"];
    }
    if ([sheetInfoResult objectForKey:@"front_bump_steer"]) {
        sheetFull.front_bump_steer = [sheetInfoResult objectForKey:@"front_bump_steer"];
    }
    if ([sheetInfoResult objectForKey:@"front_ackerman"]) {
        sheetFull.front_ackerman = [sheetInfoResult objectForKey:@"front_ackerman"];
    }
    if ([sheetInfoResult objectForKey:@"front_ackerman_bottom"]) {
        sheetFull.front_ackerman_bottom = [sheetInfoResult objectForKey:@"front_ackerman_bottom"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_top"]) {
        sheetFull.wing_setup_top = [sheetInfoResult objectForKey:@"wing_setup_top"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup"]) {
        sheetFull.wing_setup = [sheetInfoResult objectForKey:@"wing_setup"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_bottom"]) {
        sheetFull.wing_setup_bottom = [sheetInfoResult objectForKey:@"wing_setup_bottom"];
    }
    if ([sheetInfoResult objectForKey:@"differential_notes"]) {
        sheetFull.differential_notes = [sheetInfoResult objectForKey:@"differential_notes"];
    }
    if ([sheetInfoResult objectForKey:@"wing_setup_notes"]) {
        sheetFull.wing_setup_notes = [sheetInfoResult objectForKey:@"wing_setup_notes"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_shoes_spring"]) {
        sheetFull.clutch_shoes_spring = [sheetInfoResult objectForKey:@"clutch_shoes_spring"];
    }
    if ([sheetInfoResult objectForKey:@"tire_notes"]) {
        sheetFull.tire_notes = [sheetInfoResult objectForKey:@"tire_notes"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_wheel_hex"]) {
        sheetFull.front_suspension_wheel_hex = [sheetInfoResult objectForKey:@"front_suspension_wheel_hex"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_limiter_droop"]) {
        sheetFull.front_suspension_limiter_droop = [sheetInfoResult objectForKey:@"front_suspension_limiter_droop"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_steering"]) {
        sheetFull.front_suspension_steering = [sheetInfoResult objectForKey:@"front_suspension_steering"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_battery_type"]) {
        sheetFull.front_suspension_battery_type = [sheetInfoResult objectForKey:@"front_suspension_battery_type"];
    }
    if ([sheetInfoResult objectForKey:@"differential_wire_dia_1"]) {
        sheetFull.differential_wire_dia_1 = [sheetInfoResult objectForKey:@"differential_wire_dia_1"];
    }
    if ([sheetInfoResult objectForKey:@"differential_qty_spring"]) {
        sheetFull.differential_qty_spring = [sheetInfoResult objectForKey:@"differential_qty_spring"];
    }
    if ([sheetInfoResult objectForKey:@"differential_ramp_plate"]) {
        sheetFull.differential_ramp_plate = [sheetInfoResult objectForKey:@"differential_ramp_plate"];
    }
    if ([sheetInfoResult objectForKey:@"differential_grease"]) {
        sheetFull.differential_grease = [sheetInfoResult objectForKey:@"differential_grease"];
    }
    if ([sheetInfoResult objectForKey:@"differential_diff_fluid"]) {
        sheetFull.differential_diff_fluid = [sheetInfoResult objectForKey:@"differential_diff_fluid"];
    }
    
    if ([sheetInfoResult objectForKey:@"differential_wire_dia_2"]) {
        sheetFull.differential_wire_dia_2 = [sheetInfoResult objectForKey:@"differential_wire_dia_2"];
    }
    if ([sheetInfoResult objectForKey:@"differential_qty_spring_2"]) {
        sheetFull.differential_qty_spring_2 = [sheetInfoResult objectForKey:@"differential_qty_spring_2"];
    }
    if ([sheetInfoResult objectForKey:@"differential_ramp_plate_2"]) {
        sheetFull.differential_ramp_plate_2 = [sheetInfoResult objectForKey:@"differential_ramp_plate_2"];
    }
    if ([sheetInfoResult objectForKey:@"differential_grease_2"]) {
        sheetFull.differential_grease_2 = [sheetInfoResult objectForKey:@"differential_grease_2"];
    }
    if ([sheetInfoResult objectForKey:@"differential_diff_fluid_2"]) {
        sheetFull.differential_diff_fluid_2 = [sheetInfoResult objectForKey:@"differential_diff_fluid_2"];
    }
    if ([sheetInfoResult objectForKey:@"engine_engine"]) {
        sheetFull.engine_engine = [sheetInfoResult objectForKey:@"engine_engine"];
    }
    if ([sheetInfoResult objectForKey:@"engine_grow_plug"]) {
        sheetFull.engine_grow_plug = [sheetInfoResult objectForKey:@"engine_grow_plug"];
    }
    if ([sheetInfoResult objectForKey:@"engine_pipe_header"]) {
        sheetFull.engine_pipe_header = [sheetInfoResult objectForKey:@"engine_pipe_header"];
    }
    if ([sheetInfoResult objectForKey:@"engine_fuel"]) {
        sheetFull.engine_fuel = [sheetInfoResult objectForKey:@"engine_fuel"];
    }
    if ([sheetInfoResult objectForKey:@"engine_head_clearance"]) {
        sheetFull.engine_head_clearance = [sheetInfoResult objectForKey:@"engine_head_clearance"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheel_hex"]) {
        sheetFull.rear_suspension_wheel_hex = [sheetInfoResult objectForKey:@"rear_suspension_wheel_hex"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_piston_oil"]) {
        sheetFull.rear_suspension_piston_oil = [sheetInfoResult objectForKey:@"rear_suspension_piston_oil"];
    }
    if ([sheetInfoResult objectForKey:@"engine_gearing"]) {
        sheetFull.engine_gearing = [sheetInfoResult objectForKey:@"engine_gearing"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_limiter_droop"]) {
        sheetFull.rear_suspension_limiter_droop = [sheetInfoResult objectForKey:@"rear_suspension_limiter_droop"];
    }
    if ([sheetInfoResult objectForKey:@"tire_front_type"]) {
        sheetFull.tire_front_type = [sheetInfoResult objectForKey:@"tire_front_type"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_wheel_hex"]) {
        sheetFull.front_suspension_wheel_hex = [sheetInfoResult objectForKey:@"front_suspension_wheel_hex"];
    }
    if ([sheetInfoResult objectForKey:@"tire_front_compound"]) {
        sheetFull.tire_front_compound = [sheetInfoResult objectForKey:@"tire_front_compound"];
    }
    if ([sheetInfoResult objectForKey:@"tire_front_insert"]) {
        sheetFull.tire_front_insert = [sheetInfoResult objectForKey:@"tire_front_insert"];
    }
    if ([sheetInfoResult objectForKey:@"tire_rear_type"]) {
        sheetFull.tire_rear_type = [sheetInfoResult objectForKey:@"tire_rear_type"];
    }
    if ([sheetInfoResult objectForKey:@"tire_rear_compound"]) {
        sheetFull.tire_rear_compound = [sheetInfoResult objectForKey:@"tire_rear_compound"];
    }
    if ([sheetInfoResult objectForKey:@"tire_rear_insert"]) {
        sheetFull.tire_rear_insert = [sheetInfoResult objectForKey:@"tire_rear_insert"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"front_tower_mounts"]) {
        sheetFull.front_tower_mounts = [sheetInfoResult objectForKey:@"front_tower_mounts"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_mounts"]) {
        sheetFull.front_camber_mounts = [sheetInfoResult objectForKey:@"front_camber_mounts"];
    }
    if ([sheetInfoResult objectForKey:@"front_block_inserts"]) {
        sheetFull.front_block_inserts = [sheetInfoResult objectForKey:@"front_block_inserts"];
    }
    if ([sheetInfoResult objectForKey:@"front_arm_mounts"]) {
        sheetFull.front_arm_mounts = [sheetInfoResult objectForKey:@"front_arm_mounts"];
    }
    if ([sheetInfoResult objectForKey:@"steering_position"]) {
        sheetFull.steering_position = [sheetInfoResult objectForKey:@"steering_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_hub_mount"]) {
        sheetFull.front_hub_mounts = [sheetInfoResult objectForKey:@"front_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"steering_block_material"]) {
        sheetFull.steering_block_material = [sheetInfoResult objectForKey:@"steering_block_material"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower_mounts"]) {
        sheetFull.rear_tower_mounts = [sheetInfoResult objectForKey:@"rear_tower_mounts"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_mounts"]) {
        sheetFull.rear_camber_mounts = [sheetInfoResult objectForKey:@"rear_camber_mounts"];
    }
    if ([sheetInfoResult objectForKey:@"rear_toeplate"]) {
        sheetFull.rear_toeplate = [sheetInfoResult objectForKey:@"rear_toeplate"];
    }
    if ([sheetInfoResult objectForKey:@"rf_inserts"]) {
        sheetFull.rf_inserts = [sheetInfoResult objectForKey:@"rf_inserts"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_mount"]) {
        sheetFull.rear_hub_mounts = [sheetInfoResult objectForKey:@"rear_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_mounts"]) {
        sheetFull.rear_arm_mounts = [sheetInfoResult objectForKey:@"rear_arm_mounts"];
    }
    if (sheetFull.rear_arm_mounts.count == 0) {
        sheetFull.rear_arm_mounts = [sheetInfoResult objectForKey:@"rear_arm_mount"];
    }
    if ([sheetInfoResult objectForKey:@"wing_position"]) {
        sheetFull.wing_position = [sheetInfoResult objectForKey:@"wing_position"];
    }
    if ([sheetInfoResult objectForKey:@"hub_insert"]) {
        sheetFull.hub_insert = [sheetInfoResult objectForKey:@"hub_insert"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"chassis_notes"]) {
        sheetFull.chassis_notes = [sheetInfoResult objectForKey:@"chassis_notes"];
    }
    if ([sheetInfoResult objectForKey:@"front_tire_notes"]) {
        sheetFull.front_tire_notes = [sheetInfoResult objectForKey:@"front_tire_notes"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tire_notes"]) {
        sheetFull.rear_tire_notes = [sheetInfoResult objectForKey:@"rear_tire_notes"];
    }
    
    if ([sheetInfoResult objectForKey:@"steering_type"]) {
        sheetFull.steering_type = [sheetInfoResult objectForKey:@"steering_type"];
    }
    if ([sheetInfoResult objectForKey:@"top_hub_spacer"]) {
        sheetFull.top_hub_spacer = [sheetInfoResult objectForKey:@"top_hub_spacer"];
    }    if ([sheetInfoResult objectForKey:@"bottom_hub_spacer"]) {
        sheetFull.bottom_hub_spacer = [sheetInfoResult objectForKey:@"bottom_hub_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_bladder"]) {
        sheetFull.front_bladder = [sheetInfoResult objectForKey:@"front_bladder"];
    }
    if ([sheetInfoResult objectForKey:@"wing"]) {
        sheetFull.wing = [sheetInfoResult objectForKey:@"wing"];
    }
    if ([sheetInfoResult objectForKey:@"rear_bladder"]) {
        sheetFull.rear_bladder = [sheetInfoResult objectForKey:@"rear_bladder"];
    }
    if ([sheetInfoResult objectForKey:@"front_arm_caps"]) {
        sheetFull.front_arm_caps = [sheetInfoResult objectForKey:@"front_arm_caps"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_caps"]) {
        sheetFull.rear_arm_caps = [sheetInfoResult objectForKey:@"rear_arm_caps"];
    }
    if ([sheetInfoResult objectForKey:@"pipe"]) {
        sheetFull.pipe = [sheetInfoResult objectForKey:@"pipe"];
    }    if ([sheetInfoResult objectForKey:@"engine_brand"]) {
        sheetFull.engine_brand = [sheetInfoResult objectForKey:@"engine_brand"];
    }    if ([sheetInfoResult objectForKey:@"clutch"]) {
        sheetFull.clutch = [sheetInfoResult objectForKey:@"clutch"];
    }
    if ([sheetInfoResult objectForKey:@"plug"]) {
        sheetFull.plug = [sheetInfoResult objectForKey:@"plug"];
    }    if ([sheetInfoResult objectForKey:@"fuel"]) {
        sheetFull.fuel = [sheetInfoResult objectForKey:@"fuel"];
    }    if ([sheetInfoResult objectForKey:@"rear_tire"]) {
        sheetFull.rear_tire = [sheetInfoResult objectForKey:@"rear_tire"];
    }    if ([sheetInfoResult objectForKey:@"front_oil_brand"]) {
        sheetFull.front_oil_brand = [sheetInfoResult objectForKey:@"front_oil_brand"];
    }
    if ([sheetInfoResult objectForKey:@"front_oil_weight"]) {
        sheetFull.front_oil_weight = [sheetInfoResult objectForKey:@"front_oil_weight"];
    }
    if ([sheetInfoResult objectForKey:@"center_oil_brand"]) {
        sheetFull.center_oil_brand = [sheetInfoResult objectForKey:@"center_oil_brand"];
    }
    if ([sheetInfoResult objectForKey:@"center_oil_weight"]) {
        sheetFull.center_oil_weight = [sheetInfoResult objectForKey:@"center_oil_weight"];
    }
    if ([sheetInfoResult objectForKey:@"rear_oil_brand"]) {
        sheetFull.rear_oil_brand = [sheetInfoResult objectForKey:@"rear_oil_brand"];
    }
    if ([sheetInfoResult objectForKey:@"rear_oil_weight"]) {
        sheetFull.rear_oil_weight = [sheetInfoResult objectForKey:@"rear_oil_weight"];
    }
    if ([sheetInfoResult objectForKey:@"front_tower_mount"]) {
        sheetFull.front_tower_mounts = [sheetInfoResult objectForKey:@"front_tower_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_mount"]) {
        sheetFull.front_camber_mounts = [sheetInfoResult objectForKey:@"front_camber_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_block_insert"]) {
        sheetFull.front_block_inserts = [sheetInfoResult objectForKey:@"front_block_insert"];
    }
    if ([sheetInfoResult objectForKey:@"front_arm_mount"]) {
        sheetFull.front_arm_mounts = [sheetInfoResult objectForKey:@"front_arm_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower_mount"]) {
        sheetFull.rear_tower_mounts = [sheetInfoResult objectForKey:@"rear_tower_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_mount"]) {
        sheetFull.rear_camber_mounts = [sheetInfoResult objectForKey:@"rear_camber_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rr_inserts"]) {
        sheetFull.rr_inserts = [sheetInfoResult objectForKey:@"rr_inserts"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"front_toe_mount"]) {
        sheetFull.front_toe_mount = [sheetInfoResult objectForKey:@"front_toe_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_ballstud_postion"]) {
        sheetFull.front_ballstud_postion = [sheetInfoResult objectForKey:@"front_ballstud_postion"];
    }
    if ([sheetInfoResult objectForKey:@"front_swaybar_mount"]) {
        sheetFull.front_swaybar_mount = [sheetInfoResult objectForKey:@"front_swaybar_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_toe_mount"]) {
        sheetFull.rear_toe_mount = [sheetInfoResult objectForKey:@"rear_toe_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ballstud_postion"]) {
        sheetFull.rear_ballstud_postion = [sheetInfoResult objectForKey:@"rear_ballstud_postion"];
    }
    if ([sheetInfoResult objectForKey:@"rear_swaybar_mount"]) {
        sheetFull.rear_swaybar_mount = [sheetInfoResult objectForKey:@"rear_swaybar_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer_f"]) {
        sheetFull.spacer_f = [sheetInfoResult objectForKey:@"spacer_f"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_r"]) {
        sheetFull.spacer_r = [sheetInfoResult objectForKey:@"spacer_r"];
    }
    if ([sheetInfoResult objectForKey:@"caster"]) {
        sheetFull.caster = [sheetInfoResult objectForKey:@"caster"];
    }
    if ([sheetInfoResult objectForKey:@"caster"]) {
        sheetFull.casters = [sheetInfoResult objectForKey:@"caster"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheelhub"]) {
        sheetFull.front_wheelhub = [sheetInfoResult objectForKey:@"front_wheelhub"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_f"]) {
        sheetFull.rear_spacer_f = [sheetInfoResult objectForKey:@"rear_spacer_f"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_r"]) {
        sheetFull.rear_spacer_r = [sheetInfoResult objectForKey:@"rear_spacer_r"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheelspacer"]) {
        sheetFull.rear_wheelspacer = [sheetInfoResult objectForKey:@"rear_wheelspacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheelhub"]) {
        sheetFull.rear_wheelhub = [sheetInfoResult objectForKey:@"rear_wheelhub"];
    }
    if ([sheetInfoResult objectForKey:@"front_treatment"]) {
        sheetFull.front_treatment = [sheetInfoResult objectForKey:@"front_treatment"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_7"]) {
        sheetFull.spacer_7 = [sheetInfoResult objectForKey:@"spacer_7"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_8"]) {
        sheetFull.spacer_8 = [sheetInfoResult objectForKey:@"spacer_8"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_9"]) {
        sheetFull.spacer_9 = [sheetInfoResult objectForKey:@"spacer_9"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_10"]) {
        sheetFull.spacer_10 = [sheetInfoResult objectForKey:@"spacer_10"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_11"]) {
        sheetFull.spacer_11 = [sheetInfoResult objectForKey:@"spacer_11"];
    }
    if ([sheetInfoResult objectForKey:@"spacer_12"]) {
        sheetFull.spacer_12 = [sheetInfoResult objectForKey:@"spacer_12"];
    }
    if ([sheetInfoResult objectForKey:@"front_note"]) {
        sheetFull.front_note = [sheetInfoResult objectForKey:@"front_note"];
    }
    if ([sheetInfoResult objectForKey:@"front_type"]) {
        sheetFull.front_type = [sheetInfoResult objectForKey:@"front_type"];
    }
    if ([sheetInfoResult objectForKey:@"rear_type"]) {
        sheetFull.rear_type = [sheetInfoResult objectForKey:@"rear_type"];
    }
    if ([sheetInfoResult objectForKey:@"rear_note"]) {
        sheetFull.rear_note = [sheetInfoResult objectForKey:@"rear_note"];
    }
    if ([sheetInfoResult objectForKey:@"esc"]) {
        sheetFull.esc = [sheetInfoResult objectForKey:@"esc"];
    }
    if ([sheetInfoResult objectForKey:@"note"]) {
        sheetFull.note = [sheetInfoResult objectForKey:@"note"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheelspacer"]) {
        sheetFull.front_wheelspacer = [sheetInfoResult objectForKey:@"front_wheelspacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_treatment"]) {
        sheetFull.rear_treatment = [sheetInfoResult objectForKey:@"rear_treatment"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"front_rollbar"]) {
        sheetFull.front_rollbar = [sheetInfoResult objectForKey:@"front_rollbar"];
    }
    if ([sheetInfoResult objectForKey:@"steering_rack"]) {
        sheetFull.steering_rack = [sheetInfoResult objectForKey:@"steering_rack"];
    }
    if ([sheetInfoResult objectForKey:@"pin_bushing_front_position"]) {
        sheetFull.pin_bushing_front_position = [sheetInfoResult objectForKey:@"pin_bushing_front_position"];
    }
    if ([sheetInfoResult objectForKey:@"pin_bushing_rear_position"]) {
        sheetFull.pin_bushing_rear_position = [sheetInfoResult objectForKey:@"pin_bushing_rear_position"];
    }
    if ([sheetInfoResult objectForKey:@"b_plate"]) {
        sheetFull.b_plate = [sheetInfoResult objectForKey:@"b_plate"];
    }
    if ([sheetInfoResult objectForKey:@"a_plate"]) {
        sheetFull.a_plate = [sheetInfoResult objectForKey:@"a_plate"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rollbar"]) {
        sheetFull.rear_rollbar = [sheetInfoResult objectForKey:@"rear_rollbar"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase"]) {
        sheetFull.wheelbase = [sheetInfoResult objectForKey:@"wheelbase"];
    }
    if ([sheetInfoResult objectForKey:@"d_plate"]) {
        sheetFull.d_plate = [sheetInfoResult objectForKey:@"d_plate"];
    }
    if ([sheetInfoResult objectForKey:@"c_plate"]) {
        sheetFull.c_plate = [sheetInfoResult objectForKey:@"c_plate"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_brace"]) {
        sheetFull.chassis_brace = [sheetInfoResult objectForKey:@"chassis_brace"];
    }
    if ([sheetInfoResult objectForKey:@"wheel_hex"]) {
        sheetFull.wheel_hex = [sheetInfoResult objectForKey:@"wheel_hex"];
    }
    if ([sheetInfoResult objectForKey:@"caster_blocks"]) {
        sheetFull.caster_blocks = [sheetInfoResult objectForKey:@"caster_blocks"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hubs"]) {
        sheetFull.rear_hubs = [sheetInfoResult objectForKey:@"rear_hubs"];
    }
    if ([sheetInfoResult objectForKey:@"tq"]) {
        sheetFull.tq = [sheetInfoResult objectForKey:@"tq"];
    }
    if ([sheetInfoResult objectForKey:@"tower_position"]) {
        sheetFull.tower_position = [sheetInfoResult objectForKey:@"tower_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_hub"]) {
        sheetFull.front_hub = [sheetInfoResult objectForKey:@"front_hub"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower_position"]) {
        sheetFull.rear_tower_position = [sheetInfoResult objectForKey:@"rear_tower_position"];
    }
    if ([sheetInfoResult objectForKey:@"camber_position"]) {
        sheetFull.camber_position = [sheetInfoResult objectForKey:@"camber_position"];
    }
    if ([sheetInfoResult objectForKey:@"hub_position"]) {
        sheetFull.hub_position = [sheetInfoResult objectForKey:@"hub_position"];
    }
    if ([sheetInfoResult objectForKey:@"hub_position_io"]) {
        sheetFull.hub_position_io = [sheetInfoResult objectForKey:@"hub_position_io"];
    }
    if ([sheetInfoResult objectForKey:@"wing_type"]) {
        sheetFull.wing_type = [sheetInfoResult objectForKey:@"wing_type"];
    }
    if ([sheetInfoResult objectForKey:@"comments"]) {
        sheetFull.comments = [sheetInfoResult objectForKey:@"comments"];
    }
    if ([sheetInfoResult objectForKey:@"bumpsteer"]) {
        sheetFull.bumpsteer = [sheetInfoResult objectForKey:@"bumpsteer"];
    }
    if ([sheetInfoResult objectForKey:@"toe"]) {
        sheetFull.toe = [sheetInfoResult objectForKey:@"toe"];
    }
    if ([sheetInfoResult objectForKey:@"front_ride"]) {
        sheetFull.front_ride = [sheetInfoResult objectForKey:@"front_ride"];
    }
    if ([sheetInfoResult objectForKey:@"pin_bushing_front"]) {
        sheetFull.pin_bushing_front = [sheetInfoResult objectForKey:@"pin_bushing_front"];
    }
    if ([sheetInfoResult objectForKey:@"pin_bushing_rear"]) {
        sheetFull.pin_bushing_rear = [sheetInfoResult objectForKey:@"pin_bushing_rear"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ride"]) {
        sheetFull.rear_ride = [sheetInfoResult objectForKey:@"rear_ride"];
    }
    if ([sheetInfoResult objectForKey:@"toe_bushing"]) {
        sheetFull.toe_bushing = [sheetInfoResult objectForKey:@"toe_bushing"];
    }
    if ([sheetInfoResult objectForKey:@"squat_bushing"]) {
        sheetFull.squat_bushing = [sheetInfoResult objectForKey:@"squat_bushing"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_cap"]) {
        sheetFull.front_cap = [sheetInfoResult objectForKey:@"front_cap"];
    }
    if ([sheetInfoResult objectForKey:@"front_length"]) {
        sheetFull.front_length = [sheetInfoResult objectForKey:@"front_length"];
    }
    if ([sheetInfoResult objectForKey:@"front_rebound"]) {
        sheetFull.front_rebound = [sheetInfoResult objectForKey:@"front_rebound"];
    }
    if ([sheetInfoResult objectForKey:@"rear_cap"]) {
        sheetFull.rear_cap = [sheetInfoResult objectForKey:@"rear_cap"];
    }
    if ([sheetInfoResult objectForKey:@"rear_length"]) {
        sheetFull.rear_length = [sheetInfoResult objectForKey:@"rear_length"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rebound"]) {
        sheetFull.rear_rebound = [sheetInfoResult objectForKey:@"rear_rebound"];
    }
    if ([sheetInfoResult objectForKey:@"front_diff"]) {
        sheetFull.front_diff = [sheetInfoResult objectForKey:@"front_diff"];
    }
    if ([sheetInfoResult objectForKey:@"center_diff"]) {
        sheetFull.center_diff = [sheetInfoResult objectForKey:@"center_diff"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diff"]) {
        sheetFull.rear_diff = [sheetInfoResult objectForKey:@"rear_diff"];
    }
    if ([sheetInfoResult objectForKey:@"diff_notes"]) {
        sheetFull.diff_notes = [sheetInfoResult objectForKey:@"diff_notes"];
    }
    if ([sheetInfoResult objectForKey:@"body_type"]) {
        sheetFull.body_type = [sheetInfoResult objectForKey:@"body_type"];
    }
    if ([sheetInfoResult objectForKey:@"body_brand"]) {
        sheetFull.body_brand = [sheetInfoResult objectForKey:@"body_brand"];
    }
    
    if ([sheetInfoResult objectForKey:@"gap"]) {
        sheetFull.gap = [sheetInfoResult objectForKey:@"gap"];
    }
    if ([sheetInfoResult objectForKey:@"bell"]) {
        sheetFull.bell = [sheetInfoResult objectForKey:@"bell"];
    }
    if ([sheetInfoResult objectForKey:@"shoes"]) {
        sheetFull.shoes = [sheetInfoResult objectForKey:@"shoes"];
    }
    if ([sheetInfoResult objectForKey:@"springs"]) {
        sheetFull.springs = [sheetInfoResult objectForKey:@"springs"];
    }
    if ([sheetInfoResult objectForKey:@"engine"]) {
        sheetFull.engine = [sheetInfoResult objectForKey:@"engine"];
    }
    if ([sheetInfoResult objectForKey:@"restrictor"]) {
        sheetFull.restrictor = [sheetInfoResult objectForKey:@"restrictor"];
    }
    if ([sheetInfoResult objectForKey:@"temp"]) {
        sheetFull.temp = [sheetInfoResult objectForKey:@"temp"];
    }
    if ([sheetInfoResult objectForKey:@"muffler"]) {
        sheetFull.muffler = [sheetInfoResult objectForKey:@"muffler"];
    }
    if ([sheetInfoResult objectForKey:@"front_tires"]) {
        sheetFull.front_tires = [sheetInfoResult objectForKey:@"front_tires"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tires"]) {
        sheetFull.rear_tires = [sheetInfoResult objectForKey:@"rear_tires"];
    }
    if ([sheetInfoResult objectForKey:@"radio"]) {
        sheetFull.radio = [sheetInfoResult objectForKey:@"radio"];
    }
    if ([sheetInfoResult objectForKey:@"brake_bias"]) {
        sheetFull.brake_bias = [sheetInfoResult objectForKey:@"brake_bias"];
    }
    if ([sheetInfoResult objectForKey:@"t_servo"]) {
        sheetFull.t_servo = [sheetInfoResult objectForKey:@"t_servo"];
    }
    if ([sheetInfoResult objectForKey:@"s_servo"]) {
        sheetFull.s_servo = [sheetInfoResult objectForKey:@"s_servo"];
    }
    if ([sheetInfoResult objectForKey:@"t_expo"]) {
        sheetFull.t_expo = [sheetInfoResult objectForKey:@"t_expo"];
    }
    if ([sheetInfoResult objectForKey:@"b_expo"]) {
        sheetFull.b_expo = [sheetInfoResult objectForKey:@"b_expo"];
    }
    if ([sheetInfoResult objectForKey:@"s_expo"]) {
        sheetFull.s_expo = [sheetInfoResult objectForKey:@"s_expo"];
    }
    if ([sheetInfoResult objectForKey:@"dualrate"]) {
        sheetFull.dualrate = [sheetInfoResult objectForKey:@"dualrate"];
    }
    if ([sheetInfoResult objectForKey:@"qualify"]) {
        sheetFull.qualify = [sheetInfoResult objectForKey:@"qualify"];
    }
    if ([sheetInfoResult objectForKey:@"main"]) {
        sheetFull.main = [sheetInfoResult objectForKey:@"main"];
    }
    if ([sheetInfoResult objectForKey:@"finish"]) {
        sheetFull.finish = [sheetInfoResult objectForKey:@"finish"];
    }
    
    if ([sheetInfoResult objectForKey:@"wing_cut"]) {
        sheetFull.wing_cut = [sheetInfoResult objectForKey:@"wing_cut"];
    }
    if ([sheetInfoResult objectForKey:@"camber_sn"]) {
        sheetFull.camber_sn = [sheetInfoResult objectForKey:@"camber_sn"];
    }
    if ([sheetInfoResult objectForKey:@"bulkhead_sn"]) {
        sheetFull.bulkhead_sn = [sheetInfoResult objectForKey:@"bulkhead_sn"];
    }
    if ([sheetInfoResult objectForKey:@"camber_radio"]) {
        sheetFull.camber_radio = [sheetInfoResult objectForKey:@"camber_radio"];
    }
    if ([sheetInfoResult objectForKey:@"toe_radio"]) {
        sheetFull.toe_radio = [sheetInfoResult objectForKey:@"toe_radio"];
    }
    if ([sheetInfoResult objectForKey:@"caster_block_mount"]) {
        sheetFull.caster_block_mount = [sheetInfoResult objectForKey:@"caster_block_mount"];
    }
    if ([sheetInfoResult objectForKey:@"caster_spacer"]) {
        sheetFull.caster_spacer = [sheetInfoResult objectForKey:@"caster_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"spindle_spacer"]) {
        sheetFull.spindle_spacer = [sheetInfoResult objectForKey:@"spindle_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"spring"]) {
        sheetFull.spring = [sheetInfoResult objectForKey:@"spring"];
    }
    if ([sheetInfoResult objectForKey:@"shock_bladder"]) {
        sheetFull.shock_bladder = [sheetInfoResult objectForKey:@"shock_bladder"];
    }
    if ([sheetInfoResult objectForKey:@"hub_toe"]) {
        sheetFull.hub_toe = [sheetInfoResult objectForKey:@"hub_toe"];
    }
    if ([sheetInfoResult objectForKey:@"vertical_hub_mount"]) {
        sheetFull.vertical_hub_mount = [sheetInfoResult objectForKey:@"vertical_hub_mount"];
    }
    if ([sheetInfoResult objectForKey:@"antisquat_block"]) {
        sheetFull.antisquat_block = [sheetInfoResult objectForKey:@"antisquat_block"];
    }
    if ([sheetInfoResult objectForKey:@"antisquat_spacer"]) {
        sheetFull.antisquat_spacer = [sheetInfoResult objectForKey:@"antisquat_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_bladder"]) {
        sheetFull.rear_shock_bladder = [sheetInfoResult objectForKey:@"rear_shock_bladder"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_piston_text"]) {
        sheetFull.rear_piston_text = [sheetInfoResult objectForKey:@"rear_piston_text"];
    }
    
    if ([sheetInfoResult objectForKey:@"motor_position"]) {
        sheetFull.motor_position = [sheetInfoResult objectForKey:@"motor_position"];
    }
    if ([sheetInfoResult objectForKey:@"rotor_type"]) {
        sheetFull.rotor_type = [sheetInfoResult objectForKey:@"rotor_type"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_position"]) {
        sheetFull.chassis_position = [sheetInfoResult objectForKey:@"chassis_position"];
    }
    if ([sheetInfoResult objectForKey:@"comment"]) {
        sheetFull.comment = [sheetInfoResult objectForKey:@"comment"];
    }
    if ([sheetInfoResult objectForKey:@"front_ballstud_spacers"]) {
        sheetFull.front_ballstud_spacers = [sheetInfoResult objectForKey:@"front_ballstud_spacers"];
    }
    if ([sheetInfoResult objectForKey:@"front_weight"]) {
        sheetFull.front_weight = [sheetInfoResult objectForKey:@"front_weight"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_limiters"]) {
        sheetFull.front_shock_limiters = [sheetInfoResult objectForKey:@"front_shock_limiters"];
    }
    if ([sheetInfoResult objectForKey:@"rear_vertical_ballstud_spacers"]) {
        sheetFull.rear_vertical_ballstud_spacers = [sheetInfoResult objectForKey:@"rear_vertical_ballstud_spacers"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_text"]) {
        sheetFull.rear_camber_text = [sheetInfoResult objectForKey:@"rear_camber_text"];
    }
    if ([sheetInfoResult objectForKey:@"rear_weight_block"]) {
        sheetFull.rear_weight_block = [sheetInfoResult objectForKey:@"rear_weight_block"];
    }
    if ([sheetInfoResult objectForKey:@"rear_sway_bar"]) {
        sheetFull.rear_sway_bar = [sheetInfoResult objectForKey:@"rear_sway_bar"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_limiters"]) {
        sheetFull.rear_shock_limiters = [sheetInfoResult objectForKey:@"rear_shock_limiters"];
    }
    if ([sheetInfoResult objectForKey:@"rotor_type_text"]) {
        sheetFull.rotor_type_text = [sheetInfoResult objectForKey:@"rotor_type_text"];
    }
    if ([sheetInfoResult objectForKey:@"esc_type"]) {
        sheetFull.esc_type = [sheetInfoResult objectForKey:@"esc_type"];
    }
    if ([sheetInfoResult objectForKey:@"front_tyre"]) {
        sheetFull.front_tyre = [sheetInfoResult objectForKey:@"front_tyre"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheels"]) {
        sheetFull.front_wheels = [sheetInfoResult objectForKey:@"front_wheels"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tyre"]) {
        sheetFull.rear_tyre = [sheetInfoResult objectForKey:@"rear_tyre"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheels"]) {
        sheetFull.rear_wheels = [sheetInfoResult objectForKey:@"rear_wheels"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diff_oil"]) {
        sheetFull.rear_diff_oil = [sheetInfoResult objectForKey:@"rear_diff_oil"];
    }
    if ([sheetInfoResult objectForKey:@"field_rear_piston_2"]) {
        sheetFull.field_rear_piston_2 = [sheetInfoResult objectForKey:@"field_rear_piston_2"];
    }
    if ([sheetInfoResult objectForKey:@"field_front_piston_2"]) {
        sheetFull.field_front_piston_2 = [sheetInfoResult objectForKey:@"field_front_piston_2"];
    }
    
    
    
    if ([sheetInfoResult objectForKey:@"front_diff_type"]) {
        sheetFull.front_diff_type = [sheetInfoResult objectForKey:@"front_diff_type"];
    }
    if ([sheetInfoResult objectForKey:@"front_wjsj"]) {
        sheetFull.front_wjsj = [sheetInfoResult objectForKey:@"front_wjsj"];
    }
    if ([sheetInfoResult objectForKey:@"front_piece_serparate"]) {
        sheetFull.front_piece_serparate = [sheetInfoResult objectForKey:@"front_piece_serparate"];
    }
    if ([sheetInfoResult objectForKey:@"front_diff_position"]) {
        sheetFull.front_diff_position = [sheetInfoResult objectForKey:@"front_diff_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_position"]) {
        sheetFull.front_camber_position = [sheetInfoResult objectForKey:@"front_camber_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_hub_material"]) {
        sheetFull.front_hub_material = [sheetInfoResult objectForKey:@"front_hub_material"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheel_hub"]) {
        sheetFull.front_wheel_hub = [sheetInfoResult objectForKey:@"front_wheel_hub"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheel_hub"]) {
        sheetFull.front_wheel_hubs = [sheetInfoResult objectForKey:@"front_wheel_hub"];
    }
    
    if ([sheetInfoResult objectForKey:@"steering_block_material"]) {
        sheetFull.steering_block_material = [sheetInfoResult objectForKey:@"steering_block_material"];
    }
    if ([sheetInfoResult objectForKey:@"steering_block"]) {
        sheetFull.steering_block = [sheetInfoResult objectForKey:@"steering_block"];
    }
    if ([sheetInfoResult objectForKey:@"arm_material"]) {
        sheetFull.arm_material = [sheetInfoResult objectForKey:@"arm_material"];
    }
    if ([sheetInfoResult objectForKey:@"caster_angle"]) {
        sheetFull.caster_angle = [sheetInfoResult objectForKey:@"caster_angle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diff_type"]) {
        sheetFull.rear_diff_type = [sheetInfoResult objectForKey:@"rear_diff_type"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wjsj"]) {
        sheetFull.rear_wjsj = [sheetInfoResult objectForKey:@"rear_wjsj"];
    }
    if ([sheetInfoResult objectForKey:@"rear_piece_serparate"]) {
        sheetFull.rear_piece_serparate = [sheetInfoResult objectForKey:@"rear_piece_serparate"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_position"]) {
        sheetFull.rear_camber_position = [sheetInfoResult objectForKey:@"rear_camber_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_position"]) {
        sheetFull.rear_hub_position = [sheetInfoResult objectForKey:@"rear_hub_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheel_hub"]) {
        sheetFull.rear_wheel_hub = [sheetInfoResult objectForKey:@"rear_wheel_hub"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_material"]) {
        sheetFull.rear_hub_material = [sheetInfoResult objectForKey:@"rear_hub_material"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diff_position"]) {
        sheetFull.rear_diff_position = [sheetInfoResult objectForKey:@"rear_diff_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_material"]) {
        sheetFull.rear_arm_material = [sheetInfoResult objectForKey:@"rear_arm_material"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_length"]) {
        sheetFull.rear_arm_length = [sheetInfoResult objectForKey:@"rear_arm_length"];
    }
    if ([sheetInfoResult objectForKey:@"rear_caster_angle"]) {
        sheetFull.rear_caster_angle = [sheetInfoResult objectForKey:@"rear_caster_angle"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_ff"]) {
        sheetFull.front_spacer_ff = [sheetInfoResult objectForKey:@"front_spacer_ff"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_fr"]) {
        sheetFull.front_spacer_fr = [sheetInfoResult objectForKey:@"front_spacer_fr"];
    }
    if ([sheetInfoResult objectForKey:@"front_sus_block_ff"]) {
        sheetFull.front_sus_block_ff = [sheetInfoResult objectForKey:@"front_sus_block_ff"];
    }
    if ([sheetInfoResult objectForKey:@"front_sus_block_fr"]) {
        sheetFull.front_sus_block_fr = [sheetInfoResult objectForKey:@"front_sus_block_fr"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_position_1"]) {
        sheetFull.front_camber_position_1 = [sheetInfoResult objectForKey:@"front_camber_position_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_position_2"]) {
        sheetFull.front_camber_position_2 = [sheetInfoResult objectForKey:@"front_camber_position_2"];
    }
    if ([sheetInfoResult objectForKey:@"centerlink_washers"]) {
        sheetFull.centerlink_washers = [sheetInfoResult objectForKey:@"centerlink_washers"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_front_f"]) {
        sheetFull.wheelbase_front_f = [sheetInfoResult objectForKey:@"wheelbase_front_f"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_front_r"]) {
        sheetFull.wheelbase_front_r = [sheetInfoResult objectForKey:@"wheelbase_front_r"];
    }
    if ([sheetInfoResult objectForKey:@"caster_angle_text"]) {
        sheetFull.caster_angle_text = [sheetInfoResult objectForKey:@"caster_angle_text"];
    }
    if ([sheetInfoResult objectForKey:@"front_droop_pin"]) {
        sheetFull.front_droop_pin = [sheetInfoResult objectForKey:@"front_droop_pin"];
    }
    if ([sheetInfoResult objectForKey:@"front_droop_arm"]) {
        sheetFull.front_droop_arm = [sheetInfoResult objectForKey:@"front_droop_arm"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_caster_angle_text"]) {
        sheetFull.rear_caster_angle_text = [sheetInfoResult objectForKey:@"rear_caster_angle_text"];
    }
    if ([sheetInfoResult objectForKey:@"front_oring"]) {
        sheetFull.front_oring = [sheetInfoResult objectForKey:@"front_oring"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_spacer_rf"]) {
        sheetFull.rear_spacer_rf = [sheetInfoResult objectForKey:@"rear_spacer_rf"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_spacer_rr"]) {
        sheetFull.rear_spacer_rr = [sheetInfoResult objectForKey:@"rear_spacer_rr"];
    }
    if ([sheetInfoResult objectForKey:@"rear_sus_block_rf"]) {
        sheetFull.rear_sus_block_rf = [sheetInfoResult objectForKey:@"rear_sus_block_rf"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_sus_block_rr"]) {
        sheetFull.rear_sus_block_rr = [sheetInfoResult objectForKey:@"rear_sus_block_rr"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_camber_position_1"]) {
        sheetFull.rear_camber_position_1 = [sheetInfoResult objectForKey:@"rear_camber_position_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_camber_position_2"]) {
        sheetFull.rear_camber_position_2 = [sheetInfoResult objectForKey:@"rear_camber_position_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_position_1"]) {
        sheetFull.rear_hub_position_1 = [sheetInfoResult objectForKey:@"rear_hub_position_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub_position_2"]) {
        sheetFull.rear_hub_position_2 = [sheetInfoResult objectForKey:@"rear_hub_position_2"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_rear_f"]) {
        sheetFull.wheelbase_rear_f = [sheetInfoResult objectForKey:@"wheelbase_rear_f"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_rear_r"]) {
        sheetFull.wheelbase_rear_r = [sheetInfoResult objectForKey:@"wheelbase_rear_r"];
    }
    if ([sheetInfoResult objectForKey:@"rear_droop_pin"]) {
        sheetFull.rear_droop_pin = [sheetInfoResult objectForKey:@"rear_droop_pin"];
    }
    if ([sheetInfoResult objectForKey:@"rear_droop_arm"]) {
        sheetFull.rear_droop_arm = [sheetInfoResult objectForKey:@"rear_droop_arm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_oring"]) {
        sheetFull.rear_oring = [sheetInfoResult objectForKey:@"rear_oring"];
    }
    if ([sheetInfoResult objectForKey:@"spur_pitch"]) {
        sheetFull.spur_pitch = [sheetInfoResult objectForKey:@"spur_pitch"];
    }
    if ([sheetInfoResult objectForKey:@"gear_ratio"]) {
        sheetFull.gear_ratio = [sheetInfoResult objectForKey:@"gear_ratio"];
    }
    if ([sheetInfoResult objectForKey:@"centerlink_block"]) {
        sheetFull.centerlink_block = [sheetInfoResult objectForKey:@"centerlink_block"];
    }
    
    if ([sheetInfoResult objectForKey:@"ball_socket"]) {
        sheetFull.ball_socket = [sheetInfoResult objectForKey:@"ball_socket"];
    }
    if ([sheetInfoResult objectForKey:@"front_piston_holes"]) {
        sheetFull.front_piston_holes = [sheetInfoResult objectForKey:@"front_piston_holes"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_piston_holes"]) {
        sheetFull.rear_piston_holes = [sheetInfoResult objectForKey:@"rear_piston_holes"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_strap"]) {
        sheetFull.rear_strap = [sheetInfoResult objectForKey:@"rear_strap"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_position"]) {
        sheetFull.rear_shock_position = [sheetInfoResult objectForKey:@"rear_shock_position"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_wheelbase"]) {
        sheetFull.rear_wheelbase = [sheetInfoResult objectForKey:@"rear_wheelbase"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wheelbase"]) {
        sheetFull.rear_wheelbases = [sheetInfoResult objectForKey:@"rear_wheelbase"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_hub"]) {
        sheetFull.rear_hub = [sheetInfoResult objectForKey:@"rear_hub"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_alloy_hub"]) {
        sheetFull.rear_alloy_hub = [sheetInfoResult objectForKey:@"rear_alloy_hub"];
    }
    if ([sheetInfoResult objectForKey:@"rear_mount"]) {
        sheetFull.rear_mount = [sheetInfoResult objectForKey:@"rear_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_steering_mount"]) {
        sheetFull.front_steering_mount = [sheetInfoResult objectForKey:@"front_steering_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_yoke"]) {
        sheetFull.front_yoke = [sheetInfoResult objectForKey:@"front_yoke"];
    }
    if ([sheetInfoResult objectForKey:@"front_mount"]) {
        sheetFull.front_mount = [sheetInfoResult objectForKey:@"front_mount"];
    }
    if ([sheetInfoResult objectForKey:@"ext_gears"]) {
        sheetFull.ext_gears = [sheetInfoResult objectForKey:@"ext_gears"];
    }
    if ([sheetInfoResult objectForKey:@"diff_height"]) {
        sheetFull.diff_height = [sheetInfoResult objectForKey:@"diff_height"];
    }
    if ([sheetInfoResult objectForKey:@"topdeck_link"]) {
        sheetFull.topdeck_link = [sheetInfoResult objectForKey:@"topdeck_link"];
    }
    if ([sheetInfoResult objectForKey:@"rear_antisquat"]) {
        sheetFull.rear_antisquat = [sheetInfoResult objectForKey:@"rear_antisquat"];
    }
    if ([sheetInfoResult objectForKey:@"rear_linkwasher2"]) {
        sheetFull.rear_linkwasher2 = [sheetInfoResult objectForKey:@"rear_linkwasher2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_linkwasher1"]) {
        sheetFull.rear_linkwasher1 = [sheetInfoResult objectForKey:@"rear_linkwasher1"];
    }
    if ([sheetInfoResult objectForKey:@"front_linkwasher1"]) {
        sheetFull.front_linkwasher1 = [sheetInfoResult objectForKey:@"front_linkwasher1"];
    }
    if ([sheetInfoResult objectForKey:@"servo"]) {
        sheetFull.servo = [sheetInfoResult objectForKey:@"servo"];
    }
    if ([sheetInfoResult objectForKey:@"reciever"]) {
        sheetFull.reciever = [sheetInfoResult objectForKey:@"reciever"];
    }
    if ([sheetInfoResult objectForKey:@"weight_added"]) {
        sheetFull.weight_added = [sheetInfoResult objectForKey:@"weight_added"];
    }
    if ([sheetInfoResult objectForKey:@"cell_layout"]) {
        sheetFull.cell_layout = [sheetInfoResult objectForKey:@"cell_layout"];
    }
    if ([sheetInfoResult objectForKey:@"additional_washers"]) {
        sheetFull.additional_washers = [sheetInfoResult objectForKey:@"additional_washers"];
    }
    if ([sheetInfoResult objectForKey:@"notes"]) {
        sheetFull.notes = [sheetInfoResult objectForKey:@"notes"];
    }
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"front_anti_rollbar"]) {
        sheetFull.front_anti_rollbar = [sheetInfoResult objectForKey:@"front_anti_rollbar"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_dink_mount"]) {
        sheetFull.front_dink_mount = [sheetInfoResult objectForKey:@"front_dink_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_blocks"]) {
        sheetFull.front_blocks = [sheetInfoResult objectForKey:@"front_blocks"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_rake"]) {
        sheetFull.front_rake = [sheetInfoResult objectForKey:@"front_rake"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_drilled"]) {
        sheetFull.front_drilled = [sheetInfoResult objectForKey:@"front_drilled"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_cap"]) {
        sheetFull.front_shock_cap = [sheetInfoResult objectForKey:@"front_shock_cap"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_shock_cap"]) {
        sheetFull.rear_shock_cap = [sheetInfoResult objectForKey:@"rear_shock_cap"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_drilled"]) {
        sheetFull.rear_drilled = [sheetInfoResult objectForKey:@"rear_drilled"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_blocks"]) {
        sheetFull.rear_blocks = [sheetInfoResult objectForKey:@"rear_blocks"];
    }
    if ([sheetInfoResult objectForKey:@"rear_dink_mount"]) {
        sheetFull.rear_dink_mount = [sheetInfoResult objectForKey:@"rear_dink_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_anti_squat"]) {
        sheetFull.rear_anti_squat = [sheetInfoResult objectForKey:@"rear_anti_squat"];
    }
    if ([sheetInfoResult objectForKey:@"rear_pin_high"]) {
        sheetFull.rear_pin_high = [sheetInfoResult objectForKey:@"rear_pin_high"];
    }
    if ([sheetInfoResult objectForKey:@"rear_pin_low"]) {
        sheetFull.rear_pin_low = [sheetInfoResult objectForKey:@"rear_pin_low"];
    }
    
    if ([sheetInfoResult objectForKey:@"cell_position"]) {
        sheetFull.cell_position = [sheetInfoResult objectForKey:@"cell_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_inner_link_washers"]) {
        sheetFull.front_inner_link_washers = [sheetInfoResult objectForKey:@"front_inner_link_washers"];
    }
    if ([sheetInfoResult objectForKey:@"front_outter_link_washers"]) {
        sheetFull.front_outter_link_washers = [sheetInfoResult objectForKey:@"front_outter_link_washers"];
    }
    if ([sheetInfoResult objectForKey:@"front_track_washers"]) {
        sheetFull.front_track_washers = [sheetInfoResult objectForKey:@"front_track_washers"];
    }
    if ([sheetInfoResult objectForKey:@"front_hole_size"]) {
        sheetFull.front_hole_size = [sheetInfoResult objectForKey:@"front_hole_size"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hole_size"]) {
        sheetFull.rear_hole_size = [sheetInfoResult objectForKey:@"rear_hole_size"];
    }
    if ([sheetInfoResult objectForKey:@"rear_track_washers_1"]) {
        sheetFull.rear_track_washers_1 = [sheetInfoResult objectForKey:@"rear_track_washers_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_track_washers_2"]) {
        sheetFull.rear_track_washers_2 = [sheetInfoResult objectForKey:@"rear_track_washers_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_antisquat_washers1"]) {
        sheetFull.rear_antisquat_washers1 = [sheetInfoResult objectForKey:@"rear_antisquat_washers1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_antisquat_washers2"]) {
        sheetFull.rear_antisquat_washers2 = [sheetInfoResult objectForKey:@"rear_antisquat_washers2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_link_washers"]) {
        sheetFull.rear_link_washers = [sheetInfoResult objectForKey:@"rear_link_washers"];
    }
    if ([sheetInfoResult objectForKey:@"car_weight"]) {
        sheetFull.car_weight = [sheetInfoResult objectForKey:@"car_weight"];
    }
    
    
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"rear_groove"]) {
        sheetFull.rear_groove = [sheetInfoResult objectForKey:@"rear_groove"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_pip"]) {
        sheetFull.rear_pip = [sheetInfoResult objectForKey:@"rear_pip"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shaft"]) {
        sheetFull.rear_shaft = [sheetInfoResult objectForKey:@"rear_shaft"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_block"]) {
        sheetFull.rear_block = [sheetInfoResult objectForKey:@"rear_block"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_underball"]) {
        sheetFull.rear_underball = [sheetInfoResult objectForKey:@"rear_underball"];
    }
    if ([sheetInfoResult objectForKey:@"rear_underbracket"]) {
        sheetFull.rear_underbracket = [sheetInfoResult objectForKey:@"rear_underbracket"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_groove"]) {
        sheetFull.front_groove = [sheetInfoResult objectForKey:@"front_groove"];
    }
    
    if ([sheetInfoResult objectForKey:@"servo_saver"]) {
        sheetFull.servo_saver = [sheetInfoResult objectForKey:@"servo_saver"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_shaft"]) {
        sheetFull.front_shaft = [sheetInfoResult objectForKey:@"front_shaft"];
    }
    if ([sheetInfoResult objectForKey:@"steering_mount"]) {
        sheetFull.steering_mount = [sheetInfoResult objectForKey:@"steering_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_hubmount"]) {
        sheetFull.front_hubmount = [sheetInfoResult objectForKey:@"front_hubmount"];
    }
    if ([sheetInfoResult objectForKey:@"front_underball"]) {
        sheetFull.front_underball = [sheetInfoResult objectForKey:@"front_underball"];
    }
    if ([sheetInfoResult objectForKey:@"front_underbracket"]) {
        sheetFull.front_underbracket = [sheetInfoResult objectForKey:@"front_underbracket"];
    }
    if ([sheetInfoResult objectForKey:@"front_block"]) {
        sheetFull.front_block = [sheetInfoResult objectForKey:@"front_block"];
    }
    if ([sheetInfoResult objectForKey:@"gear_pitch"]) {
        sheetFull.gear_pitch = [sheetInfoResult objectForKey:@"gear_pitch"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"rear_hubwasher"]) {
        sheetFull.rear_hubwasher = [sheetInfoResult objectForKey:@"rear_hubwasher"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hubgroove"]) {
        sheetFull.rear_hubgroove = [sheetInfoResult objectForKey:@"rear_hubgroove"];
    }
    if ([sheetInfoResult objectForKey:@"rear_studwasher"]) {
        sheetFull.rear_studwasher = [sheetInfoResult objectForKey:@"rear_studwasher"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ride_heght"]) {
        sheetFull.rear_ride_heght = [sheetInfoResult objectForKey:@"rear_ride_heght"];
    }
    if ([sheetInfoResult objectForKey:@"rear_washer2"]) {
        sheetFull.rear_washer2 = [sheetInfoResult objectForKey:@"rear_washer2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_washer3"]) {
        sheetFull.rear_washer3 = [sheetInfoResult objectForKey:@"rear_washer3"];
    }
    if ([sheetInfoResult objectForKey:@"front_hubwasher"]) {
        sheetFull.front_hubwasher = [sheetInfoResult objectForKey:@"front_hubwasher"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer1"]) {
        sheetFull.front_washer1 = [sheetInfoResult objectForKey:@"front_washer1"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer2"]) {
        sheetFull.front_washer2 = [sheetInfoResult objectForKey:@"front_washer2"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_washer3"]) {
        sheetFull.front_washer3 = [sheetInfoResult objectForKey:@"front_washer3"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer4"]) {
        sheetFull.front_washer4 = [sheetInfoResult objectForKey:@"front_washer4"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer5"]) {
        sheetFull.front_washer5 = [sheetInfoResult objectForKey:@"front_washer5"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer6"]) {
        sheetFull.front_washer6 = [sheetInfoResult objectForKey:@"front_washer6"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_ride_heght"]) {
        sheetFull.front_ride_heght = [sheetInfoResult objectForKey:@"front_ride_heght"];
    }
    if ([sheetInfoResult objectForKey:@"front_toein"]) {
        sheetFull.front_toein = [sheetInfoResult objectForKey:@"front_toein"];
    }
    if ([sheetInfoResult objectForKey:@"front_toeout"]) {
        sheetFull.front_toeout = [sheetInfoResult objectForKey:@"front_toeout"];
    }
    if ([sheetInfoResult objectForKey:@"chassis"]) {
        sheetFull.chassis = [sheetInfoResult objectForKey:@"chassis"];
    }
    if ([sheetInfoResult objectForKey:@"chassis"]) {
        sheetFull.chassi = [sheetInfoResult objectForKey:@"chassis"];
    }
    
    if ([sheetInfoResult objectForKey:@"topdeck"]) {
        sheetFull.topdeck = [sheetInfoResult objectForKey:@"topdeck"];
    }
    if ([sheetInfoResult objectForKey:@"topdeck"]) {
        sheetFull.topdecks = [sheetInfoResult objectForKey:@"topdeck"];
    }
    
    if ([sheetInfoResult objectForKey:@"cells"]) {
        sheetFull.cells = [sheetInfoResult objectForKey:@"cells"];
    }
    if ([sheetInfoResult objectForKey:@"rotor"]) {
        sheetFull.rotor = [sheetInfoResult objectForKey:@"rotor"];
    }
    if ([sheetInfoResult objectForKey:@"timing"]) {
        sheetFull.timing = [sheetInfoResult objectForKey:@"timing"];
    }
    if ([sheetInfoResult objectForKey:@"wheels"]) {
        sheetFull.wheels = [sheetInfoResult objectForKey:@"wheels"];
    }
    if ([sheetInfoResult objectForKey:@"tyre"]) {
        sheetFull.tyre = [sheetInfoResult objectForKey:@"tyre"];
    }
    if ([sheetInfoResult objectForKey:@"insert"]) {
        sheetFull.insert = [sheetInfoResult objectForKey:@"insert"];
    }
    if ([sheetInfoResult objectForKey:@"tyre_additive"]) {
        sheetFull.tyre_additive = [sheetInfoResult objectForKey:@"tyre_additive"];
    }
    if ([sheetInfoResult objectForKey:@"front_time"]) {
        sheetFull.front_time = [sheetInfoResult objectForKey:@"front_time"];
    }
    if ([sheetInfoResult objectForKey:@"rear_time"]) {
        sheetFull.rear_time = [sheetInfoResult objectForKey:@"rear_time"];
    }
    if ([sheetInfoResult objectForKey:@"warmers"]) {
        sheetFull.warmers = [sheetInfoResult objectForKey:@"warmers"];
    }
    if ([sheetInfoResult objectForKey:@"tempature"]) {
        sheetFull.tempature = [sheetInfoResult objectForKey:@"tempature"];
    }
    if ([sheetInfoResult objectForKey:@"time"]) {
        sheetFull.time = [sheetInfoResult objectForKey:@"time"];
    }
    
    
    
    
    
    
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"front_suspension_weight_placement"]) {
        sheetFull.front_suspension_weight_placement = [sheetInfoResult objectForKey:@"front_suspension_weight_placement"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_ball_stud"]) {
        sheetFull.front_suspension_ball_stud = [sheetInfoResult objectForKey:@"front_suspension_ball_stud"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_spacer"]) {
        sheetFull.front_suspension_axle_spacer = [sheetInfoResult objectForKey:@"front_suspension_axle_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_spacer"]) {
        sheetFull.front_suspension_spacer = [sheetInfoResult objectForKey:@"front_suspension_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_ball_stud_1"]) {
        sheetFull.front_suspension_ball_stud_1 = [sheetInfoResult objectForKey:@"front_suspension_ball_stud_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_caster_block_position"]) {
        sheetFull.front_suspension_caster_block_position = [sheetInfoResult objectForKey:@"front_suspension_caster_block_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_ball_stud_la246"]) {
        sheetFull.front_suspension_ball_stud_la246 = [sheetInfoResult objectForKey:@"front_suspension_ball_stud_la246"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer"]) {
        sheetFull.front_suspension_axle_height_spacer = [sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_ball_stud"]) {
        sheetFull.rear_suspension_ball_stud = [sheetInfoResult objectForKey:@"rear_suspension_ball_stud"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_sus_holder_spacer_f"]) {
        sheetFull.rear_suspension_sus_holder_spacer_f = [sheetInfoResult objectForKey:@"rear_suspension_sus_holder_spacer_f"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_sus_holder_spacer_r"]) {
        sheetFull.rear_suspension_sus_holder_spacer_r = [sheetInfoResult objectForKey:@"rear_suspension_sus_holder_spacer_r"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_axle_spacer"]) {
        sheetFull.rear_suspension_axle_spacer = [sheetInfoResult objectForKey:@"rear_suspension_axle_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder_normal"]) {
        sheetFull.rear_suspension_lower_sus_holder_normal = [sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder_normal"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_rear_toe"]) {
        sheetFull.rear_suspension_rear_toe = [sheetInfoResult objectForKey:@"rear_suspension_rear_toe"];
    }
    if ([sheetInfoResult objectForKey:@"diff"]) {
        sheetFull.diff = [sheetInfoResult objectForKey:@"diff"];
    }
    if ([sheetInfoResult objectForKey:@"shock_ball_end_type_front"]) {
        sheetFull.shock_ball_end_type_front = [sheetInfoResult objectForKey:@"shock_ball_end_type_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_ball_end_type_rear"]) {
        sheetFull.shock_ball_end_type_rear = [sheetInfoResult objectForKey:@"shock_ball_end_type_rear"];
    }
    if ([sheetInfoResult objectForKey:@"truck_condition"]) {
        sheetFull.truck_condition = [sheetInfoResult objectForKey:@"truck_condition"];
    }
    if ([sheetInfoResult objectForKey:@"wing_mount"]) {
        sheetFull.wing_mount = [sheetInfoResult objectForKey:@"wing_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"wing_angle"]) {
        sheetFull.wing_angle = [sheetInfoResult objectForKey:@"wing_angle"];
    }
    if ([sheetInfoResult objectForKey:@"battery_placement_foam"]) {
        sheetFull.battery_placement_foam = [sheetInfoResult objectForKey:@"battery_placement_foam"];
    }
    if ([sheetInfoResult objectForKey:@"battery_type"]) {
        sheetFull.battery_type = [sheetInfoResult objectForKey:@"battery_type"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder_alumi"]) {
        sheetFull.rear_suspension_lower_sus_holder_alumi = [sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder_alumi"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_image_1"]) {
        sheetFull.front_suspension_image_1 = [sheetInfoResult objectForKey:@"front_suspension_image_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_image_2"]) {
        sheetFull.front_suspension_image_2 = [sheetInfoResult objectForKey:@"front_suspension_image_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_image_3"]) {
        sheetFull.front_suspension_image_3 = [sheetInfoResult objectForKey:@"front_suspension_image_3"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_image_1"]) {
        sheetFull.rear_suspension_image_1 = [sheetInfoResult objectForKey:@"rear_suspension_image_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_rr"]) {
        sheetFull.rear_suspension_rr = [sheetInfoResult objectForKey:@"rear_suspension_rr"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_mid"]) {
        sheetFull.rear_suspension_mid = [sheetInfoResult objectForKey:@"rear_suspension_mid"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_image_3"]) {
        sheetFull.rear_suspension_image_3 = [sheetInfoResult objectForKey:@"rear_suspension_image_3"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_toe_text"]) {
        sheetFull.front_suspension_toe_text = [sheetInfoResult objectForKey:@"front_suspension_toe_text"];
    }
    
    
    
    
    if ([sheetInfoResult objectForKey:@"front_suspension_tie_rod"]) {
        sheetFull.front_suspension_tie_rod = [sheetInfoResult objectForKey:@"front_suspension_tie_rod"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_weight_placement_bulk_inside_1"]) {
        sheetFull.front_suspension_weight_placement_bulk_inside_1 = [sheetInfoResult objectForKey:@"front_suspension_weight_placement_bulk_inside_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_weight_placement_bulk_inside_2"]) {
        sheetFull.front_suspension_weight_placement_bulk_inside_2 = [sheetInfoResult objectForKey:@"front_suspension_weight_placement_bulk_inside_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_weight_placement_skid_plate_1"]) {
        sheetFull.front_suspension_weight_placement_skid_plate_1 = [sheetInfoResult objectForKey:@"front_suspension_weight_placement_skid_plate_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_weight_placement_skid_plate_2"]) {
        sheetFull.front_suspension_weight_placement_skid_plate_2 = [sheetInfoResult objectForKey:@"front_suspension_weight_placement_skid_plate_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_upper_spacer"]) {
        sheetFull.front_suspension_upper_spacer = [sheetInfoResult objectForKey:@"front_suspension_upper_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_spacer_out"] && ((NSNull *)[sheetInfoResult objectForKey:@"front_suspension_axle_spacer_out"]
                                                                               != [NSNull null])) {
        sheetFull.front_suspension_axle_spacer_out = [sheetInfoResult objectForKey:@"front_suspension_axle_spacer_out"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_spacer_in"] && ((NSNull *)[sheetInfoResult objectForKey:@"front_suspension_axle_spacer_in"] != [NSNull null])) {
        sheetFull.front_suspension_axle_spacer_in = [sheetInfoResult objectForKey:@"front_suspension_axle_spacer_in"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_wheel_hub"]) {
        sheetFull.front_suspension_wheel_hub = [sheetInfoResult objectForKey:@"front_suspension_wheel_hub"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_suspension_ball_stud_text"]) {
        sheetFull.front_suspension_ball_stud_text = [sheetInfoResult objectForKey:@"front_suspension_ball_stud_text"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_ackerman_steer_spacer"]) {
        sheetFull.front_suspension_ackerman_steer_spacer = [sheetInfoResult objectForKey:@"front_suspension_ackerman_steer_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer_text_1"]) {
        sheetFull.front_suspension_axle_height_spacer_text_1 = [sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer_text_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer_text_2"]) {
        sheetFull.front_suspension_axle_height_spacer_text_2 = [sheetInfoResult objectForKey:@"front_suspension_axle_height_spacer_text_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_bump_steer_spacer"]) {
        sheetFull.front_suspension_bump_steer_spacer = [sheetInfoResult objectForKey:@"front_suspension_bump_steer_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_wheelbase_f"] && ((NSNull *)[sheetInfoResult objectForKey:@"front_suspension_wheelbase_f"] != [NSNull null])) {
        sheetFull.front_suspension_wheelbase_f = [sheetInfoResult objectForKey:@"front_suspension_wheelbase_f"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_wheelbase_r"]) {
        sheetFull.front_suspension_wheelbase_r = [sheetInfoResult objectForKey:@"front_suspension_wheelbase_r"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_comment"]) {
        sheetFull.front_suspension_comment = [sheetInfoResult objectForKey:@"front_suspension_comment"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_upper_spacer"]) {
        sheetFull.rear_suspension_upper_spacer = [sheetInfoResult objectForKey:@"rear_suspension_upper_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_sway_bar_text"]) {
        sheetFull.rear_suspension_sway_bar_text = [sheetInfoResult objectForKey:@"rear_suspension_sway_bar_text"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_universal"]) {
        sheetFull.rear_suspension_universal = [sheetInfoResult objectForKey:@"rear_suspension_universal"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_swing_shaft"]) {
        sheetFull.rear_suspension_swing_shaft = [sheetInfoResult objectForKey:@"rear_suspension_swing_shaft"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_axle_spacer_out"]) {
        sheetFull.rear_suspension_axle_spacer_out = [sheetInfoResult objectForKey:@"rear_suspension_axle_spacer_out"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_axle_spacer_in"]) {
        sheetFull.rear_suspension_axle_spacer_in = [sheetInfoResult objectForKey:@"rear_suspension_axle_spacer_in"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_comment"]) {
        sheetFull.rear_suspension_comment = [sheetInfoResult objectForKey:@"rear_suspension_comment"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_upper_spacer_2"]) {
        sheetFull.rear_suspension_upper_spacer_2 = [sheetInfoResult objectForKey:@"rear_suspension_upper_spacer_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_1_f"]) {
        sheetFull.rear_suspension_wheelbase_1_f = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_1_f"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_1_r"]) {
        sheetFull.rear_suspension_wheelbase_1_r = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_1_r"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_2_f"]) {
        sheetFull.rear_suspension_wheelbase_2_f = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_2_f"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_2_r"]) {
        sheetFull.rear_suspension_wheelbase_2_r = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_2_r"];
    }
    if ([sheetInfoResult objectForKey:@"diff_gear"]) {
        sheetFull.diff_gear = [sheetInfoResult objectForKey:@"diff_gear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_piston_front"]) {
        sheetFull.shock_shock_piston_front = [sheetInfoResult objectForKey:@"shock_shock_piston_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_piston_rear"]) {
        sheetFull.shock_shock_piston_rear = [sheetInfoResult objectForKey:@"shock_shock_piston_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_oil_front"]) {
        sheetFull.shock_shock_oil_front = [sheetInfoResult objectForKey:@"shock_shock_oil_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_oil_rear"]) {
        sheetFull.shock_shock_oil_rear = [sheetInfoResult objectForKey:@"shock_shock_oil_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_spring_front"]) {
        sheetFull.shock_shock_spring_front = [sheetInfoResult objectForKey:@"shock_shock_spring_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_spring_rear"]) {
        sheetFull.shock_shock_spring_rear = [sheetInfoResult objectForKey:@"shock_shock_spring_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_limiter_front_in"]) {
        sheetFull.shock_limiter_front_in = [sheetInfoResult objectForKey:@"shock_limiter_front_in"];
    }
    if ([sheetInfoResult objectForKey:@"shock_limiter_front_out"]) {
        sheetFull.shock_limiter_front_out = [sheetInfoResult objectForKey:@"shock_limiter_front_out"];
    }
    if ([sheetInfoResult objectForKey:@"shock_limiter_rear_in"]) {
        sheetFull.shock_limiter_rear_in = [sheetInfoResult objectForKey:@"shock_limiter_rear_in"];
    }
    if ([sheetInfoResult objectForKey:@"shock_limiter_rear_out"]) {
        sheetFull.shock_limiter_rear_out = [sheetInfoResult objectForKey:@"shock_limiter_rear_out"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_length_a_front"]) {
        sheetFull.shock_shock_length_a_front = [sheetInfoResult objectForKey:@"shock_shock_length_a_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_length_a_rear"]) {
        sheetFull.shock_shock_length_a_rear = [sheetInfoResult objectForKey:@"shock_shock_length_a_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_length_b_front"]) {
        sheetFull.shock_shock_length_b_front = [sheetInfoResult objectForKey:@"shock_shock_length_b_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_shock_length_b_rear"]) {
        sheetFull.shock_shock_length_b_rear = [sheetInfoResult objectForKey:@"shock_shock_length_b_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_o_ring_front"]) {
        sheetFull.shock_o_ring_front = [sheetInfoResult objectForKey:@"shock_o_ring_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_o_ring_rear"]) {
        sheetFull.shock_o_ring_rear = [sheetInfoResult objectForKey:@"shock_o_ring_rear"];
    }
    if ([sheetInfoResult objectForKey:@"wing_lip"]) {
        sheetFull.wing_lip = [sheetInfoResult objectForKey:@"wing_lip"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_ride_height_f"]) {
        sheetFull.chassis_ride_height_f = [sheetInfoResult objectForKey:@"chassis_ride_height_f"];
    }
    if ([sheetInfoResult objectForKey:@"chassis_ride_height_r"]) {
        sheetFull.chassis_ride_height_r = [sheetInfoResult objectForKey:@"chassis_ride_height_r"];
    }
    
    if ([sheetInfoResult objectForKey:@"battery_type_type"]) {
        sheetFull.battery_type_type = [sheetInfoResult objectForKey:@"battery_type_type"];
    }
    if ([sheetInfoResult objectForKey:@"other_motor"]) {
        sheetFull.other_motor = [sheetInfoResult objectForKey:@"other_motor"];
    }
    
    if ([sheetInfoResult objectForKey:@"other_esc"]) {
        sheetFull.other_esc = [sheetInfoResult objectForKey:@"other_esc"];
    }
    if ([sheetInfoResult objectForKey:@"other_pinion"]) {
        sheetFull.other_pinion = [sheetInfoResult objectForKey:@"other_pinion"];
    }
    if ([sheetInfoResult objectForKey:@"tire_front"]) {
        sheetFull.tire_front = [sheetInfoResult objectForKey:@"tire_front"];
    }
    if ([sheetInfoResult objectForKey:@"tire_rear"]) {
        sheetFull.tire_rear = [sheetInfoResult objectForKey:@"tire_rear"];
    }
    
    if ([sheetInfoResult objectForKey:@"tire_insert_front"]) {
        sheetFull.tire_insert_front = [sheetInfoResult objectForKey:@"tire_insert_front"];
    }
    if ([sheetInfoResult objectForKey:@"tire_insert_rear"]) {
        sheetFull.tire_insert_rear = [sheetInfoResult objectForKey:@"tire_insert_rear"];
    }
    if ([sheetInfoResult objectForKey:@"tire_wheel_front"]) {
        sheetFull.tire_wheel_front = [sheetInfoResult objectForKey:@"tire_wheel_front"];
    }
    if ([sheetInfoResult objectForKey:@"tire_wheel_rear"]) {
        sheetFull.tire_wheel_rear = [sheetInfoResult objectForKey:@"tire_wheel_rear"];
    }
    if ([sheetInfoResult objectForKey:@"tire_wheel_traction"]) {
        sheetFull.tire_wheel_traction = [sheetInfoResult objectForKey:@"tire_wheel_traction"];
    }
    
    
    
    
    
    
    
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"skid_angle_upper_front"]) {
        sheetFull.skid_angle_upper_front = [sheetInfoResult objectForKey:@"skid_angle_upper_front"];
    }
    if ([sheetInfoResult objectForKey:@"skid_angle_upper_rear"]) {
        sheetFull.skid_angle_upper_rear = [sheetInfoResult objectForKey:@"skid_angle_upper_rear"];
    }
    if ([sheetInfoResult objectForKey:@"skid_angle_under_front"]) {
        sheetFull.skid_angle_under_front = [sheetInfoResult objectForKey:@"skid_angle_under_front"];
    }
    if ([sheetInfoResult objectForKey:@"upper_arm_left_position"]) {
        sheetFull.upper_arm_left_position = [sheetInfoResult objectForKey:@"upper_arm_left_position"];
    }
    if ([sheetInfoResult objectForKey:@"shock_stay_front"]) {
        sheetFull.shock_stay_front = [sheetInfoResult objectForKey:@"shock_stay_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_stay_rear"]) {
        sheetFull.shock_stay_rear = [sheetInfoResult objectForKey:@"shock_stay_rear"];
    }
    if ([sheetInfoResult objectForKey:@"sus_arm_front"]) {
        sheetFull.sus_arm_front = [sheetInfoResult objectForKey:@"sus_arm_front"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_stay"]) {
        sheetFull.rear_shock_stay = [sheetInfoResult objectForKey:@"rear_shock_stay"];
    }
    if ([sheetInfoResult objectForKey:@"diff_gear_oil_front"]) {
        sheetFull.diff_gear_oil_front = [sheetInfoResult objectForKey:@"diff_gear_oil_front"];
    }
    
    if ([sheetInfoResult objectForKey:@"diff_gear_oil_center"]) {
        sheetFull.diff_gear_oil_center = [sheetInfoResult objectForKey:@"diff_gear_oil_center"];
    }
    if ([sheetInfoResult objectForKey:@"diff_gear_oil_rear"]) {
        sheetFull.diff_gear_oil_rear = [sheetInfoResult objectForKey:@"diff_gear_oil_rear"];
    }
    if ([sheetInfoResult objectForKey:@"toe_angle"]) {
        sheetFull.toe_angle = [sheetInfoResult objectForKey:@"toe_angle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_roll_center"]) {
        sheetFull.rear_roll_center = [sheetInfoResult objectForKey:@"rear_roll_center"];
    }
    if ([sheetInfoResult objectForKey:@"rear_roll_centerL"]) {
        sheetFull.rear_roll_centerL = [sheetInfoResult objectForKey:@"rear_roll_centerL"];
    }
    if ([sheetInfoResult objectForKey:@"upper_arm_rear_shock"]) {
        sheetFull.upper_arm_rear_shock = [sheetInfoResult objectForKey:@"upper_arm_rear_shock"];
    }
    if ([sheetInfoResult objectForKey:@"upper_arm_rear_wheelside"]) {
        sheetFull.upper_arm_rear_wheelside = [sheetInfoResult objectForKey:@"upper_arm_rear_wheelside"];
    }
    if ([sheetInfoResult objectForKey:@"sterring_plate_option"]) {
        sheetFull.sterring_plate_option = [sheetInfoResult objectForKey:@"sterring_plate_option"];
    }
    if ([sheetInfoResult objectForKey:@"sus_arm_real"]) {
        sheetFull.sus_arm_real = [sheetInfoResult objectForKey:@"sus_arm_real"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_tread_front_side"]) {
        sheetFull.rear_tread_front_side = [sheetInfoResult objectForKey:@"rear_tread_front_side"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tread_rear_side"]) {
        sheetFull.rear_tread_rear_side = [sheetInfoResult objectForKey:@"rear_tread_rear_side"];
    }
    if ([sheetInfoResult objectForKey:@"wing_angle_right"]) {
        sheetFull.wing_angle_right = [sheetInfoResult objectForKey:@"wing_angle_right"];
    }
    if ([sheetInfoResult objectForKey:@"wheel_hub_right"]) {
        sheetFull.wheel_hub_right = [sheetInfoResult objectForKey:@"wheel_hub_right"];
    }
    if ([sheetInfoResult objectForKey:@"sus_arm_rear"]) {
        sheetFull.sus_arm_rear = [sheetInfoResult objectForKey:@"sus_arm_rear"];
    }
    
    if ([sheetInfoResult objectForKey:@"upper_arm_position"]) {
        sheetFull.upper_arm_position = [sheetInfoResult objectForKey:@"upper_arm_position"];
    }
    if ([sheetInfoResult objectForKey:@"upper_arm_position_right"]) {
        sheetFull.upper_arm_position_right = [sheetInfoResult objectForKey:@"upper_arm_position_right"];
    }
    if ([sheetInfoResult objectForKey:@"sterring_plate_radio"]) {
        sheetFull.sterring_plate_radio = [sheetInfoResult objectForKey:@"sterring_plate_radio"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t_big1"]) {
        sheetFull.spur_t_big1 = [sheetInfoResult objectForKey:@"spur_t_big1"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t_big2"]) {
        sheetFull.spur_t_big2 = [sheetInfoResult objectForKey:@"spur_t_big2"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t"]) {
        sheetFull.spur_t = [sheetInfoResult objectForKey:@"spur_t"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t2"]) {
        sheetFull.spur_t2 = [sheetInfoResult objectForKey:@"spur_t2"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t3"]) {
        sheetFull.spur_t3 = [sheetInfoResult objectForKey:@"spur_t3"];
    }
    if ([sheetInfoResult objectForKey:@"spur_t4"]) {
        sheetFull.spur_t4 = [sheetInfoResult objectForKey:@"spur_t4"];
    }
    if ([sheetInfoResult objectForKey:@"spur_carbon"]) {
        sheetFull.spur_carbon = [sheetInfoResult objectForKey:@"spur_carbon"];
    }
    if ([sheetInfoResult objectForKey:@"spur_aluminum"]) {
        sheetFull.spur_aluminum = [sheetInfoResult objectForKey:@"spur_aluminum"];
    }
    if ([sheetInfoResult objectForKey:@"tires"]) {
        sheetFull.tires = [sheetInfoResult objectForKey:@"tires"];
    }
    if ([sheetInfoResult objectForKey:@"compounds"]) {
        sheetFull.compounds = [sheetInfoResult objectForKey:@"compounds"];
    }
    if ([sheetInfoResult objectForKey:@"inners"]) {
        sheetFull.inners = [sheetInfoResult objectForKey:@"inners"];
    }
    if ([sheetInfoResult objectForKey:@"wheel"]) {
        sheetFull.wheel = [sheetInfoResult objectForKey:@"wheel"];
    }
    if ([sheetInfoResult objectForKey:@"body_top"]) {
        sheetFull.body_top = [sheetInfoResult objectForKey:@"body_top"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_type"]) {
        sheetFull.rear_shock_type = [sheetInfoResult objectForKey:@"rear_shock_type"];
    }
    if ([sheetInfoResult objectForKey:@"shock_setup_oil"]) {
        sheetFull.shock_setup_oil = [sheetInfoResult objectForKey:@"shock_setup_oil"];
    }
    
    if ([sheetInfoResult objectForKey:@"shock_setup_oil_rear"]) {
        sheetFull.shock_setup_oil_rear = [sheetInfoResult objectForKey:@"shock_setup_oil_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_front_pressure_sponge"]) {
        sheetFull.shock_front_pressure_sponge = [sheetInfoResult objectForKey:@"shock_front_pressure_sponge"];
    }
    if ([sheetInfoResult objectForKey:@"shock_rear_pressure_sponge"]) {
        sheetFull.shock_rear_pressure_sponge = [sheetInfoResult objectForKey:@"shock_rear_pressure_sponge"];
    }
    if ([sheetInfoResult objectForKey:@"shock_rideheight_front"]) {
        sheetFull.shock_rideheight_front = [sheetInfoResult objectForKey:@"shock_rideheight_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_rideheight_rear"]) {
        sheetFull.shock_rideheight_rear = [sheetInfoResult objectForKey:@"shock_rideheight_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_rebound_front"]) {
        sheetFull.shock_rebound_front = [sheetInfoResult objectForKey:@"shock_rebound_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_rebound_rear"]) {
        sheetFull.shock_rebound_rear = [sheetInfoResult objectForKey:@"shock_rebound_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_camberangle_front"]) {
        sheetFull.shock_camberangle_front = [sheetInfoResult objectForKey:@"shock_camberangle_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_setup_spring"]) {
        sheetFull.shock_setup_spring = [sheetInfoResult objectForKey:@"shock_setup_spring"];
    }
    if ([sheetInfoResult objectForKey:@"shock_setup_spring_rear"]) {
        sheetFull.shock_setup_spring_rear = [sheetInfoResult objectForKey:@"shock_setup_spring_rear"];
    }
    if ([sheetInfoResult objectForKey:@"shock_camberangle_rear"]) {
        sheetFull.shock_camberangle_rear = [sheetInfoResult objectForKey:@"shock_camberangle_rear"];
    }
    if ([sheetInfoResult objectForKey:@"toe_angle_front"]) {
        sheetFull.toe_angle_front = [sheetInfoResult objectForKey:@"toe_angle_front"];
    }
    if ([sheetInfoResult objectForKey:@"oil_front"]) {
        sheetFull.oil_front = [sheetInfoResult objectForKey:@"oil_front"];
    }
    if ([sheetInfoResult objectForKey:@"oil_center"]) {
        sheetFull.oil_center = [sheetInfoResult objectForKey:@"oil_center"];
    }
    if ([sheetInfoResult objectForKey:@"oil_rear"]) {
        sheetFull.oil_rear = [sheetInfoResult objectForKey:@"oil_rear"];
    }

    
    if ([sheetInfoResult objectForKey:@"wheelbase_front_side"]) {
        sheetFull.wheelbase_front_side = [sheetInfoResult objectForKey:@"wheelbase_front_side"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_rear_side"]) {
        sheetFull.wheelbase_rear_side = [sheetInfoResult objectForKey:@"wheelbase_rear_side"];
    }
    if ([sheetInfoResult objectForKey:@"unti_roll_bar_front_above"]) {
        sheetFull.unti_roll_bar_front_above = [sheetInfoResult objectForKey:@"unti_roll_bar_front_above"];
    }
    if ([sheetInfoResult objectForKey:@"unti_roll_bar_front_below"]) {
        sheetFull.unti_roll_bar_front_below = [sheetInfoResult objectForKey:@"unti_roll_bar_front_below"];
    }
    if ([sheetInfoResult objectForKey:@"unti_roll_bar_rear_above"]) {
        sheetFull.unti_roll_bar_rear_above = [sheetInfoResult objectForKey:@"unti_roll_bar_rear_above"];
    }
    if ([sheetInfoResult objectForKey:@"unti_roll_bar_rear_below"]) {
        sheetFull.unti_roll_bar_rear_below = [sheetInfoResult objectForKey:@"unti_roll_bar_rear_below"];
    }
    if ([sheetInfoResult objectForKey:@"weight_right"]) {
        sheetFull.weight_right = [sheetInfoResult objectForKey:@"weight_right"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight_right_front"]) {
        sheetFull.weight_right_front = [sheetInfoResult objectForKey:@"weight_right_front"];
    }
    if ([sheetInfoResult objectForKey:@"weight_right_rear"]) {
        sheetFull.weight_right_rear = [sheetInfoResult objectForKey:@"weight_right_rear"];
    }
    
    
    
    
    //ULtimaSC-R
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_mount_checbox"]) {
        sheetFull.front_suspension_shock_mount_checbox = [sheetInfoResult objectForKey:@"front_suspension_shock_mount_checbox"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_wheelbase"]) {
        sheetFull.front_suspension_wheelbase = [sheetInfoResult objectForKey:@"front_suspension_wheelbase"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder"]) {
        sheetFull.rear_suspension_lower_sus_holder = [sheetInfoResult objectForKey:@"rear_suspension_lower_sus_holder"];
    }
    if ([sheetInfoResult objectForKey:@"shock_top_type_front"]) {
        sheetFull.shock_top_type_front = [sheetInfoResult objectForKey:@"shock_top_type_front"];
    }
    if ([sheetInfoResult objectForKey:@"shock_top_type_rear"]) {
        sheetFull.shock_top_type_rear = [sheetInfoResult objectForKey:@"shock_top_type_rear"];
    }
    if ([sheetInfoResult objectForKey:@"kind_spring"]) {
        sheetFull.kind_spring = [sheetInfoResult objectForKey:@"kind_spring"];
    }
    if ([sheetInfoResult objectForKey:@"body_battery_placement"]) {
        sheetFull.body_battery_placement = [sheetInfoResult objectForKey:@"body_battery_placement"];
    }
    if ([sheetInfoResult objectForKey:@"body_battery_post"]) {
        sheetFull.body_battery_post = [sheetInfoResult objectForKey:@"body_battery_post"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_shock_mount"]) {
        sheetFull.front_suspension_shock_mount = [sheetInfoResult objectForKey:@"front_suspension_shock_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_suspension_image_2"]) {
        sheetFull.rear_suspension_image_2 = [sheetInfoResult objectForKey:@"rear_suspension_image_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_image_4"]) {
        sheetFull.rear_suspension_image_4 = [sheetInfoResult objectForKey:@"rear_suspension_image_4"];
    }
    if ([sheetInfoResult objectForKey:@"front_suspension_camber_text"]) {
        sheetFull.front_suspension_camber_text = [sheetInfoResult objectForKey:@"front_suspension_camber_text"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_f"]) {
        sheetFull.rear_suspension_wheelbase_f = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_f"];
    }
    if ([sheetInfoResult objectForKey:@"rear_suspension_wheelbase_r"]) {
        sheetFull.rear_suspension_wheelbase_r = [sheetInfoResult objectForKey:@"rear_suspension_wheelbase_r"];
    }
    if ([sheetInfoResult objectForKey:@"body_wing"]) {
        sheetFull.body_wing = [sheetInfoResult objectForKey:@"body_wing"];
    }
    if ([sheetInfoResult objectForKey:@"body_body"]) {
        sheetFull.body_body = [sheetInfoResult objectForKey:@"body_body"];
    }
    if ([sheetInfoResult objectForKey:@"body_lip_hight"]) {
        sheetFull.body_lip_hight = [sheetInfoResult objectForKey:@"body_lip_hight"];
    }
    if ([sheetInfoResult objectForKey:@"body_ride_height_f"]) {
        sheetFull.body_ride_height_f = [sheetInfoResult objectForKey:@"body_ride_height_f"];
    }
    if ([sheetInfoResult objectForKey:@"body_ride_height_r"]) {
        sheetFull.body_ride_height_r = [sheetInfoResult objectForKey:@"body_ride_height_r"];
    }
    if ([sheetInfoResult objectForKey:@"tire_traction_compound"]) {
        sheetFull.tire_traction_compound = [sheetInfoResult objectForKey:@"tire_traction_compound"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_motor"]) {
        sheetFull.electronic_motor = [sheetInfoResult objectForKey:@"electronic_motor"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_pinion"]) {
        sheetFull.electronic_pinion = [sheetInfoResult objectForKey:@"electronic_pinion"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_battery"]) {
        sheetFull.electronic_battery = [sheetInfoResult objectForKey:@"electronic_battery"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_esc"]) {
        sheetFull.electronic_esc = [sheetInfoResult objectForKey:@"electronic_esc"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_radio"]) {
        sheetFull.electronic_radio = [sheetInfoResult objectForKey:@"electronic_radio"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_servo"]) {
        sheetFull.electronic_servo = [sheetInfoResult objectForKey:@"electronic_servo"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_throttle_brake_epa"]) {
        sheetFull.electronic_throttle_brake_epa = [sheetInfoResult objectForKey:@"electronic_throttle_brake_epa"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_throttle_brake_expo"]) {
        sheetFull.electronic_throttle_brake_expo = [sheetInfoResult objectForKey:@"electronic_throttle_brake_expo"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_initial_brake"]) {
        sheetFull.electronic_initial_brake = [sheetInfoResult objectForKey:@"electronic_initial_brake"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_drag_brake"]) {
        sheetFull.electronic_drag_brake = [sheetInfoResult objectForKey:@"electronic_drag_brake"];
    }
    
    if ([sheetInfoResult objectForKey:@"electronic_throttle_profile"]) {
        sheetFull.electronic_throttle_profile = [sheetInfoResult objectForKey:@"electronic_throttle_profile"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_steering_expo"]) {
        sheetFull.electronic_steering_expo = [sheetInfoResult objectForKey:@"electronic_steering_expo"];
    }
    if ([sheetInfoResult objectForKey:@"electronic_steering_speed"]) {
        sheetFull.electronic_steering_speed = [sheetInfoResult objectForKey:@"electronic_steering_speed"];
    }
    
    
    //Serpent_S4111
    if ([sheetInfoResult objectForKey:@"front_ball_diff"]) {
        sheetFull.front_ball_diff = [sheetInfoResult objectForKey:@"front_ball_diff"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ball_diff"]) {
        sheetFull.rear_ball_diff = [sheetInfoResult objectForKey:@"rear_ball_diff"];
    }
    if ([sheetInfoResult objectForKey:@"middle_shaft"]) {
        sheetFull.middle_shaft = [sheetInfoResult objectForKey:@"middle_shaft"];
    }
    if ([sheetInfoResult objectForKey:@"front_hubs"]) {
        sheetFull.front_hubs = [sheetInfoResult objectForKey:@"front_hubs"];
    }
    if ([sheetInfoResult objectForKey:@"front_steering_material"]) {
        sheetFull.front_steering_material = [sheetInfoResult objectForKey:@"front_steering_material"];
    }
    if ([sheetInfoResult objectForKey:@"front_offset"]) {
        sheetFull.front_offset = [sheetInfoResult objectForKey:@"front_offset"];
    }
    if ([sheetInfoResult objectForKey:@"front_offset"]) {
        sheetFull.front_offsets = [sheetInfoResult objectForKey:@"front_offset"];
    }
    if ([sheetInfoResult objectForKey:@"battery_position"]) {
        sheetFull.battery_position = [sheetInfoResult objectForKey:@"battery_position"];
    }
    if ([sheetInfoResult objectForKey:@"top_deck"]) {
        sheetFull.top_deck = [sheetInfoResult objectForKey:@"top_deck"];
    }
    if ([sheetInfoResult objectForKey:@"front_steering_position"]) {
        sheetFull.front_steering_position = [sheetInfoResult objectForKey:@"front_steering_position"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_ballstud_position"]) {
        sheetFull.front_ballstud_position = [sheetInfoResult objectForKey:@"front_ballstud_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_swaybar_position"]) {
        sheetFull.rear_swaybar_position = [sheetInfoResult objectForKey:@"rear_swaybar_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ballstud_position"]) {
        sheetFull.rear_ballstud_position = [sheetInfoResult objectForKey:@"rear_ballstud_position"];
    }
    if ([sheetInfoResult objectForKey:@"final_drive"]) {
        sheetFull.final_drive = [sheetInfoResult objectForKey:@"final_drive"];
    }
    if ([sheetInfoResult objectForKey:@"rollout"]) {
        sheetFull.rollout = [sheetInfoResult objectForKey:@"rollout"];
    }
    if ([sheetInfoResult objectForKey:@"armature"]) {
        sheetFull.armature = [sheetInfoResult objectForKey:@"armature"];
    }
    if ([sheetInfoResult objectForKey:@"program"]) {
        sheetFull.program = [sheetInfoResult objectForKey:@"program"];
    }
    if ([sheetInfoResult objectForKey:@"punch"]) {
        sheetFull.punch = [sheetInfoResult objectForKey:@"punch"];
    }
    if ([sheetInfoResult objectForKey:@"initial_brake"]) {
        sheetFull.initial_brake = [sheetInfoResult objectForKey:@"initial_brake"];
    }
    if ([sheetInfoResult objectForKey:@"auto_brake"]) {
        sheetFull.auto_brake = [sheetInfoResult objectForKey:@"auto_brake"];
    }
    if ([sheetInfoResult objectForKey:@"shock_brand"]) {
        sheetFull.shock_brand = [sheetInfoResult objectForKey:@"shock_brand"];
    }
    if ([sheetInfoResult objectForKey:@"front_diameter"]) {
        sheetFull.front_diameter = [sheetInfoResult objectForKey:@"front_diameter"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diameter"]) {
        sheetFull.rear_diameter = [sheetInfoResult objectForKey:@"rear_diameter"];
    }
    if ([sheetInfoResult objectForKey:@"body_position"]) {
        sheetFull.body_position = [sheetInfoResult objectForKey:@"body_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_toe_out"]) {
        sheetFull.front_toe_out = [sheetInfoResult objectForKey:@"front_toe_out"];
    }
    if ([sheetInfoResult objectForKey:@"front_toe_in"]) {
        sheetFull.front_toe_in = [sheetInfoResult objectForKey:@"front_toe_in"];
    }
    if ([sheetInfoResult objectForKey:@"front_bracket_1"]) {
        sheetFull.front_bracket_1 = [sheetInfoResult objectForKey:@"front_bracket_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_bracket_2"]) {
        sheetFull.front_bracket_2 = [sheetInfoResult objectForKey:@"front_bracket_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_wheelbase"]) {
        sheetFull.front_wheelbase = [sheetInfoResult objectForKey:@"front_wheelbase"];
    }
    if ([sheetInfoResult objectForKey:@"rear_bracket_1"]) {
        sheetFull.rear_bracket_1 = [sheetInfoResult objectForKey:@"rear_bracket_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_bracket_2"]) {
        sheetFull.rear_bracket_2 = [sheetInfoResult objectForKey:@"rear_bracket_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_shim_1"]) {
        sheetFull.front_shim_1 = [sheetInfoResult objectForKey:@"front_shim_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_shim_2"]) {
        sheetFull.front_shim_2 = [sheetInfoResult objectForKey:@"front_shim_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_offset_2"]) {
        sheetFull.front_offset_2 = [sheetInfoResult objectForKey:@"front_offset_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shim_1"]) {
        sheetFull.rear_shim_1 = [sheetInfoResult objectForKey:@"rear_shim_1"];
        
    }
    if ([sheetInfoResult objectForKey:@"rear_shim_2"]) {
        sheetFull.rear_shim_2 = [sheetInfoResult objectForKey:@"rear_shim_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_offset_2"]) {
        sheetFull.rear_offset_2 = [sheetInfoResult objectForKey:@"rear_offset_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_offset"]) {
        sheetFull.rear_offset = [sheetInfoResult objectForKey:@"rear_offset"];
    }
    if ([sheetInfoResult objectForKey:@"rear_offset"]) {
        sheetFull.rear_offsets = [sheetInfoResult objectForKey:@"rear_offset"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_downstop"]) {
        sheetFull.front_downstop = [sheetInfoResult objectForKey:@"front_downstop"];
    }
    if ([sheetInfoResult objectForKey:@"rear_downstop"]) {
        sheetFull.rear_downstop = [sheetInfoResult objectForKey:@"rear_downstop"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_negative"]) {
        sheetFull.front_camber_negative = [sheetInfoResult objectForKey:@"front_camber_negative"];
    }
    if ([sheetInfoResult objectForKey:@"front_camber_positive"]) {
        sheetFull.front_camber_positive = [sheetInfoResult objectForKey:@"front_camber_positive"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_negative"]) {
        sheetFull.rear_camber_negative = [sheetInfoResult objectForKey:@"rear_camber_negative"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_positive"]) {
        sheetFull.rear_camber_positive = [sheetInfoResult objectForKey:@"rear_camber_positive"];
    }
    if ([sheetInfoResult objectForKey:@"shim_1"]) {
        sheetFull.shim_1 = [sheetInfoResult objectForKey:@"shim_1"];
    }
    if ([sheetInfoResult objectForKey:@"battery_weight"]) {
        sheetFull.battery_weight = [sheetInfoResult objectForKey:@"battery_weight"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight_1"]) {
        sheetFull.weight_1 = [sheetInfoResult objectForKey:@"weight_1"];
    }
    if ([sheetInfoResult objectForKey:@"weight_2"]) {
        sheetFull.weight_2 = [sheetInfoResult objectForKey:@"weight_2"];
    }
    if ([sheetInfoResult objectForKey:@"weight_3"]) {
        sheetFull.weight_3 = [sheetInfoResult objectForKey:@"weight_3"];
    }
    if ([sheetInfoResult objectForKey:@"rollcenter_1"]) {
        sheetFull.rollcenter_1 = [sheetInfoResult objectForKey:@"rollcenter_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"rollcenter_2"]) {
        sheetFull.rollcenter_2 = [sheetInfoResult objectForKey:@"rollcenter_2"];
    }
    if ([sheetInfoResult objectForKey:@"rollcenter_3"]) {
        sheetFull.rollcenter_3 = [sheetInfoResult objectForKey:@"rollcenter_3"];
    }
    if ([sheetInfoResult objectForKey:@"rollcenter_4"]) {
        sheetFull.rollcenter_4 = [sheetInfoResult objectForKey:@"rollcenter_4"];
    }
    
    // Mugen_MBX7
    if ([sheetInfoResult objectForKey:@"front_tesion_rod"]) {
        sheetFull.front_tesion_rod = [sheetInfoResult objectForKey:@"front_tesion_rod"];
    }
    if ([sheetInfoResult objectForKey:@"front_uper_arm_position"]) {
        sheetFull.front_uper_arm_position = [sheetInfoResult objectForKey:@"front_uper_arm_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_uper_arm_position_below"]) {
        sheetFull.front_uper_arm_position_below = [sheetInfoResult objectForKey:@"front_uper_arm_position_below"];
    }
    if ([sheetInfoResult objectForKey:@"front_lower_arm_position"]) {
        sheetFull.front_lower_arm_position = [sheetInfoResult objectForKey:@"front_lower_arm_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_lower_arm_position_below"]) {
        sheetFull.front_lower_arm_position_below = [sheetInfoResult objectForKey:@"front_lower_arm_position_below"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_position"]) {
        sheetFull.front_shock_position = [sheetInfoResult objectForKey:@"front_shock_position"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_pivot_ball"]) {
        sheetFull.front_shock_pivot_ball = [sheetInfoResult objectForKey:@"front_shock_pivot_ball"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_damper_stay"]) {
        sheetFull.front_shock_damper_stay = [sheetInfoResult objectForKey:@"front_shock_damper_stay"];
    }
    if ([sheetInfoResult objectForKey:@"steering_ackerman"]) {
        sheetFull.steering_ackerman = [sheetInfoResult objectForKey:@"steering_ackerman"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_upper_arm_position"]) {
        sheetFull.rear_upper_arm_position = [sheetInfoResult objectForKey:@"rear_upper_arm_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_lower_arm_position"]) {
        sheetFull.rear_lower_arm_position = [sheetInfoResult objectForKey:@"rear_lower_arm_position"];
    }
    if ([sheetInfoResult objectForKey:@"rear_lower_arm_position_toe"]) {
        sheetFull.rear_lower_arm_position_toe = [sheetInfoResult objectForKey:@"rear_lower_arm_position_toe"];
    }
    if ([sheetInfoResult objectForKey:@"rear_lower_arm_position_below"]) {
        sheetFull.rear_lower_arm_position_below = [sheetInfoResult objectForKey:@"rear_lower_arm_position_below"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_pivot_ball"]) {
        sheetFull.rear_shock_pivot_ball = [sheetInfoResult objectForKey:@"rear_shock_pivot_ball"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_damper_stay"]) {
        sheetFull.rear_shock_damper_stay = [sheetInfoResult objectForKey:@"rear_shock_damper_stay"];
    }
    if ([sheetInfoResult objectForKey:@"engine_mount"]) {
        sheetFull.engine_mount = [sheetInfoResult objectForKey:@"engine_mount"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"front_suspension_toe_angle"]) {
        sheetFull.front_suspension_toe_angle = [sheetInfoResult objectForKey:@"front_suspension_toe_angle"];
    }
    if ([sheetInfoResult objectForKey:@"kingpin_ball_spacer"]) {
        sheetFull.kingpin_ball_spacer = [sheetInfoResult objectForKey:@"kingpin_ball_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_rebound_stop"]) {
        sheetFull.front_rebound_stop = [sheetInfoResult objectForKey:@"front_rebound_stop"];
    }
    if ([sheetInfoResult objectForKey:@"front_anti_roll_bar"]) {
        sheetFull.front_anti_roll_bar = [sheetInfoResult objectForKey:@"front_anti_roll_bar"];
    }
    if ([sheetInfoResult objectForKey:@"front_upper_arm_spacer"]) {
        sheetFull.front_upper_arm_spacer = [sheetInfoResult objectForKey:@"front_upper_arm_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_lower_arm_spacer"]) {
        sheetFull.front_lower_arm_spacer = [sheetInfoResult objectForKey:@"front_lower_arm_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"front_tesion"]) {
        sheetFull.front_tesion = [sheetInfoResult objectForKey:@"front_tesion"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_pistion"]) {
        sheetFull.front_shock_pistion = [sheetInfoResult objectForKey:@"front_shock_pistion"];
    }
    if ([sheetInfoResult objectForKey:@"front_shock_spring_adj"]) {
        sheetFull.front_shock_spring_adj = [sheetInfoResult objectForKey:@"front_shock_spring_adj"];
    }
    if ([sheetInfoResult objectForKey:@"rear_right_height"]) {
        sheetFull.rear_right_height = [sheetInfoResult objectForKey:@"rear_right_height"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camber_angle"]) {
        sheetFull.rear_camber_angle = [sheetInfoResult objectForKey:@"rear_camber_angle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rebound_stop"]) {
        sheetFull.rear_rebound_stop = [sheetInfoResult objectForKey:@"rear_rebound_stop"];
    }
    if ([sheetInfoResult objectForKey:@"rear_antil_roll"]) {
        sheetFull.rear_antil_roll = [sheetInfoResult objectForKey:@"rear_antil_roll"];
    }
    if ([sheetInfoResult objectForKey:@"rear_lower_arm"]) {
        sheetFull.rear_lower_arm = [sheetInfoResult objectForKey:@"rear_lower_arm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_upright_spacer"]) {
        sheetFull.rear_upright_spacer = [sheetInfoResult objectForKey:@"rear_upright_spacer"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_upper_arm"]) {
        sheetFull.rear_upper_arm = [sheetInfoResult objectForKey:@"rear_upper_arm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_pistion"]) {
        sheetFull.rear_shock_pistion = [sheetInfoResult objectForKey:@"rear_shock_pistion"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_spring_adj"]) {
        sheetFull.rear_shock_spring_adj = [sheetInfoResult objectForKey:@"rear_shock_spring_adj"];
        
    }
    if ([sheetInfoResult objectForKey:@"engine_type"]) {
        sheetFull.engine_type = [sheetInfoResult objectForKey:@"engine_type"];
    }
    if ([sheetInfoResult objectForKey:@"engine_plug"]) {
        sheetFull.engine_plug = [sheetInfoResult objectForKey:@"engine_plug"];
    }
    if ([sheetInfoResult objectForKey:@"engine_gasket"]) {
        sheetFull.engine_gasket = [sheetInfoResult objectForKey:@"engine_gasket"];
    }
    
    if ([sheetInfoResult objectForKey:@"engine_reducer"]) {
        sheetFull.engine_reducer = [sheetInfoResult objectForKey:@"engine_reducer"];
    }
    if ([sheetInfoResult objectForKey:@"engine_muffler"]) {
        sheetFull.engine_muffler = [sheetInfoResult objectForKey:@"engine_muffler"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_front"]) {
        sheetFull.diff_oil_front = [sheetInfoResult objectForKey:@"diff_oil_front"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_center"]) {
        sheetFull.diff_oil_center = [sheetInfoResult objectForKey:@"diff_oil_center"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_rear"]) {
        sheetFull.diff_oil_rear = [sheetInfoResult objectForKey:@"diff_oil_rear"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_oring1"]) {
        sheetFull.diff_oil_oring1 = [sheetInfoResult objectForKey:@"diff_oil_oring1"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_oring2"]) {
        sheetFull.diff_oil_oring2 = [sheetInfoResult objectForKey:@"diff_oil_oring2"];
    }
    if ([sheetInfoResult objectForKey:@"diff_oil_oring3"]) {
        sheetFull.diff_oil_oring3 = [sheetInfoResult objectForKey:@"diff_oil_oring3"];
    }
    
    if ([sheetInfoResult objectForKey:@"tires_front_type"]) {
        sheetFull.tires_front_type = [sheetInfoResult objectForKey:@"tires_front_type"];
    }
    if ([sheetInfoResult objectForKey:@"tires_front_compound"]) {
        sheetFull.tires_front_compound = [sheetInfoResult objectForKey:@"tires_front_compound"];
    }
    if ([sheetInfoResult objectForKey:@"tires_front_insert"]) {
        sheetFull.tires_front_insert = [sheetInfoResult objectForKey:@"tires_front_insert"];
    }
    if ([sheetInfoResult objectForKey:@"tires_front_wheel"]) {
        sheetFull.tires_front_wheel = [sheetInfoResult objectForKey:@"tires_front_wheel"];
    }
    
    if ([sheetInfoResult objectForKey:@"tires_rear_type"]) {
        sheetFull.tires_rear_type = [sheetInfoResult objectForKey:@"tires_rear_type"];
    }
    if ([sheetInfoResult objectForKey:@"tires_rear_compound"]) {
        sheetFull.tires_rear_compound = [sheetInfoResult objectForKey:@"tires_rear_compound"];
    }
    if ([sheetInfoResult objectForKey:@"tires_rear_insert"]) {
        sheetFull.tires_rear_insert = [sheetInfoResult objectForKey:@"tires_rear_insert"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"tires_rear_wheel"]) {
        sheetFull.tires_rear_wheel = [sheetInfoResult objectForKey:@"tires_rear_wheel"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_beel"]) {
        sheetFull.clutch_beel = [sheetInfoResult objectForKey:@"clutch_beel"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_spur_gear"]) {
        sheetFull.clutch_spur_gear = [sheetInfoResult objectForKey:@"clutch_spur_gear"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_shoes"]) {
        sheetFull.clutch_shoes = [sheetInfoResult objectForKey:@"clutch_shoes"];
    }
    
    if ([sheetInfoResult objectForKey:@"clutch_spring"]) {
        sheetFull.clutch_spring = [sheetInfoResult objectForKey:@"clutch_spring"];
    }
    if ([sheetInfoResult objectForKey:@"body_w"]) {
        sheetFull.body_w = [sheetInfoResult objectForKey:@"body_w"];
    }
    if ([sheetInfoResult objectForKey:@"wing_w"]) {
        sheetFull.wing_w = [sheetInfoResult objectForKey:@"wing_w"];
    }
    
    //supent
    if ([sheetInfoResult objectForKey:@"clutch_type"]) {
        sheetFull.clutch_type = [sheetInfoResult objectForKey:@"clutch_type"];
    }
    if ([sheetInfoResult objectForKey:@"brake_balancer"]) {
        sheetFull.brake_balancer = [sheetInfoResult objectForKey:@"brake_balancer"];
    }
    if ([sheetInfoResult objectForKey:@"front_orange"]) {
        sheetFull.front_orange = [sheetInfoResult objectForKey:@"front_orange"];
    }
    if ([sheetInfoResult objectForKey:@"front_red"]) {
        sheetFull.front_red = [sheetInfoResult objectForKey:@"front_red"];
    }
    if ([sheetInfoResult objectForKey:@"front_pink"]) {
        sheetFull.front_pink = [sheetInfoResult objectForKey:@"front_pink"];
    }
    if ([sheetInfoResult objectForKey:@"front_blue"]) {
        sheetFull.front_blue = [sheetInfoResult objectForKey:@"front_blue"];
    }
    if ([sheetInfoResult objectForKey:@"front_purple"]) {
        sheetFull.front_purple = [sheetInfoResult objectForKey:@"front_purple"];
    }
    if ([sheetInfoResult objectForKey:@"front_green"]) {
        sheetFull.front_green = [sheetInfoResult objectForKey:@"front_green"];
    }
    if ([sheetInfoResult objectForKey:@"front_other_1"]) {
        sheetFull.front_other_1 = [sheetInfoResult objectForKey:@"front_other_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_spindle"]) {
        sheetFull.front_spindle = [sheetInfoResult objectForKey:@"front_spindle"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"rear_orange"]) {
        sheetFull.rear_orange = [sheetInfoResult objectForKey:@"rear_orange"];
    }
    if ([sheetInfoResult objectForKey:@"rear_red"]) {
        sheetFull.rear_red = [sheetInfoResult objectForKey:@"rear_red"];
    }
    if ([sheetInfoResult objectForKey:@"rear_pink"]) {
        sheetFull.rear_pink = [sheetInfoResult objectForKey:@"rear_pink"];
    }
    if ([sheetInfoResult objectForKey:@"rear_blue"]) {
        sheetFull.rear_blue = [sheetInfoResult objectForKey:@"rear_blue"];
    }
    if ([sheetInfoResult objectForKey:@"rear_purple"]) {
        sheetFull.rear_purple = [sheetInfoResult objectForKey:@"rear_purple"];
    }
    if ([sheetInfoResult objectForKey:@"rear_green"]) {
        sheetFull.rear_green = [sheetInfoResult objectForKey:@"rear_green"];
    }
    if ([sheetInfoResult objectForKey:@"rear_other_1"]) {
        sheetFull.rear_other_1 = [sheetInfoResult objectForKey:@"rear_other_1"];
    }
    if ([sheetInfoResult objectForKey:@"stock_wing"]) {
        sheetFull.stock_wing = [sheetInfoResult objectForKey:@"stock_wing"];
    }
    if ([sheetInfoResult objectForKey:@"front_hex_adapter"]) {
        sheetFull.front_hex_adapter = [sheetInfoResult objectForKey:@"front_hex_adapter"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower_upright"]) {
        sheetFull.rear_tower_upright = [sheetInfoResult objectForKey:@"rear_tower_upright"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_hex_adapter"]) {
        sheetFull.rear_hex_adapter = [sheetInfoResult objectForKey:@"rear_hex_adapter"];
    }
    if ([sheetInfoResult objectForKey:@"front_frontinsert"]) {
        sheetFull.front_frontinsert = [sheetInfoResult objectForKey:@"front_frontinsert"];
    }
    if ([sheetInfoResult objectForKey:@"front_rearinsert"]) {
        sheetFull.front_rearinsert = [sheetInfoResult objectForKey:@"front_rearinsert"];
    }
    if ([sheetInfoResult objectForKey:@"front_brace"]) {
        sheetFull.front_brace = [sheetInfoResult objectForKey:@"front_brace"];
    }
    if ([sheetInfoResult objectForKey:@"rear_brace"]) {
        sheetFull.rear_brace = [sheetInfoResult objectForKey:@"rear_brace"];
    }
    if ([sheetInfoResult objectForKey:@"rear_frontinsert"]) {
        sheetFull.rear_frontinsert = [sheetInfoResult objectForKey:@"rear_frontinsert"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rearinsert"]) {
        sheetFull.rear_rearinsert = [sheetInfoResult objectForKey:@"rear_rearinsert"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_frontbracket"]) {
        sheetFull.rear_frontbracket = [sheetInfoResult objectForKey:@"rear_frontbracket"];
    }
    if ([sheetInfoResult objectForKey:@"steering_bar"]) {
        sheetFull.steering_bar = [sheetInfoResult objectForKey:@"steering_bar"];
    }
    if ([sheetInfoResult objectForKey:@"wheelbase_shims"]) {
        sheetFull.wheelbase_shims = [sheetInfoResult objectForKey:@"wheelbase_shims"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"holes_size_left"]) {
        sheetFull.holes_size_left = [sheetInfoResult objectForKey:@"holes_size_left"];
    }
    if ([sheetInfoResult objectForKey:@"holes_size_right"]) {
        sheetFull.holes_size_right = [sheetInfoResult objectForKey:@"holes_size_right"];
    }
    if ([sheetInfoResult objectForKey:@"rear_inner_mount"]) {
        sheetFull.rear_inner_mount = [sheetInfoResult objectForKey:@"rear_inner_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wing_mount"]) {
        sheetFull.rear_wing_mount = [sheetInfoResult objectForKey:@"rear_wing_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rearbracket"]) {
        sheetFull.rear_rearbracket = [sheetInfoResult objectForKey:@"rear_rearbracket"];
    }
    if ([sheetInfoResult objectForKey:@"front_inner_mount"]) {
        sheetFull.front_inner_mount = [sheetInfoResult objectForKey:@"front_inner_mount"];
    }
    if ([sheetInfoResult objectForKey:@"center_diff_oil"]) {
        sheetFull.center_diff_oil = [sheetInfoResult objectForKey:@"center_diff_oil"];
    }
    if ([sheetInfoResult objectForKey:@"optinal_clutch"]) {
        sheetFull.optinal_clutch = [sheetInfoResult objectForKey:@"optinal_clutch"];
    }
    if ([sheetInfoResult objectForKey:@"aluminum_clutch"]) {
        sheetFull.aluminum_clutch = [sheetInfoResult objectForKey:@"aluminum_clutch"];
    }
    if ([sheetInfoResult objectForKey:@"aluminum_spring"]) {
        sheetFull.aluminum_spring = [sheetInfoResult objectForKey:@"aluminum_spring"];
    }
    if ([sheetInfoResult objectForKey:@"carbon_clutch"]) {
        sheetFull.carbon_clutch = [sheetInfoResult objectForKey:@"carbon_clutch"];
    }
    if ([sheetInfoResult objectForKey:@"carbon_spring"]) {
        sheetFull.carbon_spring = [sheetInfoResult objectForKey:@"carbon_spring"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_bell"]) {
        sheetFull.clutch_bell = [sheetInfoResult objectForKey:@"clutch_bell"];
    }
    if ([sheetInfoResult objectForKey:@"balancer_1"]) {
        sheetFull.balancer_1 = [sheetInfoResult objectForKey:@"balancer_1"];
    }
    if ([sheetInfoResult objectForKey:@"balancer_2"]) {
        sheetFull.balancer_2 = [sheetInfoResult objectForKey:@"balancer_2"];
    }
    
    if ([sheetInfoResult objectForKey:@"balancer_3"]) {
        sheetFull.balancer_3 = [sheetInfoResult objectForKey:@"balancer_3"];
    }
    if ([sheetInfoResult objectForKey:@"balancer_4"]) {
        sheetFull.balancer_4 = [sheetInfoResult objectForKey:@"balancer_4"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shock_spring_adj"]) {
        sheetFull.rear_shock_spring_adj = [sheetInfoResult objectForKey:@"rear_shock_spring_adj"];
        
    }
    if ([sheetInfoResult objectForKey:@"balancer_5"]) {
        sheetFull.balancer_5 = [sheetInfoResult objectForKey:@"balancer_5"];
    }
    if ([sheetInfoResult objectForKey:@"front_other"]) {
        sheetFull.front_other = [sheetInfoResult objectForKey:@"front_other"];
    }
    if ([sheetInfoResult objectForKey:@"rear_other"]) {
        sheetFull.rear_other = [sheetInfoResult objectForKey:@"rear_other"];
    }
    
    if ([sheetInfoResult objectForKey:@"tire_brand"]) {
        sheetFull.tire_brand = [sheetInfoResult objectForKey:@"tire_brand"];
    }
    if ([sheetInfoResult objectForKey:@"tire_type"]) {
        sheetFull.tire_type = [sheetInfoResult objectForKey:@"tire_type"];
    }
    if ([sheetInfoResult objectForKey:@"tire_insert"]) {
        sheetFull.tire_insert = [sheetInfoResult objectForKey:@"tire_insert"];
    }
    if ([sheetInfoResult objectForKey:@"wing_brand"]) {
        sheetFull.wing_brand = [sheetInfoResult objectForKey:@"wing_brand"];
    }
    if ([sheetInfoResult objectForKey:@"manifold"]) {
        sheetFull.manifold = [sheetInfoResult objectForKey:@"manifold"];
    }
    if ([sheetInfoResult objectForKey:@"frontbracket_type"]) {
        sheetFull.frontbracket_type = [sheetInfoResult objectForKey:@"frontbracket_type"];
    }
    if ([sheetInfoResult objectForKey:@"rearbracket_type"]) {
        sheetFull.rearbracket_type = [sheetInfoResult objectForKey:@"rearbracket_type"];
    }
    if ([sheetInfoResult objectForKey:@"wing_location_rear"]) {
        sheetFull.wing_location_rear = [sheetInfoResult objectForKey:@"wing_location_rear"];
    }
    
    
    
    
    //Agama
    if ([sheetInfoResult objectForKey:@"front_tower"]) {
        sheetFull.front_tower = [sheetInfoResult objectForKey:@"front_tower"];
    }
    if ([sheetInfoResult objectForKey:@"front_cambermount"]) {
        sheetFull.front_cambermount = [sheetInfoResult objectForKey:@"front_cambermount"];
    }
    if ([sheetInfoResult objectForKey:@"ff"]) {
        sheetFull.ff = [sheetInfoResult objectForKey:@"ff"];
    }
    if ([sheetInfoResult objectForKey:@"fr"]) {
        sheetFull.fr = [sheetInfoResult objectForKey:@"fr"];
    }
    if ([sheetInfoResult objectForKey:@"rf"]) {
        sheetFull.rf = [sheetInfoResult objectForKey:@"rf"];
    }
    
    if ([sheetInfoResult objectForKey:@"rr"]) {
        sheetFull.rr = [sheetInfoResult objectForKey:@"rr"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tower"]) {
        sheetFull.rear_tower = [sheetInfoResult objectForKey:@"rear_tower"];
    }
    if ([sheetInfoResult objectForKey:@"rear_cambermount"]) {
        sheetFull.rear_cambermount = [sheetInfoResult objectForKey:@"rear_cambermount"];
    }
    
    if ([sheetInfoResult objectForKey:@"throttle_servo"]) {
        sheetFull.throttle_servo = [sheetInfoResult objectForKey:@"throttle_servo"];
    }
    
    if ([sheetInfoResult objectForKey:@"ackerman"]) {
        sheetFull.ackerman = [sheetInfoResult objectForKey:@"ackerman"];
    }
    
    if ([sheetInfoResult objectForKey:@"clutchbell"]) {
        sheetFull.clutchbell = [sheetInfoResult objectForKey:@"clutchbell"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"clutch_t"]) {
        sheetFull.clutch_t = [sheetInfoResult objectForKey:@"clutch_t"];
    }
    if ([sheetInfoResult objectForKey:@"front_ring_t"]) {
        sheetFull.front_ring_t = [sheetInfoResult objectForKey:@"front_ring_t"];
    }
    if ([sheetInfoResult objectForKey:@"front_pinion"]) {
        sheetFull.front_pinion = [sheetInfoResult objectForKey:@"front_pinion"];
    }
    if ([sheetInfoResult objectForKey:@"center_type"]) {
        sheetFull.center_type = [sheetInfoResult objectForKey:@"center_type"];
        
    }
    if ([sheetInfoResult objectForKey:@"center_spur"]) {
        sheetFull.center_spur = [sheetInfoResult objectForKey:@"center_spur"];
    }
    if ([sheetInfoResult objectForKey:@"rear_ring_t"]) {
        sheetFull.rear_ring_t = [sheetInfoResult objectForKey:@"rear_ring_t"];
    }
    if ([sheetInfoResult objectForKey:@"rear_pinion"]) {
        sheetFull.rear_pinion = [sheetInfoResult objectForKey:@"rear_pinion"];
    }
    if ([sheetInfoResult objectForKey:@"front_rideheight"]) {
        sheetFull.front_rideheight = [sheetInfoResult objectForKey:@"front_rideheight"];
    }
    if ([sheetInfoResult objectForKey:@"front_tire_1"]) {
        sheetFull.front_tire_1 = [sheetInfoResult objectForKey:@"front_tire_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_tire_2"]) {
        sheetFull.front_tire_2 = [sheetInfoResult objectForKey:@"front_tire_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_inner_1"]) {
        sheetFull.front_inner_1 = [sheetInfoResult objectForKey:@"front_inner_1"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_inner_2"]) {
        sheetFull.front_inner_2 = [sheetInfoResult objectForKey:@"front_inner_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_holes"]) {
        sheetFull.front_holes = [sheetInfoResult objectForKey:@"front_holes"];
    }
    if ([sheetInfoResult objectForKey:@"front_holes"]) {
        sheetFull.front_hole = [sheetInfoResult objectForKey:@"front_holes"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"front_spring_1"]) {
        sheetFull.front_spring_1 = [sheetInfoResult objectForKey:@"front_spring_1"];
    }
    if ([sheetInfoResult objectForKey:@"front_spring_2"]) {
        sheetFull.front_spring_2 = [sheetInfoResult objectForKey:@"front_spring_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer"]) {
        sheetFull.front_spacer = [sheetInfoResult objectForKey:@"front_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rollbar_other"]) {
        sheetFull.rollbar_other = [sheetInfoResult objectForKey:@"rollbar_other"];
    }
    if ([sheetInfoResult objectForKey:@"rollbar_space"]) {
        sheetFull.rollbar_space = [sheetInfoResult objectForKey:@"rollbar_space"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer_mm"]) {
        sheetFull.front_washer_mm = [sheetInfoResult objectForKey:@"front_washer_mm"];
    }
    if ([sheetInfoResult objectForKey:@"front_washer_pcs"]) {
        sheetFull.front_washer_pcs = [sheetInfoResult objectForKey:@"front_washer_pcs"];
    }
    if ([sheetInfoResult objectForKey:@"rear_washer_mm"]) {
        sheetFull.rear_washer_mm = [sheetInfoResult objectForKey:@"rear_washer_mm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_washer_pcs"]) {
        sheetFull.rear_washer_pcs = [sheetInfoResult objectForKey:@"rear_washer_pcs"];
    }
    if ([sheetInfoResult objectForKey:@"rear_holes"]) {
        sheetFull.rear_holes = [sheetInfoResult objectForKey:@"rear_holes"];
    }
    if ([sheetInfoResult objectForKey:@"rear_holes"]) {
        sheetFull.rear_hole = [sheetInfoResult objectForKey:@"rear_holes"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_spring_1"]) {
        sheetFull.rear_spring_1 = [sheetInfoResult objectForKey:@"rear_spring_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spring_2"]) {
        sheetFull.rear_spring_2 = [sheetInfoResult objectForKey:@"rear_spring_2"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_rideheight"]) {
        sheetFull.rear_rideheight = [sheetInfoResult objectForKey:@"rear_rideheight"];
    }
    if ([sheetInfoResult objectForKey:@"rear_tire_1"]) {
        sheetFull.rear_tire_1 = [sheetInfoResult objectForKey:@"rear_tire_1"];
        
    }
    if ([sheetInfoResult objectForKey:@"rear_tire_2"]) {
        sheetFull.rear_tire_2 = [sheetInfoResult objectForKey:@"rear_tire_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_inner_1"]) {
        sheetFull.rear_inner_1 = [sheetInfoResult objectForKey:@"rear_inner_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_inner_2"]) {
        sheetFull.rear_inner_2 = [sheetInfoResult objectForKey:@"rear_inner_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rollbar_other"]) {
        sheetFull.rear_rollbar_other = [sheetInfoResult objectForKey:@"rear_rollbar_other"];
    }
    if ([sheetInfoResult objectForKey:@"rear_rollbar_space"]) {
        sheetFull.rear_rollbar_space = [sheetInfoResult objectForKey:@"rear_rollbar_space"];
    }
    
    //JQ product
    if ([sheetInfoResult objectForKey:@"ball_type"]) {
        sheetFull.ball_type = [sheetInfoResult objectForKey:@"ball_type"];
    }
    if ([sheetInfoResult objectForKey:@"hub_pin_position"]) {
        sheetFull.hub_pin_position = [sheetInfoResult objectForKey:@"hub_pin_position"];
    }
    if ([sheetInfoResult objectForKey:@"alloy_hub"]) {
        sheetFull.alloy_hub = [sheetInfoResult objectForKey:@"alloy_hub"];
    }
    if ([sheetInfoResult objectForKey:@"engine_front"]) {
        sheetFull.engine_front = [sheetInfoResult objectForKey:@"engine_front"];
    }
    if ([sheetInfoResult objectForKey:@"top_kickup"]) {
        sheetFull.top_kickup = [sheetInfoResult objectForKey:@"top_kickup"];
    }
    
    if ([sheetInfoResult objectForKey:@"bottom_kickup"]) {
        sheetFull.bottom_kickup = [sheetInfoResult objectForKey:@"bottom_kickup"];
    }
    if ([sheetInfoResult objectForKey:@"antisquat"]) {
        sheetFull.antisquat = [sheetInfoResult objectForKey:@"antisquat"];
    }
    if ([sheetInfoResult objectForKey:@"toein"]) {
        sheetFull.toein = [sheetInfoResult objectForKey:@"toein"];
    }
    
    if ([sheetInfoResult objectForKey:@"engine_mountholes_1"]) {
        sheetFull.engine_mountholes_1 = [sheetInfoResult objectForKey:@"engine_mountholes_1"];
    }
    if ([sheetInfoResult objectForKey:@"engine_mountholes_2"]) {
        sheetFull.engine_mountholes_2 = [sheetInfoResult objectForKey:@"engine_mountholes_2"];
    }
    if ([sheetInfoResult objectForKey:@"engine_mountholes_3"]) {
        sheetFull.engine_mountholes_3 = [sheetInfoResult objectForKey:@"engine_mountholes_3"];
    }
    if ([sheetInfoResult objectForKey:@"engine_mountholes_4"]) {
        sheetFull.engine_mountholes_4 = [sheetInfoResult objectForKey:@"engine_mountholes_4"];
    }
    
    if ([sheetInfoResult objectForKey:@"engine_postion"]) {
        sheetFull.engine_postion = [sheetInfoResult objectForKey:@"engine_postion"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_downtravel"]) {
        sheetFull.front_downtravel = [sheetInfoResult objectForKey:@"front_downtravel"];
    }
    
    if ([sheetInfoResult objectForKey:@"f_turnbuckle"]) {
        sheetFull.f_turnbuckle = [sheetInfoResult objectForKey:@"f_turnbuckle"];
    }
    if ([sheetInfoResult objectForKey:@"front_downtravel0"]) {
        sheetFull.front_downtravel0 = [sheetInfoResult objectForKey:@"front_downtravel0"];
    }
    if ([sheetInfoResult objectForKey:@"front_balance"]) {
        sheetFull.front_balance = [sheetInfoResult objectForKey:@"front_balance"];
    }
    if ([sheetInfoResult objectForKey:@"front_camberlength"]) {
        sheetFull.front_camberlength = [sheetInfoResult objectForKey:@"front_camberlength"];
        
    }
    if ([sheetInfoResult objectForKey:@"steering_link"]) {
        sheetFull.steering_link = [sheetInfoResult objectForKey:@"steering_link"];
    }
    if ([sheetInfoResult objectForKey:@"rear_downtravel"]) {
        sheetFull.rear_downtravel = [sheetInfoResult objectForKey:@"rear_downtravel"];
    }
    if ([sheetInfoResult objectForKey:@"rear_balance"]) {
        sheetFull.rear_balance = [sheetInfoResult objectForKey:@"rear_balance"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camberlength"]) {
        sheetFull.rear_camberlength = [sheetInfoResult objectForKey:@"rear_camberlength"];
    }
    if ([sheetInfoResult objectForKey:@"r_turnbuckle"]) {
        sheetFull.r_turnbuckle = [sheetInfoResult objectForKey:@"r_turnbuckle"];
    }
    if ([sheetInfoResult objectForKey:@"rear_downtravel0"]) {
        sheetFull.rear_downtravel0 = [sheetInfoResult objectForKey:@"rear_downtravel0"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight_4"]) {
        sheetFull.weight_4 = [sheetInfoResult objectForKey:@"weight_4"];
    }
    if ([sheetInfoResult objectForKey:@"weight_5"]) {
        sheetFull.weight_5 = [sheetInfoResult objectForKey:@"weight_5"];
    }
    
    if ([sheetInfoResult objectForKey:@"weight_6"]) {
        sheetFull.weight_6 = [sheetInfoResult objectForKey:@"weight_6"];
    }
    if ([sheetInfoResult objectForKey:@"front_spring_2"]) {
        sheetFull.front_spring_2 = [sheetInfoResult objectForKey:@"front_spring_2"];
    }
    if ([sheetInfoResult objectForKey:@"weight_7"]) {
        sheetFull.weight_7 = [sheetInfoResult objectForKey:@"weight_7"];
    }
    if ([sheetInfoResult objectForKey:@"total_weight"]) {
        sheetFull.total_weight = [sheetInfoResult objectForKey:@"total_weight"];
    }
    if ([sheetInfoResult objectForKey:@"front_preload"]) {
        sheetFull.front_preload = [sheetInfoResult objectForKey:@"front_preload"];
    }
    if ([sheetInfoResult objectForKey:@"front_total"]) {
        sheetFull.front_total = [sheetInfoResult objectForKey:@"front_total"];
    }
    if ([sheetInfoResult objectForKey:@"rear_preload"]) {
        sheetFull.rear_preload = [sheetInfoResult objectForKey:@"rear_preload"];
    }
    if ([sheetInfoResult objectForKey:@"rear_total"]) {
        sheetFull.rear_total = [sheetInfoResult objectForKey:@"rear_total"];
    }
    if ([sheetInfoResult objectForKey:@"tire_weight"]) {
        sheetFull.tire_weight = [sheetInfoResult objectForKey:@"tire_weight"];
    }
    if ([sheetInfoResult objectForKey:@"front_gear"]) {
        sheetFull.front_gear = [sheetInfoResult objectForKey:@"front_gear"];
    }
    if ([sheetInfoResult objectForKey:@"center_oil"]) {
        sheetFull.center_oil = [sheetInfoResult objectForKey:@"center_oil"];
    }
    if ([sheetInfoResult objectForKey:@"center_gear"]) {
        sheetFull.center_gear = [sheetInfoResult objectForKey:@"center_gear"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_gear"]) {
        sheetFull.rear_gear = [sheetInfoResult objectForKey:@"rear_gear"];
    }
    if ([sheetInfoResult objectForKey:@"cspring"]) {
        sheetFull.cspring = [sheetInfoResult objectForKey:@"cspring"];
        
    }
    if ([sheetInfoResult objectForKey:@"lap"]) {
        sheetFull.lap = [sheetInfoResult objectForKey:@"lap"];
    }
    if ([sheetInfoResult objectForKey:@"runtime"]) {
        sheetFull.runtime = [sheetInfoResult objectForKey:@"runtime"];
    }
    if ([sheetInfoResult objectForKey:@"result"]) {
        sheetFull.result = [sheetInfoResult objectForKey:@"result"];
    }
    if ([sheetInfoResult objectForKey:@"qual"]) {
        sheetFull.qual = [sheetInfoResult objectForKey:@"qual"];
    }
    
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"st24"]) {
        sheetFull.st24 = [sheetInfoResult objectForKey:@"st24"];
    }
    if ([sheetInfoResult objectForKey:@"am18"]) {
        sheetFull.am18 = [sheetInfoResult objectForKey:@"am18"];
    }
    if ([sheetInfoResult objectForKey:@"am26"]) {
        sheetFull.am26 = [sheetInfoResult objectForKey:@"am26"];
    }
    if ([sheetInfoResult objectForKey:@"am06s"]) {
        sheetFull.am06s = [sheetInfoResult objectForKey:@"am06s"];
    }
    if ([sheetInfoResult objectForKey:@"at21s"]) {
        sheetFull.at21s = [sheetInfoResult objectForKey:@"at21s"];
    }
    
    if ([sheetInfoResult objectForKey:@"ff_mount"]) {
        sheetFull.ff_mount = [sheetInfoResult objectForKey:@"ff_mount"];
    }
    if ([sheetInfoResult objectForKey:@"fr_mount"]) {
        sheetFull.fr_mount = [sheetInfoResult objectForKey:@"fr_mount"];
    }
    if ([sheetInfoResult objectForKey:@"front_spindle_mount"]) {
        sheetFull.front_spindle_mount = [sheetInfoResult objectForKey:@"front_spindle_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_low_arm"]) {
        sheetFull.front_low_arm = [sheetInfoResult objectForKey:@"front_low_arm"];
    }
    if ([sheetInfoResult objectForKey:@"front_steeringarm"]) {
        sheetFull.front_steeringarm = [sheetInfoResult objectForKey:@"front_steeringarm"];
    }
    if ([sheetInfoResult objectForKey:@"front_damper"]) {
        sheetFull.front_damper = [sheetInfoResult objectForKey:@"front_damper"];
    }
    if ([sheetInfoResult objectForKey:@"front_action"]) {
        sheetFull.front_action = [sheetInfoResult objectForKey:@"front_action"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_srs"]) {
        sheetFull.front_srs = [sheetInfoResult objectForKey:@"front_srs"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_drive"]) {
        sheetFull.front_drive = [sheetInfoResult objectForKey:@"front_drive"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_diffset"]) {
        sheetFull.front_diffset = [sheetInfoResult objectForKey:@"front_diffset"];
    }
    if ([sheetInfoResult objectForKey:@"front_dogbone"]) {
        sheetFull.front_dogbone = [sheetInfoResult objectForKey:@"front_dogbone"];
    }
    if ([sheetInfoResult objectForKey:@"rf_mount"]) {
        sheetFull.rf_mount = [sheetInfoResult objectForKey:@"rf_mount"];
    }
    if ([sheetInfoResult objectForKey:@"rr_mount"]) {
        sheetFull.rr_mount = [sheetInfoResult objectForKey:@"rr_mount"];
        
    }
    if ([sheetInfoResult objectForKey:@"rear_low_arm"]) {
        sheetFull.rear_low_arm = [sheetInfoResult objectForKey:@"rear_low_arm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_steeringarm"]) {
        sheetFull.rear_steeringarm = [sheetInfoResult objectForKey:@"rear_steeringarm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_damper"]) {
        sheetFull.rear_damper = [sheetInfoResult objectForKey:@"rear_damper"];
    }
    if ([sheetInfoResult objectForKey:@"rear_action"]) {
        sheetFull.rear_action = [sheetInfoResult objectForKey:@"rear_action"];
    }
    if ([sheetInfoResult objectForKey:@"rear_srs"]) {
        sheetFull.rear_srs = [sheetInfoResult objectForKey:@"rear_srs"];
    }
    if ([sheetInfoResult objectForKey:@"rear_drive"]) {
        sheetFull.rear_drive = [sheetInfoResult objectForKey:@"rear_drive"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_diffset"]) {
        sheetFull.rear_diffset = [sheetInfoResult objectForKey:@"rear_diffset"];
    }
    if ([sheetInfoResult objectForKey:@"rear_dogbone"]) {
        sheetFull.rear_dogbone = [sheetInfoResult objectForKey:@"rear_dogbone"];
    }
    
    if ([sheetInfoResult objectForKey:@"st27"]) {
        sheetFull.st27 = [sheetInfoResult objectForKey:@"st27"];
    }
    if ([sheetInfoResult objectForKey:@"front_five"]) {
        sheetFull.front_five = [sheetInfoResult objectForKey:@"front_five"];
    }
    if ([sheetInfoResult objectForKey:@"rear_eleven"]) {
        sheetFull.rear_eleven = [sheetInfoResult objectForKey:@"rear_eleven"];
    }
    if ([sheetInfoResult objectForKey:@"front_six"]) {
        sheetFull.front_six = [sheetInfoResult objectForKey:@"front_six"];
    }
    if ([sheetInfoResult objectForKey:@"center_eight"]) {
        sheetFull.center_eight = [sheetInfoResult objectForKey:@"center_eight"];
    }
    if ([sheetInfoResult objectForKey:@"rear_three"]) {
        sheetFull.rear_three = [sheetInfoResult objectForKey:@"rear_three"];
    }
    if ([sheetInfoResult objectForKey:@"center_holes"]) {
        sheetFull.center_holes = [sheetInfoResult objectForKey:@"center_holes"];
    }
    if ([sheetInfoResult objectForKey:@"ackerman_position"]) {
        sheetFull.ackerman_position = [sheetInfoResult objectForKey:@"ackerman_position"];
    }
    if ([sheetInfoResult objectForKey:@"am24"]) {
        sheetFull.am24 = [sheetInfoResult objectForKey:@"am24"];
    }
    if ([sheetInfoResult objectForKey:@"fl_tire"]) {
        sheetFull.fl_tire = [sheetInfoResult objectForKey:@"fl_tire"];
    }
    if ([sheetInfoResult objectForKey:@"fr_tire"]) {
        sheetFull.fr_tire = [sheetInfoResult objectForKey:@"fr_tire"];
    }
    if ([sheetInfoResult objectForKey:@"rl_tire"]) {
        sheetFull.rl_tire = [sheetInfoResult objectForKey:@"rl_tire"];
    }
    
    if ([sheetInfoResult objectForKey:@"rr_tire"]) {
        sheetFull.rr_tire = [sheetInfoResult objectForKey:@"rr_tire"];
    }
    if ([sheetInfoResult objectForKey:@"motor_layout"]) {
        sheetFull.motor_layout = [sheetInfoResult objectForKey:@"motor_layout"];
        
    }
    if ([sheetInfoResult objectForKey:@"servo_layout"]) {
        sheetFull.servo_layout = [sheetInfoResult objectForKey:@"servo_layout"];
    }
    if ([sheetInfoResult objectForKey:@"esc_layout"]) {
        sheetFull.esc_layout = [sheetInfoResult objectForKey:@"esc_layout"];
    }
    if ([sheetInfoResult objectForKey:@"bat_layout"]) {
        sheetFull.bat_layout = [sheetInfoResult objectForKey:@"bat_layout"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_1"]) {
        sheetFull.front_spacer_1 = [sheetInfoResult objectForKey:@"front_spacer_1"];
    }

    if ([sheetInfoResult objectForKey:@"front_spacer_2"]) {
        sheetFull.front_spacer_2 = [sheetInfoResult objectForKey:@"front_spacer_2"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_3"]) {
        sheetFull.front_spacer_3 = [sheetInfoResult objectForKey:@"front_spacer_3"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_4"]) {
        sheetFull.front_spacer_4 = [sheetInfoResult objectForKey:@"front_spacer_4"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_5"]) {
        sheetFull.front_spacer_5 = [sheetInfoResult objectForKey:@"front_spacer_5"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_6"]) {
        sheetFull.front_spacer_6 = [sheetInfoResult objectForKey:@"front_spacer_6"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_7"]) {
        sheetFull.front_spacer_7 = [sheetInfoResult objectForKey:@"front_spacer_7"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_gfmm"]) {
        sheetFull.front_gfmm = [sheetInfoResult objectForKey:@"front_gfmm"];
    }
    if ([sheetInfoResult objectForKey:@"front_spacer_8"]) {
        sheetFull.front_spacer_8 = [sheetInfoResult objectForKey:@"front_spacer_8"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_spacer_9"]) {
        sheetFull.front_spacer_9 = [sheetInfoResult objectForKey:@"front_spacer_9"];
    }
    if ([sheetInfoResult objectForKey:@"front_upstop"]) {
        sheetFull.front_upstop = [sheetInfoResult objectForKey:@"front_upstop"];
    }
    if ([sheetInfoResult objectForKey:@"front_stabilizer"]) {
        sheetFull.front_stabilizer = [sheetInfoResult objectForKey:@"front_stabilizer"];
    }
    if ([sheetInfoResult objectForKey:@"front_lower_arm"]) {
        sheetFull.front_lower_arm = [sheetInfoResult objectForKey:@"front_lower_arm"];
    }
    if ([sheetInfoResult objectForKey:@"front_steerarm"]) {
        sheetFull.front_steerarm = [sheetInfoResult objectForKey:@"front_steerarm"];
    }
    if ([sheetInfoResult objectForKey:@"f_spring"]) {
        sheetFull.f_spring = [sheetInfoResult objectForKey:@"f_spring"];
    }
    if ([sheetInfoResult objectForKey:@"f_damper"]) {
        sheetFull.f_damper = [sheetInfoResult objectForKey:@"f_damper"];
    }
    if ([sheetInfoResult objectForKey:@"front_diffoil"]) {
        sheetFull.front_diffoil = [sheetInfoResult objectForKey:@"front_diffoil"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_1"]) {
        sheetFull.rear_spacer_1 = [sheetInfoResult objectForKey:@"rear_spacer_1"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_2"]) {
        sheetFull.rear_spacer_2 = [sheetInfoResult objectForKey:@"rear_spacer_2"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_3"]) {
        sheetFull.rear_spacer_3 = [sheetInfoResult objectForKey:@"rear_spacer_3"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_4"]) {
        sheetFull.rear_spacer_4 = [sheetInfoResult objectForKey:@"rear_spacer_4"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_spacer_5"]) {
        sheetFull.rear_spacer_5 = [sheetInfoResult objectForKey:@"rear_spacer_5"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_6"]) {
        sheetFull.rear_spacer_6 = [sheetInfoResult objectForKey:@"rear_spacer_6"];
        
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_7"]) {
        sheetFull.rear_spacer_7 = [sheetInfoResult objectForKey:@"rear_spacer_7"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_8"]) {
        sheetFull.rear_spacer_8 = [sheetInfoResult objectForKey:@"rear_spacer_8"];
    }
    if ([sheetInfoResult objectForKey:@"rear_gfmm"]) {
        sheetFull.rear_gfmm = [sheetInfoResult objectForKey:@"rear_gfmm"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_spacer_9"]) {
        sheetFull.rear_spacer_9 = [sheetInfoResult objectForKey:@"rear_spacer_9"];
    }
    if ([sheetInfoResult objectForKey:@"rear_spacer_10"]) {
        sheetFull.rear_spacer_10 = [sheetInfoResult objectForKey:@"rear_spacer_10"];
        
    }
    
    
    if ([sheetInfoResult objectForKey:@"rear_caster"]) {
        sheetFull.rear_caster = [sheetInfoResult objectForKey:@"rear_caster"];
    }
    if ([sheetInfoResult objectForKey:@"rear_upstop"]) {
        sheetFull.rear_upstop = [sheetInfoResult objectForKey:@"rear_upstop"];
    }
    if ([sheetInfoResult objectForKey:@"rear_stabilizer"]) {
        sheetFull.rear_stabilizer = [sheetInfoResult objectForKey:@"rear_stabilizer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_steerarm"]) {
        sheetFull.rear_steerarm = [sheetInfoResult objectForKey:@"rear_steerarm"];
    }
    if ([sheetInfoResult objectForKey:@"r_spring"]) {
        sheetFull.r_spring = [sheetInfoResult objectForKey:@"r_spring"];
    }
    if ([sheetInfoResult objectForKey:@"r_damper"]) {
        sheetFull.r_damper = [sheetInfoResult objectForKey:@"r_damper"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diffoil"]) {
        sheetFull.rear_diffoil = [sheetInfoResult objectForKey:@"rear_diffoil"];
    }
    if ([sheetInfoResult objectForKey:@"topdeck_info"]) {
        sheetFull.topdeck_info = [sheetInfoResult objectForKey:@"topdeck_info"];
    }
    if ([sheetInfoResult objectForKey:@"steering_washer"]) {
        sheetFull.steering_washer = [sheetInfoResult objectForKey:@"steering_washer"];
    }
    if ([sheetInfoResult objectForKey:@"front_additive"]) {
        sheetFull.front_additive = [sheetInfoResult objectForKey:@"front_additive"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_additive"]) {
        sheetFull.rear_additive = [sheetInfoResult objectForKey:@"rear_additive"];
    }
    if ([sheetInfoResult objectForKey:@"fdr"]) {
        sheetFull.fdr = [sheetInfoResult objectForKey:@"fdr"];
        
    }
    if ([sheetInfoResult objectForKey:@"offset"]) {
        sheetFull.offset = [sheetInfoResult objectForKey:@"offset"];
    }
    if ([sheetInfoResult objectForKey:@"final"]) {
        sheetFull.final = [sheetInfoResult objectForKey:@"final"];
    }
    if ([sheetInfoResult objectForKey:@"esc_setting"]) {
        sheetFull.esc_setting = [sheetInfoResult objectForKey:@"esc_setting"];
    }
    if ([sheetInfoResult objectForKey:@"contact"]) {
        sheetFull.contact = [sheetInfoResult objectForKey:@"contact"];
    }
 
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"front_caster"]) {
        sheetFull.front_casters = [sheetInfoResult objectForKey:@"front_caster"];
    }
    if ([sheetInfoResult objectForKey:@"front_trail"]) {
        sheetFull.front_trail = [sheetInfoResult objectForKey:@"front_trail"];
    }
    if ([sheetInfoResult objectForKey:@"front_kick"]) {
        sheetFull.front_kick = [sheetInfoResult objectForKey:@"front_kick"];
    }
    if ([sheetInfoResult objectForKey:@"front_pivot"]) {
        sheetFull.front_pivot = [sheetInfoResult objectForKey:@"front_pivot"];
    }
    if ([sheetInfoResult objectForKey:@"front_kick_shim"]) {
        sheetFull.front_kick_shim = [sheetInfoResult objectForKey:@"front_kick_shim"];
    }
    if ([sheetInfoResult objectForKey:@"front_bump_stud"]) {
        sheetFull.front_bump_stud = [sheetInfoResult objectForKey:@"front_bump_stud"];
    }
    if ([sheetInfoResult objectForKey:@"front_ack_stud"]) {
        sheetFull.front_ack_stud = [sheetInfoResult objectForKey:@"front_ack_stud"];
    }
    if ([sheetInfoResult objectForKey:@"chassi"]) {
        sheetFull.chassi = [sheetInfoResult objectForKey:@"chassi"];
    }
    if ([sheetInfoResult objectForKey:@"rollcenter"]) {
        sheetFull.rollcenter = [sheetInfoResult objectForKey:@"rollcenter"];
    }
    if ([sheetInfoResult objectForKey:@"front_height"]) {
        sheetFull.front_height = [sheetInfoResult objectForKey:@"front_height"];
    }
    if ([sheetInfoResult objectForKey:@"front_color"]) {
        sheetFull.front_color = [sheetInfoResult objectForKey:@"front_color"];
    }
    if ([sheetInfoResult objectForKey:@"front_limiters"]) {
        sheetFull.front_limiters = [sheetInfoResult objectForKey:@"front_limiters"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_location"]) {
        sheetFull.front_location = [sheetInfoResult objectForKey:@"front_location"];
    }
    if ([sheetInfoResult objectForKey:@"front_bump"]) {
        sheetFull.front_bump = [sheetInfoResult objectForKey:@"front_bump"];
        
    }
    if ([sheetInfoResult objectForKey:@"front_ack"]) {
        sheetFull.front_ack = [sheetInfoResult objectForKey:@"front_ack"];
    }
    if ([sheetInfoResult objectForKey:@"front_camlink"]) {
        sheetFull.front_camlink = [sheetInfoResult objectForKey:@"front_camlink"];
    }
    if ([sheetInfoResult objectForKey:@"spacer1"]) {
        sheetFull.spacer1 = [sheetInfoResult objectForKey:@"spacer1"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer2"]) {
        sheetFull.spacer2 = [sheetInfoResult objectForKey:@"spacer2"];
    }
    if ([sheetInfoResult objectForKey:@"spacer3"]) {
        sheetFull.spacer3 = [sheetInfoResult objectForKey:@"spacer3"];
        
    }
 
    if ([sheetInfoResult objectForKey:@"spacer4"]) {
        sheetFull.spacer4 = [sheetInfoResult objectForKey:@"spacer4"];
    }
    if ([sheetInfoResult objectForKey:@"rear_height"]) {
        sheetFull.rear_height = [sheetInfoResult objectForKey:@"rear_height"];
    }
    if ([sheetInfoResult objectForKey:@"rear_hub"]) {
        sheetFull.rear_hubString = [sheetInfoResult objectForKey:@"rear_hub"];
    }
    if ([sheetInfoResult objectForKey:@"hub_width"]) {
        sheetFull.hub_width = [sheetInfoResult objectForKey:@"hub_width"];
    }
    if ([sheetInfoResult objectForKey:@"rear_color"]) {
        sheetFull.rear_color = [sheetInfoResult objectForKey:@"rear_color"];
    }
    if ([sheetInfoResult objectForKey:@"rear_limiters"]) {
        sheetFull.rear_limiters = [sheetInfoResult objectForKey:@"rear_limiters"];
    }
    if ([sheetInfoResult objectForKey:@"rear_diffoil"]) {
        sheetFull.rear_diffoil = [sheetInfoResult objectForKey:@"rear_diffoil"];
    }
    if ([sheetInfoResult objectForKey:@"rear_camlink"]) {
        sheetFull.rear_camlink = [sheetInfoResult objectForKey:@"rear_camlink"];
    }
    if ([sheetInfoResult objectForKey:@"rear_location"]) {
        sheetFull.rear_location = [sheetInfoResult objectForKey:@"rear_location"];
    }
    if ([sheetInfoResult objectForKey:@"batt_position"]) {
        sheetFull.batt_position = [sheetInfoResult objectForKey:@"batt_position"];
    }
    
    if ([sheetInfoResult objectForKey:@"spacer5"]) {
        sheetFull.spacer5 = [sheetInfoResult objectForKey:@"spacer5"];
    }
    if ([sheetInfoResult objectForKey:@"spacer6"]) {
        sheetFull.spacer6 = [sheetInfoResult objectForKey:@"spacer6"];
        
    }
    if ([sheetInfoResult objectForKey:@"brake"]) {
        sheetFull.brake = [sheetInfoResult objectForKey:@"brake"];
    }
    if ([sheetInfoResult objectForKey:@"drag_brake"]) {
        sheetFull.drag_brake = [sheetInfoResult objectForKey:@"drag_brake"];
    }
    if ([sheetInfoResult objectForKey:@"throttle_profile"]) {
        sheetFull.throttle_profile = [sheetInfoResult objectForKey:@"throttle_profile"];
    }
    if ([sheetInfoResult objectForKey:@"throttle_brake"]) {
        sheetFull.throttle_brake = [sheetInfoResult objectForKey:@"throttle_brake"];
    }
    if ([sheetInfoResult objectForKey:@"servo_expo"]) {
        sheetFull.servo_expo = [sheetInfoResult objectForKey:@"servo_expo"];
    }
    if ([sheetInfoResult objectForKey:@"brake_epa"]) {
        sheetFull.brake_epa = [sheetInfoResult objectForKey:@"brake_epa"];
    }
    if ([sheetInfoResult objectForKey:@"add_front"]) {
        sheetFull.add_front = [sheetInfoResult objectForKey:@"add_front"];
    }
    if ([sheetInfoResult objectForKey:@"add_rear"]) {
        sheetFull.add_rear = [sheetInfoResult objectForKey:@"add_rear"];
    }
    
    
    
    
    

    if ([sheetInfoResult objectForKey:@"bulkhead"]) {
        sheetFull.bulkhead = [sheetInfoResult objectForKey:@"bulkhead"];
    }
    if ([sheetInfoResult objectForKey:@"bulkhead_in"]) {
        sheetFull.bulkhead_in = [sheetInfoResult objectForKey:@"bulkhead_in"];
    }
    if ([sheetInfoResult objectForKey:@"cbinserts"]) {
        sheetFull.cbinserts = [sheetInfoResult objectForKey:@"cbinserts"];
    }
    if ([sheetInfoResult objectForKey:@"cbinserts_deg"]) {
        sheetFull.cbinserts_deg = [sheetInfoResult objectForKey:@"cbinserts_deg"];
    }
    if ([sheetInfoResult objectForKey:@"spindles"]) {
        sheetFull.spindles = [sheetInfoResult objectForKey:@"spindles"];
    }
    if ([sheetInfoResult objectForKey:@"toe_block"]) {
        sheetFull.toe_block = [sheetInfoResult objectForKey:@"toe_block"];
    }
    if ([sheetInfoResult objectForKey:@"fl_block"]) {
        sheetFull.fl_block = [sheetInfoResult objectForKey:@"fl_block"];
    }
    if ([sheetInfoResult objectForKey:@"fr_block"]) {
        sheetFull.fr_block = [sheetInfoResult objectForKey:@"fr_block"];
    }
    if ([sheetInfoResult objectForKey:@"rl_block"]) {
        sheetFull.rl_block = [sheetInfoResult objectForKey:@"rl_block"];
    }
    if ([sheetInfoResult objectForKey:@"rr_block"]) {
        sheetFull.rr_block = [sheetInfoResult objectForKey:@"rr_block"];
    }
    if ([sheetInfoResult objectForKey:@"bulk_weight"]) {
        sheetFull.bulk_weight = [sheetInfoResult objectForKey:@"bulk_weight"];
    }
    if ([sheetInfoResult objectForKey:@"hex"]) {
        sheetFull.hex = [sheetInfoResult objectForKey:@"hex"];
    }
    
    if ([sheetInfoResult objectForKey:@"toe_type"]) {
        sheetFull.toe_type = [sheetInfoResult objectForKey:@"toe_type"];
    }
    if ([sheetInfoResult objectForKey:@"caster_stud"]) {
        sheetFull.caster_stud = [sheetInfoResult objectForKey:@"caster_stud"];
        
    }
    if ([sheetInfoResult objectForKey:@"tower_mount"]) {
        sheetFull.tower_mount = [sheetInfoResult objectForKey:@"tower_mount"];
    }
    if ([sheetInfoResult objectForKey:@"out_link"]) {
        sheetFull.out_link = [sheetInfoResult objectForKey:@"out_link"];
    }
    if ([sheetInfoResult objectForKey:@"arm_mount"]) {
        sheetFull.arm_mount = [sheetInfoResult objectForKey:@"arm_mount"];
    }
    
    if ([sheetInfoResult objectForKey:@"in_link"]) {
        sheetFull.in_link = [sheetInfoResult objectForKey:@"in_link"];
    }
    if ([sheetInfoResult objectForKey:@"hub_mount"]) {
        sheetFull.hub_mount = [sheetInfoResult objectForKey:@"hub_mount"];
        
    }
    
    if ([sheetInfoResult objectForKey:@"rear_inner"]) {
        sheetFull.rear_inner = [sheetInfoResult objectForKey:@"rear_inner"];
    }
    if ([sheetInfoResult objectForKey:@"swaybar_mount"]) {
        sheetFull.swaybar_mount = [sheetInfoResult objectForKey:@"swaybar_mount"];
    }
    if ([sheetInfoResult objectForKey:@"shock_mount"]) {
        sheetFull.shock_mount = [sheetInfoResult objectForKey:@"shock_mount"];
    }
    if ([sheetInfoResult objectForKey:@"out_hub"]) {
        sheetFull.out_hub = [sheetInfoResult objectForKey:@"out_hub"];
    }
    if ([sheetInfoResult objectForKey:@"front_barsize"]) {
        sheetFull.front_barsize = [sheetInfoResult objectForKey:@"front_barsize"];
    }
    if ([sheetInfoResult objectForKey:@"front_toedegrees"]) {
        sheetFull.front_toedegrees = [sheetInfoResult objectForKey:@"front_toedegrees"];
    }
    if ([sheetInfoResult objectForKey:@"caster_shims"]) {
        sheetFull.caster_shims = [sheetInfoResult objectForKey:@"caster_shims"];
    }
    if ([sheetInfoResult objectForKey:@"bump_shims"]) {
        sheetFull.bump_shims = [sheetInfoResult objectForKey:@"bump_shims"];
    }
    if ([sheetInfoResult objectForKey:@"inner_shims"]) {
        sheetFull.inner_shims = [sheetInfoResult objectForKey:@"inner_shims"];
    }
    if ([sheetInfoResult objectForKey:@"rear_barsize"]) {
        sheetFull.rear_barsize = [sheetInfoResult objectForKey:@"rear_barsize"];
    }
    
    if ([sheetInfoResult objectForKey:@"hub_shims"]) {
        sheetFull.hub_shims = [sheetInfoResult objectForKey:@"hub_shims"];
    }
    if ([sheetInfoResult objectForKey:@"rear_toedegrees"]) {
        sheetFull.rear_toedegrees = [sheetInfoResult objectForKey:@"rear_toedegrees"];
        
    }
    if ([sheetInfoResult objectForKey:@"rear_squat"]) {
        sheetFull.rear_squat = [sheetInfoResult objectForKey:@"rear_squat"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shims"]) {
        sheetFull.rear_shims = [sheetInfoResult objectForKey:@"rear_shims"];
    }
    if ([sheetInfoResult objectForKey:@"pistonf"]) {
        sheetFull.pistonf = [sheetInfoResult objectForKey:@"pistonf"];
    }
    if ([sheetInfoResult objectForKey:@"pistonf_holes"]) {
        sheetFull.pistonf_holes = [sheetInfoResult objectForKey:@"pistonf_holes"];
    }
    if ([sheetInfoResult objectForKey:@"pistonr"]) {
        sheetFull.pistonr = [sheetInfoResult objectForKey:@"pistonr"];
    }
    if ([sheetInfoResult objectForKey:@"pistonr_holes"]) {
        sheetFull.pistonr_holes = [sheetInfoResult objectForKey:@"pistonr_holes"];
    }
    if ([sheetInfoResult objectForKey:@"oilf"]) {
        sheetFull.oilf = [sheetInfoResult objectForKey:@"oilf"];
    }
    if ([sheetInfoResult objectForKey:@"oilr"]) {
        sheetFull.oilr = [sheetInfoResult objectForKey:@"oilr"];
    }
    if ([sheetInfoResult objectForKey:@"springf"]) {
        sheetFull.springf = [sheetInfoResult objectForKey:@"springf"];
    }
    if ([sheetInfoResult objectForKey:@"springr"]) {
        sheetFull.springr = [sheetInfoResult objectForKey:@"springr"];
    }
    if ([sheetInfoResult objectForKey:@"limitersf"]) {
        sheetFull.limitersf = [sheetInfoResult objectForKey:@"limitersf"];
    }
    if ([sheetInfoResult objectForKey:@"limitersr"]) {
        sheetFull.limitersr = [sheetInfoResult objectForKey:@"limitersr"];
    }
    if ([sheetInfoResult objectForKey:@"lengthf"]) {
        sheetFull.lengthf = [sheetInfoResult objectForKey:@"lengthf"];
    }
    if ([sheetInfoResult objectForKey:@"lengthr"]) {
        sheetFull.lengthr = [sheetInfoResult objectForKey:@"lengthr"];
    }
    if ([sheetInfoResult objectForKey:@"reboundf"]) {
        sheetFull.reboundf = [sheetInfoResult objectForKey:@"reboundf"];
    }
    if ([sheetInfoResult objectForKey:@"reboundr"]) {
        sheetFull.reboundr = [sheetInfoResult objectForKey:@"reboundr"];
    }
    if ([sheetInfoResult objectForKey:@"bladderf"]) {
        sheetFull.bladderf = [sheetInfoResult objectForKey:@"bladderf"];
    }
    if ([sheetInfoResult objectForKey:@"bladderr"]) {
        sheetFull.bladderr = [sheetInfoResult objectForKey:@"bladderr"];
    }
    if ([sheetInfoResult objectForKey:@"bucketf"]) {
        sheetFull.bucketf = [sheetInfoResult objectForKey:@"bucketf"];
    }
    if ([sheetInfoResult objectForKey:@"bucketr"]) {
        sheetFull.bucketr = [sheetInfoResult objectForKey:@"bucketr"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_bias"]) {
        sheetFull.front_bias = [sheetInfoResult objectForKey:@"front_bias"];
    }
    if ([sheetInfoResult objectForKey:@"rear_bias"]) {
        sheetFull.rear_bias = [sheetInfoResult objectForKey:@"rear_bias"];
        
    }
    if ([sheetInfoResult objectForKey:@"distance"]) {
        sheetFull.distance = [sheetInfoResult objectForKey:@"distance"];
    }
    if ([sheetInfoResult objectForKey:@"ratio"]) {
        sheetFull.ratio = [sheetInfoResult objectForKey:@"ratio"];
    }
    if ([sheetInfoResult objectForKey:@"finaldrive"]) {
        sheetFull.finaldrive = [sheetInfoResult objectForKey:@"finaldrive"];
    }
    
    if ([sheetInfoResult objectForKey:@"diffoil"]) {
        sheetFull.diffoil = [sheetInfoResult objectForKey:@"diffoil"];
    }
    if ([sheetInfoResult objectForKey:@"wing_rear"]) {
        sheetFull.wing_rear = [sheetInfoResult objectForKey:@"wing_rear"];
        
    }
    
    if ([sheetInfoResult objectForKey:@"wing_width"]) {
        sheetFull.wing_width = [sheetInfoResult objectForKey:@"wing_width"];
    }
    if ([sheetInfoResult objectForKey:@"wing_front"]) {
        sheetFull.wing_front = [sheetInfoResult objectForKey:@"wing_front"];
    }
    if ([sheetInfoResult objectForKey:@"front_comp"]) {
        sheetFull.front_comp = [sheetInfoResult objectForKey:@"front_comp"];
    }
    if ([sheetInfoResult objectForKey:@"front_add"]) {
        sheetFull.front_add = [sheetInfoResult objectForKey:@"front_add"];
    }
    if ([sheetInfoResult objectForKey:@"rear_comp"]) {
        sheetFull.rear_comp = [sheetInfoResult objectForKey:@"rear_comp"];
    }
    if ([sheetInfoResult objectForKey:@"rear_add"]) {
        sheetFull.rear_add = [sheetInfoResult objectForKey:@"rear_add"];
    }
    if ([sheetInfoResult objectForKey:@"wing_angle"]) {
        sheetFull.wing_angles = [sheetInfoResult objectForKey:@"wing_angle"];
    }
    
    
    


    if ([sheetInfoResult objectForKey:@"front_axle"]) {
        sheetFull.front_axle = [sheetInfoResult objectForKey:@"front_axle"];
    }
    if ([sheetInfoResult objectForKey:@"hub_spacing"]) {
        sheetFull.hub_spacing = [sheetInfoResult objectForKey:@"hub_spacing"];
    }
    if ([sheetInfoResult objectForKey:@"rollbar"]) {
        sheetFull.rollbar = [sheetInfoResult objectForKey:@"rollbar"];
    }
    if ([sheetInfoResult objectForKey:@"a_up"]) {
        sheetFull.a_up = [sheetInfoResult objectForKey:@"a_up"];
    }
    if ([sheetInfoResult objectForKey:@"b_up"]) {
        sheetFull.b_up = [sheetInfoResult objectForKey:@"b_up"];
    }
    if ([sheetInfoResult objectForKey:@"difftype"]) {
        sheetFull.difftype = [sheetInfoResult objectForKey:@"difftype"];
    }
    if ([sheetInfoResult objectForKey:@"size"]) {
        sheetFull.size = [sheetInfoResult objectForKey:@"size"];
    }
    if ([sheetInfoResult objectForKey:@"surface"]) {
        sheetFull.surface = [sheetInfoResult objectForKey:@"surface"];
    }
    if ([sheetInfoResult objectForKey:@"traction"]) {
        sheetFull.traction = [sheetInfoResult objectForKey:@"traction"];
    }
    if ([sheetInfoResult objectForKey:@"moisture"]) {
        sheetFull.moisture = [sheetInfoResult objectForKey:@"moisture"];
    }
    if ([sheetInfoResult objectForKey:@"condition"]) {
        sheetFull.condition = [sheetInfoResult objectForKey:@"condition"];
    }
    if ([sheetInfoResult objectForKey:@"front_instud"]) {
        sheetFull.front_instud = [sheetInfoResult objectForKey:@"front_instud"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_armmount"]) {
        sheetFull.front_armmount = [sheetInfoResult objectForKey:@"front_armmount"];
    }
    if ([sheetInfoResult objectForKey:@"front_outstud"]) {
        sheetFull.front_outstud = [sheetInfoResult objectForKey:@"front_outstud"];
        
    }
    if ([sheetInfoResult objectForKey:@"a_hub"]) {
        sheetFull.a_hub = [sheetInfoResult objectForKey:@"a_hub"];
    }
    if ([sheetInfoResult objectForKey:@"b_hub"]) {
        sheetFull.b_hub = [sheetInfoResult objectForKey:@"b_hub"];
    }
    if ([sheetInfoResult objectForKey:@"rear_instud"]) {
        sheetFull.rear_instud = [sheetInfoResult objectForKey:@"rear_instud"];
    }
    
    if ([sheetInfoResult objectForKey:@"rear_armmount"]) {
        sheetFull.rear_armmount = [sheetInfoResult objectForKey:@"rear_armmount"];
    }
    if ([sheetInfoResult objectForKey:@"front_armtype"]) {
        sheetFull.front_armtype = [sheetInfoResult objectForKey:@"front_armtype"];
        
    }
    
    if ([sheetInfoResult objectForKey:@"front_towertype"]) {
        sheetFull.front_towertype = [sheetInfoResult objectForKey:@"front_towertype"];
    }
    if ([sheetInfoResult objectForKey:@"caster_insert"]) {
        sheetFull.caster_insert = [sheetInfoResult objectForKey:@"caster_insert"];
    }
    if ([sheetInfoResult objectForKey:@"steering_stop"]) {
        sheetFull.steering_stop = [sheetInfoResult objectForKey:@"steering_stop"];
    }
    if ([sheetInfoResult objectForKey:@"cbs1"]) {
        sheetFull.cbs1 = [sheetInfoResult objectForKey:@"cbs1"];
    }
    if ([sheetInfoResult objectForKey:@"cbs2"]) {
        sheetFull.cbs2 = [sheetInfoResult objectForKey:@"cbs2"];
    }
    if ([sheetInfoResult objectForKey:@"front_ackstud"]) {
        sheetFull.front_ackstud = [sheetInfoResult objectForKey:@"front_ackstud"];
    }
    if ([sheetInfoResult objectForKey:@"front_instud_spacer"]) {
        sheetFull.front_instud_spacer = [sheetInfoResult objectForKey:@"front_instud_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"rear_instud_spacer"]) {
        sheetFull.rear_instud_spacer = [sheetInfoResult objectForKey:@"rear_instud_spacer"];
    }
    if ([sheetInfoResult objectForKey:@"throttle_epa"]) {
        sheetFull.throttle_epa = [sheetInfoResult objectForKey:@"throttle_epa"];
    }
    if ([sheetInfoResult objectForKey:@"wind"]) {
        sheetFull.wind = [sheetInfoResult objectForKey:@"wind"];
    }
    
    if ([sheetInfoResult objectForKey:@"setting"]) {
        sheetFull.setting = [sheetInfoResult objectForKey:@"setting"];
    }
    if ([sheetInfoResult objectForKey:@"clutch_pads"]) {
        sheetFull.clutch_pads = [sheetInfoResult objectForKey:@"clutch_pads"];
        
    }
    if ([sheetInfoResult objectForKey:@"clutch_notes"]) {
        sheetFull.clutch_notes = [sheetInfoResult objectForKey:@"clutch_notes"];
    }
    if ([sheetInfoResult objectForKey:@"front_cup"]) {
        sheetFull.front_cup = [sheetInfoResult objectForKey:@"front_cup"];
    }
    if ([sheetInfoResult objectForKey:@"rear_cup"]) {
        sheetFull.rear_cup = [sheetInfoResult objectForKey:@"rear_cup"];
    }
    if ([sheetInfoResult objectForKey:@"front_stroke"]) {
        sheetFull.front_stroke = [sheetInfoResult objectForKey:@"front_stroke"];
    }
    if ([sheetInfoResult objectForKey:@"rear_stroke"]) {
        sheetFull.rear_stroke = [sheetInfoResult objectForKey:@"rear_stroke"];
    }
    if ([sheetInfoResult objectForKey:@"front_free"]) {
        sheetFull.front_free = [sheetInfoResult objectForKey:@"front_free"];
    }
    if ([sheetInfoResult objectForKey:@"rear_free"]) {
        sheetFull.rear_free = [sheetInfoResult objectForKey:@"rear_free"];
    }
    if ([sheetInfoResult objectForKey:@"shock_note"]) {
        sheetFull.shock_note = [sheetInfoResult objectForKey:@"shock_note"];
    }
    if ([sheetInfoResult objectForKey:@"ambient"]) {
        sheetFull.ambient = [sheetInfoResult objectForKey:@"ambient"];
    }
    if ([sheetInfoResult objectForKey:@"track_temp"]) {
        sheetFull.track_temp = [sheetInfoResult objectForKey:@"track_temp"];
    }
    if ([sheetInfoResult objectForKey:@"track_note"]) {
        sheetFull.track_note = [sheetInfoResult objectForKey:@"track_note"];
    }
    if ([sheetInfoResult objectForKey:@"wheel_front"]) {
        sheetFull.wheel_front = [sheetInfoResult objectForKey:@"wheel_front"];
    }
    if ([sheetInfoResult objectForKey:@"wheel_rear"]) {
        sheetFull.wheel_rear = [sheetInfoResult objectForKey:@"wheel_rear"];
    }
    if ([sheetInfoResult objectForKey:@"tire_note"]) {
        sheetFull.tire_note = [sheetInfoResult objectForKey:@"tire_note"];
    }
    if ([sheetInfoResult objectForKey:@"wing_notes"]) {
        sheetFull.wing_notes = [sheetInfoResult objectForKey:@"wing_notes"];
    }
    if ([sheetInfoResult objectForKey:@"body_notes"]) {
        sheetFull.body_notes = [sheetInfoResult objectForKey:@"body_notes"];
    }
    if ([sheetInfoResult objectForKey:@"general_notes"]) {
        sheetFull.general_notes = [sheetInfoResult objectForKey:@"general_notes"];
    }
    
    
    
    
    if ([sheetInfoResult objectForKey:@"ack_hole"]) {
        sheetFull.ack_hole = [sheetInfoResult objectForKey:@"ack_hole"];
    }
    if ([sheetInfoResult objectForKey:@"sweep"]) {
        sheetFull.sweep = [sheetInfoResult objectForKey:@"sweep"];
    }
    if ([sheetInfoResult objectForKey:@"front_posttype"]) {
        sheetFull.front_posttype = [sheetInfoResult objectForKey:@"front_posttype"];
    }
    if ([sheetInfoResult objectForKey:@"front_posthieght"]) {
        sheetFull.front_posthieght = [sheetInfoResult objectForKey:@"front_posthieght"];
    }
    if ([sheetInfoResult objectForKey:@"rear_posttype"]) {
        sheetFull.rear_posttype = [sheetInfoResult objectForKey:@"rear_posttype"];
    }
    if ([sheetInfoResult objectForKey:@"rear_posthieght"]) {
        sheetFull.rear_posthieght = [sheetInfoResult objectForKey:@"rear_posthieght"];
    }
    if ([sheetInfoResult objectForKey:@"hub_top"]) {
        sheetFull.hub_top = [sheetInfoResult objectForKey:@"hub_top"];
    }
    if ([sheetInfoResult objectForKey:@"hub_bot"]) {
        sheetFull.hub_bot = [sheetInfoResult objectForKey:@"hub_bot"];
    }
    if ([sheetInfoResult objectForKey:@"squat"]) {
        sheetFull.squat = [sheetInfoResult objectForKey:@"squat"];
    }
    if ([sheetInfoResult objectForKey:@"brakes"]) {
        sheetFull.brakes = [sheetInfoResult objectForKey:@"brakes"];
    }
    if ([sheetInfoResult objectForKey:@"ack_top"]) {
        sheetFull.ack_top = [sheetInfoResult objectForKey:@"ack_top"];
    }
    if ([sheetInfoResult objectForKey:@"ack_bot"]) {
        sheetFull.ack_bot = [sheetInfoResult objectForKey:@"ack_bot"];
    }
    if ([sheetInfoResult objectForKey:@"bump_top"]) {
        sheetFull.bump_top = [sheetInfoResult objectForKey:@"bump_top"];
    }
    if ([sheetInfoResult objectForKey:@"bump_bot"]) {
        sheetFull.bump_bot = [sheetInfoResult objectForKey:@"bump_bot"];
    }
    if ([sheetInfoResult objectForKey:@"saver"]) {
        sheetFull.saver = [sheetInfoResult objectForKey:@"saver"];
    }
    if ([sheetInfoResult objectForKey:@"front_shocktype"]) {
        sheetFull.front_shocktype = [sheetInfoResult objectForKey:@"front_shocktype"];
    }
    if ([sheetInfoResult objectForKey:@"rear_shocktype"]) {
        sheetFull.rear_shocktype = [sheetInfoResult objectForKey:@"rear_shocktype"];
    }
    
    
    
    
    
    
    
    
    
    
    if ([sheetInfoResult objectForKey:@"front_link"]) {
        sheetFull.front_link = [sheetInfoResult objectForKey:@"front_link"];
    }
    if ([sheetInfoResult objectForKey:@"rear_link"]) {
        sheetFull.rear_link = [sheetInfoResult objectForKey:@"rear_link"];
    }
    if ([sheetInfoResult objectForKey:@"hub_link"]) {
        sheetFull.hub_link = [sheetInfoResult objectForKey:@"hub_link"];
    }
    if ([sheetInfoResult objectForKey:@"front_arm"]) {
        sheetFull.front_arm = [sheetInfoResult objectForKey:@"front_arm"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm"]) {
        sheetFull.rear_arm = [sheetInfoResult objectForKey:@"rear_arm"];
    }
    if ([sheetInfoResult objectForKey:@"hub_hole"]) {
        sheetFull.hub_hole = [sheetInfoResult objectForKey:@"hub_hole"];
    }
    if ([sheetInfoResult objectForKey:@"position"]) {
        sheetFull.position = [sheetInfoResult objectForKey:@"position"];
    }
    if ([sheetInfoResult objectForKey:@"ackside"]) {
        sheetFull.ackside = [sheetInfoResult objectForKey:@"ackside"];
    }
    if ([sheetInfoResult objectForKey:@"ackplus"]) {
        sheetFull.ackplus = [sheetInfoResult objectForKey:@"ackplus"];
    }
    if ([sheetInfoResult objectForKey:@"winglevel"]) {
        sheetFull.winglevel = [sheetInfoResult objectForKey:@"winglevel"];
    }
    
    if ([sheetInfoResult objectForKey:@"front_innermm"]) {
        sheetFull.front_innermm = [sheetInfoResult objectForKey:@"front_innermm"];
    }
    if ([sheetInfoResult objectForKey:@"front_outtermm"]) {
        sheetFull.front_outtermm = [sheetInfoResult objectForKey:@"front_outtermm"];
    }
    if ([sheetInfoResult objectForKey:@"spindle_top"]) {
        sheetFull.spindle_top = [sheetInfoResult objectForKey:@"spindle_top"];
    }
    if ([sheetInfoResult objectForKey:@"spindle_bot"]) {
        sheetFull.spindle_bot = [sheetInfoResult objectForKey:@"spindle_bot"];
    }
    if ([sheetInfoResult objectForKey:@"arm_plates"]) {
        sheetFull.arm_plates = [sheetInfoResult objectForKey:@"arm_plates"];
    }
    if ([sheetInfoResult objectForKey:@"rear_arm_plates"]) {
        sheetFull.rear_arm_plates = [sheetInfoResult objectForKey:@"rear_arm_plates"];
    }
    if ([sheetInfoResult objectForKey:@"front_wt"]) {
        sheetFull.front_wt = [sheetInfoResult objectForKey:@"front_wt"];
    }
    if ([sheetInfoResult objectForKey:@"center_brand"]) {
        sheetFull.center_brand = [sheetInfoResult objectForKey:@"center_brand"];
    }
    if ([sheetInfoResult objectForKey:@"center_wt"]) {
        sheetFull.center_wt = [sheetInfoResult objectForKey:@"center_wt"];
    }
    if ([sheetInfoResult objectForKey:@"rear_wt"]) {
        sheetFull.rear_wt = [sheetInfoResult objectForKey:@"rear_wt"];
    }
    if ([sheetInfoResult objectForKey:@"steerarm"]) {
        sheetFull.steerarm = [sheetInfoResult objectForKey:@"steerarm"];
    }
    if ([sheetInfoResult objectForKey:@"bumpshim"]) {
        sheetFull.bumpshim = [sheetInfoResult objectForKey:@"bumpshim"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"studshim"]) {
        sheetFull.studshim = [sheetInfoResult objectForKey:@"studshim"];
    }
    if ([sheetInfoResult objectForKey:@"armshimf"]) {
        sheetFull.armshimf = [sheetInfoResult objectForKey:@"armshimf"];
    }
    if ([sheetInfoResult objectForKey:@"armshimr"]) {
        sheetFull.armshimr = [sheetInfoResult objectForKey:@"armshimr"];
    }
    
    
    
    
    if ([sheetInfoResult objectForKey:@"ack_ball"]) {
        sheetFull.ack_ball = [sheetInfoResult objectForKey:@"ack_ball"];
    }
    if ([sheetInfoResult objectForKey:@"bump_ball"]) {
        sheetFull.bump_ball = [sheetInfoResult objectForKey:@"bump_ball"];
    }
    if ([sheetInfoResult objectForKey:@"wing_pos"]) {
        sheetFull.wing_pos = [sheetInfoResult objectForKey:@"wing_pos"];
    }
    if ([sheetInfoResult objectForKey:@"eng_pos"]) {
        sheetFull.eng_pos = [sheetInfoResult objectForKey:@"eng_pos"];
    }
    if ([sheetInfoResult objectForKey:@"stop_washers"]) {
        sheetFull.stop_washers = [sheetInfoResult objectForKey:@"stop_washers"];
    }
    if ([sheetInfoResult objectForKey:@"suspension_notes"]) {
        sheetFull.suspension_notes = [sheetInfoResult objectForKey:@"suspension_notes"];
    }
    if ([sheetInfoResult objectForKey:@"shock_notes"]) {
        sheetFull.shock_notes = [sheetInfoResult objectForKey:@"shock_notes"];
    }
    if ([sheetInfoResult objectForKey:@"tread_front"]) {
        sheetFull.tread_front = [sheetInfoResult objectForKey:@"tread_front"];
    }
    if ([sheetInfoResult objectForKey:@"tread_rear"]) {
        sheetFull.tread_rear = [sheetInfoResult objectForKey:@"tread_rear"];
    }
    if ([sheetInfoResult objectForKey:@"diff_note"]) {
        sheetFull.diff_note = [sheetInfoResult objectForKey:@"diff_note"];
    }
    if ([sheetInfoResult objectForKey:@"servo_steer"]) {
        sheetFull.servo_steer = [sheetInfoResult objectForKey:@"servo_steer"];
    }
    if ([sheetInfoResult objectForKey:@"servo_gas"]) {
        sheetFull.servo_gas = [sheetInfoResult objectForKey:@"servo_gas"];
    }
    if ([sheetInfoResult objectForKey:@"fbias"]) {
        sheetFull.fbias = [sheetInfoResult objectForKey:@"fbias"];
    }
    if ([sheetInfoResult objectForKey:@"rbias"]) {
        sheetFull.rbias = [sheetInfoResult objectForKey:@"rbias"];
    }
    
    
    if ([sheetInfoResult objectForKey:@"drive_notes"]) {
        sheetFull.drive_notes = [sheetInfoResult objectForKey:@"drive_notes"];
    }
    if ([sheetInfoResult objectForKey:@"equip_notes"]) {
        sheetFull.equip_notes = [sheetInfoResult objectForKey:@"equip_notes"];
    }
    if ([sheetInfoResult objectForKey:@"trak"]) {
        sheetFull.trak = [sheetInfoResult objectForKey:@"trak"];
    }
    if ([sheetInfoResult objectForKey:@"trak_springs"]) {
        sheetFull.trak_springs = [sheetInfoResult objectForKey:@"trak_springs"];
    }
    
    return sheetFull;
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end

