//
//  CSCreateNewSheetManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/16/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SheetFullInfoModel.h"
#import "SBJson.h"
@protocol CSCreateNewSheetManagerDelegate;
@interface CSCreateNewSheetManager : NSObject{
    id <CSCreateNewSheetManagerDelegate> _delegate;
    BOOL checkCreateNewSuccess;
    BOOL checkLoginAgain;

    
}
@property (nonatomic, assign) id <CSCreateNewSheetManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkLoginAgain;
@property (nonatomic, assign) BOOL checkCreateNewSuccess;
- (void)loadCreateNewSheet:(SheetFullInfoModel *)sheetInfo;
@end

@protocol CSCreateNewSheetManagerDelegate <NSObject>

@optional
- (void) finishloadCreateNewSheet;
@end