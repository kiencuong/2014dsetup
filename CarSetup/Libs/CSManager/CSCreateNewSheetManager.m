//
//  CSCreateNewSheetManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/16/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCreateNewSheetManager.h"
#import "AFNetworking.h"
@implementation CSCreateNewSheetManager
@synthesize delegate;
@synthesize checkCreateNewSuccess;
@synthesize checkLoginAgain;
- (id)init{
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)loadCreateNewSheet:(SheetFullInfoModel *)sheetInfo{
    
//    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
//    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS],@"token",
//                          [NSNumber numberWithInt:sheetInfo._typeID], @"type",
//                          [NSNumber numberWithInt:sheetInfo._brandID], @"brand",
//                          [NSNumber numberWithInt:sheetInfo._modelID], @"model",
//                          sheetInfo.name, @"name",
//                          sheetInfo.name_race, @"race",
//                          sheetInfo.track, @"track_name",
//                          sheetInfo.city, @"city",
//                          sheetInfo.country, @"country",
//                          sheetInfo.date, @"date",
//                          sheetInfo._traction_level , @"traction",
//                          sheetInfo._track_type , @"track_type",
//                          sheetInfo._track_size , @"track_size",
//                          [sheetInfo._track_condition JSONRepresentation] , @"condition",
//                          sheetInfo._track_surface , @"surface",
//                          sheetInfo.password , @"password",
//                          sheetInfo.jsonDetail , @"json_detail",
//                          nil];
//    
//    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    
//    
//    
//    
//    [httpClient postPath:@"api/sheet/create" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSString *responseString = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
//        NSLog(@"%@",responseString);
//        NSDictionary *results = [responseString JSONValue];
//        NSInteger error = [[results objectForKey:@"status"]intValue];
////        NSString *message = [results objectForKey:@"message"];
//        if(error == 1)
//        {
//            checkCreateNewSuccess = YES;
//            
//            
//        } else {
//            checkCreateNewSuccess = NO;
////            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
////                checkLoginAgain = YES;
////            } else {
//                [self alertStatus:@"Have errol when update sheet." :@"Message"];
////            }
//            
//            
//        }
//        if ([delegate respondsToSelector:@selector(finishloadCreateNewSheet)])
//            [delegate finishloadCreateNewSheet];
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
//        if ([delegate respondsToSelector:@selector(finishloadCreateNewSheet)])
//            [delegate finishloadCreateNewSheet];
//    }];
//    
//
    NSString *post =[[NSString alloc] initWithFormat:XML_CREATE_NEW_SHEETS,[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS],sheetInfo._typeID,sheetInfo._brandID,sheetInfo._modelID,sheetInfo.name,sheetInfo.name_race,sheetInfo.track,sheetInfo.city,sheetInfo.country,sheetInfo.date,sheetInfo._traction_level,sheetInfo._track_type,sheetInfo._track_size,[sheetInfo._track_condition JSONRepresentation],sheetInfo._track_surface,sheetInfo.password,sheetInfo.jsonDetail];
    
    NSURL *url=[NSURL URLWithString:URL_CREATE_NEW_SHEETS];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:60];
    
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//         NSLog(@"Response code: %d", [response statusCode]);
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseString = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseString);
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            checkCreateNewSuccess = YES;
            
        }else {
            checkCreateNewSuccess = NO;
            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
                checkLoginAgain = YES;
            } else {
                [self alertStatus:message :@"Message"];
            }
            
        }
        
    }  else {
            checkCreateNewSuccess = NO;
        [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
    }
    if ([delegate respondsToSelector:@selector(finishloadCreateNewSheet)])
        [delegate finishloadCreateNewSheet];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
