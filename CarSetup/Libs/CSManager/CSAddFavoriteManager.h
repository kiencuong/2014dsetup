//
//  CSAddFavoriteManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
@protocol addFavoriteManagerDelegate;
@interface CSAddFavoriteManager : NSObject{
    BOOL checkAddSuccess;
    BOOL checkLoginAgain;
}
@property (nonatomic, assign) BOOL checkLoginAgain;
@property (nonatomic, assign) id<addFavoriteManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkAddSuccess;
- (void)addFavorite:(NSInteger)_id;
@end

@protocol addFavoriteManagerDelegate <NSObject>
@optional
- (void)finishAddFavorite;

@end
