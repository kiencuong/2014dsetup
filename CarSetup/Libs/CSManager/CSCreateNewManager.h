//
//  CSCreateNewManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/4/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MySetupBrankModel.h"
#import "SBJson.h"
@protocol CreateNewManagerDelegate;
@interface CSCreateNewManager : NSObject{
    id <CreateNewManagerDelegate> _delegate;
    NSMutableArray *_arrayType;
    NSMutableArray *_arraySheet;
    BOOL checkSheetExist;
    NSString *dateString;

}
@property (nonatomic, assign) id <CreateNewManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkSheetExist;
@property (nonatomic, strong) NSMutableArray *_arrayType;
@property (nonatomic, strong) NSMutableArray *_arraySheet;
@property (nonatomic, strong) NSString *dateString;
- (void)loadListCreateNew;
- (void)checkSheetExist:(NSInteger) brandID :(NSInteger) modelID;
@end

@protocol CreateNewManagerDelegate <NSObject>

@optional
- (void) finishGetlistCreateNew;
- (void) finishcheckSheetExist;
@end
