//
//  CSRemoveFavoriteManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSRemoveFavoriteManager.h"
#import "AFNetworking.h"
@implementation CSRemoveFavoriteManager
@synthesize delegate;
@synthesize checkRemoveSheetSuccess;
@synthesize checkLoginAgain;
- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)removeFavorite:(NSInteger)_id{
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS],@"token",[NSNumber numberWithInt:_id],@"sheet_id", nil];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    [httpClient postPath:@"api/user/removeFavorite" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseString);
        NSDictionary *results = [responseString JSONValue];
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            checkRemoveSheetSuccess = YES;
            
            
        } else {
            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
                checkLoginAgain = YES;
            } else {
                [self alertStatus:message :@"Message"];
            }
            
        }

        
        
        if ([delegate respondsToSelector:@selector(finishremoveFavorite)])
            [delegate finishremoveFavorite];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        if ([delegate respondsToSelector:@selector(finishremoveFavorite)])
            [delegate finishremoveFavorite];
    }];
    
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
