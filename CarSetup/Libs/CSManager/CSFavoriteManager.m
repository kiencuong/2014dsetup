//
//  CSFavoriteManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSFavoriteManager.h"
#import "AFNetworking.h"
@implementation CSFavoriteManager
@synthesize delegate;
@synthesize _arrayNameBrankModel;
@synthesize checkLoginAgain;
@synthesize arraySheetFavoriteDownload;
- (id)init{
    self = [super init];
    if (self) {
        
        _arrayNameBrankModel = [[NSMutableArray alloc]init];
        _arraySheet = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)loadDataListSetup{
    
    
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS], @"token",
                            nil];
    [httpClient postPath:@"api/user/favoriteSheet" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            [_arrayNameBrankModel removeAllObjects];
            [_arraySheet removeAllObjects];
            arraySheetFavoriteDownload = [[NSMutableArray alloc]init];
            
            NSDictionary *getListCategoryResult = [results objectForKey:@"data"];
            NSArray *getListsheetsResult = [getListCategoryResult objectForKey:@"sheets"];
            
            NSArray *listCarInfoResult = [getListCategoryResult objectForKey:@"Brand Model"];
            for (NSDictionary *brankModelInfo in listCarInfoResult) {
                MySetupBrankModel *brankModelCarInfo = [[MySetupBrankModel alloc]init];
                brankModelCarInfo._brand_id = [[brankModelInfo objectForKey:@"brand_id"] intValue];
                brankModelCarInfo._model_id = [[brankModelInfo objectForKey:@"model_id"] intValue];
                brankModelCarInfo._brand_name = [brankModelInfo objectForKey:@"brand_name"];
                brankModelCarInfo._model_name = [brankModelInfo objectForKey:@"model_name"];
                brankModelCarInfo.checkMySetup = 0;
                [_arrayNameBrankModel addObject:brankModelCarInfo];
            }
            
            for (NSDictionary *carInfo in getListsheetsResult) {
                MySetupModel *CarInfo = [[MySetupModel alloc]init];
                CarInfo._id = [[carInfo objectForKey:@"id"] intValue];
                CarInfo._date = [carInfo objectForKey:@"date"];
                CarInfo._raceName = [carInfo objectForKey:@"race"];
                CarInfo._brandId = [[carInfo objectForKey:@"brand_id"] intValue];
                CarInfo._modelId = [[carInfo objectForKey:@"model_id"] intValue];
                CarInfo._type_Id = [[carInfo objectForKey:@"type_id"] intValue];
                CarInfo._isFavorite = [[carInfo objectForKey:@"isFavorite"] intValue];
                [arraySheetFavoriteDownload addObject:[carInfo objectForKey:@"id"]];
                [_arraySheet addObject:CarInfo];
            }
            [X_AppDelegate delete:0 withUserName:[CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS]];
            for (int i=0; i <[_arrayNameBrankModel count]; i++) {
                for (int j =0;j <[_arraySheet count]; j++) {
                    
                    if ([[_arrayNameBrankModel objectAtIndex:i]_brand_id] == [[_arraySheet objectAtIndex:j]_brandId] && [[_arrayNameBrankModel objectAtIndex:i]_model_id] == [[_arraySheet objectAtIndex:j]_modelId])
                    {
                        [[[_arrayNameBrankModel objectAtIndex:i]sheetOfBrankModel] addObject:[_arraySheet objectAtIndex:j]];
                    }
                }
                [X_AppDelegate create:[_arrayNameBrankModel objectAtIndex:i]];
            }
            
            // done, call to delegate
            
        } else {
            
            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
                checkLoginAgain = YES;
            } else {
                [self alertStatus:message :@"Message"];
            }
            
        }
        if ([delegate respondsToSelector:@selector(finishGetListSetupSheet)])
            [delegate finishGetListSetupSheet];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        if ([delegate respondsToSelector:@selector(finishGetListSetupSheet)])
            [delegate finishGetListSetupSheet];
    }];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
