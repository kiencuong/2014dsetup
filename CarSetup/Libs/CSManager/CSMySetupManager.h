//
//  CSMySetupManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/2/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MySetupModel.h"
#import "MySetupBrankModel.h"
#import "SBJson.h"
#import "AppDelegate.h"
@protocol MySetupManagerDelegate;
@interface CSMySetupManager : NSObject{
    id <MySetupManagerDelegate> _delegate;
    NSMutableArray *_arrayNameBrankModel;
    NSMutableArray *_arraySheet;
    MySetupModel *_mySetupModel;
    MySetupBrankModel *_mySetupBrankModel;
    NSMutableArray *arraySheetDownload;
    BOOL checkLoginAgain;
}
- (void)loadDataListSetup;
@property (nonatomic, assign) id<MySetupManagerDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *_arrayNameBrankModel;
@property (nonatomic, strong) NSMutableArray *arraySheetDownload;
@property (nonatomic, assign) BOOL checkLoginAgain;
@end;

@protocol MySetupManagerDelegate <NSObject>
@optional
- (void)finishGetListSetupSheet;
@end

