//
//  CSRemoveFavoriteManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
@protocol removeFavoriteManagerDelegate;
@interface CSRemoveFavoriteManager : NSObject{
    BOOL checkRemoveSheetSuccess;
    BOOL checkLoginAgain;

}
@property (nonatomic, assign) BOOL checkLoginAgain;
@property (nonatomic, assign) id<removeFavoriteManagerDelegate> delegate;
@property (nonatomic, assign) BOOL checkRemoveSheetSuccess;
- (void)removeFavorite:(NSInteger)_id;
@end

@protocol removeFavoriteManagerDelegate <NSObject>
@optional
- (void)finishremoveFavorite;

@end