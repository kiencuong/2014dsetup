//
//  CSSearchResultManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 6/1/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MySetupModel.h"
#import "MySetupBrankModel.h"
#import "JSON.h"
#import "SearchResultModel.h"
@protocol SearchResultManagerDelegate;
@interface CSSearchResultManager : NSObject{
    id <SearchResultManagerDelegate> _delegate;
    NSMutableArray *_arrayNameBrankModel;
    NSMutableArray *_arraySheet;
    MySetupModel *_mySetupModel;
    MySetupBrankModel *_mySetupBrankModel;
}
- (void)loadDataListResultSearch : (SearchResultModel *)searchResultModel;
@property (nonatomic, assign) id<SearchResultManagerDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *_arrayNameBrankModel;
@end;

@protocol SearchResultManagerDelegate <NSObject>
@optional
- (void)finishGetListResultSheet;
@end