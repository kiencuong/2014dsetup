//
//  CSSearchManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSearchManager.h"
#import "AFNetworking.h"
@implementation CSSearchManager
@synthesize searchModel;
@synthesize delegate;
- (id)init{
    self = [super init];
    if (self) {
        searchModel = [[SearchModel alloc]init];
        
    }
    return self;
}

- (void)loadListDataSearch{
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];

    [httpClient postPath:@"api/user/typeBrandModel" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            NSDictionary *getListCategoryResult = [results objectForKey:@"data"];
            NSArray *getListTypes = [getListCategoryResult objectForKey:@"types"];
            
            
            for (NSDictionary *typeInfo in getListTypes) {
                MySetupBrankModel *typeCarInfo = [[MySetupBrankModel alloc]init];
                typeCarInfo._type_id = [[typeInfo objectForKey:@"id"] intValue];
                typeCarInfo._type_name = [typeInfo objectForKey:@"name"];
                [searchModel._arrayType addObject:typeCarInfo];
            }

            
            NSArray *getListsheetsResult = [getListCategoryResult objectForKey:@"data"];
            for (NSDictionary *carInfo in getListsheetsResult) {
                MySetupBrankModel *createModel = [[MySetupBrankModel alloc]init];
                createModel._type_id= [[carInfo objectForKey:@"type_id"] intValue];
                createModel._type_name = [carInfo objectForKey:@"type_name"];
                createModel._brand_id = [[carInfo objectForKey:@"brand_id"]intValue];
                createModel._brand_name = [carInfo objectForKey:@"brand_name"];
                createModel._model_id = [[carInfo objectForKey:@"model_id"] intValue];
                createModel._model_name = [carInfo objectForKey:@"model_name"];
                
                [searchModel._arraySheets addObject:createModel];
            }
            
            
            
        } else {
            
            [self alertStatus:message :@"Message"];
            
        }
        if ([delegate respondsToSelector:@selector(finishGetlistSearch)])
            [delegate finishGetlistSearch];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        if ([delegate respondsToSelector:@selector(finishGetlistSearch)])
            [delegate finishGetlistSearch];
    }];
    
  
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end

