//
//  CSSearchResultManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 6/1/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSearchResultManager.h"

@implementation CSSearchResultManager
@synthesize delegate;
@synthesize _arrayNameBrankModel;
- (id)init{
    self = [super init];
    if (self) {
        
        _arrayNameBrankModel = [[NSMutableArray alloc]init];
        _arraySheet = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)loadDataListResultSearch : (SearchResultModel *)searchResultModel{
    NSString *post =[[NSString alloc] initWithFormat:XML_FORMAT_LIST_RESULT_SEARCH,[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS], searchResultModel._typeID, searchResultModel._brandID, searchResultModel._modelID, searchResultModel._tractionLevel, searchResultModel._trackTypeID, searchResultModel._trackSizeID, searchResultModel._surfaceID, searchResultModel._driverName,searchResultModel._cityName,searchResultModel._trackName];
    
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LIST_RESULT_SEARCH];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setTimeoutInterval:60];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    //        NSLog(@"Response code: %d", [response statusCode]);
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseString = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            [_arrayNameBrankModel removeAllObjects];
            [_arraySheet removeAllObjects];
            NSDictionary *getListCategoryResult = [results objectForKey:@"data"];
            NSArray *listCarInfoResult = [getListCategoryResult objectForKey:@"BrandsModels"];
            NSArray *getListsheetsResult = [getListCategoryResult objectForKey:@"sheets"];
            for (NSDictionary *brankModelInfo in listCarInfoResult) {
                MySetupBrankModel *brankModelCarInfo = [[MySetupBrankModel alloc]init];
                brankModelCarInfo._brand_id = [[brankModelInfo objectForKey:@"brand_id"] intValue];
                brankModelCarInfo._model_id = [[brankModelInfo objectForKey:@"model_id"] intValue];
                brankModelCarInfo._brand_name = [brankModelInfo objectForKey:@"brand_name"];
                brankModelCarInfo._model_name = [brankModelInfo objectForKey:@"model_name"];
                [_arrayNameBrankModel addObject:brankModelCarInfo];
            }
            
            for (NSDictionary *carInfo in getListsheetsResult) {
                MySetupModel *CarInfo = [[MySetupModel alloc]init];
                CarInfo._id = [[carInfo objectForKey:@"id"] intValue];
                CarInfo._date = [carInfo objectForKey:@"date"];
                CarInfo._raceName = [carInfo objectForKey:@"race"];
                CarInfo._brandId = [[carInfo objectForKey:@"brand_id"] intValue];
                CarInfo._modelId = [[carInfo objectForKey:@"model_id"] intValue];
                CarInfo._type_Id = [[carInfo objectForKey:@"type_id"] intValue];
                CarInfo._isFavorite = [[carInfo objectForKey:@"isFavorite"] intValue];
                
                [_arraySheet addObject:CarInfo];
            }
            for (int i=0; i <[_arrayNameBrankModel count]; i++) {
                for (int j =0;j <[_arraySheet count]; j++) {
                    
                    if ([[_arrayNameBrankModel objectAtIndex:i]_brand_id] == [[_arraySheet objectAtIndex:j]_brandId] && [[_arrayNameBrankModel objectAtIndex:i]_model_id] == [[_arraySheet objectAtIndex:j]_modelId])
                    {
                        [[[_arrayNameBrankModel objectAtIndex:i]sheetOfBrankModel] addObject:[_arraySheet objectAtIndex:j]];
                    }
                }
            }
            
            //            NSLog(@"count brankModel :%d",[_arrayNameBrankModel count]);
            //            NSLog(@"count sheet :%d",[[[_arrayNameBrankModel objectAtIndex:1]sheetOfBrankModel] count]);
            // done, call to delegate
            
        } else {
            if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
            } else {
                [self alertStatus:message :@"Message"];
            }
            
        }
        
    }  else {
        [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
    }
    if ([delegate respondsToSelector:@selector(finishGetListResultSheet)])
        [delegate finishGetListResultSheet];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
