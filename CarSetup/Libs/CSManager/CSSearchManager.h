//
//  CSSearchManager.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MySetupBrankModel.h"
#import "SearchModel.h"
#import "SBJson.h"
@protocol SearchManagerDelegate;
@interface CSSearchManager : NSObject{
    id <SearchManagerDelegate> _delegate;
    SearchModel *searchModel;
}
@property (nonatomic, strong) SearchModel *searchModel;
@property (nonatomic, assign) id <SearchManagerDelegate> delegate;
- (void)loadListDataSearch;
@end

@protocol SearchManagerDelegate <NSObject>

@optional
- (void) finishGetlistSearch;

@end
