//
//  CSEditSheetManager.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/18/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSEditSheetManager.h"

@implementation CSEditSheetManager
@synthesize checkEditNewSuccess;
@synthesize delegate;
@synthesize checkLoginAgain;
- (id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (SheetFullInfoModel *)getInfoSheetFromHTML:(NSString *)stringJsonHTML {
    stringJsonHTML = [stringJsonHTML stringByReplacingOccurrencesOfString:@"\\\"" withString:@"&#34"];
    NSDictionary *results = [stringJsonHTML JSONValue];
    NSMutableDictionary *sheetInfo = [results objectForKey:@"sheets"];
    SheetFullInfoModel *sheetFullModel ;
    sheetFullModel = [[SheetFullInfoModel alloc]init];
    sheetFullModel._track_size = [sheetInfo objectForKey:@"track_size"];
    sheetFullModel._traction_level = [sheetInfo objectForKey:@"traction_level"];
    sheetFullModel._track_type = [sheetInfo objectForKey:@"track_type"];
    sheetFullModel._track_surface = [sheetInfo objectForKey:@"track_surface"];

    
    
    if ([sheetInfo objectForKey:@"name"]) {
        sheetFullModel.name = [sheetInfo objectForKey:@"name"];
    }
    if ([sheetInfo objectForKey:@"track"]) {
        sheetFullModel.track= [sheetInfo objectForKey:@"track"];
    }
    if ([sheetInfo objectForKey:@"city"]) {
        sheetFullModel.city = [sheetInfo objectForKey:@"city"];
    }
    
    if ([sheetInfo objectForKey:@"country"]) {
        sheetFullModel.country = [sheetInfo objectForKey:@"country"];
    }
    
    if ([sheetInfo objectForKey:@"race"]) {
        sheetFullModel.name_race = [sheetInfo objectForKey:@"race"];
    }
    
    
    if ([sheetInfo objectForKey:@"track_condition"]) {
        sheetFullModel._track_condition = [sheetInfo objectForKey:@"track_condition"];
    }
    
    if (![sheetInfo objectForKey:@"password"]|| ![sheetInfo objectForKey:@"check_password"]) {
        [sheetInfo setObject:@"" forKey:@"password"];
    } else 
        sheetFullModel.password = [sheetInfo objectForKey:@"password"];
    sheetFullModel.jsonDetail = [[[sheetInfo JSONRepresentation]stringByReplacingOccurrencesOfString:@"/n" withString:@"\\r\\n"]stringByReplacingOccurrencesOfString:@"&#34" withString:@"\\\"" ];
    return sheetFullModel;
    
}
- (void)EditSheet:(SheetFullInfoModel *)sheetInfo{
    NSString *post =[[NSString alloc] initWithFormat:XML_EDIT_SHEETS,[CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS],sheetInfo._ID,sheetInfo.name,sheetInfo.name_race,sheetInfo.track,sheetInfo.city,sheetInfo.country,sheetInfo.date,sheetInfo._traction_level,sheetInfo._track_type,sheetInfo._track_size,[sheetInfo._track_condition JSONRepresentation],sheetInfo._track_surface,sheetInfo.password,sheetInfo.jsonDetail];
    
    NSURL *url=[NSURL URLWithString:URL_EDIT_SHEETS];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:60];
    [request setHTTPBody:postData];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *response = nil;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSLog(@"Response code: %d", [response statusCode]);
    if ([response statusCode] >=200 && [response statusCode] <300)
    {
        NSString *responseString = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        
        NSInteger error = [[results objectForKey:@"status"]intValue];
//        NSString *message = [results objectForKey:@"message"];
        if(error == 1)
        {
            checkEditNewSuccess = YES;
            
        } else {
                checkEditNewSuccess = NO;
//                if ([message isEqualToString:@"Token is not valid"]|| [message isEqualToString:@"Username or Password is incorrect"]) {
//                    checkLoginAgain = YES;
//                } else {
            [self alertStatus:@"Have errol when update sheet." :@"Message"];
//                }
            
            }
        
    }  else {
        checkEditNewSuccess = NO;
        [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
    }
    if ([delegate respondsToSelector:@selector(finishEditSheet)])
        [delegate finishEditSheet];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
@end
