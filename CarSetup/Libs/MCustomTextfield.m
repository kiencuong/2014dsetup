//
//  MCustomTextfield.m
//  Mtaxi
//
//  Created by thinhpham on 5/11/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "MCustomTextfield.h"

@implementation MCustomTextfield
@synthesize leftMargin;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // default margin left = 10
        leftMargin = 10;
        self.clearsOnBeginEditing = YES;
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (CGRect)textRectForBounds:(CGRect)bounds {
    
    CGRect inset = CGRectMake(bounds.origin.x + leftMargin, bounds.origin.y, bounds.size.width - leftMargin, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    CGRect inset = CGRectMake(bounds.origin.x + leftMargin, bounds.origin.y, bounds.size.width - leftMargin, bounds.size.height);
    return inset;
}


@end
