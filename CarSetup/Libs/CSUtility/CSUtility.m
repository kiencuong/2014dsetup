//
//  CSUtility.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSUtility.h"

@implementation CSUtility

+ (BOOL)checkNetworkAvailable{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
//    
//    Reachability *reach = [Reachability reachabilityForInternetConnection];
//    NetworkStatus netStatus = [reach currentReachabilityStatus];
//    switch (netStatus)
//    {
//        case NotReachable:
//        {
//            //NSLog(@"The internet is down.");
//            return FALSE;
//        }
//        default:{
//            reach = [Reachability reachabilityWithHostName:@"google.com.vn"];
//            netStatus = [reach currentReachabilityStatus];
//            switch (netStatus)
//            {
//                case NotReachable:
//                {
//                    //NSLog(@"The host is down.");
//                    return NO;
//                }
//                default:{
//                    return YES;
//                }
//            }
//        }
//    }
}

+ (void)saveStatusLogin:(BOOL)check forKey:(NSString*)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:check forKey:key];
    
    [defaults synchronize];
}
+ (BOOL )loadStatusLogin:(NSString*)forKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:forKey];
}
+ (void)saveInfoUserLogin:(NSString *)check forKey:(NSString*)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:check forKey:key];
    
    [defaults synchronize];
}
+ (NSString*)loadInfoUserLogin:(NSString*)forKey{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     return [defaults objectForKey:forKey];
}
+ (NSString*)convertDateStringMySetup:(NSString*)inputDateString{
    NSString *trimmedString = [inputDateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *string1=[inputDateString stringByAppendingString:@" 00:00:00 +0000"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [dateFormatter dateFromString:trimmedString];
    [dateFormatter setDateFormat:@"dd/MMM/yy "];
    return [dateFormatter stringFromDate:date1];
}
+ (NSString*)convertDateSheetInfo:(NSString*)inputDateString{
    NSString *trimmedString = [inputDateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *string1=[inputDateString stringByAppendingString:@" 00:00:00 +0000"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [dateFormatter dateFromString:trimmedString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd "];
    return [dateFormatter stringFromDate:date1];
}

+(NSString *)getStringForKey:(NSString *)string{
    NSString *stringRespone;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"tuningGuide"
                                                     ofType:@"strings"
                                                inDirectory:nil
                                            forLocalization:@"ja"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    stringRespone = [dict objectForKey:string];
    return stringRespone;
}

+(float)checkHeightForString:(NSString*)str withFront:(UIFont*)font withInitSize:(CGSize)size{
    
    CGSize sizeText = [str sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16]
                            constrainedToSize:(CGSize){size.width, CGFLOAT_MAX}
                                lineBreakMode:NSLineBreakByWordWrapping];
    
    return sizeText.height+20;
}

@end
