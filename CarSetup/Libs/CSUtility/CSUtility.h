//
//  CSUtility.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@interface CSUtility : NSObject
+ (BOOL)checkNetworkAvailable;
+ (void)saveStatusLogin:(BOOL)check forKey:(NSString*)key;
+ (BOOL)loadStatusLogin:(NSString*)forKey;
+ (void)saveInfoUserLogin:(NSString *)check forKey:(NSString*)key;
+ (NSString*)loadInfoUserLogin:(NSString*)forKey;
+ (NSString*)convertDateStringMySetup:(NSString*)inputDateString;
+ (NSString*)convertDateSheetInfo:(NSString*)inputDateString;
+ (NSString *)getStringForKey:(NSString *)string;
+(float)checkHeightForString:(NSString*)str withFront:(UIFont*)font withInitSize:(CGSize)size;
@end
