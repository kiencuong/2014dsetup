//
//  CSDefine.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#define CHECK_APP_STARTUP_FIRST @"CHECK_APP_STARTUP_FIRST"
#define CHECK_USER_LOGIN_SUCCESS @"CHECK_USER_LOGIN_SUCCESS"
#define USERNAME_LOGIN_SUCCESS @"USERNAME_LOGIN_SUCCESS"
#define FIRSTLAST_NAME @"FIRSTLAST_NAME"
#define PASSWORD_LOGIN_SUCCESS @"PASSWORD_LOGIN_SUCCESS"
#define ID_USER_LOGIN_SUCCESS @"ID_USER_LOGIN_SUCCESS"
#define APP_ID_FACEBOOK @"110717202432308"

#define DETECT_OS7    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_IPHONE   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds ].size.height == 568)

#define CHECK_IS_DOWNLOADING_SHEETS_FAVORITE @"CHECK_IS_DOWNLOADING_SHEETS_FAVORITE"

#define CHECK_IS_DOWNLOADING_SHEETS @"CHECK_IS_DOWNLOADING_SHEETS"


#define XML_FORMAT_LOGIN @"username=%@&password=%@"
 
#define URL_FORMAT_LOGIN @"http://dialedsetups.com/"
#define URL_FORMAT_REGISTER @"http://dialedsetups.com/register"

#define URL_GET_INFO_SHEET @"http://dialedsetups.com/api/user/infoSheet"
#define XML_GET_INFO_SHEET @"token=%@&sheet_id=%d"


#define URL_CREATE_NEW_SHEETS @"http://dialedsetups.com/api/sheet/create"
#define XML_CREATE_NEW_SHEETS @"token=%@&type=%d&brand=%d&model=%d&name=%@&race=%@&track_name=%@&city=%@&country=%@&date=%@&traction=%@&track_type=%@&track_size=%@&condition=%@&surface=%@&password=%@&json_detail=%@"

#define URL_EDIT_SHEETS @"http://dialedsetups.com/api/sheet/edit"
#define XML_EDIT_SHEETS @"token=%@&sheet_id=%d&name=%@&race=%@&track_name=%@&city=%@&country=%@&date=%@&traction=%@&track_type=%@&track_size=%@&condition=%@&surface=%@&password=%@&json_detail=%@"
#define URL_LIST_SEARCH @"http://dialedsetups.com/api/user/typeBrandModel"

#define XML_FORMAT_LIST_RESULT_SEARCH  @"token=%@&type=%@&brand=%@&model=%@&traction=%@&track_type=%@&track_size=%@&surface=%@&driver=%@&city=%@&track_name=%@"
#define URL_FORMAT_LIST_RESULT_SEARCH @"http://dialedsetups.com/api/sheet/search"

@interface CSDefine : NSObject

@end
