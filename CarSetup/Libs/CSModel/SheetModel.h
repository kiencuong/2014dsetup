//
//  SheetModel.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/10/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SheetModel : NSObject{
    NSString *_date;
    NSInteger _id;
    NSInteger _isFavorite;
    NSString *_raceName;
    NSInteger _brandId;
    NSInteger _modelId;
    NSInteger _type_Id;
    NSString *_brand_name;
    NSString *_model_name;
    NSString *_type_name;
    NSString *_country_name;
    NSString *_city_name;
    NSString *_track_name;
    NSString *_user_name;
    NSString *_driver_name;
    NSString *_first_name;
    NSString *_last_name;
    NSString *_link;

}
@property (nonatomic,strong) NSString *_date;
@property (nonatomic,strong) NSString *_link;
@property (nonatomic,assign) NSInteger _id;
@property (nonatomic,assign) NSInteger _isFavorite;
@property (nonatomic,strong) NSString *_raceName;
@property (nonatomic,assign) NSInteger _brandId;
@property (nonatomic,assign) NSInteger _modelId;
@property (nonatomic,assign) NSInteger _type_Id;
@property (nonatomic,strong) NSString *_brand_name;
@property (nonatomic,strong) NSString *_model_name;
@property (nonatomic,strong) NSString *_type_name;
@property (nonatomic,strong) NSString *_track_name;
@property (nonatomic,strong) NSString *_driver_name;
@property (nonatomic,strong) NSString *_first_name;
@property (nonatomic,strong) NSString *_last_name;
@end
