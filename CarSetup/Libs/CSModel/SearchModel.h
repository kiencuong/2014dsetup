//
//  SearchModel.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchModel : NSObject{
    NSMutableArray *_arrayType;
    NSMutableArray *_arrayBrand;
    NSMutableArray *_arrayModel;
    NSMutableArray *_arrayTrackType;
    NSMutableArray *_arrayTractionLevel;
    NSMutableArray *_arrayTrackSize;
    NSMutableArray *_arraySurface;
    NSMutableArray *_arraySheets;
    NSString *_driverName;
}
@property (strong, nonatomic) NSMutableArray *_arrayType;
@property (strong, nonatomic) NSMutableArray *_arrayBrand;
@property (strong, nonatomic) NSMutableArray *_arrayModel;
@property (strong, nonatomic) NSMutableArray *_arrayTrackType;
@property (strong, nonatomic) NSMutableArray *_arrayTractionLevel;
@property (strong, nonatomic) NSMutableArray *_arrayTrackSize;
@property (strong, nonatomic) NSString *_driverName;
@property (strong, nonatomic) NSMutableArray *_arraySurface;
@property (strong, nonatomic) NSMutableArray *_arraySheets;
@end
