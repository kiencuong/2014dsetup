//
//  MySetupModel.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/22/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "MySetupModel.h"

@implementation MySetupModel
@synthesize _date;
@synthesize _id;
@synthesize _isFavorite;
@synthesize _raceName;
@synthesize _brandId;
@synthesize _modelId;
@synthesize _type_Id;
-(id)init{
    self = [super init];
    if (self) {
        _raceName = @"";
    }
    return self;
}

@end
