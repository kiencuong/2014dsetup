//
//  SearchResultModel.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/4/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "SearchResultModel.h"

@implementation SearchResultModel
@synthesize _typeID;
@synthesize _brandID;
@synthesize _modelID;
@synthesize _trackSizeID;
@synthesize _trackTypeID;
@synthesize _driverName;
@synthesize _surfaceID;
@synthesize _tractionLevel;
@synthesize _cityName;
@synthesize _trackName;
-(id)init{
    self = [super init];
    if (self) {
        _typeID = @"";
        _brandID = @"";
        _modelID = @"";
        _trackSizeID = @"";
        _trackTypeID = @"";
        _tractionLevel = @"";
        _driverName = @"";
        _surfaceID = @"";
        _cityName = @"";
        _trackName = @"";
    }
    return self;
}
@end
