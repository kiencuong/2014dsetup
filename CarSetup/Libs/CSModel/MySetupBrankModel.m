//
//  MySetupBrankModel.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/1/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "MySetupBrankModel.h"

@implementation MySetupBrankModel
@synthesize _brand_id;
@synthesize _model_id;
@synthesize _brand_name;
@synthesize _model_name;
@synthesize _type_id;
@synthesize _type_name;
@synthesize sheetOfBrankModel;
@synthesize checkMySetup;
-(id)init{
    self = [super init];
    if (self) {
        sheetOfBrankModel = [[NSMutableArray alloc]init];
        _brand_name = @"";
        _type_name = @"";
        _model_name = @"";
    }
    return self;
}
@end
