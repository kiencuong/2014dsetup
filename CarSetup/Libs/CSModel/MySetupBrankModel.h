//
//  MySetupBrankModel.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/1/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySetupBrankModel : NSObject{
    NSInteger _brand_id;
    NSInteger _model_id;
    NSString *_brand_name;
    NSString *_model_name;
    NSInteger _type_id;
    NSString *_type_name;
    NSMutableArray *sheetOfBrankModel;
    NSInteger checkMySetup;
}
@property (nonatomic,assign) NSInteger _brand_id;
@property (nonatomic,assign) NSInteger _model_id;
@property (nonatomic,strong) NSString *_brand_name;
@property (nonatomic,strong) NSString *_model_name;
@property (nonatomic,strong) NSMutableArray *sheetOfBrankModel;
@property (nonatomic,assign) NSInteger _type_id;
@property (nonatomic,assign) NSInteger checkMySetup;
@property (nonatomic,strong) NSString *_type_name;
@end
