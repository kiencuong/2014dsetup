//
//  SearchResultModel.h
//  CarSetup
//
//  Created by Pham Van Thinh on 4/4/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchResultModel : NSObject{
    NSString *_typeID;
    NSString *_brandID;
    NSString *_modelID;
    NSString *_trackTypeID;
    NSString *_tractionLevel;
    NSString *_trackSizeID;
    NSString *_surfaceID;
    NSString *_driverName;
    NSString *_cityName;
    NSString *_trackName;
}
@property (strong, nonatomic) NSString *_typeID;
@property (strong, nonatomic) NSString *_brandID;
@property (strong, nonatomic) NSString *_modelID;
@property (strong, nonatomic) NSString *_trackTypeID;
@property (strong, nonatomic) NSString *_tractionLevel;
@property (strong, nonatomic) NSString *_trackSizeID;
@property (strong, nonatomic) NSString *_surfaceID;
@property (strong, nonatomic) NSString *_driverName;
@property (strong, nonatomic) NSString *_cityName;
@property (strong, nonatomic) NSString *_trackName;
@end