//
//  SheetModel.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/10/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "SheetModel.h"

@implementation SheetModel
@synthesize _date;
@synthesize _id;
@synthesize _isFavorite;
@synthesize _raceName;
@synthesize _brandId;
@synthesize _modelId;
@synthesize _type_Id;
@synthesize _brand_name;
@synthesize _model_name;
@synthesize _type_name;
@synthesize _track_name;
@synthesize _driver_name;
@synthesize _last_name;
@synthesize _first_name;
@synthesize _link;
-(id)init{
    self = [super init];
    if (self) {
    }
    return self;
}
@end
