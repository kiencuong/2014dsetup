//
//  MySetupModel.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/22/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySetupModel : NSObject{
    NSString *_date;
    NSInteger _id;
    NSInteger _isFavorite;
    NSString *_raceName;
    NSInteger _brandId;
    NSInteger _modelId;
    NSInteger _type_Id;
}
@property (nonatomic,strong) NSString *_date;
@property (nonatomic,assign) NSInteger _id;
@property (nonatomic,assign) NSInteger _isFavorite;
@property (nonatomic,strong) NSString *_raceName;
@property (nonatomic,assign) NSInteger _brandId;
@property (nonatomic,assign) NSInteger _modelId;
@property (nonatomic,assign) NSInteger _type_Id;
@end
