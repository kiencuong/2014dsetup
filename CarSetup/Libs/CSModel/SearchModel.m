//
//  SearchModel.m
//  CarSetup
//
//  Created by Pham Van Thinh on 4/3/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchModel
@synthesize _arrayType;
@synthesize _arrayBrand;
@synthesize _arrayModel;
@synthesize _arrayTrackType;
@synthesize _arrayTrackSize;
@synthesize _arrayTractionLevel;
@synthesize _driverName;
@synthesize _arraySurface;
@synthesize _arraySheets;
-(id)init{
    self = [super init];
    if (self) {
        _arrayType = [[NSMutableArray alloc]initWithObjects:@"", nil];
        _arrayBrand = [[NSMutableArray alloc]initWithObjects:@"", nil];

        _arrayModel = [[NSMutableArray alloc]initWithObjects:@"", nil];

        _arrayTrackSize = [[NSMutableArray alloc]init];
        _arrayTrackType = [[NSMutableArray alloc]init];
        _arrayTractionLevel = [[NSMutableArray alloc]init];
        _arraySurface = [[NSMutableArray alloc]init];
        _arraySheets = [[NSMutableArray alloc]init];
    }
    return self;
}

@end
