//
//  SheetInfo.h
//  CarSetup
//
//  Created by Pham Van Thinh on 5/15/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SheetInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * brand_id;
@property (nonatomic, retain) NSString * conditions;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * isFavorite;
@property (nonatomic, retain) NSString * json_detail;
@property (nonatomic, retain) NSNumber * model_id;
@property (nonatomic, retain) NSString * track_size;
@property (nonatomic, retain) NSString * track_type;
@property (nonatomic, retain) NSString * traction_level;
@property (nonatomic, retain) NSString *track_surface;
@property (nonatomic, retain) NSNumber * type_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_race;
@property (nonatomic, retain) NSString * track_name;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *country;

@end
