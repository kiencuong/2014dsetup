//
//  SheetInfo.m
//  CarSetup
//
//  Created by Pham Van Thinh on 5/15/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "SheetInfo.h"


@implementation SheetInfo

@dynamic brand_id;
@dynamic conditions;
@dynamic id;
@dynamic isFavorite;
@dynamic json_detail;
@dynamic model_id;
@dynamic track_size;
@dynamic track_type;
@dynamic traction_level;
@dynamic type_id;
@dynamic name;
@dynamic name_race;
@dynamic track_name;
@dynamic track_surface;
@dynamic date;
@dynamic city;
@dynamic country;
@end
