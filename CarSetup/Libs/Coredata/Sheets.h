//
//  Sheets.h
//  CarSetup
//
//  Created by Pham Van Thinh on 5/13/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BrandModel;

@interface Sheets : NSManagedObject

@property (nonatomic, retain) NSNumber * brandID;
@property (nonatomic, retain) NSNumber * modelID;
@property (nonatomic, retain) NSString * raceName;
@property (nonatomic, retain) NSNumber * idSheet;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSNumber * checkMysetup;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) BrandModel *brandModel;

@end
