//
//  BrandModel.h
//  CarSetup
//
//  Created by thinhpham on 12/7/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sheets;

@interface BrandModel : NSManagedObject

@property (nonatomic, retain) NSNumber * brandID;
@property (nonatomic, retain) NSString * brandName;
@property (nonatomic, retain) NSNumber * checkMysetup;
@property (nonatomic, retain) NSNumber * modelID;
@property (nonatomic, retain) NSString * modelName;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *sheetRecord;
@end

@interface BrandModel (CoreDataGeneratedAccessors)

- (void)addSheetRecordObject:(Sheets *)value;
- (void)removeSheetRecordObject:(Sheets *)value;
- (void)addSheetRecord:(NSSet *)values;
- (void)removeSheetRecord:(NSSet *)values;

@end
