//
//  BrandModel.m
//  CarSetup
//
//  Created by thinhpham on 12/7/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "BrandModel.h"
#import "Sheets.h"


@implementation BrandModel

@dynamic brandID;
@dynamic brandName;
@dynamic checkMysetup;
@dynamic modelID;
@dynamic modelName;
@dynamic userName;
@dynamic sheetRecord;

@end
