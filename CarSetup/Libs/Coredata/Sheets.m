//
//  Sheets.m
//  CarSetup
//
//  Created by Pham Van Thinh on 5/13/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "Sheets.h"
#import "BrandModel.h"


@implementation Sheets

@dynamic brandID;
@dynamic modelID;
@dynamic raceName;
@dynamic idSheet;
@dynamic date;
@dynamic brandModel;
@dynamic checkMysetup;
@dynamic userName;
@end
