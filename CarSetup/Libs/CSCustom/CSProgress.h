//
//  CSProgress.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface CSProgress : UIView{
    UIActivityIndicatorView *activityView;
    
    CALayer *transparentLayer;
    
    UILabel *lblLoading;
    BOOL isWithLabel;
}

- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;

- (id)initWithLoadingText;
@end