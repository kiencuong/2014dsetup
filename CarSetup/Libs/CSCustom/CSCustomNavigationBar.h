//
//  CSCustomNavigationBar.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol CSCustomNavigationBarDelegate <NSObject>
@optional
- (void)shouldGoBack;
- (void)shouldGoSearch;
-(void)shoulSignIn;
-(void)shouldGoShare;
- (void)shouldGoSave;
@end

@interface CSCustomNavigationBar : UIView{
    NSObject <CSCustomNavigationBarDelegate> *delegate;
    NSString *title;
    //    BOOL hideSearchButton;
    BOOL checkHomeViewScreen;
    
    
}
@property (nonatomic, strong) NSObject <CSCustomNavigationBarDelegate> *delegate;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL checkHomeViewScreen;
@property (nonatomic, assign) NSInteger heightIOS7;
- (void)addSignInButton:(NSString *)imageSignIn;
- (void)layoutCustomBar : (NSString *)imageNavigationBar;
- (void)addBackButton;
- (void)addBackButton_ipad;
- (void)addImageLogo;
- (void)addImageTitleView:(NSString *)name;
- (void)addImageTitleCreateView:(CGSize) size :(NSString *)name;
- (void)addShareButton;
- (void)addSaveButton;
- (void)addTitleLabelHome:(NSInteger)titleViewWidth;
@end
