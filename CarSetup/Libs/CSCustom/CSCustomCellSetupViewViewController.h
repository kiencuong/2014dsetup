//
//  CSCustomCellSetupViewViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/21/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CSCustomSetupViewDelegate;
@interface CSCustomCellSetupViewViewController : UITableViewCell{
    id <CSCustomSetupViewDelegate> _delegate;  
    IBOutlet UIButton *addButton;
    IBOutlet UILabel *bandNameLabel;
}

@property (strong, nonatomic) IBOutlet UILabel *bandNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic,assign) id <CSCustomSetupViewDelegate> delegate;
- (IBAction)addButtonClick:(id)sender;

@end

@protocol CSCustomSetupViewDelegate <NSObject>

@optional
-(void)expandRowTableSetup:(NSInteger)index;
@end