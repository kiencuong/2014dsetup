//
//  CSCustomNavigationBar.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCustomNavigationBar.h"

@implementation CSCustomNavigationBar
@synthesize delegate;
@synthesize title;
@synthesize checkHomeViewScreen;
@synthesize heightIOS7;
- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (id)init{
    CGRect fr = [[UIScreen mainScreen] bounds];
    heightIOS7 = DETECT_OS7? 20 :0;
	if (self = [super initWithFrame:CGRectMake(0.0, 0.0, fr.size.width, 44.0+heightIOS7)]) {
        
        
    }
    return self;
}

- (void)layoutCustomBar : (NSString *)imageNavigationBar{
    // set background to image
    UIColor *backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"bg-NavigationBar.png"]];
    self.backgroundColor = backgroundColor;
    if (!checkHomeViewScreen) {
        [self addTitleLabel];
    }

    
}

- (void)addTitleLabel{
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 120)/2, 0.0,120, self.frame.size.height)];
    lblTitle.text = title;
    
    lblTitle.font = [UIFont boldSystemFontOfSize:18.0];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor whiteColor];
    
    [self addSubview:lblTitle];
}
- (void)addTitleLabelHome:(NSInteger)titleViewWidth{
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - titleViewWidth)/2, heightIOS7,titleViewWidth, self.frame.size.height-heightIOS7)];
    lblTitle.text = title;
    
    lblTitle.font = [UIFont boldSystemFontOfSize:18.0];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor whiteColor];
    
    [self addSubview:lblTitle];
    
    
}
- (void)addImageTitleView:(NSString *)name{
    UIImage *imageIcon = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",name]];
    UIImageView *nagivationBarImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - imageIcon.size.width)/2, (44 -imageIcon.size.height)/2+heightIOS7, imageIcon.size.width, imageIcon.size.height)];
    nagivationBarImage.image = imageIcon;
    [self addSubview:nagivationBarImage];
}
- (void)addImageTitleCreateView:(CGSize) size :(NSString *)name{
    UIImage *imageIcon = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",name]];
    UIImageView *nagivationBarImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width-size.width)/2, (44-size.height)/2+heightIOS7, size.width, size.height)];
    nagivationBarImage.image = imageIcon;
    [self addSubview:nagivationBarImage];
}


- (void)addBackButton{
    // add back button
    UIImage *bgImg = [UIImage imageNamed:@"backButton.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame= CGRectMake(5.0, 6.0+heightIOS7, bgImg.size.width, bgImg.size.height);
    [button setBackgroundImage:bgImg forState:UIControlStateNormal];
    [button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}

- (void)addBackButton_ipad{
    // add back button
    UIImage *bgImg = [UIImage imageNamed:@"back_btn_ipad.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame= CGRectMake(15.0, 6.0+heightIOS7, bgImg.size.width, bgImg.size.height);
    [button setBackgroundImage:bgImg forState:UIControlStateNormal];
    [button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}
- (void)addSignInButton:(NSString *)imageSignIn{
    // add back button
    UIImage *bgImg = [UIImage imageNamed:imageSignIn];
    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    signInButton.frame= CGRectMake(self.frame.size.width - 15 - 61, 7.0+heightIOS7, 61, 30);
    [signInButton setBackgroundImage:bgImg forState:UIControlStateNormal];
    [signInButton addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    
    // init the right button padding 10 from right border
    //UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, bgImg.size.width +10, bgImg.size.height) ];
    //[view addSubview:button];
    
    [self addSubview:signInButton];
}

- (void)addShareButton{
    // add back button
    UIImage *bgImg = [UIImage imageNamed:@"shareButton_ipad.png"];
    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signInButton.frame= CGRectMake(self.frame.size.width - 7 - bgImg.size.width , 6.0+heightIOS7, bgImg.size.width - 10.0, bgImg.size.height - 10.0);
    [signInButton setBackgroundImage:bgImg forState:UIControlStateNormal];
    [signInButton addTarget:self action:@selector(shareIn) forControlEvents:UIControlEventTouchUpInside];
    
    // init the right button padding 10 from right border
    //UIView *view =[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, bgImg.size.width +10, bgImg.size.height) ];
    //[view addSubview:button];
    
    [self addSubview:signInButton];
}
- (void)addSaveButton{
    // add back button
    UIImage *bgImg = [UIImage imageNamed:@"saveButton_ipad.png"];
    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    signInButton.frame= CGRectMake(self.frame.size.width - 7 - bgImg.size.width -5, 6.0+heightIOS7, bgImg.size.width, bgImg.size.height );
    [signInButton setBackgroundImage:bgImg forState:UIControlStateNormal];
    [signInButton addTarget:self action:@selector(saveIn) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:signInButton];
}

- (void)addImageLogo{
    UIImage *bgImg = [UIImage imageNamed:@"icon-DS.png"];
    UIImageView *imageLogo = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width - 7 - bgImg.size.width - 4.0, 6.0+heightIOS7, bgImg.size.width -12.0, bgImg.size.height -12.0)];
    imageLogo.image = bgImg;
    
    [self addSubview:imageLogo];
}
-(void)signIn{
    if ([delegate respondsToSelector:@selector(shoulSignIn)])
        [delegate shoulSignIn];
}
- (void)goBack{
    if ([delegate respondsToSelector:@selector(shouldGoBack)])
        [delegate shouldGoBack];
}

- (void)shareIn{
    if ([delegate respondsToSelector:@selector(shouldGoShare)])
        [delegate shouldGoShare];
}
-(void)saveIn{
    if ([delegate respondsToSelector:@selector(shouldGoSave)])
        [delegate shouldGoSave];
}
@end
