//
//  CSCustomCellSetupViewexpandViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/21/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSCustomCellSetupViewexpandViewController : UITableViewCell{
    
    IBOutlet UILabel *trackNameLabel;
    
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *monthLabel;
    
    IBOutlet UILabel *yearLabel;
}
@property (strong, nonatomic) IBOutlet UILabel *trackNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *yearLabel;


@end
