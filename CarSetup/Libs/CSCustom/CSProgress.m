//
//  CSProgress.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSProgress.h"

@interface CSProgress (PrivateMethods)
- (CGRect)applicationFrame;
@end
@implementation CSProgress

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (id)init {
	if (self = [super initWithFrame:[self applicationFrame]]) {
		transparentLayer = [CALayer layer];
		transparentLayer.frame = self.bounds;
		transparentLayer.backgroundColor = [UIColor blackColor].CGColor;
		transparentLayer.opacity = 0.4f;
		[self.layer addSublayer:transparentLayer];
		activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		activityView.hidesWhenStopped = YES;
		[self addSubview:activityView];
		isWithLabel = NO;
		[self setNeedsLayout];
        
    }
    return self;
}

- (id)initWithLoadingText {
	if (self = [super initWithFrame:[self applicationFrame]]) {
		transparentLayer = [CALayer layer];
		transparentLayer.frame = self.bounds;
		transparentLayer.backgroundColor = [UIColor blackColor].CGColor;
		transparentLayer.opacity = 0.4f;
		[self.layer addSublayer:transparentLayer];
        
		activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		activityView.hidesWhenStopped = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[self addSubview:activityView];
        
        lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.bounds.size.height/2)+20, self.bounds.size.width, 40.0)];
        lblLoading.text = @"Loading, please wait...";
        lblLoading.backgroundColor = [UIColor clearColor];
//        lblLoading.textAlignment = UITextAlignmentCenter;
        lblLoading.font = [UIFont systemFontOfSize:14];
        lblLoading.textColor = [UIColor whiteColor];
		isWithLabel = YES;
        [self addSubview:lblLoading];
		//[self setNeedsLayout];
        [self layoutIfNeeded];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)layoutSubviews {
	transparentLayer.frame = self.bounds;
	[transparentLayer setNeedsDisplay];
    CGPoint origin = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMaxY(self.bounds) * 1 / 2);
    if (isWithLabel) {
        activityView.center = CGPointMake(origin.x, origin.y - 20);
        lblLoading.center = CGPointMake(origin.x, origin.y + 20);
    } else {
        activityView.center = CGPointMake(origin.x, origin.y);
    }
    
}

#pragma mark - animation processing
- (CGRect)applicationFrame {
	return [[UIScreen mainScreen] bounds];
}


- (void)startAnimating{
    if (self.superview == nil) {
		self.frame = [self applicationFrame];
    }
    AppDelegate *appDelegate  = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.window addSubview:self];
    [activityView startAnimating];
    [self setNeedsLayout];
	
}


- (void)stopAnimating{
    if (self.superview) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[activityView stopAnimating];
		[self removeFromSuperview];
		[self setNeedsLayout];
	}
}

- (BOOL)isAnimating{
    return [activityView isAnimating];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
@end