//
//  CSCustomNavigationViewController.m
//  CarSetup
//
//  Created by thinhpham on 3/29/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "CSCustomNavigationViewController.h"

@interface CSCustomNavigationViewController ()

@end

@implementation CSCustomNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark rotation delegate
//
//-(BOOL)shouldAutorotate {
//    return [[self.viewControllers lastObject] shouldAutorotate];
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
//}
//
//- (NSUInteger)supportedInterfaceOrientations{
//     return [[self.viewControllers lastObject] supportedInterfaceOrientations];
//}
@end
