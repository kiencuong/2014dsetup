//
//  CSCustomCellSetupViewViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/21/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSCustomCellSetupViewViewController.h"

@interface CSCustomCellSetupViewViewController ()

@end

@implementation CSCustomCellSetupViewViewController
@synthesize addButton;
@synthesize bandNameLabel;
@synthesize delegate;
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return  self;
}

- (IBAction)addButtonClick:(id)sender {
    UIButton *adButton = (UIButton *)sender;
    [delegate expandRowTableSetup:adButton.tag];
}
@end
