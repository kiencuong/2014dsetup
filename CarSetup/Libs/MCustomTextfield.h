//
//  MCustomTextfield.h
//  Mtaxi
//
//  Created by thinhpham on 5/11/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCustomTextfield : UITextField
@property (nonatomic) NSInteger leftMargin;
@end
