//
//  CSFavoriteSheetViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/25/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import <MessageUI/MessageUI.h>
#import "MySetupModel.h"
#import "SheetModel.h"
#import "CSRemoveFavoriteManager.h"
#import "CSGetInfoSheetManager.h"
#import <Twitter/Twitter.h>
@class CSProgress;
@interface CSFavoriteSheetViewController : UIViewController<CSCustomNavigationBarDelegate,FBDialogDelegate,FBSessionDelegate, removeFavoriteManagerDelegate, GetInfoSheetManagerDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate, UITextFieldDelegate,UIScrollViewDelegate> {
    
    IBOutlet UIButton *unfavoriteButton;
    IBOutlet UITextField *checkPasswordTextField;
    IBOutlet UIView *viewCheckPasswordSheet;
    IBOutlet UIView *viewSubCheckPasswordSheet;

    IBOutlet UIImageView *iconNavigationBarImage;
    IBOutlet UIView *actionSheetView;
    IBOutlet UIView *actionSheetViewLanscape;
    IBOutlet UIView *viewCoverSheetViewLanscape;
    IBOutlet UIView *popupView;
    IBOutlet UILabel *favoriteLabel;
    IBOutlet UIWebView *webviewSheet;
    
    UIWebView *webViewTweet;
    UIView *uibg;
    UIButton *_doneButton;
    
    Facebook *_facebook;
    CSGetInfoSheetManager *getInfoManager;
    CSProgress *progress;
    MySetupModel *modelSheetInfo;
    CSRemoveFavoriteManager *removeFavorite;
    SheetFullInfoModel *_sheetModelInfo;
    
    NSInteger _idSheetInfo;
    NSInteger checkRotation;
    NSInteger _scaleWebview;
    
    BOOL _checkRotationPortait;
    BOOL _checkRemoveSheetSuccess;
    BOOL _checkRemoveSheet;
    BOOL _checkShareSuccess;
    BOOL _checkisEditting;
    BOOL _checkisLanscape;
    CGSize screenSize;
    NSString *stringlinkSheet;
    int heightIOS7;

}
@property (strong, nonatomic) IBOutlet UIWebView *webviewSheet;
@property (strong, nonatomic) IBOutlet UIView *viewCheckPasswordSheet;
@property (strong, nonatomic) IBOutlet UITextField *checkPasswordTextField;
@property (strong, nonatomic) IBOutlet UIView *viewSubCheckPasswordSheet;
@property (strong, nonatomic) IBOutlet UIImageView *iconNavigationBarImage;
@property (strong, nonatomic) IBOutlet UIButton *unfavoriteButton;
@property (strong, nonatomic) IBOutlet UIView *actionSheetView;
@property (strong, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UIView *actionSheetViewLanscape;
@property (strong, nonatomic) IBOutlet UIView *viewCoverSheetViewLanscape;
@property (strong, nonatomic) CSGetInfoSheetManager *getInfoManager;
@property (strong, nonatomic) MySetupModel *modelSheetInfo;
@property (assign, nonatomic) BOOL _checkRemoveSheetSuccess;
@property (assign, nonatomic) NSInteger _idSheetInfo;
@property (strong, nonatomic) CSRemoveFavoriteManager *removeFavorite;

- (IBAction)mailButtonClick:(id)sender;
- (IBAction)facebookButtonClick:(id)sender;
- (IBAction)twitterButonClick:(id)sender;
- (IBAction)unFavoriteButtonClick:(id)sender;
- (IBAction)editButtonClick:(id)sender;
- (IBAction)cancelButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;
- (IBAction)shareButtonClick:(id)sender;
- (IBAction)popUpViewClick:(id)sender;
- (IBAction)submitButtonClick:(id)sender;
- (IBAction)viewCheckPasswordClick:(id)sender;
- (IBAction)viewSubCheckPasswordSheet:(id)sender;
@end