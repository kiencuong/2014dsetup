//
//  CSFavoriteViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSFavoriteViewController.h"
#import "CSProgress.h"
@interface CSFavoriteViewController ()

@end

@implementation CSFavoriteViewController
@synthesize _carNameTable;
@synthesize cell1;
@synthesize aboutButton;
@synthesize signOutButton;
@synthesize favoriteViewSheet;
@synthesize favoriteManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arraySectionExpand = [[NSMutableArray alloc]init];
        _arrayNameBrankModel = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    heightIOS7 = DETECT_OS7? 20 :0;
    // Do any additional setup after loading the view from its nib.
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
        [navigationCustomBar addImageTitleView:@"icon-FavoriteNavigationBar"];

    }else {
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
        [navigationCustomBar addImageTitleView:@"icon-FavoriteNavigationBar_ipad"];
    }
    navigationCustomBar.delegate = self;
    [self.view addSubview:navigationCustomBar];
    [navigationCustomBar addImageLogo];
    [aboutButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    favoriteManager = [[CSFavoriteManager alloc]init];
    favoriteManager.delegate = self;
    [ self loadListSetupSheet];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (favoriteViewSheet._checkRemoveSheetSuccess) {
        [ self loadListSetupSheet];
    }

}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addButtonClick:(id)sender {
    UIButton *adButton = (UIButton *)sender;
    [self expandRowTableSetup:adButton.tag];
}

-(void)expandRowTableSetup:(NSInteger)index{
    
    
    if ([[_arraySectionExpand objectAtIndex:index] isEqualToString:@"YES"]) {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"NO"];
    } else {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"YES"];
    }
    
    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndex:index];
    [_carNameTable reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    
}

- (IBAction)aboutButtonClick:(id)sender {
    [aboutButton setSelected:YES];
    [signOutButton setSelected:NO];
    
}

- (IBAction)signOutButtonClick:(id)sender {
    [aboutButton setSelected:NO];
    [signOutButton setSelected:YES];
    [CSUtility saveStatusLogin:NO forKey:CHECK_USER_LOGIN_SUCCESS  ];
    [CSUtility saveInfoUserLogin:@"" forKey:ID_USER_LOGIN_SUCCESS];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark get list sheet
- (void)loadListSetupSheet{
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    if (![CSUtility checkNetworkAvailable]) {
//        [self alertStatus:@"Network connection needed for this feature!" :@""];
        _arrayNameBrankModel = [X_AppDelegate read:0 withUserName:[CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS]];
        if ([_arrayNameBrankModel count]==0) {
            [self alertStatus:@"No result found!" :@"Message"];
        } else {
            for (int i=0; i <[_arrayNameBrankModel count]; i++) {
                [_arraySectionExpand addObject:@"NO"];
            }
            [_carNameTable reloadData];
        }
        [self stopLoading];
        return;
    }
    
    [favoriteManager loadDataListSetup];
    
}

- (void)finishGetListSetupSheet{
    if (favoriteManager.checkLoginAgain) {
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
    } else {

    _arrayNameBrankModel = favoriteManager._arrayNameBrankModel;
    if ([favoriteManager._arrayNameBrankModel count]==0) {
        [self alertStatus:@"No result found!" :@"Message"];
    } else {
        
        if (favoriteManager.arraySheetFavoriteDownload.count >0) {
            [AppDelegate shareAppDelegate].checkViewDownload = 1;
            if ([CSUtility loadStatusLogin:CHECK_IS_DOWNLOADING_SHEETS_FAVORITE])
                [AppDelegate shareAppDelegate].checkCancelDownloadFavorite = YES;
            [[AppDelegate shareAppDelegate] performSelector:@selector(autoDownloadSheet:) withObject:favoriteManager.arraySheetFavoriteDownload afterDelay:1];
        }
        
    for (int i=0; i <[_arrayNameBrankModel count]; i++) {
        [_arraySectionExpand addObject:@"NO"];
    }
        
    }
    [_carNameTable reloadData];
    }
    [self stopLoading];
    
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrayNameBrankModel count];;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"YES"]) {
        return [[[_arrayNameBrankModel objectAtIndex:section]sheetOfBrankModel] count];
    }
    return 0;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
      return [NSString stringWithFormat:@"%@ %@",[[_arrayNameBrankModel objectAtIndex:section]_brand_name],[[_arrayNameBrankModel objectAtIndex:section]_model_name]];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton *expandButton = [[UIButton alloc]init];
    [expandButton setBackgroundColor:[UIColor clearColor]];
    UIImageView *imageExpand = [[UIImageView alloc]init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        expandButton.frame = CGRectMake(0, 0, 300, 44);
        expandButton.tag = section;
        imageExpand.frame = CGRectMake(13, 11, 25, 24);
        if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
            imageExpand.image = [UIImage imageNamed:@"buttonAdd.png"];
        } else {
            imageExpand.image = [UIImage imageNamed:@"buttonExpand.png"];
        }
        [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        // Create label with section title
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(52, 11, 216, 21);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
        label.shadowOffset = CGSizeMake(0.0, 1.0);
        label.font = [UIFont boldSystemFontOfSize:16];
        label.text = sectionTitle;
        
        // Create header view and add label as a subview
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model.png"];
        UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300,44)];
        backgroundImageSection.image=image_section;
        [view addSubview:backgroundImageSection];
        [view addSubview:label];
        [view addSubview:imageExpand];
        [view addSubview:expandButton];
        return view;
    }
    expandButton.frame = CGRectMake(0, 0, 618, 70);
    imageExpand.frame = CGRectMake(25, 14, 45, 44);
    expandButton.tag = section;
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
        imageExpand.image = [UIImage imageNamed:@"buttonAdd_ipad.png"];
    } else {
        imageExpand.image = [UIImage imageNamed:@"buttonExpand_ipad.png"];
    }
    [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(90, 24, 480, 35);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:26];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(76, 90, 618, 70)];
    UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model_ipad.png"];
    UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 618,70)];
    backgroundImageSection.image=image_section;
    [view addSubview:backgroundImageSection];
    [view addSubview:label];
    [view addSubview:imageExpand];
    [view addSubview:expandButton];
    
    return view;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    
    if ([[_arraySectionExpand objectAtIndex:indexPath.section]isEqualToString:@"YES"]) {
        
        cell1 = (CSCustomCellSetupViewexpandViewController *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil) {
            NSArray *topLevelObjects;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CSCustomCellSetupViewexpandViewController" owner:self options:nil];
            } else {
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CSCustomCellSetupViewexpandViewController_ipad" owner:self options:nil];
            }
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[CSCustomCellSetupViewexpandViewController class]]) {
                    cell1 = (CSCustomCellSetupViewexpandViewController *) currentObject;
                    break;
                }
            }
        }
        
        cell1.trackNameLabel.text = [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_raceName];
        NSString *dateconvert = [CSUtility convertDateStringMySetup: [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_date]];
        NSArray *dateComponent = [dateconvert componentsSeparatedByString:@"/"];
        cell1.dateLabel.text = [dateComponent objectAtIndex:0];
        cell1.monthLabel.text = [dateComponent objectAtIndex:1];
        cell1.yearLabel.text = [dateComponent objectAtIndex:2];
        return cell1;
        
        
    }
     cell1.trackNameLabel.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
    
    return cell1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 44;
    }
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 44;
    }
    return 70;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            favoriteViewSheet = [[CSFavoriteSheetViewController alloc]initWithNibName:@"CSFavoriteSheetViewController" bundle:nil];
        } else {
            favoriteViewSheet = [[CSFavoriteSheetViewController alloc]initWithNibName:@"CSFavoriteSheetViewController_ipad" bundle:nil];
        }
        favoriteViewSheet.modelSheetInfo =  [[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel]objectAtIndex:indexPath.row];
        favoriteViewSheet._idSheetInfo =  [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel]objectAtIndex:indexPath.row]_id];
        [self.navigationController pushViewController:favoriteViewSheet animated:YES];
    });

}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (favoriteManager.checkLoginAgain) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
}
-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}

@end
