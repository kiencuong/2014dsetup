//
//  CSFavoriteViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCustomCellSetupViewexpandViewController.h"
#import "CSFavoriteSheetViewController.h"
#import "MySetupModel.h"
#import "MySetupBrankModel.h"
#import "CSFavoriteManager.h"
@class CSProgress;
@interface CSFavoriteViewController : UIViewController<CSCustomNavigationBarDelegate, UITableViewDataSource, UITableViewDelegate, FavoriteManagerDelegate>{

    IBOutlet UITableView *_carNameTable;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    
    NSMutableArray *_arrayNameBrankModel;
    NSMutableArray *_arraySectionExpand;
    
    CSCustomCellSetupViewexpandViewController *cell1;
    CSProgress *progress;
    CSFavoriteSheetViewController *favoriteViewSheet;
    CSFavoriteManager *favoriteManager;
    int heightIOS7;
}
@property (strong, nonatomic) IBOutlet UITableView *_carNameTable;
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) CSFavoriteManager *favoriteManager;
@property (strong, nonatomic) CSCustomCellSetupViewexpandViewController *cell1;
@property (strong, nonatomic) CSFavoriteSheetViewController *favoriteViewSheet;

- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
@end

