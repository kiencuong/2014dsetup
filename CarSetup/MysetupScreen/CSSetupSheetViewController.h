//
//  CSSetupSheetViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/25/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import <MessageUI/MessageUI.h>
#import "CSSetupSheetViewEdittingViewController.h"
#import "MySetupModel.h"
#import "SheetFullInfoModel.h"
#import "CSAddFavoriteManager.h"
#import "CSRemoveFavoriteManager.h"
#import "CSGetInfoSheetManager.h"
#import "AppDelegate.h"
@class CSProgress;

@interface CSSetupSheetViewController : UIViewController<CSCustomNavigationBarDelegate,addFavoriteManagerDelegate, removeFavoriteManagerDelegate, GetInfoSheetManagerDelegate, FBDialogDelegate,FBSessionDelegate,MFMailComposeViewControllerDelegate, UIWebViewDelegate , UITextFieldDelegate,UIScrollViewDelegate,UIScrollViewDelegate> {
    IBOutlet UIWebView *webviewSheet;
    IBOutlet UITextField *checkPasswordTextField;
    
    IBOutlet UIButton *unfavoriteButton;
    IBOutlet UIView *viewCoverSheetViewLanscape;
    IBOutlet UIImageView *iconNavigationBarImage;
    
    IBOutlet UIView *viewCheckPasswordSheet;
    IBOutlet UIView *viewSubCheckPasswordSheet;
    
    IBOutlet UIView *actionSheetView;
    IBOutlet UIView *actionSheetViewLanscape;
    IBOutlet UIView *popupView;
    IBOutlet UILabel *favoriteLabel;
    
    UIButton *doneButton;
    UIWebView *webViewTweet;
    CGSize screenSize;
    UIView *uibg;
    NSTimer *myTimer;
    UIAlertView *alertView;
    UIButton *_doneButton;

    Facebook *_facebook;
    CSSetupSheetViewEdittingViewController *setupSheetViewEdittingView;
    CSProgress *progress;
    CSGetInfoSheetManager *getInfoManager;
    CSAddFavoriteManager *addFavorite;
    CSRemoveFavoriteManager *removeFavorite;
    SheetFullInfoModel *_sheetModelInfo;
    
    NSString *_stringlinkSheet;
    NSInteger _checkRotation;
    NSInteger _idSheetInfo;
    NSInteger _scaleWebview;
    
    BOOL _checkAddSheetSuccess;
    BOOL _checkShareSuccess;
    BOOL _checkClickEditButton;
    BOOL _checkisEditting;
    BOOL _checkisLanscape;
    BOOL _checkRotationPortait;
    BOOL _checkRemoveSheetSuccess;
    int heightIOS7;

}
@property (strong, nonatomic) IBOutlet UIWebView *webviewSheet;
@property (strong, nonatomic) IBOutlet UIView *viewCheckPasswordSheet;
@property (strong, nonatomic) IBOutlet UITextField *checkPasswordTextField;
@property (strong, nonatomic) IBOutlet UIView *viewSubCheckPasswordSheet;

@property (strong, nonatomic) IBOutlet UIImageView *iconNavigationBarImage;
@property (strong, nonatomic) IBOutlet UIButton *unfavoriteButton;
@property (strong, nonatomic) IBOutlet UIView *actionSheetView;
@property (strong, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UIView *actionSheetViewLanscape;
@property (strong, nonatomic) IBOutlet UIView *viewCoverSheetViewLanscape;
@property (strong, nonatomic) IBOutlet CSSetupSheetViewEdittingViewController *setupSheetViewEdittingView;
@property (assign, nonatomic) NSInteger _idSheetInfo;
@property (strong, nonatomic) CSAddFavoriteManager *addFavorite;
@property (strong, nonatomic) CSGetInfoSheetManager *getInfoManager;

- (IBAction)mailButtonClick:(id)sender;
- (IBAction)facebookButtonClick:(id)sender;
- (IBAction)twitterButonClick:(id)sender;
- (IBAction)unFavoriteButtonClick:(id)sender;
- (IBAction)editButtonClick:(id)sender;
- (IBAction)cancelButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;
- (IBAction)shareButtonClick:(id)sender;

- (IBAction)popUpViewClick:(id)sender;
- (IBAction)submitButtonClick:(id)sender;
- (IBAction)viewCheckPasswordClick:(id)sender;
- (IBAction)viewSubCheckPasswordSheet:(id)sender;


@end

