//
//  CSSetupSheetViewEdittingViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/28/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SheetFullInfoModel.h"
#import "CSEditSheetManager.h"
#import "SBJson.h"
#import "CSProgress.h"
#import "CSGetInfoSheetManager.h"
@class  CSMySetupViewController;
@interface CSSetupSheetViewEdittingViewController : UIViewController<UIWebViewDelegate, CSEditSheetManagerDelegate,UIScrollViewDelegate>{

    IBOutlet UIImageView *iconNavigationBarImage;
    IBOutlet UIWebView *webviewSheet;
    UIButton *_doneButton;
    
    CGSize screenSize;
    NSInteger _checkPortait;
    NSInteger _scaleWebview;
    BOOL _checkPortaitOritation;
    
    SheetFullInfoModel *sheetFull;
    SheetFullInfoModel *sheetEditModel;
    CSEditSheetManager *editManager;
    CSMySetupViewController *setupView;
    CSProgress *progress;
    CSGetInfoSheetManager *getInfoManager;
    int heightIOS7;
    
}
@property (strong, nonatomic) IBOutlet UIWebView *webviewSheet;
@property (strong, nonatomic) SheetFullInfoModel *sheetFull;
@property (strong, nonatomic) CSEditSheetManager *editManager;
@property (strong, nonatomic) IBOutlet UIImageView *iconNavigationBarImage;
@property (strong, nonatomic) CSProgress *progress;
@property (assign, nonatomic) NSInteger _checkPortait;
@property (assign, nonatomic) BOOL _checkPortaitOritation;

- (IBAction)backButtonClick:(id)sender;
- (IBAction)saveButtonClick:(id)sender;

@end
