//
//  CSMySetupViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSMySetupViewController.h"
#import "CSProgress.h"

@interface CSMySetupViewController ()

@end

@implementation CSMySetupViewController
@synthesize _carNameTable;
@synthesize cell1;
@synthesize aboutButton;
@synthesize signOutButton;
@synthesize setupManager;
@synthesize deleteManager;
@synthesize _checkEditSheetSuccess;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arraySectionExpand = [[NSMutableArray alloc]init];
        _arrayNameBrankModel = [[NSMutableArray alloc]init];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initView];
    [aboutButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    setupManager = [[CSMySetupManager alloc]init];
    setupManager.delegate = self;
    [ self loadListSetupSheet];

}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_checkEditSheetSuccess) {
        _checkEditSheetSuccess = NO;
        [ self loadListSetupSheet];
    }
}
- (void)initView {
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleView:@"icon-SetupNavigationBar"];

        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    }else {
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleView:@"icon-SetupNavigationBar_ipad"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
    }
        navigationCustomBar.delegate = self;
    
        [self.view addSubview:navigationCustomBar];
        [navigationCustomBar addImageLogo];
}

- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addButtonClick:(id)sender {
    UIButton *adButton = (UIButton *)sender;
    [self expandRowTableSetup:adButton.tag];
}

-(void)expandRowTableSetup:(NSInteger)index{
    
    
    if ([[_arraySectionExpand objectAtIndex:index] isEqualToString:@"YES"]) {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"NO"];
    } else {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"YES"];
    }

    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndex:index];
    [_carNameTable reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
}

- (IBAction)aboutButtonClick:(id)sender {
    [aboutButton setSelected:YES];
    [signOutButton setSelected:NO];
    
}

- (IBAction)signOutButtonClick:(id)sender {
    [aboutButton setSelected:NO];
    [signOutButton setSelected:YES];
    [CSUtility saveStatusLogin:NO forKey:CHECK_USER_LOGIN_SUCCESS  ];
    [CSUtility saveInfoUserLogin:@"" forKey:ID_USER_LOGIN_SUCCESS];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark get list sheet
- (void)loadListSetupSheet{
//    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [self startLoading];
    if (![CSUtility checkNetworkAvailable]) {
        _arrayNameBrankModel = [NSMutableArray arrayWithArray:[X_AppDelegate read:1 withUserName:[CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS]]];        
        if ([_arrayNameBrankModel count]==0) {
            [self alertStatus:@"No result found!" :@"Message"];
        } else {
            for (int i=0; i <[_arrayNameBrankModel count]; i++) {
                [_arraySectionExpand addObject:@"NO"];
            }
            [_carNameTable reloadData];
        }
        [self stopLoading];
        return;
    }
    
    [setupManager loadDataListSetup];

}

- (void)finishGetListSetupSheet{
     [self stopLoading];
    if (setupManager.checkLoginAgain) {
        
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
    } else {
    _arrayNameBrankModel = setupManager._arrayNameBrankModel;
    if ([setupManager._arrayNameBrankModel count]==0) {
        [self alertStatus:@"No result found!" :@"Message"];
    }
    else {
    
            
        if (setupManager.arraySheetDownload.count >0) {
            [AppDelegate shareAppDelegate].checkViewDownload = 0;
            if ([CSUtility loadStatusLogin:CHECK_IS_DOWNLOADING_SHEETS])
                [AppDelegate shareAppDelegate].checkCancelDownload = YES;
                
                [[AppDelegate shareAppDelegate] performSelector:@selector(autoDownloadSheet:) withObject:setupManager.arraySheetDownload afterDelay:1];
        }

        for (int i=0; i <[_arrayNameBrankModel count]; i++) {
            [_arraySectionExpand addObject:@"NO"];
//            NSLog(@"array");
        }
    [_carNameTable reloadData];
    }
    }
    
}
#pragma mark delete list sheet
- (void)deleteSetupSheet{
    if (![CSUtility checkNetworkAvailable]) {
        [self stopLoading];
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    deleteManager = nil;
    deleteManager = [[CSDeleteMySetupSheet alloc]init];
    deleteManager.delegate = self;
    [deleteManager deleteSheet:[[[[_arrayNameBrankModel objectAtIndex:_indexpathRow.section]sheetOfBrankModel]objectAtIndex:_indexpathRow.row]_id]];
    
}

- (void)finishdeleteSheet {
    if (deleteManager.checkLoginAgain) {
        [self stopLoading];
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
        return;
    }
    if (deleteManager.checkRemoveSheetSuccess) {
       
        
        [[[_arrayNameBrankModel objectAtIndex:_indexpathRow.section]sheetOfBrankModel]removeObjectAtIndex: _indexpathRow.row];

        if ([[[_arrayNameBrankModel objectAtIndex:_indexpathRow.section]sheetOfBrankModel] count]==0) {
                [X_AppDelegate deleteBrandModel:1 withbrandid:[[_arrayNameBrankModel objectAtIndex:_indexpathRow.section]_brand_id] withmodelID:[[_arrayNameBrankModel objectAtIndex:_indexpathRow.section]_model_id]withUserName:[CSUtility loadInfoUserLogin:USERNAME_LOGIN_SUCCESS]];
                [_arrayNameBrankModel removeObjectAtIndex:_indexpathRow.section];
                [_arraySectionExpand removeObjectAtIndex:_indexpathRow.section];
//            [_carNameTable deleteSections:[NSIndexSet indexSetWithIndex:_indexpathRow.section] withRowAnimation:UITableViewRowAnimationTop];
            [_carNameTable reloadData];
        } else {
            [_carNameTable beginUpdates];
            [_carNameTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:_indexpathRow, nil] withRowAnimation:UITableViewRowAnimationTop];
            [_carNameTable endUpdates];
        }
        
        //        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationFade];
       
    } else {
         
        [_carNameTable setEditing:NO animated:YES];
    }
    [self stopLoading];
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrayNameBrankModel count];;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"YES"]) {
//        int numberRow;
        return [[[_arrayNameBrankModel objectAtIndex:section]sheetOfBrankModel] count];
    }
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton *expandButton = [[UIButton alloc]init];
    [expandButton setBackgroundColor:[UIColor clearColor]];
    UIImageView *imageExpand = [[UIImageView alloc]init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        expandButton.frame = CGRectMake(0, 0, 300, 44);
        expandButton.tag = section;
        imageExpand.frame = CGRectMake(13, 11, 25, 24);
        if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
            imageExpand.image = [UIImage imageNamed:@"buttonAdd.png"];
        } else {
            imageExpand.image = [UIImage imageNamed:@"buttonExpand.png"];
        }
        [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        // Create label with section title
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(52, 11, 216, 21);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
        label.shadowOffset = CGSizeMake(0.0, 1.0);
        label.font = [UIFont boldSystemFontOfSize:16];
        label.text = sectionTitle;
        
        // Create header view and add label as a subview
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model.png"];
        UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300,44)];
        backgroundImageSection.image=image_section;
        [view addSubview:backgroundImageSection];
        [view addSubview:label];
        [view addSubview:imageExpand];
        [view addSubview:expandButton];
        return view;
    }
    expandButton.frame = CGRectMake(0, 0, 618, 70);
    imageExpand.frame = CGRectMake(25, 14, 45, 44);
    expandButton.tag = section;
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
        imageExpand.image = [UIImage imageNamed:@"buttonAdd_ipad.png"];
    } else {
        imageExpand.image = [UIImage imageNamed:@"buttonExpand_ipad.png"];
    }
    [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(90, 24, 480,35);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:26];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(76, 90, 618, 70)];
    UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model_ipad.png"];
    UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 618,70)];
    backgroundImageSection.image=image_section;
    [view addSubview:backgroundImageSection];
    [view addSubview:label];
    [view addSubview:imageExpand];
    [view addSubview:expandButton];
    
    return view;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    

    if ([[_arraySectionExpand objectAtIndex:indexPath.section]isEqualToString:@"YES"]) {
        
        cell1 = (CSCustomCellSetupViewexpandViewController *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil) {
            NSArray *topLevelObjects;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CSCustomCellSetupViewexpandViewController" owner:self options:nil];
            } else {
                topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CSCustomCellSetupViewexpandViewController_ipad" owner:self options:nil];
            }
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[CSCustomCellSetupViewexpandViewController class]]) {
                    cell1 = (CSCustomCellSetupViewexpandViewController *) currentObject;
                    break;
                }
            }
        }
        cell1.trackNameLabel.text = [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_raceName];
        if(((NSNull *) [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_date] != [NSNull null]) && ([[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_date] != nil) && ![[[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_date] isEqual:@""]) {
            NSString *dateconvert = [CSUtility convertDateStringMySetup: [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel] objectAtIndex:indexPath.row]_date]];
            
            NSArray *dateComponent = [dateconvert componentsSeparatedByString:@"/"];
            cell1.dateLabel.text = [dateComponent objectAtIndex:0];
            cell1.monthLabel.text = [dateComponent objectAtIndex:1];
            cell1.yearLabel.text = [dateComponent objectAtIndex:2];
        }

            return cell1;

        
    }
    cell1.trackNameLabel.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];

    return cell1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 
    return [NSString stringWithFormat:@"%@ %@",[[_arrayNameBrankModel objectAtIndex:section]_brand_name],[[_arrayNameBrankModel objectAtIndex:section]_model_name]];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 44;
    }
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
    return 44;
    }
    return 70;
}

//-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0)
//        return UITableViewCellEditingStyleNone;
//    else
//        return UITableViewCellEditingStyleDelete;
//}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete) {
        _checkEditCell = YES;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Confirm Delete?"
                                                           delegate:self
                                                  cancelButtonTitle:@"NO"
                                                  otherButtonTitles:@"YES", nil];
        [alertView show];
        _indexpathRow = indexPath;
 
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        CSSetupSheetViewController *setupViewSheet;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            setupViewSheet = [[CSSetupSheetViewController alloc]initWithNibName:@"CSSetupSheetViewController" bundle:nil];
        } else {
            setupViewSheet = [[CSSetupSheetViewController alloc]initWithNibName:@"CSSetupSheetViewController_ipad" bundle:nil];
        }
        setupViewSheet._idSheetInfo =  [[[[_arrayNameBrankModel objectAtIndex:indexPath.section]sheetOfBrankModel]objectAtIndex:indexPath.row]_id];
        [self.navigationController pushViewController:setupViewSheet animated:YES];
        
    });
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (setupManager.checkLoginAgain || deleteManager.checkLoginAgain) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    if (_checkEditCell) {
        if (buttonIndex == 1) {
            double delayInSeconds = .1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
                [self deleteSetupSheet];
//            [self performSelector:@selector(deleteSetupSheet) withObject:nil afterDelay:0.5];

             });
        } else {
            [_carNameTable setEditing:NO animated:YES];
        }
        
    }
}

-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}

- (void)viewDidUnload {
    _carNameTable = nil;
    [self set_carNameTable:nil];
    [super viewDidUnload];
}
@end
