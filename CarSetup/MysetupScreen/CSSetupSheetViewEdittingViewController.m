//
//  CSSetupSheetViewEdittingViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/28/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSetupSheetViewEdittingViewController.h"
#import "CSMySetupViewController.h"

@interface CSSetupSheetViewEdittingViewController ()

@end

@implementation CSSetupSheetViewEdittingViewController
@synthesize iconNavigationBarImage;
@synthesize _checkPortait;
@synthesize _checkPortaitOritation;
@synthesize webviewSheet;
@synthesize sheetFull;
@synthesize editManager;
@synthesize progress;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        editManager = [[CSEditSheetManager alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    editManager.delegate = self;
    heightIOS7 = DETECT_OS7? 20 :0;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
//    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"]];
//	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//	[webviewSheet loadRequest:requestObj];
    webviewSheet.scrollView.delegate = self;
    [self createDoneButton];

    getInfoManager = [[CSGetInfoSheetManager alloc]init];
    [self getContentFormHTML];
}

-(void) getContentFormHTML{
    NSString *contentHtml;
    if (sheetFull._typeID == 1) {
        switch (sheetFull._brandID) {
            case 1:
            {
                switch (sheetFull._modelID) {
                    case 1:
                        contentHtml = [getInfoManager getContentHtml:sheetFull];
                        break;
                    case 12:
                        contentHtml = [getInfoManager getContentHtmlTLR22T:sheetFull];
                        break;
                    case 14:
                        contentHtml = [getInfoManager getContentHtmlTLR22SCT:sheetFull];
                        break;
                    case 34:
                        contentHtml = [getInfoManager getContentHtmlTLRSCTE:sheetFull];
                        break;
                    case 46:
                        contentHtml = [getInfoManager getContentHtmlTLR22_22_0:sheetFull];
                        break;
                    case 49:
                        contentHtml = [getInfoManager getContentHtmlTLR22_4:sheetFull];
                        break;
                    case 50:
                        contentHtml = [getInfoManager getContentHtmlTLR8ightE:sheetFull];
                        break;
                    
                    default:
                        contentHtml = [getInfoManager getContentHtmlTLR22SCT:sheetFull];
                        break;
                }
                break;
            }
            case 7:{
                if (sheetFull._modelID == 45) {
                    contentHtml = [getInfoManager getContentHtmlSchumacher_SV2:sheetFull];
                }else if(sheetFull._modelID == 13){
                    contentHtml = [getInfoManager getContentHtmlSchumacher_K1:sheetFull];
                }

                
                break;
            }
            case 8:
            {
                switch (sheetFull._modelID) {
                    case 15:
                        contentHtml = [getInfoManager getContentHtmlAssociated_SC:sheetFull];
                        break;
                    case 17:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B42:sheetFull];
                        break;
                    case 18:
                        contentHtml = [getInfoManager getContentHtmlAssociated_T42:sheetFull];
                        break;
                    case 19:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B442:sheetFull];
                        break;
                    case 48:
                        contentHtml = [getInfoManager getContentHtmlAssociated_C42:sheetFull];
                        break;
                    case 53:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B5:sheetFull];
                        break;
                    case 54:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B5M:sheetFull];
                        break;
                }
                break;
            }
            case 9:{
                if (sheetFull._modelID == 36) {
                    contentHtml = [getInfoManager getContentHtmlKyosho_RB6:sheetFull];
                } else if (sheetFull._modelID == 37){
                    contentHtml = [getInfoManager getContentHtmlKyosho_UltimaSCR:sheetFull];
                }
                
                break;
            }
            case 12:
            {
                switch (sheetFull._modelID) {
                    case 22:
                        contentHtml = [getInfoManager getContentHtmlDurango_DESC210:sheetFull];
                        break;
                    case 23:
                        contentHtml = [getInfoManager getContentHtmlDurango_DEX210:sheetFull];
                        break;
                    case 24:
                        contentHtml = [getInfoManager getContentHtmlDurango_DESC410:sheetFull];
                        break;
                    case 25:
                        contentHtml = [getInfoManager getContentHtmlDurango_DEX410:sheetFull];
                        break;
                }
                break;
            }
            case 29:
            {
                switch (sheetFull._modelID) {
                    case 51:
                        contentHtml = [getInfoManager getContentHtmlSerpent_SRX2:sheetFull];
                        break;
                    case 52:
                        contentHtml = [getInfoManager getContentHtmlSerpent_SRX2_MID:sheetFull];
                        break;
                }
                break;
            }

            case 10:
            {
                if (sheetFull._modelID == 40) {
                        contentHtml = [getInfoManager getContentHtmlYokomo_BMAX2:sheetFull];
                }
                break;
            }
            case 30:
            {
                if (sheetFull._modelID == 56) {
                    contentHtml = [getInfoManager getContentHtmlTekno_SCT410:sheetFull];
                    break;
                } else if (sheetFull._modelID == 60) {
                    contentHtml = [getInfoManager getContentHtmlTekno_EB482:sheetFull];
                } else if (sheetFull._modelID == 61) {
                    contentHtml = [getInfoManager getContentHtmlTekno_ET48:sheetFull];
                }
                break;
            }
            case 31:
            {
                if (sheetFull._modelID == 57) {
                    contentHtml = [getInfoManager getContentHtmlHotbodies_D413:sheetFull];
                    
                }
                break;
            }
                
            default:
                break;
        }

    }else if (sheetFull._typeID == 2){
        switch (sheetFull._brandID) {
            case 13:
            {
                if (sheetFull._modelID == 26) {
                    contentHtml = [getInfoManager getContentHtmlSerpent_S411:sheetFull];
                }
                break;
            }
            case 17:
            {
                if (sheetFull._modelID == 29) {
                        contentHtml = [getInfoManager getContentHtmlHotbodies_TCXX:sheetFull];
                }
                break;
            }
            case 23:
            {
                if (sheetFull._modelID == 39) {
                        contentHtml = [getInfoManager getContentHtmlYokomo_BD7:sheetFull];
                }
                break;
            }
            case 24:{
                if (sheetFull._modelID == 41) {
                    contentHtml = [getInfoManager getContentHtmlSchumacher_Mi5:sheetFull];
                    
                }
                break;
            }
            case 28:
            {
                if (sheetFull._modelID == 47) {
                    contentHtml = [getInfoManager getContentHtmlAwesomatix:sheetFull];
                }
                break;
            }
        }
    }else if (sheetFull._typeID == 3){
        switch (sheetFull._brandID) {
            case 20:
            {
                if (sheetFull._modelID == 35) {
                        contentHtml = [getInfoManager getContentHtmlTLR_8ight30:sheetFull];
                }
                break;
            }
            case 11:
            {
                if (sheetFull._modelID == 21) {
                    contentHtml = [getInfoManager getContentHtmlMugen_MBX7:sheetFull];
                }
                break;
            }
            case 14:
            {
                if (sheetFull._modelID == 27) {
                    contentHtml = [getInfoManager getContentHtmlSerpent_811B:sheetFull];
                }
                break;
            }

            case 16:
            {
                if (sheetFull._modelID == 28) {
                        contentHtml = [getInfoManager getContentHtmlHotbodies_D812:sheetFull];
                }
                break;
            }
            case 27:
            {
                if (sheetFull._modelID == 44) {
                        contentHtml = [getInfoManager getContentHtmlAssociated_RC82:sheetFull];
                }
                break;
            }
            case 21:
            {
                if (sheetFull._modelID == 16) {
                    contentHtml = [getInfoManager getContentHtmlKyosho_MP9:sheetFull];
                }
                break;
            }
            case 25:
            {
                if (sheetFull._modelID == 42) {
                    contentHtml = [getInfoManager getContentHtmlAgama_A8Evo:sheetFull];
                }
                break;
            }
            case 26:
            {
                if (sheetFull._modelID == 43) {
                    contentHtml = [getInfoManager getContentHtmlJQProducts_TheCAR:sheetFull];
                }
                break;
            }
            case 32:
            {
                if (sheetFull._modelID == 58) {
                    contentHtml = [getInfoManager getContentHtmlTekno_NB48:sheetFull];
                } else if (sheetFull._modelID == 59) {
                    contentHtml = [getInfoManager getContentHtmlTekno_NT48:sheetFull];
                }
                break;
            }

        }
    }

    
    
    [self.webviewSheet loadHTMLString:contentHtml baseURL:nil];

}

#pragma mark custom button done in ipad keyboard
- (void)doneButton:(id)sender {
    
    [self.view endEditing:YES];
}

- (void)createDoneButton {
	// create custom button
    
    if (_doneButton == nil) {
        
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.adjustsImageWhenHighlighted = NO;
        [_doneButton setImage:[UIImage imageNamed:@"doneButton.png"] forState:UIControlStateNormal];
        [_doneButton setImage:[UIImage imageNamed:@"doneButton.png"] forState:UIControlStateHighlighted];
        
        [_doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
        
        _doneButton.hidden = YES;  // we hide/unhide him from here on in with the appropriate method
    }
}

- (void)hideDoneButton
{
    [_doneButton removeFromSuperview];
    _doneButton.hidden = YES;
}

- (void)unhideDoneButton
{
    // this here is a check that prevents NSRangeException crashes that were happening on retina devices
    int windowCount = [[[UIApplication sharedApplication] windows] count];
    if (windowCount < 2) {
        return;
    }
    
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex: 1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++) {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        
        // THIS IS THE HACK BELOW.  I MEAN, PROPERLY HACKY!
        if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
        {
            _doneButton.frame = CGRectMake(keyboard.frame.size.width-53-35, -3, 106, 53);
            [keyboard addSubview: _doneButton];
        }
        else if([[keyboard description] hasPrefix:@"<UIKeyboardA"] == YES)
        {
            _doneButton.frame = CGRectMake(keyboard.frame.size.width-53-35, -3, 106, 53);
            [keyboard addSubview: _doneButton];
        }
    }

    _doneButton.hidden = NO;
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    /* No longer listen for keyboard */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    
    [super viewWillDisappear:animated];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    [self unhideDoneButton];
    // unused.
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    [self unhideDoneButton];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    // unused
    [self hideDoneButton];
}
- (void)keyboardDidHide:(NSNotification *)notification
{
    // unused
    [self hideDoneButton];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    NSLog(@"%@ for textField %i", NSStringFromSelector(_cmd), textField.tag);
    
    [self hideDoneButton];
    
    return YES;
}


- (void) viewWillAppear:(BOOL)animated {
    /* Listen for keyboard */
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    [super viewWillAppear:animated];

}
#pragma mark edit sheet
- (void)editSheetInfo{
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [editManager EditSheet:sheetEditModel];
    
    
}
- (void)finishEditSheet {
    [self stopLoading];
    if (editManager.checkEditNewSuccess) {
        setupView = (CSMySetupViewController *)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-3];
        setupView._checkEditSheetSuccess = YES;
        [self.navigationController popToViewController:setupView animated:YES];
    }
}
#pragma mark interface Orientation
// only run in iOS 5 & iOS 4.3
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    NSInteger rotationDevice = [[UIDevice currentDevice] orientation];
    if (_checkPortait < 2) {
        _checkPortait++;
        _checkPortaitOritation = YES;
        [self updateViewPortait];
        return UIInterfaceOrientationMaskPortrait;
    }
    if (rotationDevice <=2 ) {
        iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
        _checkPortaitOritation = YES;
        [self updateViewPortait];
        return UIInterfaceOrientationMaskPortrait;
        
    } else {
        _checkPortaitOritation = NO;
        [self updateViewLanscape];
        return UIInterfaceOrientationMaskLandscape;
    }
}

// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    NSInteger rotationDevice = [[UIDevice currentDevice] orientation];
        if (_checkPortait < 2) {
            _checkPortait++;
            _checkPortaitOritation = YES;
            [self updateViewPortait];
            return UIInterfaceOrientationMaskPortrait;
        }
        if (rotationDevice <=2 ) {
            iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
            _checkPortaitOritation = YES;
            [self updateViewPortait];
            return UIInterfaceOrientationMaskPortrait;
            
        } else {
            _checkPortaitOritation = NO;
             [self updateViewLanscape];
            return UIInterfaceOrientationMaskLandscape;
        }

}
-(BOOL)shouldAutorotate{
    return YES;
}
#pragma mark webview delegate
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSArray *urlParamsArray;
    NSString *url = [[request URL] absoluteString];
    if([url rangeOfString:@"%7B" options:NSCaseInsensitiveSearch].length > 0){
         urlParamsArray= [url componentsSeparatedByString:@"/%7B"];
        NSString *jsonString = [[[NSString stringWithFormat:@"%@%@", @"%7B", [urlParamsArray objectAtIndex:1]] stringByReplacingOccurrencesOfString:@"/%22" withString:@"\\\%22"]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //        NSLog(@" string convert :%@",jsonString);
        sheetEditModel = [[SheetFullInfoModel alloc]init];
        sheetEditModel = [editManager getInfoSheetFromHTML:jsonString];
        sheetEditModel._ID = sheetFull._ID;
        sheetEditModel._typeID = sheetFull._typeID;
        sheetEditModel._brandID = sheetFull._brandID;
        sheetEditModel._modelID = sheetFull._modelID;
        sheetEditModel.date = sheetFull.date;
//        NSDictionary *dictJson = [jsonString JSONValue];
//        sheetEditModel.jsonDetail = [[dictJson objectForKey:@"sheets"] JSONRepresentation];
        [self editSheetInfo];
        return NO;
    } else {
        if([url rangeOfString:@"param" options:NSCaseInsensitiveSearch].length > 0){
            urlParamsArray= [url componentsSeparatedByString:@"param="];
            NSInteger idField = [[urlParamsArray objectAtIndex:1]intValue];
            switch (idField) {
                case 1:
                    [self alertStatus:@"Form can't be send.\nPlease fill name field.":@"" ];
                    break;
                case 2:
                    [self alertStatus:@"Form can't be send.\nPlease fill track field.":@"" ];
                    break;
                case 3:
                    [self alertStatus:@"Form can't be send.\nPlease fill city field.":@"" ];
                    break;
                case 4:
                    [self alertStatus:@"Form can't be send.\nPlease fill race field.":@"" ];
                    break;
                    
                default:
                    [self alertStatus:@"Form can't be send.\nPlease fill country field.":@"" ];
                    break;
            }
            return NO;
        }
        return YES;
    }

}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
    
    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    _scaleWebview = (float) viewSize.width / contentSize.width;
    
}
-(void) updateViewPortait {
    if (_scaleWebview == 1) {
        [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.0;"];
        
    }
    float ratioHeigthWidth;
    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webviewSheet.scrollView.minimumZoomScale = ratioHeigthWidth;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
    } else {
        iconNavigationBarImage.frame = CGRectMake((screenSize.width-29)/2, 7+heightIOS7, 29, 30);
    }
}

-(void) updateViewLanscape {
    if (_scaleWebview == 1) {
        
        // for ipad
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.4;"];
            // for iphone
        }else{
            if (screenSize.height == 568) {
                [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.9;"];
            } else {
                [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.6;"];
            }
        }
        
    }
    float ratioHeigthWidth;
    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webviewSheet.scrollView.minimumZoomScale = ratioHeigthWidth;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((screenSize.height-38)/2, 3+heightIOS7, 38, 36);
        
    } else {
        iconNavigationBarImage.frame = CGRectMake((screenSize.height-29)/2, 7+heightIOS7, 29, 30);
    }
}

- (IBAction)saveButtonClick:(id)sender {
    [self.view endEditing:YES];
    [webviewSheet stringByEvaluatingJavaScriptFromString:@"getInfoSheets();"];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backButtonClick:(id)sender {
    webviewSheet = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    webviewSheet = nil;
    [self setWebviewSheet:nil];
    [super viewDidUnload];
}
@end

