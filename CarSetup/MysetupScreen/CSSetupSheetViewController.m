 //
//  CSSetupSheetViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/25/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSetupSheetViewController.h"
#import "CSProgress.h"
#import <Twitter/Twitter.h>
@interface CSSetupSheetViewController ()

@end

@implementation CSSetupSheetViewController
@synthesize unfavoriteButton;
@synthesize actionSheetView;
@synthesize favoriteLabel;
@synthesize popupView;
@synthesize viewCoverSheetViewLanscape;
@synthesize iconNavigationBarImage;
@synthesize actionSheetViewLanscape;
@synthesize setupSheetViewEdittingView;
@synthesize _idSheetInfo;
@synthesize addFavorite;
@synthesize getInfoManager;
@synthesize webviewSheet;
@synthesize checkPasswordTextField;
@synthesize viewCheckPasswordSheet;
@synthesize viewSubCheckPasswordSheet;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _sheetModelInfo = [[SheetFullInfoModel alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2, (viewCheckPasswordSheet.frame.size.height - 200)/2, 320, 200);
    if (screenSize.height == 568) {
        actionSheetView.frame = CGRectMake(0, 0,320, screenSize.height-20);
        actionSheetViewLanscape.frame = CGRectMake(50, 0+heightIOS7, 460, 276);
    } else {
        actionSheetViewLanscape.frame = CGRectMake(0, 0+heightIOS7, 460, 276);
    }
    _checkRotationPortait = YES;
    
    viewCoverSheetViewLanscape.hidden = YES;
    popupView.hidden=YES;
    getInfoManager = [[CSGetInfoSheetManager alloc]init];
    getInfoManager.delegate = self;
    checkPasswordTextField.delegate = self;
    
    webviewSheet.scrollView.delegate = self;
    
    [ self loadInfoSheetSetup ];

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (setupSheetViewEdittingView) {
        if (setupSheetViewEdittingView._checkPortaitOritation) {
            actionSheetView.hidden = NO;
            viewCoverSheetViewLanscape.hidden = YES;
            _checkRotationPortait = YES;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
            } else {
                iconNavigationBarImage.frame = CGRectMake((screenSize.width-29)/2, 7+heightIOS7, 29, 30);
            }
        } else {
            actionSheetView.hidden = YES;
            viewCoverSheetViewLanscape.hidden = NO;
            _checkRotationPortait = NO;
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                iconNavigationBarImage.frame = CGRectMake((screenSize.height-38)/2, 3+heightIOS7, 38, 36);
            } else {
                iconNavigationBarImage.frame = CGRectMake((screenSize.height-29)/2, 7+heightIOS7, 29, 30);
            }
        }

    }
}



-(void) hideShowNotification:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    
    //    if (contactView) {
    //        contactView.alpha = 0.0;
    //    }
    if (popupView) {
        popupView.hidden = YES;
    }
    
    [UIView commitAnimations];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark load info sheet
- (void)loadInfoSheetSetup{
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    if (![CSUtility checkNetworkAvailable]) {
        
        _sheetModelInfo = [getInfoManager parserJsonSheetFull:[X_AppDelegate readSheetInfo:_idSheetInfo]];
        if (_sheetModelInfo._isFavorite) {
            favoriteLabel.text = @"Unfavorite";
        } else {
            favoriteLabel.text = @"Favorite";
        }
        [self getContentFormHTML];
        
        [self stopLoading];
        return;
    }
    
    
    [getInfoManager loadDataListSetup:_idSheetInfo];
    
}

- (void)finishLoadInfoSheet{
    [self stopLoading];
    if (getInfoManager.checkLoginAgain) {
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
        return;
    }
    _sheetModelInfo = getInfoManager._sheetModel;
    if (_sheetModelInfo._isFavorite) {
        favoriteLabel.text = @"Unfavorite";
    } else {
        favoriteLabel.text = @"Favorite";
    }
    [self getContentFormHTML];
    
}
#pragma mark webview delegate

//-(void)webViewDidFinishLoad:(UIWebView *)webView{
//    //    [webView stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 2.0;"];
//    //    webView.scrollView.maximumZoomScale = 2;
//}

-(void) getContentFormHTML{
    NSString *contentHtml;
    if (_sheetModelInfo._typeID == 1) {
        switch (_sheetModelInfo._brandID) {
            case 1:
            {
                switch (_sheetModelInfo._modelID) {
                    case 1:
                        contentHtml = [getInfoManager getContentHtml:_sheetModelInfo];
                        break;
                    case 12:
                        contentHtml = [getInfoManager getContentHtmlTLR22T:_sheetModelInfo];
                        break;
                    case 14:
                        contentHtml = [getInfoManager getContentHtmlTLR22SCT:_sheetModelInfo];
                        break;
                    case 34:
                        contentHtml = [getInfoManager getContentHtmlTLRSCTE:_sheetModelInfo];
                        break;
                    case 46:
                        contentHtml = [getInfoManager getContentHtmlTLR22_22_0:_sheetModelInfo];
                        break;
                    case 49:
                        contentHtml = [getInfoManager getContentHtmlTLR22_4:_sheetModelInfo];
                        break;
                    case 50:
                        contentHtml = [getInfoManager getContentHtmlTLR8ightE:_sheetModelInfo];
                        break;
                    

                    default:
                        contentHtml = [getInfoManager getContentHtmlTLR22SCT:_sheetModelInfo];
                        break;
                }
                break;
            }
            case 7:{
                if (_sheetModelInfo._modelID == 45) {
                    contentHtml = [getInfoManager getContentHtmlSchumacher_SV2:_sheetModelInfo];
                } else if(_sheetModelInfo._modelID == 13){
                    contentHtml = [getInfoManager getContentHtmlSchumacher_K1:_sheetModelInfo];
                }
               
                break;
            }
            case 8:
            {
                switch (_sheetModelInfo._modelID) {
                    case 15:
                        contentHtml = [getInfoManager getContentHtmlAssociated_SC:_sheetModelInfo];
                        break;
                    case 17:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B42:_sheetModelInfo];
                        break;
                    case 18:
                        contentHtml = [getInfoManager getContentHtmlAssociated_T42:_sheetModelInfo];
                        break;
                    case 19:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B442:_sheetModelInfo];
                        break;
                    case 48:
                        contentHtml = [getInfoManager getContentHtmlAssociated_C42:_sheetModelInfo];
                        break;
                    case 53:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B5:_sheetModelInfo];
                        break;
                    case 54:
                        contentHtml = [getInfoManager getContentHtmlAssociated_B5M:_sheetModelInfo];
                        break;
                }
                break;
            }
                
            case 9:{
                if (_sheetModelInfo._modelID == 36) {
                    contentHtml = [getInfoManager getContentHtmlKyosho_RB6:_sheetModelInfo];
                } else if (_sheetModelInfo._modelID == 37){
                    contentHtml = [getInfoManager getContentHtmlKyosho_UltimaSCR:_sheetModelInfo];
                }
                
                break;
            }
            case 12:
            {
                switch (_sheetModelInfo._modelID) {
                    case 22:
                        contentHtml = [getInfoManager getContentHtmlDurango_DESC210:_sheetModelInfo];
                        break;
                    case 23:
                        contentHtml = [getInfoManager getContentHtmlDurango_DEX210:_sheetModelInfo];
                        break;
                    case 24:
                        contentHtml = [getInfoManager getContentHtmlDurango_DESC410:_sheetModelInfo];
                        break;
                    case 25:
                        contentHtml = [getInfoManager getContentHtmlDurango_DEX410:_sheetModelInfo];
                        break;
                }
                break;
            }
            case 29:
            {
                switch (_sheetModelInfo._modelID) {
                    case 51:
                        contentHtml = [getInfoManager getContentHtmlSerpent_SRX2:_sheetModelInfo];
                        break;
                    case 52:
                        contentHtml = [getInfoManager getContentHtmlSerpent_SRX2_MID:_sheetModelInfo];
                        break;
                }
                break;
            }

            case 10:
            {
                if (_sheetModelInfo._modelID == 40) {
                        contentHtml = [getInfoManager getContentHtmlYokomo_BMAX2:_sheetModelInfo];
                        break;
                }
                break;
            }
            case 30:
            {
                if (_sheetModelInfo._modelID == 56) {
                    contentHtml = [getInfoManager getContentHtmlTekno_SCT410:_sheetModelInfo];
                    break;
                } else if (_sheetModelInfo._modelID == 60) {
                    contentHtml = [getInfoManager getContentHtmlTekno_EB482:_sheetModelInfo];
                } else if (_sheetModelInfo._modelID == 61) {
                    contentHtml = [getInfoManager getContentHtmlTekno_ET48:_sheetModelInfo];
                }
                break;
            }
            case 31:
            {
                if (_sheetModelInfo._modelID == 57) {
                    contentHtml = [getInfoManager getContentHtmlHotbodies_D413:_sheetModelInfo];
                    
                }
                break;
            }
                
            default:
                break;
        }
    }else if (_sheetModelInfo._typeID == 2){
        switch (_sheetModelInfo._brandID) {
            case 13:
            {
                if (_sheetModelInfo._modelID == 26) {
                    contentHtml = [getInfoManager getContentHtmlSerpent_S411:_sheetModelInfo];
                }
                break;
            }
            case 17:
            {
               if (_sheetModelInfo._modelID == 29) {
                        contentHtml = [getInfoManager getContentHtmlHotbodies_TCXX:_sheetModelInfo];
                }
                break;
            }
            case 23:
            {
                if (_sheetModelInfo._modelID == 39) {
                        contentHtml = [getInfoManager getContentHtmlYokomo_BD7:_sheetModelInfo];
                }
                break;
            }
            case 24:{
                if (_sheetModelInfo._modelID == 41) {
                    contentHtml = [getInfoManager getContentHtmlSchumacher_Mi5:_sheetModelInfo];
                    
                }
                break;
            }
            case 28:
            {
                if (_sheetModelInfo._modelID == 47) {
                    contentHtml = [getInfoManager getContentHtmlAwesomatix:_sheetModelInfo];
                }
                break;
            }
        }
    }else if (_sheetModelInfo._typeID == 3){
        switch (_sheetModelInfo._brandID) {
            case 20:
            {
                if (_sheetModelInfo._modelID == 35) {
                        contentHtml = [getInfoManager getContentHtmlTLR_8ight30:_sheetModelInfo];
                        break;
                }
                break;
            }
            case 11:
            {
                if (_sheetModelInfo._modelID == 21) {
                    contentHtml = [getInfoManager getContentHtmlMugen_MBX7:_sheetModelInfo];
                }
                break;
            }
            case 14:
            {
                if (_sheetModelInfo._modelID == 27) {
                    contentHtml = [getInfoManager getContentHtmlSerpent_811B:_sheetModelInfo];
                }
                break;
            }
                
            case 16:
            {
                if (_sheetModelInfo._modelID == 28) {
                        contentHtml = [getInfoManager getContentHtmlHotbodies_D812:_sheetModelInfo];

                }
                break;
            }
            case 27:
            {
               if (_sheetModelInfo._modelID == 44) {
                        contentHtml = [getInfoManager getContentHtmlAssociated_RC82:_sheetModelInfo];
                }
                break;
            }
            case 21:
            {
                if (_sheetModelInfo._modelID == 16) {
                    contentHtml = [getInfoManager getContentHtmlKyosho_MP9:_sheetModelInfo];
                }
                break;
            }
            case 25:
            {
                if (_sheetModelInfo._modelID == 42) {
                    contentHtml = [getInfoManager getContentHtmlAgama_A8Evo:_sheetModelInfo];
                }
                break;
            }
            case 26:
            {
                if (_sheetModelInfo._modelID == 43) {
                    contentHtml = [getInfoManager getContentHtmlJQProducts_TheCAR:_sheetModelInfo];
                }
                break;
            }
            case 32:
            {
                if (_sheetModelInfo._modelID == 58) {
                    contentHtml = [getInfoManager getContentHtmlTekno_NB48:_sheetModelInfo];
                } else if (_sheetModelInfo._modelID == 59) {
                    contentHtml = [getInfoManager getContentHtmlTekno_NT48:_sheetModelInfo];
                }
                break;
            }
            
        }
    }

    [self.webviewSheet loadHTMLString:contentHtml baseURL:nil];

    [self stopLoading];
    if ([_sheetModelInfo.check_Password isEqualToString:@""]) {
        viewCheckPasswordSheet.hidden = YES;
    } else {
        viewCheckPasswordSheet.hidden = NO;
    }
    _stringlinkSheet = [[NSString stringWithFormat:@"%@sheets/download/id/%ld/%@-%@-%@-%ld.pdf",URL_FORMAT_LOGIN,(long)_sheetModelInfo._ID,_sheetModelInfo._brandName, _sheetModelInfo._modelName,_sheetModelInfo.name_race,(long)_sheetModelInfo._ID]stringByReplacingOccurrencesOfString:@" " withString:@"-"];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    // Disable user selection
    [webviewSheet stringByEvaluatingJavaScriptFromString:@"disableFormInput();"];

}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(copy:) ||
        action == @selector(paste:)||
        action == @selector(cut:))
    {
        return NO;
    }
    return [super canPerformAction:action withSender:sender];
}

#pragma mark action view
- (IBAction)mailButtonClick:(id)sender {
    [self hideShowNotification:sender];
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *stringTitle = [NSString stringWithFormat:@"%@'s %@%@ Setup ", _sheetModelInfo.driver, _sheetModelInfo._brandName, _sheetModelInfo._modelName];
    NSString *bodyString = [NSString stringWithFormat:@"Link to Download %@'s setup sheet for a %@%@ from the %@. <a href=%@>%@</a>. Check it out and more at <a href=\"http://www.DialedSetups.com/\">%@</a>", _sheetModelInfo.driver,  _sheetModelInfo._brandName,  _sheetModelInfo._modelName, _sheetModelInfo.name_race,_stringlinkSheet,_stringlinkSheet,URL_FORMAT_LOGIN];
    [params setObject:stringTitle forKey:@"title"];
    [params setObject:stringTitle forKey:@"subject"];
    [params setObject:bodyString forKey:@"body"];
    [self showEmailComposer:params];
}

- (IBAction)facebookButtonClick:(id)sender {
    [self hideShowNotification:sender];
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    NSString *stringName = [NSString stringWithFormat:@"%@'s %@%@ Setup ", _sheetModelInfo.driver, _sheetModelInfo._brandName, _sheetModelInfo._modelName];
    NSMutableDictionary *params =  [[NSMutableDictionary alloc] init];
    [params setObject:APP_ID_FACEBOOK forKey:@"app_id"];
    [params setObject:_stringlinkSheet forKey:@"link"];
    [params setObject:[NSString stringWithFormat:@"%@images/sheet_fb.png",URL_FORMAT_LOGIN] forKey:@"picture"];
    [params setObject:stringName forKey:@"name"];
    [params setObject:URL_FORMAT_LOGIN forKey:@"caption"];
    [params setObject:[NSString stringWithFormat:@"%@ %@",[_sheetModelInfo.name_race uppercaseString] ,[_sheetModelInfo.track uppercaseString]] forKey:@"description"];
    _facebook =  [[Facebook alloc] initWithAppId:APP_ID_FACEBOOK andDelegate:self];
    
    [_facebook dialog:@"feed" andParams:params andDelegate:self];
}
- (void) dialogCompleteWithUrl:(NSURL*) url
{
    if ([url.absoluteString rangeOfString:@"post_id="].location != NSNotFound) {
        //alert user of successful post
        _checkShareSuccess = YES;
        [self alertStatus:@"Shared to Facebook successfully!" :@""];
    } else {
        //user pressed "cancel"
    }
}
- (IBAction)twitterButonClick:(id)sender {
    [self hideShowNotification:sender];
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        NSString *stringName = [NSString stringWithFormat:@"%@'s %@%@ Setup ", _sheetModelInfo.driver, _sheetModelInfo._brandName, _sheetModelInfo._modelName];
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
//        [tweetSheet setInitialText:[NSString stringWithFormat:@"%@\n%@\n%@",stringName,_sheetModelInfo.name_race,_sheetModelInfo.track ]];
        [tweetSheet setInitialText:stringName];
        [tweetSheet addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@images/sheet_fb.png",URL_FORMAT_LOGIN]]]]];
        [tweetSheet addURL:[NSURL URLWithString:_sheetModelInfo._linkShareTwitter]];
        tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult res) {
            if (res == TWTweetComposeViewControllerResultDone) {
                // Composed
                _checkShareSuccess = YES;
                [self alertStatus:@"Shared to Twitter successfully!" :@""];
            } else if (res == TWTweetComposeViewControllerResultCancelled) {
                // Cancelled
            }
            [self dismissModalViewControllerAnimated:YES];
            
        };
        [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        [self alertStatus:@"There are no Twitter accounts configured. Please go to your Phone settings to add or create a Twitter account." :@""];
    }
    
}

- (IBAction)unFavoriteButtonClick:(id)sender {
    
    [self hideShowNotification:sender];
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    
    
    double delayInSeconds = .1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
        //        [self startLoading];
        if (_sheetModelInfo._isFavorite) {
            removeFavorite = nil;
            removeFavorite = [[CSRemoveFavoriteManager alloc]init];
            removeFavorite.delegate = self;
            [removeFavorite removeFavorite:_idSheetInfo] ;
            
        } else {
            
            addFavorite = nil;
            addFavorite = [[CSAddFavoriteManager alloc]init];
            addFavorite.delegate = self;
            [addFavorite addFavorite:_idSheetInfo] ;
            
        }
        
        
    });
    
    
    
}

-(void) finishAddFavorite {
    [self stopLoading];
    if (addFavorite.checkLoginAgain) {
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
        return;
    }
    
    if (addFavorite.checkAddSuccess) {
        _sheetModelInfo._isFavorite = 0;
        _checkAddSheetSuccess = YES;
        favoriteLabel.text = @"Unfavorite";
        [self alertStatus:@"Add to Favorites successful" :@""];
    }
}
-(void) finishremoveFavorite {
    if (removeFavorite.checkLoginAgain) {
        [self stopLoading];
        [self alertStatus:@"Another device has signed in with your account. Please login again!" :@"Message"];
        return;
    }
    [self stopLoading];
    if (removeFavorite.checkRemoveSheetSuccess) {
        _sheetModelInfo._isFavorite =1;
        _checkRemoveSheetSuccess = YES;
        favoriteLabel.text = @"Favorite";
        [self alertStatus:@"Remove from Favorites successful" :@""];
    }
}

- (IBAction)editButtonClick:(id)sender {
    [self hideShowNotification:sender];
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    if ([viewCheckPasswordSheet isHidden]) {
        _checkClickEditButton = NO;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            setupSheetViewEdittingView = [[CSSetupSheetViewEdittingViewController alloc]initWithNibName:@"CSSetupSheetViewEdittingViewController" bundle:nil];
        }else {
            setupSheetViewEdittingView = [[CSSetupSheetViewEdittingViewController alloc]initWithNibName:@"CSSetupSheetViewEdittingViewController_ipad" bundle:nil];
        }
        if (_checkRotationPortait) {
            setupSheetViewEdittingView._checkPortait = 0;
        } else {
            setupSheetViewEdittingView._checkPortait = 3;
        }
        setupSheetViewEdittingView.sheetFull = _sheetModelInfo;
        [self.navigationController pushViewController:setupSheetViewEdittingView animated:YES];
    } else {
        _checkClickEditButton = YES;
    }
        

}

- (IBAction)cancelButtonClick:(id)sender {
    popupView.hidden=YES;
}

- (IBAction)backButtonClick:(id)sender {
    webviewSheet = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareButtonClick:(id)sender {
    popupView.hidden=NO;
}

- (IBAction)popUpViewClick:(id)sender {
    [self hideShowNotification:sender];
}
- (void)setFrameTextfield {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        if (_checkRotationPortait) {
            if (checkPasswordTextField.frame.origin.y ==85) {
                viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2,80, 320, 200);
            }
        } else {
            viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2,-25, 320, 200);
        }
        
    } else {
        if (!_checkRotationPortait) {
            viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2,150, 320, 200);
        }
        
    }

}
#pragma mark textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _checkisEditting = YES;
    [self setFrameTextfield];
  }

-(void)textFieldDidEndEditing:(UITextField *)textField{
    _checkisEditting = NO;
        viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2, (viewCheckPasswordSheet.frame.size.height - 200)/2, 320, 200);

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    //    [dangNhapButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    return YES;
}
- (IBAction)submitButtonClick:(id)sender {
    [self.view endEditing:YES];
    if ([checkPasswordTextField.text isEqualToString: _sheetModelInfo.password]) {
        viewCheckPasswordSheet.hidden = YES;
    } else {
        [self alertStatus:@"Password is incorrect. Please try again!" :@"Message"];
    }
    if (_checkClickEditButton) {
        [self editButtonClick:sender];
    }
}
- (IBAction)viewCheckPasswordClick:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)viewSubCheckPasswordSheet:(id)sender {
    [self.view endEditing:YES];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (getInfoManager.checkLoginAgain ||addFavorite.checkLoginAgain ||
        removeFavorite.checkLoginAgain) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    if (buttonIndex ==0 ) {
        
        if (_checkAddSheetSuccess || _checkRemoveSheetSuccess || _checkShareSuccess) {
            _checkRemoveSheetSuccess = NO;
            _checkAddSheetSuccess = NO;
            _checkShareSuccess = NO;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

#pragma mark mail delegate
- (void) showEmailComposer:(NSMutableDictionary*)options
{
    
//    NSString* toRecipientsString = [options valueForKey:@"toRecipients"];
    NSString* title = [options valueForKey:@"title"];
	NSString* subject = [options valueForKey:@"subject"];
    NSString* body = [options valueForKey:@"body"];
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        if ([mailClass canSendMail]) {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            // Set title
            if(title != nil)
                [picker setTitle:title];
            
            // Set subject
            if(subject != nil)
                [picker setSubject:[NSString stringWithFormat:@"%@",subject]];
            
            // set body
           	if(body != nil)
              {
           			[picker setMessageBody:body isHTML:YES];
            }
            // Set recipients
//            if(toRecipientsString != nil)
//            {
//                if (toRecipientsString.length > 0) {
//                    [picker setToRecipients:[ toRecipientsString componentsSeparatedByString:@","]];
//                }
//            }
            [super presentModalViewController:picker animated:YES];
            

		} else {
           [self alertStatus:@"Please setup an email on your device!" :@""]; 
        }
        
    }
    else {
        [self alertStatus:@"Please setup an email on your device!" :@""];
    }
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    if (!_checkRotationPortait) {
        _checkisLanscape = YES;
    }
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            _checkShareSuccess = YES;
            [self alertStatus:@" Your email was sent successfully!" :@""];
            break;
        case MFMailComposeResultFailed:

            break;
        default:
            break;
    }
    //    [self popoverDismiss:nil];
    [super dismissModalViewControllerAnimated:YES];
    
}

#pragma mark interface Orientation
// only run in iOS 5 & iOS 4.3
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (_checkisLanscape) {
        _checkisLanscape = NO;
        [self updateViewLanscape];
        return YES;
    }
    if (([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft) ||
        ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)) {
        _checkRotationPortait = NO;
        [self updateViewLanscape];
    } else {
        _checkRotationPortait = YES;
        [self updateViewPortait];
    }
    return YES;
}

// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    if (_checkisLanscape) {
        _checkisLanscape = NO;
        [self updateViewLanscape];
        return UIInterfaceOrientationMaskLandscape;
    }
    if (_checkRotation < 2) {
        actionSheetView.hidden = NO;
        viewCoverSheetViewLanscape.hidden = YES;
        _checkRotationPortait = YES;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
        } else {
            iconNavigationBarImage.frame = CGRectMake((screenSize.width-29)/2, 7+heightIOS7, 29, 30);
        }
        _checkRotation++;
        return UIInterfaceOrientationMaskPortrait;
    }
    NSInteger rotationDevice = [[UIDevice currentDevice] orientation];
    if (rotationDevice <=2 ) {
        _checkRotationPortait = YES;
        [self updateViewPortait];
        return UIInterfaceOrientationMaskPortrait;
        
    } else {
        _checkRotationPortait = NO;
        [self updateViewLanscape];
        return UIInterfaceOrientationMaskLandscape;
    }
    
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale{
//    NSLog(@"scale:%f",scale);

    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    _scaleWebview = (float) viewSize.width / contentSize.width;
    
}
-(void) updateViewPortait {
    if (_scaleWebview == 1) {
        [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.0;"];

    }
    float ratioHeigthWidth;
    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webviewSheet.scrollView.minimumZoomScale = ratioHeigthWidth;

    actionSheetView.hidden = NO;
    viewCoverSheetViewLanscape.hidden = YES;

    viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2, (viewCheckPasswordSheet.frame.size.height - 200)/2, 320, 200);
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((screenSize.width-38)/2, 3+heightIOS7, 38, 36);
    } else {
        iconNavigationBarImage.frame = CGRectMake((screenSize.width-29)/2, 7+heightIOS7, 29, 30);
    }
    if (_checkisEditting) {
        
        [self setFrameTextfield];
    }
}

-(void) updateViewLanscape {
    if (_scaleWebview == 1) {

        // for ipad
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.4;"];
         // for iphone
        }else{
            if (screenSize.height == 568) {
                [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.9;"];
            } else {
                [webviewSheet stringByEvaluatingJavaScriptFromString:@"document. body.style.zoom = 1.6;"];
            }
        }

    }
    float ratioHeigthWidth;
    CGSize contentSize = webviewSheet.scrollView.contentSize;
    CGSize viewSize = self.webviewSheet.bounds.size;
    
    ratioHeigthWidth = (float) viewSize.width / contentSize.width;
    webviewSheet.scrollView.minimumZoomScale = ratioHeigthWidth;
    

    actionSheetView.hidden = YES;
    viewCoverSheetViewLanscape.hidden = NO;
//    viewCheckPasswordSheet.frame = CGRectMake(0, 44,screenSize.height , screenSize.width-44-20);

        viewSubCheckPasswordSheet.frame = CGRectMake((viewCheckPasswordSheet.frame.size.width-320)/2, (viewCheckPasswordSheet.frame.size.height - 200)/2, 320, 200);
//        NSLog(@"lanscape: %f-%f",viewCheckPasswordSheet.frame.size.width, viewCheckPasswordSheet.frame.size.height);
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        iconNavigationBarImage.frame = CGRectMake((screenSize.height-38)/2, 3+heightIOS7, 38, 36);

    } else {
        iconNavigationBarImage.frame = CGRectMake((screenSize.height-29)/2, 7+heightIOS7, 29, 30);
    }
    if (_checkisEditting) {
        
        [self setFrameTextfield];
    }
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}

- (void)viewDidUnload {
    unfavoriteButton = nil;
    [self setUnfavoriteButton:nil];
    actionSheetView = nil;
    [self setActionSheetView:nil];
    favoriteLabel = nil;
    [self setFavoriteLabel:nil];
    viewCoverSheetViewLanscape = nil;
    iconNavigationBarImage = nil;
    [self setIconNavigationBarImage:nil];
    webviewSheet = nil;
    [self setWebviewSheet:nil];
    viewCheckPasswordSheet = nil;
    [self setViewCheckPasswordSheet:nil];
    checkPasswordTextField = nil;
    [self setCheckPasswordTextField:nil];
    viewSubCheckPasswordSheet = nil;
    [self setViewSubCheckPasswordSheet:nil];
    [super viewDidUnload];
}
@end
