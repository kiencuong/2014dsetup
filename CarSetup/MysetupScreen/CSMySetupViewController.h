//
//  CSMySetupViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCustomCellSetupViewexpandViewController.h"
#import "CSSetupSheetViewController.h"
#import "MySetupModel.h"
#import "MySetupBrankModel.h"
#import "CSMySetupManager.h"
#import "CSDeleteMySetupSheet.h"
#import "CSGetInfoSheetManager.h"
@class CSProgress;
@interface CSMySetupViewController : UIViewController<CSCustomNavigationBarDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MySetupManagerDelegate, deleteSheetManagerDelegate>{
    
    IBOutlet UITableView *_carNameTable;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    
    NSMutableArray *_arrayNameCell;
    NSMutableArray *_arrayNameCellExpand;
    NSMutableArray *_arrayNameCellExpand1;
    NSMutableArray *_arrayNameCellExpand2;
    NSMutableArray *_arraySectionExpand;
    NSMutableArray *_arrayNameBrankModel;
    
    CSProgress *progress;
    MySetupModel *_mySetupModel;
    MySetupBrankModel *_mySetupBrankModel;
    CSCustomCellSetupViewexpandViewController *cell1;
    CSMySetupManager *setupManager;
    CSDeleteMySetupSheet *deleteManager;
    
    NSIndexPath *_indexpathRow;
    BOOL _checkEditCell;
    BOOL _checkEditSheetSuccess;
    
}
@property (strong, nonatomic) IBOutlet UITableView *_carNameTable;
@property (strong, nonatomic) CSCustomCellSetupViewexpandViewController *cell1;
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) CSMySetupManager *setupManager;
@property (strong, nonatomic) CSDeleteMySetupSheet *deleteManager;
@property (assign, nonatomic) BOOL _checkEditSheetSuccess;

- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
@end
