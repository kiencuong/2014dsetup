//
//  CSHomeViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSMySetupViewController.h"
#import "CSCreateNewViewController.h"
#import "CSFavoriteViewController.h"
#import "CSSearchViewController.h"
#import "CSSignInViewController.h"
#import "CSProgress.h"
@interface CSHomeViewController : UIViewController<CSCustomNavigationBarDelegate>{
    
    IBOutlet UIButton *mySetupButton;
    IBOutlet UIButton *createNewButton;
    IBOutlet UIButton *favoriteButton;
    IBOutlet UIButton *searchButton;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    IBOutlet UIImageView *logoImage;
    
    CSProgress *progress;
    
    NSString *_titleNavigationBar;
    BOOL _checkCreateNew;
    int heightIOS7;
    
}
@property (strong, nonatomic) IBOutlet UIButton *mySetupButton;
@property (strong, nonatomic) IBOutlet UIButton *createNewButton;
@property (strong, nonatomic) IBOutlet UIButton *favoriteButton;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonTool;
@property (strong, nonatomic) IBOutlet UIImageView *logoImage;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) NSString *_titleNavigationBar;
@property (assign, nonatomic) BOOL _checkCreateNew;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewHome;

- (IBAction)mySetupButtonClick:(id)sender;
- (IBAction)createNewButtonClick:(id)sender;
- (IBAction)favoriteButtonClick:(id)sender;
- (IBAction)searchButtonClick:(id)sender;
- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
- (IBAction)buttonToolClick:(id)sender;

@end
