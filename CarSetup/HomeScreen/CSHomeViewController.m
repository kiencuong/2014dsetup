//
//  CSHomeViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSHomeViewController.h"
#import "AFNetworking.h"
#import "TuningScreenViewController.h"
@interface CSHomeViewController ()

@end

@implementation CSHomeViewController
@synthesize mySetupButton;
@synthesize createNewButton;
@synthesize favoriteButton;
@synthesize searchButton;
@synthesize aboutButton;
@synthesize signOutButton;
@synthesize _checkCreateNew;
@synthesize _titleNavigationBar;
@synthesize logoImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [mySetupButton setSelected:NO];
    [createNewButton setSelected:NO];
    [favoriteButton setSelected:NO];
    [searchButton setSelected:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)initView {

    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if (![[CSUtility loadInfoUserLogin:FIRSTLAST_NAME] isEqualToString:@""]){
        navigationCustomBar.title = [CSUtility loadInfoUserLogin:FIRSTLAST_NAME];
        navigationCustomBar.checkHomeViewScreen = YES;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addTitleLabelHome:170];
    }else {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar_ipad.png"];
        [navigationCustomBar addTitleLabelHome:200];
    }
    
    navigationCustomBar.delegate = self;
    
    [self.view addSubview:navigationCustomBar];
    [aboutButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    _scrollViewHome.frame = CGRectMake(0, 44+heightIOS7, 320, screenSize.height-44-heightIOS7-32);
    _scrollViewHome.contentSize = CGSizeMake(320, 473);

//    if (screenSize.height == 568) {
//        mySetupButton.frame = CGRectMake(25, 100+heightIOS7, 126, 126);
//        createNewButton.frame = CGRectMake(165, 100+heightIOS7, 126, 126);
//        favoriteButton.frame = CGRectMake(25, 240+heightIOS7, 126, 126);
//        searchButton.frame = CGRectMake(165, 240+heightIOS7, 126, 126);
//        logoImage.frame = CGRectMake(138, 420+heightIOS7, 44, 43);
//    }
}

- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    mySetupButton = nil;
    [self setMySetupButton:nil];
    createNewButton = nil;
    favoriteButton = nil;
    searchButton = nil;
    [self setCreateNewButton:nil];
    [self setFavoriteButton:nil];
    [self setSearchButton:nil];
    aboutButton = nil;
    [self setAboutButton:nil];
    signOutButton = nil;
    [self setSignOutButton:nil];
    [super viewDidUnload];
}

#pragma mark action view
- (IBAction)mySetupButtonClick:(id)sender {
    
    [mySetupButton setSelected:YES];
    [createNewButton setSelected:NO];
    [favoriteButton setSelected:NO];
    [searchButton setSelected:NO];
    CSMySetupViewController *mysetupViewcontroller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        mysetupViewcontroller = [[CSMySetupViewController alloc]initWithNibName:@"CSMySetupViewController" bundle:nil];
    } else {
        mysetupViewcontroller = [[CSMySetupViewController alloc]initWithNibName:@"CSMySetupViewController_ipad" bundle:nil]; 
    }
    [self.navigationController pushViewController:mysetupViewcontroller animated:YES];
}

- (IBAction)createNewButtonClick:(id)sender {
    
    [mySetupButton setSelected:NO];
    [createNewButton setSelected:YES];
    [favoriteButton setSelected:NO];
    [searchButton setSelected:NO];
    CSCreateNewViewController *createViewcontroller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        createViewcontroller = [[CSCreateNewViewController alloc]initWithNibName:@"CSCreateNewViewController" bundle:nil];
    } else {
        createViewcontroller = [[CSCreateNewViewController alloc]initWithNibName:@"CSCreateNewViewController_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:createViewcontroller animated:YES];
    
}

- (IBAction)favoriteButtonClick:(id)sender {
    
    [mySetupButton setSelected:NO];
    [createNewButton setSelected:NO];
    [favoriteButton setSelected:YES];
    [searchButton setSelected:NO];
    CSFavoriteViewController *favoriteViewcontroller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        favoriteViewcontroller = [[CSFavoriteViewController alloc]initWithNibName:@"CSFavoriteViewController" bundle:nil];
    } else {
        favoriteViewcontroller = [[CSFavoriteViewController alloc]initWithNibName:@"CSFavoriteViewController_ipad" bundle:nil];
    }
        [self.navigationController pushViewController:favoriteViewcontroller animated:YES];
    
}

- (IBAction)searchButtonClick:(id)sender {
    [mySetupButton setSelected:NO];
    [createNewButton setSelected:NO];
    [favoriteButton setSelected:NO];
    [searchButton setSelected:YES];
    CSSearchViewController *searchViewcontroller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        searchViewcontroller = [[CSSearchViewController alloc]initWithNibName:@"CSSearchViewController" bundle:nil];
    } else {
        searchViewcontroller = [[CSSearchViewController alloc]initWithNibName:@"CSSearchViewController_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:searchViewcontroller animated:YES];
}


- (IBAction)buttonToolClick:(id)sender {
    TuningScreenViewController *tuningView;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        tuningView = [[TuningScreenViewController alloc]initWithNibName:@"TuningScreenViewController" bundle:nil];
    } else {
        tuningView = [[TuningScreenViewController alloc]initWithNibName:@"TuningScreenViewController_ipad" bundle:nil];
    }
    
    [self.navigationController pushViewController:tuningView animated:YES];
}

- (IBAction)aboutButtonClick:(id)sender {
    [aboutButton setSelected:YES];
    [signOutButton setSelected:NO];

}

- (IBAction)signOutButtonClick:(id)sender {

    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    NSURL *url=[NSURL URLWithString:URL_FORMAT_LOGIN];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [CSUtility loadInfoUserLogin:ID_USER_LOGIN_SUCCESS], @"token",
                            nil];
    [httpClient postPath:@"api/user/logout" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        NSInteger error = [[results objectForKey:@"status"]intValue];
        NSString *message = [results objectForKey:@"message"];
        
        if(error == 1)
        {
            [AppDelegate shareAppDelegate].checkCancelDownloadFavorite = YES;
            [AppDelegate shareAppDelegate].checkCancelDownload = YES;
            
            [aboutButton setSelected:NO];
            [signOutButton setSelected:YES];
            [CSUtility saveStatusLogin:NO forKey:CHECK_USER_LOGIN_SUCCESS  ];
            [CSUtility saveInfoUserLogin:@"" forKey:ID_USER_LOGIN_SUCCESS];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        } else if (error == 0) {
            [self alertStatus:message :@"Message"];
            
        }
        [self stopLoading];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [self alertStatus:@"Connection is errol.Please try again!" :@"Message"];
        [self stopLoading];
    }];
    
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return YES;
}
@end
