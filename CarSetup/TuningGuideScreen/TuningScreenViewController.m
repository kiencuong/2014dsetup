//
//  TuningScreenViewController.m
//  CarSetup
//
//  Created by thinhpham on 6/22/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "TuningScreenViewController.h"
#import "CSTuningGuideViewController.h"
#import "CSNitroRunTimeViewController.h"

@interface TuningScreenViewController (){
    int heightIOS7;
}


@end

@implementation TuningScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    
    CGSize sizeIconNavigationBar;
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        sizeIconNavigationBar = CGSizeMake(30, 30);
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-CaculatorNavigationBar"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    }else {
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleView:@"icon-CaculatorNavigationBar_ipad"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
    }
    navigationCustomBar.delegate = self;
    [navigationCustomBar addImageLogo];
    [self.view addSubview:navigationCustomBar];
}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Action View
- (IBAction)buttonTuningGuide:(id)sender {
    // TODO: button tuning click
    CSTuningGuideViewController *tuningView;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        tuningView = [[CSTuningGuideViewController alloc] initWithNibName:@"CSTuningGuideViewController" bundle:nil];
    } else {
        tuningView = [[CSTuningGuideViewController alloc] initWithNibName:@"CSTuningGuideViewController_ipad" bundle:nil];
    }

    [self.navigationController pushViewController:tuningView animated:YES];
    
}
- (IBAction)buttonCaculatorClick:(id)sender {
    // TODO: button caculator click
}
- (IBAction)buttonRuntimeClick:(id)sender {
    // TODO: button runtime click
    CSNitroRunTimeViewController *runtimeView;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        runtimeView = [[CSNitroRunTimeViewController alloc] initWithNibName:@"CSNitroRunTimeViewController" bundle:nil];
    } else {
        runtimeView = [[CSNitroRunTimeViewController alloc] initWithNibName:@"CSNitroRunTimeViewController_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:runtimeView animated:YES];
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}
@end
