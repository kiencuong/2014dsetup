//
//  CarSetup
//
//  Created by thinhpham on 6/22/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSTuningGuideViewController : UIViewController<CSCustomNavigationBarDelegate>{
    NSMutableArray *_arraySectionExpand;
    NSArray *_arrayBrand;
}

@property (weak, nonatomic) IBOutlet UITableView *tableViewTuning;

@end
