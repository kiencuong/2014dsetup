//
//  CSNitroRunTimeViewController.m
//  CarSetup
//
//  Created by thinhpham on 6/22/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "CSNitroRunTimeViewController.h"
static const CGFloat kMinimumScrollOffsetPadding = 10;
@interface CSNitroRunTimeViewController (){
    
    int heightIOS7;
    
    __weak IBOutlet UITextField *textfield_tank;
    __weak IBOutlet UITextField *textfield_driver_minute;
    __weak IBOutlet UITextField *textfield_driver_second;
    __weak IBOutlet UITextField *textfield_fuel;
    __weak IBOutlet UILabel *label_runtime;
    __weak IBOutlet UIScrollView *scrollViewRuntime;
    UIPickerView *pickerView;
    
    int indexMinute;
    int indexSecond;
    BOOL isMinute;
    NSMutableArray *arrayMinute;
    NSMutableArray *arraySecond;
}

@end

@implementation CSNitroRunTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayMinute = [[NSMutableArray alloc]init];
        arraySecond = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    for (UIView *subview in [scrollViewRuntime subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            subview.layer.cornerRadius = 3;
        }
    }
    label_runtime.layer.cornerRadius = 3;
    
    heightIOS7 = DETECT_OS7? 20 :0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    
    CGSize sizeIconNavigationBar;
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        sizeIconNavigationBar = CGSizeMake(63, 25);
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-NitroRunTimeNavigationBar"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    } else {
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleView:@"icon-NitroRunTimeNavigationBar_ipad"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
    }
    navigationCustomBar.delegate = self;
    [navigationCustomBar addImageLogo];
    [self.view addSubview:navigationCustomBar];
    indexMinute = 0;
    indexSecond = 0;
    
    // custom picker view
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, screenBound.size.height - 216, screenBound.size.width, 216)];
    [textfield_driver_minute setInputView:pickerView];
    [textfield_driver_second setInputView:pickerView];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
    for (int i=1; i <= 20; i++) {
        [arrayMinute addObject:[NSString stringWithFormat:@"%02i",i]];
    }
    for (int i=1; i <= 59; i++) {
        [arraySecond addObject:[NSString stringWithFormat:@"%02i",i]];
    }
    
    scrollViewRuntime.delegate = self;
    if (IS_IPHONE) {
        scrollViewRuntime.frame = CGRectMake(0, 44+heightIOS7, 320, screenBound.size.height -44-heightIOS7);
    }
    
    // init BSKeyboard
    NSArray *fields = @[ textfield_tank, textfield_driver_minute, textfield_driver_second, textfield_fuel];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    
    
}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (![label_runtime.text isEqualToString:@""]) {
        label_runtime.text = @"";
    }
    isMinute = NO;
    if (textField == textfield_driver_minute) {
        isMinute = YES;
        [pickerView reloadAllComponents];
        [pickerView selectRow:indexMinute inComponent:0 animated:NO];
        textfield_driver_minute.text = [arrayMinute objectAtIndex:indexMinute];
    } else if (textField == textfield_driver_second){
        [pickerView reloadAllComponents];
        [pickerView selectRow:indexSecond inComponent:0 animated:NO];
        textfield_driver_second.text = [arraySecond objectAtIndex:indexSecond];
    }
    [self.keyboardControls setActiveField:textField];
    if (IS_IPHONE) {
        [self scrollviewAvoidKeyboard:textField.frame];
    }

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Replace the string manually in the textbox
     NSString * textInput = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //perform any logic here now that you are sure the textbox text has changed
    if (textField == textfield_tank) {
        if (textInput.length > 4) {
            return NO;
        }
    } else if(textField == textfield_driver_minute){
        if (textInput.length > 2) {
            return NO;
        }
    } else if(textField == textfield_driver_second){
        if (textInput.length > 2) {
            return NO;
        }
    }else if(textField == textfield_fuel){
        if (textInput.length > 4) {
            return NO;
        }
    }
    
    return YES; //this make iOS not to perform any action
}


#pragma mark action view
- (IBAction)resetButtonClick:(id)sender {
    for (UITextField *subview in [scrollViewRuntime subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            subview.text = @"";
        }
    }
    label_runtime.text = @"";
    indexMinute = 0;
    indexSecond = 0;
    
}
- (IBAction)caculatorButtonClick:(id)sender {
    
    int driver_time = [textfield_driver_minute.text intValue] *60 + [textfield_driver_second.text intValue];
    if (driver_time == 0) {
        [self alertStatus:@"Please Check Data and Try Again." :@""];
        return;
    }
    int used = ([textfield_tank.text intValue] - [textfield_fuel.text intValue]);
    if (used == 0) {
        [self alertStatus:@"Please Check Data and Try Again." :@""];
        return;
    }
    float cc_per_minute = used/(float)driver_time;
    
    CGFloat cc_minute = floorf(cc_per_minute * 100 +0.5) / 100;
    NSString *ccminute = [NSString stringWithFormat:@"%.2f",cc_minute];
    
    int project_run_time = [textfield_tank.text intValue]/[ccminute floatValue];

    
    
    NSInteger min = (int)(project_run_time/60);
    NSInteger sec = round(project_run_time - min * 60);
    label_runtime.text = [NSString stringWithFormat:@"%02ld:%02ld",(long)min,(long)sec];
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}


#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    if (isMinute) {
        return arrayMinute.count;
    }
    return arraySecond.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if (isMinute) {
        return arrayMinute[row];
    }
    return arraySecond[row];
}

#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    if (isMinute) {
        textfield_driver_minute.text = arrayMinute[row];
        indexMinute = (int)row;
        return;
    }
    
    textfield_driver_second.text = arraySecond[row];
    indexSecond = (int)row;
    return;
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark -
#pragma mark Keyboard Controls Delegate

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    if (IS_IPHONE) {
        [self scrollviewAvoidKeyboard:field.frame];
    }

}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:YES];
    [scrollViewRuntime setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)scrollviewAvoidKeyboard:(CGRect) frame{
    CGFloat offset = 0.0;
    CGFloat offsetKeyboard = [[UIScreen mainScreen] bounds].size.height - 216-80-heightIOS7;
    offset = frame.origin.y + frame.size.height + kMinimumScrollOffsetPadding;
    
    if (offset <= offsetKeyboard) {
        offset = 0;
    } else {
        offset = offset - offsetKeyboard;
    }
    
    [scrollViewRuntime setContentOffset:CGPointMake(0, offset) animated:YES];
}
#pragma mark Scroolview delegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

@end
