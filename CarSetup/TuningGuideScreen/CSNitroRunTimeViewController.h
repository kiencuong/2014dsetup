//
//  CarSetup
//
//  Created by thinhpham on 6/22/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
@interface CSNitroRunTimeViewController : UIViewController<CSCustomNavigationBarDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, BSKeyboardControlsDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end
