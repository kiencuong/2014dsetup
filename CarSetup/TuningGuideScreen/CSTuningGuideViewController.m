//
//  CSTuningGuideViewController.m
//  CarSetup
//
//  Created by thinhpham on 6/22/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "CSTuningGuideViewController.h"

@interface CSTuningGuideViewController (){
    int heightIOS7;
    
}

@end

@implementation CSTuningGuideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arraySectionExpand = [[NSMutableArray alloc]init];
        _arrayBrand = @[@"Caster",@"Camber",@"Ride Height",@"Toe",@"Track-Width",@"Droop",@"Kick-Up",@"Anti-Squat",@"Wheelbase",@"Sway Bars",@"Diff Fluid",@"Shock Oil",@"Shock Pistons",@"Shock Springs",@"Shock Positions",@"Roll Center",@"Clutch",@"Gearing",@"Weight"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    
    CGSize sizeIconNavigationBar;
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        sizeIconNavigationBar = CGSizeMake(33, 33);
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-TurningGuideNavigationBar"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    } else {
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleView:@"icon-TurningGuideNavigationBar_ipad"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
    }
    navigationCustomBar.delegate = self;
    [navigationCustomBar addImageLogo];
    [self.view addSubview:navigationCustomBar];
    
    NSArray *sortedArray;
    sortedArray = [_arrayBrand sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = a;
        NSString *second = b;
        return [first compare:second];
    }];
    _arrayBrand = sortedArray;
    
    
    for (int i=0; i <[_arrayBrand count]; i++) {
        [_arraySectionExpand addObject:@"NO"];
    }
}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addButtonClick:(id)sender {
    UIButton *adButton = (UIButton *)sender;
    [self expandRowTableSetup:adButton.tag];
}
-(void)expandRowTableSetup:(NSInteger)index{
    
    
    if ([[_arraySectionExpand objectAtIndex:index] isEqualToString:@"YES"]) {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"NO"];
    } else {
        [_arraySectionExpand replaceObjectAtIndex:index withObject:@"YES"];
    }
    
    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndex:index];
    [_tableViewTuning reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [_arrayBrand count];;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"YES"]) {
        //        int numberRow;
        return 1;
    }
    return 0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIButton *expandButton = [[UIButton alloc]init];
    [expandButton setBackgroundColor:[UIColor clearColor]];
    UIImageView *imageExpand = [[UIImageView alloc]init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        expandButton.frame = CGRectMake(0, 0, 300, 44);
        expandButton.tag = section;
        imageExpand.frame = CGRectMake(13, 11, 25, 24);
        if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
            imageExpand.image = [UIImage imageNamed:@"buttonAdd.png"];
        } else {
            imageExpand.image = [UIImage imageNamed:@"buttonExpand.png"];
        }
        [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        // Create label with section title
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(52, 11, 216, 21);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
        label.shadowOffset = CGSizeMake(0.0, 1.0);
        label.font = [UIFont boldSystemFontOfSize:16];
        label.text = sectionTitle;
        
        // Create header view and add label as a subview
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model.png"];
        UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300,44)];
        backgroundImageSection.image=image_section;
        [view addSubview:backgroundImageSection];
        [view addSubview:label];
        [view addSubview:imageExpand];
        [view addSubview:expandButton];
        return view;
    }
    expandButton.frame = CGRectMake(0, 0, 618, 70);
    imageExpand.frame = CGRectMake(25, 14, 45, 44);
    expandButton.tag = section;
    if ([[_arraySectionExpand objectAtIndex:section] isEqualToString:@"NO"]) {
        imageExpand.image = [UIImage imageNamed:@"buttonAdd_ipad.png"];
    } else {
        imageExpand.image = [UIImage imageNamed:@"buttonExpand_ipad.png"];
    }
    [expandButton addTarget:self action:@selector(addButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(90, 24, 480,35);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:106.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:30];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(76, 90, 618, 70)];
    UIImage *image_section=[UIImage imageNamed:@"bg_cell_Model_ipad.png"];
    UIImageView *backgroundImageSection=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 618,70)];
    backgroundImageSection.image=image_section;
    [view addSubview:backgroundImageSection];
    [view addSubview:label];
    [view addSubview:imageExpand];
    [view addSubview:expandButton];
    
    return view;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    if ([[_arraySectionExpand objectAtIndex:indexPath.section]isEqualToString:@"YES"]) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [CSUtility getStringForKey:[_arrayBrand objectAtIndex:indexPath.section] ];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.contentView.backgroundColor = [UIColor whiteColor];
        return cell;
        
    }
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [_arrayBrand objectAtIndex:section];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return [CSUtility checkHeightForString:[CSUtility getStringForKey:[_arrayBrand objectAtIndex:indexPath.section] ] withFront:[UIFont fontWithName:@"Helvetica" size:16] withInitSize:CGSizeMake(270, 44)];
    }
    return [CSUtility checkHeightForString:[CSUtility getStringForKey:[_arrayBrand objectAtIndex:indexPath.section] ] withFront:[UIFont fontWithName:@"Helvetica" size:16] withInitSize:CGSizeMake(578, 44)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 44;
    }
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

@end
