//
//  CSSearchViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSearchResultViewController.h"
#import "CSSearchManager.h"
#import "SearchModel.h"
#import "SearchResultModel.h"
@class CSProgress;
@interface CSSearchViewController : UIViewController<CSCustomNavigationBarDelegate,SearchManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>{
    
    IBOutlet UIScrollView *searchScroolView;
    IBOutlet UIView *viewSearch;
    
    IBOutlet UIButton *selectTypeButton;
    IBOutlet UIButton *selectBrandButton;
    IBOutlet UIButton *selectModelButton;
    IBOutlet UIButton *buttonAdvanceSearch;

    IBOutlet UIButton *selectTrackTypeButton;
    IBOutlet UIButton *selectTractionLevelButton;
    IBOutlet UIButton *selectTrackSizeButton;
    IBOutlet UIButton *selectSurfaceButton;
    
    IBOutlet UIView *viewAdvanceSearch;
    IBOutlet UITextField *driverNameTextfield;

    IBOutlet UIButton *searchButton;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    IBOutlet UIButton *prevButton;
    IBOutlet UIButton *nextButton;
    IBOutlet UIButton *doneButtonSelectPickerView;
    IBOutlet UIView *subviewPicker;
    IBOutlet UIPickerView *pickerView;
    IBOutlet UIImageView *imageBGScroolVIew;
    
    UIView *popupView;
    CGSize screenSize ;
    NSMutableArray *_arrayTrackType;
    NSMutableArray *_arrayTrackLevel;
    NSMutableArray *_arrayTrackSize;
    NSMutableArray *_arraySurface;
    
    CSSearchManager *searchManager;
    SearchModel *searchModelList;
    SearchResultModel *searchResultModel;
    CSProgress *progress;
    
    NSInteger _indexRowType;
    NSInteger _indexRowBrand;
    NSInteger _indexRowTrackType;
    NSInteger _indexRowTractionLevel;
    NSInteger _indexRowTrackSize;
    NSInteger _indexRowModel;
    NSInteger _indexRowSurface;
    NSInteger _indexButtonPopup;
    
    BOOL _checkAdvanceSearch;
    BOOL _checkNetwork;
    BOOL _setButtonPopupSelected;
    NSString *_stringBrandName;
    int heightIOS7;

}
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) IBOutlet UIScrollView *searchScroolView;

@property (strong, nonatomic) IBOutlet UIView *viewSearch;
@property (strong, nonatomic) IBOutlet UIButton *selectTypeButton;
@property (strong, nonatomic) IBOutlet UIButton *selectBrandButton;
@property (strong, nonatomic) IBOutlet UIButton *selectModelButton;
@property (strong, nonatomic) IBOutlet UIButton *buttonAdvanceSearch;

@property (strong, nonatomic) IBOutlet UIView *viewAdvanceSearch;
@property (strong, nonatomic) IBOutlet UIButton *selectTrackTypeButton;
@property (strong, nonatomic) IBOutlet UIButton *selectTractionLevelButton;
@property (strong, nonatomic) IBOutlet UIButton *selectTrackSizeButton;
@property (strong, nonatomic) IBOutlet UITextField *driverNameTextfield;
@property (strong, nonatomic) IBOutlet UIButton *selectSurfaceButton;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *trackNameTextField;


@property (strong, nonatomic) IBOutlet UIButton *prevButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIView *subviewPicker;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIImageView *imageBGScroolVIew;

@property (strong, nonatomic) CSSearchManager *searchManager;



- (IBAction)doneButtonSelectPickerViewClick:(id)sender;
- (IBAction)buttonAdvanceSearch:(id)sender;
- (IBAction)selectTypeButtonClick:(id)sender;
- (IBAction)selectBrandButtonClick:(id)sender;
- (IBAction)selectModelButtonClick:(id)sender;
- (IBAction)selectSurfaceButtonClick:(id)sender;

- (IBAction)selectTrackTypeButtonClick:(id)sender;
- (IBAction)selectTractionLevelButtonClick:(id)sender;
- (IBAction)selectTrackSizeButtonClick:(id)sender;
- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
- (IBAction)searchButtonClick:(id)sender;
- (IBAction)subViewPickerClick:(id)sender;
- (IBAction)prevButtonClick:(id)sender;
- (IBAction)nextButtonClick:(id)sender;
- (IBAction)viewAdvanceSearchClick:(id)sender;
- (IBAction)viewCoverClick:(id)sender;

@end
