//
//  CSSearchScreenShareViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/25/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import <MessageUI/MessageUI.h>
#import "CSAddFavoriteManager.h"
#import "CSRemoveFavoriteManager.h"
#import "CSProgress.h"
#import "CSGetInfoSheetManager.h"
#import "SheetModel.h"
@interface CSSearchScreenShareViewController : UIViewController<CSCustomNavigationBarDelegate,FBDialogDelegate,FBSessionDelegate,addFavoriteManagerDelegate, GetInfoSheetManagerDelegate, removeFavoriteManagerDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate, UITextFieldDelegate,UIScrollViewDelegate> {
    
    IBOutlet UIWebView *webviewSheet;
    IBOutlet UIButton *unfavoriteButton;
    IBOutlet UITextField *checkPasswordTextField;
    IBOutlet UIView *viewCheckPasswordSheet;
    IBOutlet UIView *viewSubCheckPasswordSheet;

    IBOutlet UIImageView *iconNavigationBarImage;
    IBOutlet UIView *actionSheetView;
    IBOutlet UIView *actionSheetViewLanscape;
    IBOutlet UIView *popupView;
    IBOutlet UILabel *favoriteLabel;
    IBOutlet UIView *viewCoverSheetViewLanscape;
    

    UIWebView *webViewTweet;
    CGSize screenSize;
    UIView *uibg;
    UIButton *_doneButton;

    Facebook *_facebook;
    CSProgress *progress;
    SheetFullInfoModel *_sheetModelInfo;
    CSAddFavoriteManager *addFavorite;
    CSRemoveFavoriteManager *removeFavorite;
    CSGetInfoSheetManager *getInfoManager;
    
    NSInteger checkRotation;
    NSInteger _idSheet;
    NSInteger _scaleWebview;
    BOOL _checkShareSuccess;
    BOOL _checkisEditting;
    BOOL _checkisLanscape;
    BOOL _checkRotationPortait;
    BOOL _checkAddSheetSuccess;
    BOOL _checkRemoveSheetSuccess;
    
    NSString *stringlinkSheet;
    int heightIOS7;

    
}
@property (strong, nonatomic) IBOutlet UIWebView *webviewSheet;
@property (strong, nonatomic) IBOutlet UIView *viewCheckPasswordSheet;
@property (strong, nonatomic) IBOutlet UITextField *checkPasswordTextField;
@property (strong, nonatomic) IBOutlet UIView *viewSubCheckPasswordSheet;
@property (strong, nonatomic) IBOutlet UIImageView *iconNavigationBarImage;
@property (strong, nonatomic) IBOutlet UIButton *unfavoriteButton;
@property (strong, nonatomic) IBOutlet UIView *actionSheetView;
@property (strong, nonatomic) IBOutlet UILabel *favoriteLabel;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UIView *actionSheetViewLanscape;
@property (strong, nonatomic) IBOutlet UIView *viewCoverSheetViewLanscape;
@property (strong, nonatomic) CSAddFavoriteManager *addFavorite;
@property (strong, nonatomic) CSGetInfoSheetManager *getInfoManager;
@property (assign, nonatomic) NSInteger _idSheet;

- (IBAction)mailButtonClick:(id)sender;
- (IBAction)facebookButtonClick:(id)sender;
- (IBAction)twitterButonClick:(id)sender;
- (IBAction)unFavoriteButtonClick:(id)sender;
- (IBAction)editButtonClick:(id)sender;
- (IBAction)cancelButtonClick:(id)sender;
- (IBAction)backButtonClick:(id)sender;
- (IBAction)shareButtonClick:(id)sender;
- (IBAction)popUpViewClick:(id)sender;

- (IBAction)submitButtonClick:(id)sender;
- (IBAction)viewCheckPasswordClick:(id)sender;
- (IBAction)viewSubCheckPasswordSheet:(id)sender;
@end
