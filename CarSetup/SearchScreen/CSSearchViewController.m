//
//  CSSearchViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/20/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSSearchViewController.h"
#import "CSProgress.h"
@interface CSSearchViewController ()

@end

@implementation CSSearchViewController
@synthesize aboutButton;
@synthesize signOutButton;
@synthesize selectTypeButton;
@synthesize selectBrandButton;
@synthesize selectModelButton;
@synthesize buttonAdvanceSearch;
@synthesize selectTrackTypeButton;
@synthesize selectTractionLevelButton;
@synthesize selectTrackSizeButton;
@synthesize selectSurfaceButton;
@synthesize driverNameTextfield;
@synthesize cityTextField;
@synthesize trackNameTextField;
@synthesize viewSearch;
@synthesize viewAdvanceSearch;
@synthesize searchScroolView;
@synthesize searchButton;
@synthesize pickerView;
@synthesize subviewPicker;
@synthesize searchManager;
@synthesize imageBGScroolVIew;
@synthesize prevButton;
@synthesize nextButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrayTrackLevel = [[NSMutableArray alloc]initWithObjects:@"",@"High",@"Medium",@"Low", nil];
        _arrayTrackSize = [[NSMutableArray alloc]initWithObjects:@"",@"Large",@"Medium",@"Small", nil];
        _arrayTrackType = [[NSMutableArray alloc]initWithObjects:@"",@"Open",@"Medium",@"Tight", nil];
        _arraySurface = [[NSMutableArray alloc]initWithObjects:@"",@"Asphalt",@"Astro",@"Carpet",@"Gravel",@"Indoors",@"Outdoors", nil];
//        searchModelList = [[SearchModel alloc]init];
        searchResultModel = [[SearchResultModel alloc]init];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    heightIOS7 = DETECT_OS7? 20 :0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    [self initView];
    searchManager = [[CSSearchManager alloc]init];
    searchManager.delegate = self;
    [self loadListSearchSheet];
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _indexRowType = 0;
    searchResultModel._typeID = @"";
    [selectTypeButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowBrand = 0;
    searchResultModel._brandID = @"";
    [selectBrandButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowModel = 0;
    searchResultModel._modelID = @"";
    [selectModelButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowTrackType = 0;
    searchResultModel._trackTypeID = @"";
    [selectTrackTypeButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowTractionLevel = 0;
    searchResultModel._tractionLevel = @"";
    [selectTractionLevelButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowTrackSize = 0;
    searchResultModel._trackSizeID = @"";
    [selectTrackSizeButton setTitle:@"" forState:UIControlStateNormal];
    _indexRowSurface = 0;
    searchResultModel._surfaceID = @"";
    [selectSurfaceButton setTitle:@"" forState:UIControlStateNormal];

}
-(void) initView{
    
    CGSize sizeIconNavigationBar;
    CSCustomNavigationBar *navigationCustomBar = [[CSCustomNavigationBar alloc] init];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        sizeIconNavigationBar = CGSizeMake(38, 36);
        [navigationCustomBar addBackButton];
        [navigationCustomBar layoutCustomBar : @"bg-NavigationBar.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-SearchNavigationBar@2x"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen.png"]];
    }else {
        sizeIconNavigationBar = CGSizeMake(33, 34);
        [navigationCustomBar addBackButton_ipad];
        [navigationCustomBar layoutCustomBar : @"bg_screenSetup_ipad.png"];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-SearchNavigationBar_ipad@2x"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-HomeScreen_ipad.png"]];
        [navigationCustomBar addImageTitleCreateView:sizeIconNavigationBar :@"icon-SearchNavigationBar_ipad@2x"];
    }
    navigationCustomBar.delegate = self;
    [navigationCustomBar addImageLogo];
    [self.view addSubview:navigationCustomBar];
    [aboutButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    selectTypeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectBrandButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectBrandButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectModelButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectModelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    selectTrackTypeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectTrackTypeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectTractionLevelButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectTractionLevelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectTrackSizeButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectTrackSizeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    selectSurfaceButton.titleEdgeInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    selectSurfaceButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    driverNameTextfield.delegate = self;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    if (screenSize.height == 568) {
        subviewPicker.frame = CGRectMake(0, heightIOS7,320, screenSize.height-20);
    }
    UIView *uibg =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    uibg.backgroundColor = [UIColor blackColor];
    uibg.opaque = YES;
    uibg.alpha = 0.3;
    popupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    popupView.backgroundColor = [UIColor clearColor];
    popupView.opaque = YES;
    popupView.alpha = 1.0;
    [self.view addSubview:popupView];
    [popupView addSubview:uibg];
    [popupView insertSubview:subviewPicker aboveSubview:uibg];
    popupView.hidden=YES;
    
    viewAdvanceSearch.hidden = YES;
    //    searchButton.frame = CGRectMake(21,325, 278, 45);
    searchScroolView.contentOffset = CGPointMake(0, 0);
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        searchScroolView.contentSize = CGSizeMake(320, screenSize.height *300/460);
    } else {
        searchScroolView.contentSize = CGSizeMake(622, 460);
    }
}
#pragma mark get list sheet
- (void)loadListSearchSheet{
    if (![CSUtility checkNetworkAvailable]) {
        [self alertStatus:@"Network connection needed for this feature!" :@""];
        return;
    }
    _checkNetwork = YES;
    [NSThread detachNewThreadSelector:@selector(startLoading) toTarget:self withObject:nil];
    [searchManager loadListDataSearch];
    
}
- (void) finishGetlistSearch{
    [self stopLoading];
    searchModelList = searchManager.searchModel;
    searchModelList._arrayTrackSize = _arrayTrackSize;
    searchModelList._arrayTrackType = _arrayTrackType;
    searchModelList._arrayTractionLevel = _arrayTrackLevel;
    searchModelList._arraySurface = _arraySurface;
    if (_setButtonPopupSelected) {
        _setButtonPopupSelected = NO;
        switch (_indexButtonPopup) {
            case 0:
                [selectTypeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
            case 1:
                [selectBrandButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
            case 2:
                [selectModelButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
            case 3:
                [selectTrackTypeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
            case 4:
                [selectTractionLevelButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
            case 5:
                [selectTrackSizeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
            default:
                [selectSurfaceButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                break;
        }
    }
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

// back button click
- (void)shouldGoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark textField delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        if (screenSize.height == 568) {
            searchScroolView.contentOffset = CGPointMake(0,470);
        } else {
            searchScroolView.contentOffset = CGPointMake(0,554);
        }
    }else {
         searchScroolView.contentOffset = CGPointMake(0,740);
    }
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField{
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
     if (screenSize.height == 568) {
//         searchScroolView.contentOffset = CGPointMake(0.0f, 506);
         if (textField == driverNameTextfield) {
             searchScroolView.contentOffset = CGPointMake(0.0f, 556);
         } else if(textField == cityTextField){
             searchScroolView.contentOffset = CGPointMake(0.0f, 586);
         } else {
             searchScroolView.contentOffset = CGPointMake(0.0f, 666);
         }

     } else {
         if (textField == driverNameTextfield) {
             searchScroolView.contentOffset = CGPointMake(0.0f, 506);
         } else if(textField == cityTextField){
             searchScroolView.contentOffset = CGPointMake(0.0f, 586);
         } else {
             searchScroolView.contentOffset = CGPointMake(0.0f, 666);
         }
             
         
     }
     } else {
         if (textField == driverNameTextfield) {
             searchScroolView.contentOffset = CGPointMake(0.0f, 506);
         } else if(textField == cityTextField){
             searchScroolView.contentOffset = CGPointMake(0.0f, 586);
         } else {
             searchScroolView.contentOffset = CGPointMake(0.0f, 666);
         }
     }
}

#pragma mark interface Orientation
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark - picker view
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//Here we are setting the number of rows in the pickerview to the number of objects of the NSArray. You should understand this since we covered it in the tableview tutorial.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (_indexButtonPopup) {
        case 0:
            return [searchModelList._arrayType count];
            break;
        case 1:
            return [searchModelList._arrayBrand count];
            break;
        case 2:
            return [searchModelList._arrayModel count];
            break;
        case 3:
            return [searchModelList._arrayTrackType  count];
            break;
        case 4:
            return [searchModelList._arrayTractionLevel  count];
            break;
        case 5:
            return [searchModelList._arrayTrackSize  count];
            break;
        default:
            return [searchModelList._arraySurface  count];
            break;
    }
}

//Here we are setting the values of the NSArray to the pickerview's rows. This is pretty much identical to the way you load a table view.
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row == 0) {
        return @"";
    }
    switch (_indexButtonPopup) {
        case 0:
            return [[searchModelList._arrayType objectAtIndex:row]_type_name];
            break;
        case 1:
            return [[searchModelList._arrayBrand objectAtIndex:row]_brand_name];
            break;
        case 2:
            return [[searchModelList._arrayModel objectAtIndex:row]_model_name];
            break;
        case 3:
            return [searchModelList._arrayTrackType  objectAtIndex:row];
            break;
        case 4:
            return [searchModelList._arrayTractionLevel  objectAtIndex:row];
            break;
        case 5:
            return [searchModelList._arrayTrackSize  objectAtIndex:row];
            break;
        default:
            return [searchModelList._arraySurface  objectAtIndex:row];
            break;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (_indexButtonPopup) {
        case 0:
            if (_indexRowType != row) {
                if (row == 0) {
                    searchResultModel._typeID = @"";
                    [selectTypeButton setTitle:@"" forState:UIControlStateNormal];
                    [selectBrandButton setEnabled:NO];
                    [selectModelButton setEnabled:NO];
                } else {
                    [selectBrandButton setEnabled:YES];
                    [selectModelButton setEnabled:YES];
                    [selectTypeButton setTitle:[[searchModelList._arrayType objectAtIndex:row]_type_name] forState:UIControlStateNormal];
                    searchResultModel._typeID = [NSString stringWithFormat:@"%d",[[searchModelList._arrayType objectAtIndex:row]_type_id]];
                }
                _indexRowBrand = 0;
                _indexRowModel = 0;

                [selectBrandButton setTitle:@"" forState:UIControlStateNormal];
                [selectModelButton setTitle:@"" forState:UIControlStateNormal];
            }
            _indexRowType = row;

            break;
        case 1:
            if (_indexRowBrand != row) {
                if (row == 0) {
                    searchResultModel._brandID = @"";
                    [selectBrandButton setTitle:@"" forState:UIControlStateNormal];
                    [selectModelButton setEnabled:NO];
                } else {
                    [selectModelButton setEnabled:YES];
                    [selectBrandButton setTitle:[[searchModelList._arrayBrand objectAtIndex:row]_brand_name] forState:UIControlStateNormal];
                    searchResultModel._brandID = [NSString stringWithFormat:@"%d",[[searchModelList._arrayBrand objectAtIndex:row]_brand_id]];
                }
                _indexRowModel = 0;
                [selectModelButton setTitle:@"" forState:UIControlStateNormal];
            }
            
            _indexRowBrand = row;

            
            break;
        case 2:
            _indexRowModel = row;
            if (row==0) {
                searchResultModel._modelID = @"";
                [selectModelButton setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            [selectModelButton setTitle:[[searchModelList._arrayModel objectAtIndex:row]_model_name] forState:UIControlStateNormal];
            searchResultModel._modelID = [NSString stringWithFormat:@"%d",[[searchModelList._arrayModel objectAtIndex:row]_model_id]];
            
            break;
        case 3:
            _indexRowTrackType = row;
            if (row==0) {
                searchResultModel._trackTypeID = @"";
                [selectTrackTypeButton setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            [selectTrackTypeButton setTitle:[searchModelList._arrayTrackType objectAtIndex:row] forState:UIControlStateNormal];
            searchResultModel._trackTypeID = [NSString stringWithFormat:@"%d",row ];
            break;
        case 4:
            _indexRowTractionLevel = row;
            if (row==0) {
                searchResultModel._tractionLevel = @"";
                [selectTractionLevelButton setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            [selectTractionLevelButton setTitle:[searchModelList._arrayTractionLevel objectAtIndex:row] forState:UIControlStateNormal];
            searchResultModel._tractionLevel = [NSString stringWithFormat:@"%d",row ];;

            break;
        case 5:
            _indexRowTrackSize = row;
            if (row==0) {
                searchResultModel._trackSizeID = @"";
                [selectTrackSizeButton setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            [selectTrackSizeButton setTitle:[searchModelList._arrayTrackSize objectAtIndex:row] forState:UIControlStateNormal];
            searchResultModel._trackSizeID = [NSString stringWithFormat:@"%d",row ];
            
            break;
        default:
            _indexRowSurface = row;
            if (row==0) {
                searchResultModel._surfaceID = @"";
                [selectSurfaceButton setTitle:@"" forState:UIControlStateNormal];
                break;
            }
            [selectSurfaceButton setTitle:[searchModelList._arraySurface objectAtIndex:row] forState:UIControlStateNormal];
            searchResultModel._surfaceID = [NSString stringWithFormat:@"%d",row ];
            
            break;
    }
}

#pragma mark action View
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectTypeButtonClick:(id)sender {
    _indexButtonPopup = 0;
    [self checkButtonPopup];
    
    if (!_indexRowType ) {
        [selectTypeButton setTitle:@"" forState:UIControlStateNormal];
        searchResultModel._typeID = @"";

    }

}

- (IBAction)selectBrandButtonClick:(id)sender {
    _indexButtonPopup = 1;
    if (!_indexRowBrand) {
        [selectBrandButton setTitle:@"" forState:UIControlStateNormal];
            searchResultModel._brandID = @"";
        
    }
    [self checkButtonPopup];

}

- (IBAction)selectModelButtonClick:(id)sender {
    _indexButtonPopup = 2;
    if (!_indexRowModel) {
        [selectModelButton setTitle:@"" forState:UIControlStateNormal];
        searchResultModel._modelID = @"";
        
    }
    [self checkButtonPopup];

}


- (IBAction)selectTrackTypeButtonClick:(id)sender {
    _indexButtonPopup = 3;
    if (!_indexRowTrackType && [searchModelList._arrayTrackType count]>0) {
        [selectTrackTypeButton setTitle:[searchModelList._arrayTrackType objectAtIndex:0] forState:UIControlStateNormal];
        searchResultModel._trackTypeID = @"";
    }
    [self checkButtonPopup];

}

- (IBAction)selectTractionLevelButtonClick:(id)sender {
    _indexButtonPopup = 4;
    if (!_indexRowTractionLevel && [searchModelList._arrayTractionLevel count]>0) {
        [selectTractionLevelButton setTitle:[searchModelList._arrayTractionLevel objectAtIndex:0] forState:UIControlStateNormal];
        searchResultModel._tractionLevel = @"";
    }
    [self checkButtonPopup];

}

- (IBAction)selectTrackSizeButtonClick:(id)sender {
    _indexButtonPopup = 5;

    if (!_indexRowTrackSize && [searchModelList._arrayTrackSize count]>0) {
        [selectTrackSizeButton setTitle:[searchModelList._arrayTrackSize objectAtIndex:0]forState:UIControlStateNormal];
        searchResultModel._trackSizeID = @"";
        
    }
    [self checkButtonPopup];
}
- (IBAction)selectSurfaceButtonClick:(id)sender {
    _indexButtonPopup = 6;
    if (!_indexRowSurface && [searchModelList._arraySurface count]>0) {
        [selectSurfaceButton setTitle:[searchModelList._arraySurface objectAtIndex:0] forState:UIControlStateNormal];
        searchResultModel._surfaceID = @"";
    }
    [self checkButtonPopup];
    
}
- (IBAction)buttonAdvanceSearch:(id)sender {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        if ([buttonAdvanceSearch isSelected]) {
            _checkAdvanceSearch = NO;
            [buttonAdvanceSearch setSelected:NO];
            viewAdvanceSearch.hidden = YES;
            searchButton.frame = CGRectMake(21, 315, 280, 45);
            searchScroolView.contentSize = CGSizeMake(320, 384);
        } else {
            _checkAdvanceSearch = YES;
            [buttonAdvanceSearch setSelected:YES];
            viewAdvanceSearch.hidden = NO;
            searchButton.frame = CGRectMake(21, 877, 280, 45);
            searchScroolView.contentSize = CGSizeMake(320, 938);
            searchScroolView.contentOffset =CGPointMake(0.0f, 300);
        }
    } else {
        if ([buttonAdvanceSearch isSelected]) {
            _checkAdvanceSearch = NO;
            [buttonAdvanceSearch setSelected:NO];
            viewAdvanceSearch.hidden = YES;
            searchButton.frame = CGRectMake(142, 490, 337, 55);
            searchScroolView.contentSize = CGSizeMake(622, 460);
            imageBGScroolVIew.frame = CGRectMake(73, 104+heightIOS7, 622, 596);
            searchScroolView.frame = CGRectMake(73, 104+heightIOS7, 622, 596);
            
        } else {
            _checkAdvanceSearch = YES;
            [buttonAdvanceSearch setSelected:YES];
            viewAdvanceSearch.hidden = NO;
            searchButton.frame = CGRectMake(142, 1415, 337, 55);
            searchScroolView.contentSize = CGSizeMake(622, 1520);
            imageBGScroolVIew.frame = CGRectMake(73, 104+heightIOS7, 622, 796);
            searchScroolView.frame = CGRectMake(73, 104+heightIOS7, 622, 796);
            searchScroolView.contentOffset =CGPointMake(0.0f, 460);
        }
    
    }

}

- (IBAction)aboutButtonClick:(id)sender {
    [aboutButton setSelected:YES];
    [signOutButton setSelected:NO];
    
}

- (IBAction)signOutButtonClick:(id)sender {
    [aboutButton setSelected:NO];
    [signOutButton setSelected:YES];
    [CSUtility saveStatusLogin:NO forKey:CHECK_USER_LOGIN_SUCCESS  ];
    [CSUtility saveInfoUserLogin:@"" forKey:ID_USER_LOGIN_SUCCESS];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark check button popup
-(void) checkButtonPopup{
    [self.view endEditing:YES];
    if (!_checkNetwork) {
        _setButtonPopupSelected = YES;
        [self loadListSearchSheet];
        return;
    }
    [popupView setHidden:NO];
    
    if (_checkAdvanceSearch) {
        switch (_indexButtonPopup) {
            case 0:
                
                prevButton.enabled = NO;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:YES];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                break;
            case 1:
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:YES];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                [searchModelList._arrayBrand removeAllObjects];
                [searchModelList._arrayBrand addObject:@""];
                if (_indexRowType) {
                    [self selectBrand: [[searchModelList._arrayType objectAtIndex:_indexRowType]_type_id]];
                }
                
                break;
            case 2:
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:YES];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                [searchModelList._arrayModel removeAllObjects];
                [searchModelList._arrayModel addObject:@""];
                if (_indexRowBrand) {
                    [self selectModel:[[searchModelList._arrayType objectAtIndex:_indexRowType]_type_id] :[[searchModelList._arrayBrand objectAtIndex:_indexRowBrand]_brand_id]];
                }
                
                break;
            case 3:
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:YES];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                break;
            case 4:
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:YES];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                break;
            case 5:
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:YES];
                [selectSurfaceButton setSelected:NO];
                break;
            default:
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:YES];
                prevButton.enabled = YES;
                nextButton.enabled = NO;
                break;
        }
    } else {
        switch (_indexButtonPopup) {
            case 0:
                prevButton.enabled = NO;
                nextButton.enabled = YES;
                [selectTypeButton setSelected:YES];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                break;
            case 1:
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:YES];
                [selectModelButton setSelected:NO];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];

                [selectSurfaceButton setSelected:NO];
                prevButton.enabled = YES;
                nextButton.enabled = YES;
                [searchModelList._arrayBrand removeAllObjects];
                [searchModelList._arrayBrand addObject:@""];
                if (_indexRowType) {
                    [self selectBrand: [[searchModelList._arrayType objectAtIndex:_indexRowType]_type_id]];
                }
                
                break;
            default:
                [selectTypeButton setSelected:NO];
                [selectBrandButton setSelected:NO];
                [selectModelButton setSelected:YES];
                [selectTrackTypeButton setSelected:NO];
                [selectTractionLevelButton setSelected:NO];
                [selectTrackSizeButton setSelected:NO];
                [selectSurfaceButton setSelected:NO];
                prevButton.enabled = YES;
                nextButton.enabled = NO;
                [searchModelList._arrayModel removeAllObjects];
                [searchModelList._arrayModel addObject:@""];
                if (_indexRowBrand) {
                    [self selectModel:[[searchModelList._arrayType objectAtIndex:_indexRowType]_type_id] :[[searchModelList._arrayBrand objectAtIndex:_indexRowBrand]_brand_id]];
                }
                
                break;
        }
    }
    [pickerView reloadAllComponents]; 
    if (screenSize.height == 568) {
        switch (_indexButtonPopup) {
            case 0:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowType inComponent:0 animated:YES];
                break;
            case 1:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowBrand inComponent:0 animated:YES];
                break;
            case 2:{
                if (searchScroolView.contentOffset.y >40) {
                    searchScroolView.contentOffset = CGPointMake(0, 300);
                } else {
                searchScroolView.contentOffset = CGPointMake(0, 40);
                }
            }
                [pickerView selectRow:_indexRowModel inComponent:0 animated:YES];
                break;
            case 3:
                searchScroolView.contentOffset = CGPointMake(0, 300);
                [pickerView selectRow:_indexRowTrackType inComponent:0 animated:YES];
                break;
            case 4:
                searchScroolView.contentOffset = CGPointMake(0, 300);
                [pickerView selectRow:_indexRowTractionLevel inComponent:0 animated:YES];
                break;
            case 5:
                searchScroolView.contentOffset = CGPointMake(0, 300);
                [pickerView selectRow:_indexRowTrackSize inComponent:0 animated:YES];
                break;
            default:
                searchScroolView.contentOffset = CGPointMake(0, 386);
                [pickerView selectRow:_indexRowSurface inComponent:0 animated:YES];
                break;
        }
    } else if(screenSize.height == 480){
        switch (_indexButtonPopup) {
            case 0:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowType inComponent:0 animated:YES];
                break;
            case 1:
                searchScroolView.contentOffset = CGPointMake(0, 60);
                [pickerView selectRow:_indexRowBrand inComponent:0 animated:YES];
                break;
            case 2:
                searchScroolView.contentOffset = CGPointMake(0.0f, 120);
                [pickerView selectRow:_indexRowModel inComponent:0 animated:YES];
                break;
            case 3:
                searchScroolView.contentOffset = CGPointMake(0.0f, 300);
                [pickerView selectRow:_indexRowTrackType inComponent:0 animated:YES];
                break;
            case 4:
                searchScroolView.contentOffset = CGPointMake(0.0f, 300);
                [pickerView selectRow:_indexRowTractionLevel inComponent:0 animated:YES];
                break;
            case 5:
                searchScroolView.contentOffset = CGPointMake(0.0f, 390);
                [pickerView selectRow:_indexRowTrackSize inComponent:0 animated:YES];
                break;
                
            default:
                searchScroolView.contentOffset = CGPointMake(0.0f, 476);
                [pickerView selectRow:_indexRowSurface inComponent:0 animated:YES];
                break;
                
        }
    } else {
        switch (_indexButtonPopup) {
            case 0:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowType inComponent:0 animated:YES];
                break;
            case 1:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowBrand inComponent:0 animated:YES];
                break;
            case 2:
                searchScroolView.contentOffset = CGPointMake(0, 0);
                [pickerView selectRow:_indexRowModel inComponent:0 animated:YES];
                break;
            case 3:
                searchScroolView.contentOffset = CGPointMake(0.0f, 460);
                [pickerView selectRow:_indexRowTrackType inComponent:0 animated:YES];
                break;
            case 4:
                searchScroolView.contentOffset = CGPointMake(0.0f, 460);
                [pickerView selectRow:_indexRowTractionLevel inComponent:0 animated:YES];
                break;
            case 5:
                searchScroolView.contentOffset = CGPointMake(0.0f, 460);
                [pickerView selectRow:_indexRowTrackSize inComponent:0 animated:YES];
                break;
                
            default:
                searchScroolView.contentOffset = CGPointMake(0.0f, 460);
                [pickerView selectRow:_indexRowSurface inComponent:0 animated:YES];
                break;
                
        }
    }
  
}
- (void)selectBrand: (NSInteger)typeId {
    _stringBrandName = @"";
    for (int j =0;j <[searchModelList._arraySheets count]; j++) {
        if (typeId == [[searchModelList._arraySheets objectAtIndex:j]_type_id])
        {
            if ([_stringBrandName isEqualToString: [[searchModelList._arraySheets objectAtIndex:j]_brand_name]]) {
                continue;
            }
            _stringBrandName =  [[searchModelList._arraySheets objectAtIndex:j]_brand_name];
            
            NSLog(@"brancd:%@",_stringBrandName);
            
            [searchModelList._arrayBrand addObject:[searchModelList._arraySheets objectAtIndex:j]];
        }
    }
    NSLog(@"count array: %d",searchModelList._arrayBrand.count);
}

- (void)selectModel: (NSInteger)typeId :(NSInteger)brandId  {
    for (int j =0;j <[searchModelList._arraySheets count]; j++) {
        
        if (typeId == [[searchModelList._arraySheets objectAtIndex:j]_type_id] && brandId == [[searchModelList._arraySheets objectAtIndex:j]_brand_id])
        {
            [searchModelList._arrayModel addObject:[searchModelList._arraySheets objectAtIndex:j]];
        }
    }
    
}
- (IBAction)prevButtonClick:(id)sender {
    _indexButtonPopup--;
    [self checkButtonPopup];
}

- (IBAction)nextButtonClick:(id)sender {
    _indexButtonPopup++;
    [self checkButtonPopup];
}
- (IBAction)searchButtonClick:(id)sender {
    if (driverNameTextfield.text) {
        searchResultModel._driverName = driverNameTextfield.text;
    }
    if (cityTextField.text) {
        searchResultModel._cityName = cityTextField.text;
    }
    if (trackNameTextField.text) {
        searchResultModel._trackName = trackNameTextField.text;
    }
    CSSearchResultViewController *searchResultView;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        searchResultView = [[CSSearchResultViewController alloc]initWithNibName:@"CSSearchResultViewController" bundle:nil];
    } else {
        searchResultView = [[CSSearchResultViewController alloc]initWithNibName:@"CSSearchResultViewController_ipad" bundle:nil]; 
    }
    searchResultView.searchModel = searchResultModel;
    [self.navigationController pushViewController:searchResultView animated:YES];
}

- (IBAction)subViewPickerClick:(id)sender {
    [popupView setHidden:YES];
    [selectTypeButton setSelected:NO];
    [selectBrandButton setSelected:NO];
    [selectModelButton setSelected:NO];
    [selectTrackTypeButton setSelected:NO];
    [selectTractionLevelButton setSelected:NO];
    [selectTrackSizeButton setSelected:NO];
    [selectTypeButton setSelected:NO];
    [selectSurfaceButton setSelected:NO];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        
        if (screenSize.height == 568) {
            if (searchScroolView.contentOffset.y >=300) {
                searchScroolView.contentOffset = CGPointMake(0,300);
            } else {
            searchScroolView.contentOffset = CGPointMake(0, 0);
            }
        } else {
            if (searchScroolView.contentOffset.y >200) {
                searchScroolView.contentOffset = CGPointMake(0,300);
            } else {
                searchScroolView.contentOffset = CGPointMake(0, 0);
            }
        }
    } else {
        if (searchScroolView.contentOffset.y >450) {
            searchScroolView.contentOffset = CGPointMake(0,460);
        } else {
            searchScroolView.contentOffset = CGPointMake(0, 0);
        }
    }
}

- (IBAction)viewAdvanceSearchClick:(id)sender {
    [self.view endEditing:YES];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        if (screenSize.height == 568) {
            searchScroolView.contentOffset = CGPointMake(0,470);
        } else {
            searchScroolView.contentOffset = CGPointMake(0,554);
        }
    } else {
        searchScroolView.contentOffset = CGPointMake(0,740);
    }
    
}

- (IBAction)viewCoverClick:(id)sender {
    [self.view endEditing:YES];
    if (searchScroolView.contentOffset.y >=430) {
        searchScroolView.contentOffset = CGPointMake(0,740);
    }
    
    
}
- (IBAction)doneButtonSelectPickerViewClick:(id)sender {
    [popupView setHidden:YES];
    [selectTypeButton setSelected:NO];
    [selectBrandButton setSelected:NO];
    [selectModelButton setSelected:NO];
    [selectTrackTypeButton setSelected:NO];
    [selectTractionLevelButton setSelected:NO];
    [selectTrackSizeButton setSelected:NO];
    [selectTypeButton setSelected:NO];
    [selectSurfaceButton setSelected:NO];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        
        if (screenSize.height == 568) {
            if (searchScroolView.contentOffset.y >=300) {
                searchScroolView.contentOffset = CGPointMake(0,300);
            } else {
                searchScroolView.contentOffset = CGPointMake(0, 0);
            }
        } else {
            if (searchScroolView.contentOffset.y >200) {
                searchScroolView.contentOffset = CGPointMake(0,300);
            } else {
                searchScroolView.contentOffset = CGPointMake(0, 0);
            }
        }
    } else {
        if (searchScroolView.contentOffset.y >450) {
            searchScroolView.contentOffset = CGPointMake(0,460);
        } else {
            searchScroolView.contentOffset = CGPointMake(0, 0);
        }
    }
}
-(void)startLoading{
    if (progress==nil) {
        progress=[[CSProgress alloc]init];
    }
    [progress startAnimating];
}

-(void)stopLoading{
    if (progress != nil && progress.isAnimating) {
        [progress stopAnimating];
    }
    progress=nil;
}
- (void)viewDidUnload {
    selectTypeButton = nil;
    [self setSelectTypeButton:nil];
    selectBrandButton = nil;
    [self setSelectBrandButton:nil];
    selectModelButton = nil;
    [self setSelectModelButton:nil];
    buttonAdvanceSearch = nil;
    [self setButtonAdvanceSearch:nil];
    viewSearch = nil;
    [self setViewSearch:nil];
    viewAdvanceSearch = nil;
    [self setViewAdvanceSearch:nil];
    selectTrackTypeButton = nil;
    [self setSelectTrackTypeButton:nil];
    selectTractionLevelButton = nil;
    [self setSelectTractionLevelButton:nil];
    selectTrackSizeButton = nil;
    [self setSelectTrackSizeButton:nil];
    searchScroolView = nil;
    [self setSearchScroolView:nil];
    [self setSearchButton:nil];
    searchButton = nil;
    driverNameTextfield = nil;
    [self setDriverNameTextfield:nil];
    selectSurfaceButton = nil;
    [self setSelectSurfaceButton:nil];
    [self setCityTextField:nil];
    [self setTrackNameTextField:nil];
    [super viewDidUnload];
}
@end
