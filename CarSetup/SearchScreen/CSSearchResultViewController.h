//
//  CSSearchResultViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/25/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCustomCellSetupViewexpandViewController.h"
#import "CSSearchScreenShareViewController.h"
#import "CSProgress.h"
#import "CSSearchResultManager.h"
#import "SearchResultModel.h"
@interface CSSearchResultViewController : UIViewController<CSCustomNavigationBarDelegate, UITableViewDataSource, UITableViewDelegate,SearchResultManagerDelegate>{

    IBOutlet UITableView *_carNameTable;
    IBOutlet UIButton *aboutButton;
    IBOutlet UIButton *signOutButton;
    
    NSMutableArray *_arrayNameCell;
    NSMutableArray *_arrayNameCellExpand;
    NSMutableArray *_arraySectionExpand;
    NSMutableArray *_arrayNameBrankModel;
    
    CSCustomCellSetupViewexpandViewController *cell1;
    CSProgress *progress;
    CSSearchResultManager *searchResultManager;
    SearchResultModel *searchModel;

}
@property (strong, nonatomic) CSCustomCellSetupViewexpandViewController *cell1;
@property (strong, nonatomic) IBOutlet UITableView *_carNameTable;
@property (strong, nonatomic) IBOutlet UIButton *aboutButton;
@property (strong, nonatomic) IBOutlet UIButton *signOutButton;
@property (strong, nonatomic) CSSearchResultManager *searchResultManager;
@property (strong, nonatomic) SearchResultModel *searchModel;

- (IBAction)aboutButtonClick:(id)sender;
- (IBAction)signOutButtonClick:(id)sender;
@end
