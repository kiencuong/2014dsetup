//
//  CSWellcomeViewController.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSignInViewController.h"
#import "CSHomeViewController.h"
@interface CSWellcomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    IBOutlet UITableView *tableWellCome;
    IBOutlet UIView *viewBackgroundTable;
    
    CSSignInViewController *signInView ;
    CGSize _screenSize;
}
@property (strong, nonatomic) IBOutlet UITableView *tableWellCome;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundTable;

@end
