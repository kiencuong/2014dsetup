//
//  CSWellcomeViewController.m
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import "CSWellcomeViewController.h"

@interface CSWellcomeViewController ()

@end

@implementation CSWellcomeViewController
@synthesize tableWellCome;
@synthesize viewBackgroundTable;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self performSelector:@selector(animationTableView) withObject:nil afterDelay:3];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    _screenSize = screenBound.size;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (![CSUtility loadStatusLogin:CHECK_USER_LOGIN_SUCCESS]) {
//    NSLog(@"Logged out of facebook");
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// animation tableview login
- (void)animationTableView{
    if ([CSUtility loadStatusLogin:CHECK_USER_LOGIN_SUCCESS]) {
        CSHomeViewController *homeViewController ;
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            homeViewController = [[CSHomeViewController alloc]initWithNibName:@"CSHomeViewController" bundle:nil];
            viewBackgroundTable.frame = CGRectMake(18, _screenSize.height*320/460, 283, 91);
        } else {
            homeViewController = [[CSHomeViewController alloc]initWithNibName:@"CSHomeViewController_ipad" bundle:nil];
            viewBackgroundTable.frame = CGRectMake(204, _screenSize.height*486/1024, 359, 115);
        }
            [self.navigationController pushViewController:homeViewController animated:NO];
    } else {
    
    [UIView beginAnimations:nil context:nil];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [UIView setAnimationDuration:1];
        viewBackgroundTable.frame = CGRectMake(18, _screenSize.height*320/460, 283, 91);
    } else {
        [UIView setAnimationDuration:2];
        viewBackgroundTable.frame = CGRectMake(204, _screenSize.height*486/1024, 359, 115);
    }
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView commitAnimations];
    }
}

#pragma mark interface Orientation
// only run in iOS 6
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
    
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *myIndentifier = @"MyIndentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:myIndentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myIndentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return 45;
    }
    return 52;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_FORMAT_REGISTER]];
    } else {
         if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
             signInView = [[CSSignInViewController alloc]initWithNibName:@"CSSignInViewController" bundle:nil];
         } else {
             signInView = [[CSSignInViewController alloc]initWithNibName:@"CSSignInViewController_ipad" bundle:nil];
         }
        [self.navigationController pushViewController:signInView animated:YES];
    }

}

- (void)viewDidUnload {
    tableWellCome = nil;
    [self setTableWellCome:nil];
    viewBackgroundTable = nil;
    [self setViewBackgroundTable:nil];
    [super viewDidUnload];
}
@end
