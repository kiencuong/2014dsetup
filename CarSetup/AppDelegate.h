//
//  AppDelegate.h
//  CarSetup
//
//  Created by Pham Van Thinh on 3/19/13.
//  Copyright (c) 2013 Pham Van Thinh. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CSWellcomeViewController;
#import "MySetupBrankModel.h"
#import "MySetupModel.h"
#import "SheetFullInfoModel.h"
#import "SheetInfo.h"
#import "SBJson.h"
@class CSCustomNavigationViewController;
@class  CSGetInfoSheetManager;
#define   X_AppDelegate (AppDelegate *) [[UIApplication sharedApplication] delegate]
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    CSWellcomeViewController *wellcomeView;
    CSGetInfoSheetManager *autoDowwnloadSheet;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CSCustomNavigationViewController *navigationWellcomeView;
@property (strong, nonatomic) NSMutableArray *sheets;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *
persistentStoreCoordinator;
@property (nonatomic, assign) BOOL checkCancelDownload;
@property (nonatomic, assign) BOOL checkCancelDownloadFavorite;
@property (nonatomic, assign) NSInteger checkViewDownload;
- (void) create:(MySetupBrankModel *) brandModel;
- (NSMutableArray *) read:(NSInteger)checkMysetup withUserName:(NSString *) username;
- (void) delete :(NSInteger)checkDeleteMysetup withUserName:(NSString *) username;
- (void) deleteSheet :(NSInteger) checkSetup withid:(NSInteger)idsheet withUserName:(NSString *) username;
- (void) deleteBrandModel :(NSInteger) checkSetup withbrandid:(NSInteger) brandId withmodelID:(NSInteger) modelID withUserName:(NSString *) username;
- (void) createSheetInfo:(SheetFullInfoModel *) sheetFullModel;
- (SheetFullInfoModel *) readSheetInfo:(NSInteger)sheetID;
- (void) deleteSheetInfo:(NSInteger)idsheet;
+ (AppDelegate *)shareAppDelegate;
- (void) autoDownloadSheet:(NSMutableArray *)arraySheet;
@end
